<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('municipios_list/{id}/towns', 'TownController@getNameMunicipio');

Route::get('viewinstitucion/{idinstitucion}/reporte', 'GraphicsController@getReportesVeiwsInstitucion');

Route::get('listmostpopularprograms/{idinstitucion}/reporte', 'GraphicsController@getReportesViewsMostPopularPrograms');

Route::get('publication/{idinstitucion}/{idpublicacion}/reporte', 'GraphicsController@getReportesViewsResultPublication');


Route::get('comparadorinstitutions/{slug_a}/{slug_b}/', 'UserCommunityController@comparador_result');

Route::get('programsclick/{idinstitucion}/reporte', 'GraphicsController@getTotalClickInscripcion');

Route::get('realtime_followers/{idinstitucion}/reporte', 'InstitutionController@notificacion_followers');

Route::get('/getInstitutions', 'FilterController@getInstitutions');
Route::get('/get-details-institution/{username}', 'FilterController@getDetails');

/*

http://127.0.0.1:8000/api/comparadorinstitutions/carrera64/carrera42/
http://127.0.0.1:8000/api/publication/1/4/reporte
http://127.0.0.1:8000/api/viewinstitucion/1/reporte
http://127.0.0.1:8000/api/listmostpopularprograms/1/reporte
http://127.0.0.1:8000/api/municipios_list/1/towns
http://127.0.0.1:8000/api/programsclick/1/reporte
http://127.0.0.1:8000/api/realtime_followers/1/reporte
http://127.0.0.1:8000/api/getInstitutions
http://127.0.0.1:8000/api/get-details-institution/sergioarboledasm

*/