<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*

    Route::get('/', function () {
       return '';
    });

*/


Route::get('/', function () {
    //return view('welcome');
    header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
    header('Cache-Control: no-store, no-cache, must-revalidate');
    header('Cache-Control: post-check=0, pre-check=0',false);
    header('Pragma: no-cache');
    

    if (auth()->check()){
        if (Auth::User()->rols_id == 3) {

            $institution = \Delphos\Institution::where('users_id', Auth::User()->id)->first();
            $type_institutions = \Delphos\TypeInstitution::where('id', $institution->type_institutions_id)->first();

            return view('index', compact('type_institutions'));
        }
        return view('index');
        
    }else{
        return view('index');
    }
});


Route::get('/logout', function () {
    return view('index');
});


Auth::routes();

Route::resource('/administrativos', 'DelphosController');
Route::get('/getInstituciones-name-avatar', 'FilterController@getInstitutions');
Route::get('/consultar-reporte-mensual-institucion/{username}', 'DelphosController@reporte_mes_adm');



Route::resource('/community', 'UserCommunityController');
Route::resource('/institutions', 'InstitutionController');
Route::resource('/publications', 'PublicationController');


Route::resource('/programs', 'ProgramController');
Route::resource('/graphics', 'GraphicsController');
Route::resource('/panel', 'FilterController');
Route::resource('/alianzas-estrategicas', 'AlianzasController');


Route::get('/presentacion/{username}', 'InstitutionController@presentacion')->name('presentacion');

//Rutas publicas
Route::get('/programs/ofertas/{id}', 'ProgramController@list')->name('list');

Route::post('/programs/like-program', 'ProgramController@like')->name('like');

Route::get('panel-buscar/', 'FilterController@search')->name('search');

Route::get('favourites-programs', 'UserCommunityController@misprograms')->name('misprograms');

Route::get('/publications/{idinstitucion}/{slug}/analytics', 'PublicationController@analytics')->name('analytics');


// LOGIN: RUTAS REDES SOCIALES
Route::get('login/facebook', 'SocialiteController@redirectToProvider');
Route::get('login/facebook/callback/', 'SocialiteController@handleProviderCallback');

Route::get('login/google', 'SocialiteController@redirectToProvider');
Route::get('login/google/callback', 'SocialiteController@handleProviderCallback');


Route::get('login/facebook', 'SocialiteController@redirectToProvider');
Route::get('login/facebook/callback', 'SocialiteController@handleProviderCallback');




Route::get('/comparar-oferta-academica/{idprograma}', 'ProgramController@comparar_oferta')->name('comparar_oferta');

Route::get('/comparar-instituciones', 'UserCommunityController@comparador_principal')->name('comparador_principal');

Route::get('/notificaciones', 'UserCommunityController@notificaciones')->name('notificaciones');

Route::get('/successful/reset', function () {
    header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
    header('Cache-Control: no-store, no-cache, must-revalidate');
    header('Cache-Control: post-check=0, pre-check=0',false);
    header('Pragma: no-cache');
    
    return view('auth.successful_send_reset_password');
});

Route::post('/count-click/', 'ProgramController@FunctionCountClick')->name('FunctionCountClick');

Route::get('/ranking', 'InstitutionController@ranking')->name('ranking');

Route::get('/descubrir-ofertas-academicas', 'FilterController@FiltrarOfertas')->name('FiltrarOfertas');
Route::post('/descubrir', 'FilterController@getResultFilterOfertas')->name('getResultFilterOfertas');
Route::get('/descubrir', 'FilterController@getResultFilterOfertas')->name('getResultFilterOfertas');

Route::resource('questions', 'QuestionController');
Route::get('/question-one'  , 'QuestionController@questionOne'  )->name('questionOne');
Route::post('/question-one/create'  , 'QuestionController@store_QuestionOne'  )->name('store_QuestionOne');

Route::get('/question-two'  , 'QuestionController@questionTwo'  )->name('questionTwo');
Route::post('/question-two/create'  , 'QuestionController@store_QuestionTwo'  )->name('store_QuestionTwo');

Route::get('/question-three', 'QuestionController@questionThree')->name('questionThree');
Route::post('/question-three/create', 'QuestionController@store_QuestionThree')->name('store_QuestionThree');

Route::get('/question-four/{id}', 'QuestionController@questionFour')->name('questionFour');

Route::get('/terms_and_conditions', function () {
    header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
    header('Cache-Control: no-store, no-cache, must-revalidate');
    header('Cache-Control: post-check=0, pre-check=0',false);
    header('Pragma: no-cache');
    
    return view('content_extra.terms_and_conditions');
});


Route::get('/institutions-googlemaps/{username}', 'InstitutionController@view_maps')->name('view_maps');
Route::get('/List-of-Followers', 'InstitutionController@list_followers')->name('list_followers');

Route::get('/notification/get', 'ProgramController@notification')->name('notification');

Route::get('enviarEmail/{id}', function ($id) {
    return view("mail.nuevo", ["id" => $id]);
})->name('vistaNuevoEmail');


Route::post('/email', 'QuestionController@enviarEmail')->name('enviarEmail');



