
$(function() {
    $('#select-project').on('change', onSelectProjectChange);
});

function onSelectProjectChange() {

    var project_id = $(this).val();
    
    if (!project_id) {
        $('#select-level').html('<option value="" >Seleccione nivel</option>');
        return;
    }

    $.get('/api/municipios_list/' + project_id + '/towns', function(data) {
        //console.log(data);
        var html_select = '<option disabled="" selected="" value="0">Seleccione municipio</option>';
        $.each(data, function(index, alumno) {
            html_select += '<option value="' + data[index].id + '">' + data[index].name + '</option>';
        });
        $("#select-level").html(html_select);
        $("#select-level").formSelect();
    });

}

