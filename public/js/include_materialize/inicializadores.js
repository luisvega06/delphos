$(document).ready(function(){


	$('.sidenav').sidenav();
	M.updateTextFields();
	$('select').formSelect();
	$(".dropdown-trigger").dropdown();
	$('.tooltipped').tooltip();
	$('.datepicker').datepicker();
	$('.tap-target').tapTarget();
	$('.modal').modal();
	$('.fixed-action-btn').floatingActionButton();
	$('.parallax').parallax();
	$('.collapsible').collapsible();
	$('.tabs').tabs();
	$('input#input_text, textarea#textarea2').characterCounter();
	$('.materialboxed').materialbox();
	$('.scrollspy').scrollSpy();



});
