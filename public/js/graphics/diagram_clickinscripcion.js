var doc = new jsPDF();

Chart.defaults.global.defaultFontFamily = "Lato";
Chart.defaults.global.defaultFontSize = 12;

function load_diagrams_inscripciones(val) {

    var list_labels = [];
    var list_value  = [];
    var total = 0;
    var row = 20, column =40, increment = 5, lineo = 120 , line_inic = 0, line_end = 0, limite_page = 300;
    var title_report = "Interactividad Usuarios Accion Inscribirme";

    doc = new jsPDF();
    doc.setFontSize(40)
    doc.setFontType('bold')
    doc.text(20, 20, 'LISTADO DE USUARIOS')

    
    
    

    
    $.get('/api/programsclick/'+val+'/reporte', function(data) {
        

        $.each(data, function(i, item) {
            list_labels.push(item.label.substring(0, 50));
            list_value.push(item.count);
            total += item.count;

            if (item.add) {

                row += 20;
                doc.setFontSize(8)
                doc.setFontType('bold')

                if ( (row + 30 + ( increment * 7 ) ) > limite_page) {
                    doc.addPage();
                    row = 20;
                }
                
                
                var tam = item.label.length;

                if (tam >= lineo) {
                    var sw = 0, aux_tam = tam;
                    var linferior = 0, lsuperior = lineo;
                    var pcontent = x = Math.round((tam/lineo));

                    for (var i = 0; i <= pcontent; i++) {

                        doc.text(20, row, item.label.substring(linferior, lsuperior));
                        row += 5;

                        linferior += lineo;

                        if (lsuperior + lineo > tam) {
                            lsuperior = tam;
                        } else {
                            lsuperior += lineo;
                        }
                    }
                }else{
                    doc.text(20, row, item.label);
                    row += 5;
                }
                
                
                
                $.each(item.users,  function(j, item_users) {  

                    if ( (row + (increment * 7) ) > limite_page) {
                        doc.addPage();
                        row = 20;
                    }

                    line_inic = row + increment - 2;

                    row += increment;
                    doc.setFontSize(10);
                    doc.setFontType('normal');
                    doc.text(25, row, 'Username: ');
                    doc.text(25 + column, row, item_users.username);

                    row += increment;
                    doc.setFontSize(10);
                    doc.setFontType('normal');
                    doc.text(25, row, 'Nombre Completo: ');
                    doc.text(25 + column, row, item_users.name);
                    
                    row += increment;
                    doc.setFontSize(10);
                    doc.setFontType('normal');
                    doc.text(25, row, 'Correo: ');
                    doc.text(25 + column, row, item_users.email);

                    row += increment;
                    doc.setFontSize(10);
                    doc.setFontType('normal');
                    doc.text(25, row, 'Celular: ');
                    doc.text(25 + column, row, item_users.phone);

                    row += increment;
                    doc.setFontSize(10);
                    doc.setFontType('normal');
                    doc.text(25, row, 'Fecha Nac.: ');
                    doc.text(25 + column, row, item_users.date);

                    row += increment;
                    doc.setFontSize(10);
                    doc.setFontType('normal');
                    doc.text(25, row, 'Ubicación: ');
                    doc.text(25 + column, row, item_users.municipio);

                    line_end = row;
                    doc.setLineWidth(0.3);
                    doc.line(20, line_inic, 20, line_end); // horizontal line

                    row += (increment*2);

                });
            }
        });


        $("#div-download-3-pdf").removeClass("scale-transition scale-out");
        $("#div-download-3-pdf").addClass("scale-transition scale-in");
        
        

        var backgroundColor = [
            "#0d47a1",
            "#009688",
            "#84FF63",
            "#263238",
            "#6384FF",
            "#3e95cd", 
            "#8e5ea2",
            "#3cba9f",
            "#e8c3b9",
            "#c45850"
        ];


        
        //INICIO - Diagrama de torta (Pie Chart)
        new Chart(document.getElementById("pie-chart-inscripciones"), {
            type: 'pie',
            data: {
              labels: list_labels,
              datasets: [{
                label: "Inscribirme (usuarios)",
                backgroundColor: backgroundColor,
                data: list_value
            }]
            },
            options: {
                  title: {
                    display: true,
                    text: title_report
                }
            }
        });
        //FIN - Diagrama torta
        
        
    });
   
}

function Download_PDF_3() {
    doc.save('listado_usuarios_interaccion_inscripcion.pdf');
}