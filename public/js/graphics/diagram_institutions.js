var doc = new jsPDF();

Chart.defaults.global.defaultFontFamily = "Lato";
Chart.defaults.global.defaultFontSize = 12;

function load_diagrams_publications(val) {


    //console.log("vine con valor: " + val);

    var list_labels = [];
    var list_value  = [];
    var total = 0;
    var row = 20, column =40, increment = 5, lineo = 120 , line_inic = 0, line_end = 0, limite_page = 300;
    var title_report = "Visititas Perfil Institución";

    doc = new jsPDF();
    doc.setFontSize(40)
    doc.setFontType('bold')
    doc.text(20, 20, 'LISTADO DE USUARIOS')

    
    
    


    $.get('/api/viewinstitucion/'+val+'/reporte', function(data) {
        

        $.each(data, function(i, item) {
            list_labels.push(item.label);
            list_value.push(item.count);
            total += item.count;

            if (item.add) {

                row += 20;
                //doc.setFontType('normal')
                doc.setFontSize(8)
                doc.setFontType('bold')

                if ( (row + 30 + ( increment * 7 ) ) > limite_page) {
                    doc.addPage();
                    row = 20;
                }
                
                
                var tam = item.label.length;

                if (tam >= lineo) {
                    var sw = 0, aux_tam = tam;
                    var linferior = 0, lsuperior = lineo;
                    var pcontent = x = Math.round((tam/lineo));
                    /*
                    console.log("\n item.label: " + item.label);
                    console.log("\n Esta contenido en "+ lineo +" "+ pcontent + " veces.");
                    */

                    for (var i = 0; i <= pcontent; i++) {

                        /*
                        console.log("\n\n L. Inf= " + linferior + " - L. Sup= " + lsuperior );
                        console.log("\n Content: " + item.label.substring(linferior, lsuperior));
                        console.log("\n\n");
                        */

                        doc.text(20, row, item.label.substring(linferior, lsuperior));
                        row += 5;

                        linferior += lineo;

                        if (lsuperior + lineo > tam) {
                            lsuperior = tam;
                        } else {
                            lsuperior += lineo;
                        }
                    }
                }else{
                    doc.text(20, row, item.label);
                    row += 5;
                }
                
                
                
                $.each(item.users,  function(j, item_users) {  

                    if ( (row + (increment * 7) ) > limite_page) {
                        doc.addPage();
                        row = 20;
                    }

                    line_inic = row + increment - 2;

                    row += increment;
                    doc.setFontSize(10);
                    doc.setFontType('normal');
                    doc.text(25, row, 'Username: ');
                    doc.text(25 + column, row, item_users.username);

                    row += increment;
                    doc.setFontSize(10);
                    doc.setFontType('normal');
                    doc.text(25, row, 'Nombre Completo: ');
                    doc.text(25 + column, row, item_users.name);
                    
                    row += increment;
                    doc.setFontSize(10);
                    doc.setFontType('normal');
                    doc.text(25, row, 'Correo: ');
                    doc.text(25 + column, row, item_users.email);

                    row += increment;
                    doc.setFontSize(10);
                    doc.setFontType('normal');
                    doc.text(25, row, 'Celular: ');
                    doc.text(25 + column, row, item_users.phone);

                    row += increment;
                    doc.setFontSize(10);
                    doc.setFontType('normal');
                    doc.text(25, row, 'Fecha Nac.: ');
                    doc.text(25 + column, row, item_users.date);

                    row += increment;
                    doc.setFontSize(10);
                    doc.setFontType('normal');
                    doc.text(25, row, 'Ubicación: ');
                    doc.text(25 + column, row, item_users.municipio);

                    row += increment;
                    doc.setFontSize(10);
                    doc.setFontType('normal');
                    doc.text(25, row, 'Fecha visita: ');
                    doc.text(25 + column, row, item_users.visito);

                    line_end = row;
                    doc.setLineWidth(0.3);
                    doc.line(20, line_inic, 20, line_end); // horizontal line

                    row += (increment*2);

                });
            }
        });


        $("#div-download-pdf").removeClass("scale-transition scale-out");
        $("#div-download-pdf").addClass("scale-transition scale-in");
        
        

        var backgroundColor = [
            "#0d47a1",
            "#009688",
            "#84FF63",
            "#263238",
            "#6384FF",
            "#3e95cd", 
            "#8e5ea2",
            "#3cba9f",
            "#e8c3b9",
            "#c45850"
        ];


        
        //INICIO - Diagrama de torta (Pie Chart)
        new Chart(document.getElementById("pie-chart"), {
            type: 'pie',
            data: {
              labels: list_labels,
              datasets: [{
                label: "publicacion (usuarios)",
                backgroundColor: backgroundColor,
                data: list_value
            }]
            },
            options: {
                  title: {
                    display: true,
                    text: title_report
                }
            }
        });
        //FIN - Diagrama torta

        
        //INICIO - Diagrama Barras
        new Chart(document.getElementById("bar-chart"), {
            type: 'bar',
            data: {
                labels: list_labels,
                datasets: [{
                        label: "Vistas (persona)",
                        backgroundColor: backgroundColor,
                        data: list_value,
                    }]
            },
            options: {

                legend: { display: false },
                title: {
                    display: true,
                    text: title_report
                },
                scales: {
                xAxes: [{
                    gridLines: {
                        display: false,
                        color: "black"
                    },
                    ticks: {
                        beginAtZero:true,
                        stepSize: 1
                    },
                    scaleLabel: {
                        display: true,
                        labelString: "Visitas",
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    gridLines: {
                        color: "black",
                        borderDash: [2, 5],
                    },
                    ticks: {
                        beginAtZero:true,
                        stepSize: 1
                    },
                    scaleLabel: {
                        display: true,
                        labelString: "Personas",
                        fontColor: "black"
                    }
                }]
            },
                elements: {
                    rectangle: {
                      borderSkipped: 'left',
                  }
              }
            }
        });
        //FIN - Diagrama Barras
        
        
    });




    
}

function Download_PDF() {
    doc.save('listado_usuarios_visitas_perfil.pdf');
}