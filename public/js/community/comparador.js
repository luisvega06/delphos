$(document).ready(function(){
	$( "#btn-compare" ).click(function() {

		var vala = $('#institucion_a').val()
		var valb = $('#institucion_b').val()

		
		if (vala == null || valb == null) {
			var message = "";
			if (vala == null) {
				message += 'Debe seleccionar una institucion académica entre las opcion A.\n';
			}

			if (valb == null) {
				message += 'Debe seleccionar una institucion académica entre las opcion B.\n';
			}

			//"error", "success" and "info"
			swal({
				icon  : 'error',
				title : 'Oups!',
				text  : message,
				button: 'ok',
			});



			return;
		}

	
		$.get('/api/comparadorinstitutions/' + vala + '/' + valb + '/', function(data) {

			$.each(data, function(i, item) {

				if (item.index == 1) {
					$("#title-name-program-a").html('<b>' + item.content.name.toUpperCase() + '</b><br><i> &nbsp; &nbsp; ' +item.content.programs_name.toUpperCase() + '</i>');
	            	$("#avatar-a").attr("src", item.content.avatar);
					$("#type-institution-a").html(item.content.tipo_institucion);
					$("#name-a").html(item.content.name);
					$("#ubicacion-a").html(item.content.ubicacion);
					$("#address-a").html(item.content.address);
					$("#formacion-a").html(item.content.formacion);
					$("#modalidad-a").html(item.content.modalidad);
					$("#programs_name-a").html(item.content.programs_name);
					$("#duracion-a").html(item.content.duracion);
					$("#program_credits-a").html(item.content.program_credits);
					$("#program_codigo_snies-a").html(item.content.program_codigo_snies);
					$("#program_last_accreditation-a").html(item.content.program_last_accreditation);
					$("#costo-a").html(item.content.program_price);
					$("#costo_inscripcion-a").html(item.content.program_inscription_price);
					$("#pensum-a").attr('href', item.content.program_url_pensum);

					var chips_content = "";
					for (var i = 0; i < item.afines.length ; i++) {
						chips_content += '<div class="chip">'+item.afines[i]+'</div>';
					}
					$("#chips-a").html(chips_content);

	            }else{
	            	if (item.index == 2) {
	            		$("#title-name-program-b").html('<b>' + item.content.name.toUpperCase() + '</b><br><i>' +item.content.programs_name.toUpperCase() + '</i> &nbsp; &nbsp; ');
	            		$("#avatar-b").attr("src", item.content.avatar);
						$("#type-institution-b").html(item.content.tipo_institucion);
						$("#name-b").html(item.content.name);
						$("#ubicacion-b").html(item.content.ubicacion);
						$("#address-b").html(item.content.address);
						$("#formacion-b").html(item.content.formacion);
						$("#modalidad-b").html(item.content.modalidad);
						$("#programs_name-b").html(item.content.programs_name);
						$("#duracion-b").html(item.content.duracion);
						$("#program_credits-b").html(item.content.program_credits);
						$("#program_codigo_snies-b").html(item.content.program_codigo_snies);
						$("#program_last_accreditation-b").html(item.content.program_last_accreditation);
						$("#costo-b").html(item.content.program_price);
						$("#costo_inscripcion-b").html(item.content.program_inscription_price);
						$("#pensum-b").attr('href', item.content.program_url_pensum);

						var chips_content = "";
						for (var i = 0; i < item.afines.length ; i++) {
							chips_content += '<div class="chip">'+item.afines[i]+'</div>';
						}
						$("#chips-b").html(chips_content);
					}
	            }

	        });
			var h =  $( window ).height();
			var body = $("html, body");
			body.stop().animate({scrollTop:h*2}, h*2, 'swing', function() {});


		});

	});
});