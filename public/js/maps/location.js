window.addEventListener("load", inicio);

function inicio() {
  navigator.geolocation.getCurrentPosition(alExito, alError);
}

function alExito(info) {
  var latitude  = info.coords.latitude;
  var longitude = info.coords.longitude;

  var myLatLng = {lat: latitude, lng: longitude};
  initMap_update(myLatLng);
}

function alError(info) {
  alert("Ocurrio un error");
}