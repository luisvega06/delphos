/*
<meta name="csrf-token" content="{{ csrf_token() }}">
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

*/

$(function() {
	/*Define some constants */
	//const ARTICLE_TITLE =  document.title;
	//const ARTICLE_URL = encodeURIComponent(window.location.href);
	//const MAIN_IMAGE_URL = encodeURIComponent($('meta[property="og:image"]').attr('content'));

	$('#share-fb').click(function(){
		open_window('https://www.facebook.com/sharer/sharer.php?u='+$('meta[property="og:url"]').attr('content')+'&amp;src=sdkpreparse', 'facebook_share');
	});

	
	$('#share-twitter').click(function(){
		open_window('http://twitter.com/intent/tweet?url='+$('meta[property="og:url"]').attr('content')+'&amp;text= Visita &amp;hashtags=Universidad%2CEstudios& &amp;via=WebDelphos&', 'twitter_share');
	});

	$('#share-google-plus').click(function(){
		open_window('https://plus.google.com/share?url='+$('meta[property="og:url"]').attr('content'), 'google_share');
	});

	function open_window(url, name){
		window.open(url, name, 'height=320, width=640, toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=no, directories=no, status=no');
	}

	
});