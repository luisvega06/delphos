$(document).ready(function() {
  //Autocomplete
  $(function() {
    $.ajax({
      type: 'GET',
      url: '/getInstituciones-name-avatar',
      success: function(response) {
        var countryArray = response;
        var dataCountry = {};
        for (var i = 0; i < countryArray.length; i++) {
          
          dataCountry[countryArray[i].name] = countryArray[i].avatar;
        }
        $('input.autocomplete').autocomplete({
          data: dataCountry,
          limit: 5,
        });
      }
    });
  });
});