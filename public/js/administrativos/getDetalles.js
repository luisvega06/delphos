function info(argument) {

 $.get('/api/get-details-institution/' + argument + '/', function(data) {
    
    $.each(data, function(i, item) {
      
      $("#avatar").attr("src", item.avatar);
      $("#nombre").html(item.name);
      $("#email").html(item.email);
      $("#celular").html(item.number);
      $("#departamento").html(item.state);
      $("#ciudad").html(item.city);
      $("#dirección").html(item.address);
      $("#fecha").html(item.fecha);
      
      $('#modal1').modal('open');

    });
  });
}
