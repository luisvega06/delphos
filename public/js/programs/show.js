function flinke(btn, icon, inst, prog, u, t) {

    /*
    // Versión Corazón
    var click    =  'fas fa-heart fa-2x';
    var notclick =  'far fa-heart fa-2x';
    */

    /*
    
    // Versión Diamante
    var click    =  'fas fa-gem fa-2x';
    var notclick =  'far fa-gem fa-2x';
    */

    var click    =  'fas fa-bookmark fa-2x';
    var notclick =  'far fa-bookmark fa-2x';

    var idinstitucion = inst;
    var idprograma    = prog;
    var iduser        = u;

    if ($("#"+icon ).hasClass( click )) {
      //console.log("notclick");
      $("#"+icon).removeClass(click);
      $("#"+icon).addClass(notclick);

    } else {
      //console.log("click");
      $("#"+icon).removeClass(notclick);
      $("#"+icon).addClass(click);
    }
    
    $.ajax({
      method: 'post',
      url: '/programs/like-program',
      data: {
        _token : t,
        idinstitucion : idinstitucion,
        idprograma    : idprograma,
        iduser        : iduser,
      },
      success: function(data){
        console.log(' Respuesta:  ' + data);
        
      },
      error: function(data){
        var errors = data.responseJSON;
        console.log('Problemas de conexión con el servidor ' + errors);
      }
    });

}

function delete_function(argument) {
  //alert("id: " + argument);
  var form = document.getElementById("form_delete"+argument);
  form.submit();
}