function clickcount(_t, arg1, arg2, arg3) {
	$.ajax({
    method: 'POST',
    url: '/programs/count-click',
    data: {
      _token : _t,
      arg1   : arg1,
      arg2   : arg2,
      arg3   : arg3,
    }
    ,success: function(data){}
    ,error: function(data){
      var errors = data.responseJSON;
      console.log('Problemas de conexión con el servidor ' + errors);
    }
  });
}