function verificar(){

    var suma = 0;
    var los_cboxes = document.getElementsByClassName('chk');
    var list_options = [];

    for (var i = 0, j = los_cboxes.length; i < j; i++) {

        if(los_cboxes[i].checked == true){
            suma++;
            //console.log("\n\n Select: " + los_cboxes[i].name);
            list_options.push(los_cboxes[i].name);
        }
    }

    if(suma == 0){
        //alert('debe seleccionar por lo menos 1 materia maximo 3');
        swal ( "Oops" ,  'Debe seleccionar por lo menos [1] opción.' ,  "error" );

        return false;
    }else{
        if(suma > 3){
                //alert('solo puede seleccionar maximo 3 materias');
                swal ( "Oops" ,  'Debe seleccionar maximo 3 opciones.' ,  "error" );
                return false;
        }else {

            //alert(suma);
            //document.location.href ="/question-three";

            var contenido = {
                'contenedor': list_options
            };
            saved_option(list_options, "/question-three")
        }

    }

}

function saved_option(list_options, URL) {
    $.ajax({
        method: 'POST',
        url: '/question-two/create',
        data: {
            contenido: list_options,
            _token: $('meta[name="csrf-token"]').attr('content'),
        },
        success: function(data){
            //console.log(data);
            location.href = URL;
        },
        error: function(data){
            location.href = "/question-two";
            
        }
    });
}