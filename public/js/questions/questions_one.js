$(document).ready(function(){

	$(".chk").click(function(){

        //console.log('\n\nselecionada' + this.id + ", Name= " + this.name + "\n\n");

        if (this.id == 'cb1' || this.id == 'cb2') {

        	//location.href ="/question-two";
        	saved_option(this.name, '/question-two');

        }else{
        	//location.href ="/";
        	saved_option(this.name, '/');
        }
    });

});

function saved_option(argument, URL) {
	$.ajax({
		method: 'POST',
		url: '/question-one/create',
		data: {
			name : argument,
			_token: $('meta[name="csrf-token"]').attr('content'),
		},
		success: function(data){
			location.href = URL;
		},
		error: function(data){
			var errors = data.responseJSON;
			console.log(errors);
			location.href = "/question-one";
		}
	});
}