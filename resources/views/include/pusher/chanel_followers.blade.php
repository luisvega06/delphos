
@auth
	@if (Auth::user()->rols_id == 3)
	
		{{-- receive notifications --}}
		<script src="{{ asset('js/echo.js') }}"></script>
		<script src="{{ asset('js/pusher.min.js') }}"></script>
		{{-- <script src="https://js.pusher.com/4.3/pusher.min.js"></script> --}}

		<script>

			Pusher.logToConsole = false;

			window.Echo = new Echo({
				broadcaster: 'pusher',
				key: '5ce7a14ad6495f63acbf',
				cluster: 'mt1',
				encrypted: true,
				logToConsole: true
			});

			Echo.private('user.{{ Auth::user()->id }}')
				.listen('.NewFollowerNotification', (e) => {
					var count =  parseInt($('#label-count').text());
					$("label[for = label-count]").text(count+1);
					//alert(JSON.stringify(e));
		  			$("#icon-focus").addClass('yellow-text');

					Push.create('Buenas noticias!', {
					    body: "{{Auth::user()->name }}, tú listado de followers se ha actualizado.",
					    icon: '{{ asset(Auth::user()->avatar) }}',
					    timeout: 4000,
					    onClick: function () {
					        window.focus();
					        window.open("https://www.delphosacademico.com//List-of-Followers");
					        this.close();
					    }
					});
			});

		</script>
		{{-- receive notifications --}}

	@endif

@endauth