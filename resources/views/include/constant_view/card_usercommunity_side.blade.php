<div class="user-view">
	<div class="background" style="height: 200px">
		<img src="{{ asset('images/img_default/background.jpg') }}" height="220"  />
	</div>
	
	<img class="circle" src="{{ asset(Auth::user()->avatar) }}" width="64" height="64" />

	<span class="white-text name">
		<i class="fas fa-user white-text"></i>
		<b>{{ Auth::user()->name }}</b>
	</span>
	<label class="white-text email">
		<label class="white-text"><i class="fas fa-envelope white-text"></i></label>
		<label class="white-text">{{ Auth::user()->email }}</label>
	</label>
</div>