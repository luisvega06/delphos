
<a  href="{{ url('/') }}" class="waves-effect waves-light white-text">
	<div class="row">
		<div class="col">
			<img src="{{ asset('images/img_logos_delphos/lgdelphosnotext.png') }}" height="30" width="30" style="margin-top: 10px;" />
		</div>
		<div class="col">
			<b>Delphos Académico</b>
		</div>
	</div>	
</a>
