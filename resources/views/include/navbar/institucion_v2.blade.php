      
<!-- INICIO Dropdown Structure -->
<ul class="dropdown-content dropdowncontent collection " id="dropdown1">
  <h6 class="black-text center">Tu información: </h6>
  <li class="divider"></li>
  <li class="collection-item avatar valign-wrapper white">
    <img class="circle" src="{{ asset(Auth::user()->avatar) }}"/>
    <span class="title center" style="font-size: medium; line-height: normal;">
      {{Auth::user()->name}}
    </span>
  </li>
  <li>
    <a class="black-text" href="{{ url('/institutions') }}" >
      <i class="fas fa-home"></i>Home
    </a>
  </li>
  <li>
    <a class="black-text" href="/institutions/{{Auth::user()->id}}/edit" >

      <i class="fas fa-edit"></i>Perfil
    </a>
  </li>
  <li class="">
    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form1').submit();" class="black-text">
      {{ __('Salir') }}
      <i class="fas fa-university"></i>
    </a>

    <form id="logout-form1" action="{{ route('logout') }}" method="POST" style="display: none;">
      @csrf
    </form>
  </li>
</ul>

<!-- FIN Dropdown Structure -->

<div class="navbar-fixed">
  <nav class="black navbar-fixed" style="height: 64px;">
    <div class="nav-wrapper">

      <ul class="right hide-on-med-and-down">
        <li>
          <form method="GET" action="/panel-buscar/">
            
            <div class="input-field">

              <input id="search" name="search" type="search" placeholder="Barra de busqueda" style="height: 64px; margin-bottom: 0px;"maxlength="255" required = "" >

              <label class="label-icon" for="search">
                <i class="material-icons ">search</i>

              </label>

              <i class="material-icons" onclick="funcion_limpiar()" >close</i>

            </div>
          </form>
        </li>

        <!-- Dropdown Trigger -->
        <li>
          <a class="dropdown-trigger"  data-target='dropdown1' style="margin-right: 70px;">
            <img align="center" class="circle" src="{{ asset(Auth::user()->avatar) }}" width="35px" height="35px" />
            <i class="fas fa-sort-down"></i>
          </a>
        </li>

      </ul>

      
      <a href="#" data-target="slide-out" class="sidenav-trigger">
        <i class="fas fa-bars"></i>
      </a>


    </div>
  </nav>
</div>

@include('include.index.navbarsearch')

