      
<!-- INICIO Dropdown Structure -->

<ul class="dropdown-content dropdowncontent collection " id="dropdown1">
  <h6 class="black-text center">Tu información: </h6>
  <li class="divider"></li>
  <li class="collection-item avatar valign-wrapper white">
    <img class="circle" src="{{ asset(Auth::user()->avatar) }}"/>
    <span class="title center" style="font-size: medium; line-height: normal;">
      {{Auth::user()->name}}
    </span>
  </li>
  <li>
    <a class="black-text" href="{{ url('/community') }}" >
      <i class="fas fa-home"></i>Inicio
    </a>
  </li>
  <li>
    <a class="black-text" href="/community/{{Auth::user()->id}}" >
      <i class="fas fa-edit"></i>Perfil
    </a>
  </li>
  <li>
    <a class="black-text" href="/community/{{Auth::user()->id}}/edit" >

      <i class="fas fa-cog"></i>Configuraciones
    </a>
  </li>
  <li class="">
    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form1').submit();" class="black-text">
      {{ __('Salir') }}
      <i class="fas fa-university"></i>
    </a>

    <form id="logout-form1" action="{{ route('logout') }}" method="POST" style="display: none;">
      @csrf
    </form>
  </li>
</ul>

<!-- FIN Dropdown Structure -->

<div class="navbar-fixed">
  <nav class="black navbar-fixed" style="height: 64px;">
    <div class="nav-wrapper">

      <div class="p-l-m-50">

        <a href="{{ url('/community') }}" class="brand-logo">
          <img src="{{ asset('images/img_logos_delphos/logo.png') }}" height="60" width="160" >
        </a>

        
        <ul class="right hide-on-med-and-down">


          <li>
            <form method="GET" action="/panel-buscar/">
              
              <div class="input-field">

                <input id="search" name="search" type="search" placeholder="Barra de busqueda" style="height: 64px; margin-bottom: 0px;" maxlength="255" required>

                <label class="label-icon" for="search">
                  <i class="material-icons ">search</i>

                </label>

                <i class="material-icons" onclick="funcion_limpiar()" >close</i>

              </div>
            </form>
          </li>

          <li>

            <a href="/notificaciones">
              @if(sizeof($notificaciones) > 0)
              <i class="fas fa-lightbulb yellow-text tooltipped" data-position="bottom" data-tooltip="{{sizeof($notificaciones) }}  Notif."></i>
              
              @else
              
              <i class="fas fa-lightbulb"></i>

              @endif
            </a>
            
          </li>

          <li>
            <a href="/comparar-instituciones">
              
              <i class="fas fa-balance-scale"></i>
            </a>
          </li>

          <li>
            <a href="/descubrir-ofertas-academicas">
              
              <i class="fas fa-binoculars"></i>

            </a>
          </li>
          
          <li>
            <a href="/ranking">
              <img align="center" src="{{ asset('images/img_logos_delphos/lgdelphosnotext.png') }}" width="25px" height="25px" />
            </a>
          </li>

          <!-- Dropdown Trigger -->
          <li>
            <a class="dropdown-trigger"  data-target='dropdown1' style="margin-right: 70px;">
              <img align="center" class="circle" src="{{ asset(Auth::user()->avatar) }}" width="35px" height="35px" />
              <i class="fas fa-sort-down"></i>
            </a>
          </li>

        </ul>

      </div>
      <a href="#" data-target="slide-out" class="sidenav-trigger">
        <i class="fas fa-bars"></i>
      </a>


    </div>
  </nav>
</div>

@include('include.index.navbarsearch')

