<nav>
	<div class="nav-wrapper black">
		
		<button data-target="slide-out" class="sidenav-trigger btn btn-floating transparent">
			<i class="fas fa-bars"></i>
		</button>
		
		@auth
			@switch(Auth::user()->rols_id)
				@case(2) 
					<a href="{{ url('/administrativos') }}" class="brand-logo center">
						<img src="{{ asset('images/img_logos_delphos/logo.png') }}" height="60" width="160" >
					</a>
				@break
				@default
					<a href="{{ url('/') }}" class="brand-logo center">
						<img src="{{ asset('images/img_logos_delphos/logo.png') }}" height="60" width="160" >
					</a>
				@break
			@endswitch
		@endauth


	</div>
</nav>