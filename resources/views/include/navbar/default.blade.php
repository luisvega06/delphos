<div class="navbar-fixed">
	<nav class="black">
		<div class="nav-wrapper">
			<div class="container">

				<a href="#" data-target="slide-out" class="sidenav-trigger">
			        <i class="fas fa-bars"></i>
			    </a>

				<a href="/community" class="brand-logo">
					<img src="{{ asset('images/img_logos_delphos/logo.png') }}" height="60" width="160" >
				</a>

				<ul class="right hide-on-med-and-down">

					@auth

						@switch(Auth::user()->rols_id)


						@case(1) 
							<!-- Barra usuario Community -->
							<li>
								<a href="{{ url('/community/')}}">
									Home
								</a>
							</li>
							<li>
								<a class="waves-effect waves-light" href="/community/{{Auth::user()->id}}" >
									Perfil
								</a>
							</li>

						@break

						<!-- Barra usuario Delphos -->
						@case(2)
							<li>
								<a href="{{ url('/administrativos') }}">
									Home Delphos
								</a>
							</li>
							<li>
								<a class="waves-effect waves-light" href="{{ url('/administrativos/' . Auth::user()->username ) }}"  >
									Perfil
								</a>
							</li>
						@break
						<!-- Barra usuario Institutions -->
						@case(3)
						
							<li>
								<a  href="/institutions">
									Home Insitutions
								</a>
							</li>
							<li>
								<a class="waves-effect waves-light" href="/institutions/{{Auth::user()->id}}/edit" >
									Perfil
								</a>
							</li>
						@break
						<!-- Barra usuario SuperUser -->
						@case(4)
							<li>
								<a href="">
									Home Super user
								</a>
							</li>
							<li>
								<a class="waves-effect waves-light" >
									Perfil
								</a>
							</li>
						@break

						@default
						<span> Soy tipo xX {{Auth::user()->rols_id}}</span>
						@break
						@endswitch

					<li>
					    <a class="waves-effect waves-light btn red" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form1').submit();" class="white-text">
					      {{ __('Salir') }}
					    </a>

					    <form id="logout-form1" action="{{ route('logout') }}" method="POST" style="display: none;">
					      @csrf
					    </form>
					</li>

					@else
						@if(false)
						<li>
							<a href="{{ url('/community/create')}}">
								Regístrate
							</a>
						</li>
						<li>
							<a class="waves-effect waves-light btn blue darken-3 btn-radius" href="{{ url('/login')}}">
								Iniciar Sesión
							</a>
						</li>
						@endif
					@endauth
				</ul>

			</div>
		</div>
	</nav>
</div>
