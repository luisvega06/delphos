	<li style="height: 200px">
		@include('include.constant_view.card_user_side')
	</li>

	<li >

		<a href="/List-of-Followers" target="_self" class="white-text">

			<span class="badge white-text" id="conte-notif" name="conte-notif">

				@if($count_followers > 0)
					<i class="fas fa-lightbulb yellow-text" id="icon-focus" name="icon-focus"></i> 
				@else
					<i class="fas fa-lightbulb" id="icon-focus" name="icon-focus"></i> 
				@endif
				
				<label for="label-count" id="label-count" name="label-count">{{$count_followers}}</label>
				
			</span>
			
			<b>Notificaciones</b>

		</a>
		
	</li>
	
	<li  style="line-height: normal">
		<a href="{{ url('/institutions') }}" class="white-text">
			<i class="fas fa-home white-text" ></i>
			Home
		</a>
	</li>

	<li id="element" name="element">
      <div class="collapsible-header white-text" id="button" name="button"  onclick="function_set_icon();">

      	
      		<i class="fas fa-graduation-cap white-text fa-xs" style="font-size: 16px; margin-left: 10px;"></i><b style="padding-left: 16px;">Programas</b>
      		<i  id="icon_click" name="icon_click" class="fa fa-angle-right"></i>
      	

      </div>

      <div class="collapsible-body grey darken-1">
      	<ul >

      		<li>
      			<a href="{{ url('/programs/create') }}" class="white-text">
      				<i class="fas fa-plus white-text"></i>
      				Crear programa
      			</a>
      		</li>

      		<li>
      			<a href="{{ url('/programs') }}" class="white-text">
      				<i class="far fa-eye white-text"></i>
      				Ver programas
      			</a>
      		</li>
      	</ul>
      </div>
    </li>

	<li  style="line-height: normal">
		<a href="{{ url('/ranking') }}" class="white-text">
			<i class="fas fa-broadcast-tower white-text"></i>
			Ranking
		</a>
	</li>

	<li  style="line-height: normal">
		<a href="{{ url('/alianzas-estrategicas/'.$user->username) }}" class="white-text">
			<i class="far fa-handshake white-text"></i>
			Financiación
		</a>
	</li>


	<li  style="line-height: normal">
		<a href="{{ url('/institutions-googlemaps/' . $institution->slug) }}" class="white-text">
			<i class="fas fa-map-marker-alt white-text"></i>
			Ubicación
		</a>
	</li>

	
	<li  style="line-height: normal">
		<a href="graphics/{{$institution->slug}}" class="white-text">
			<i class="fas fa-chart-bar white-text"></i>
			Indicadores
		</a>
	</li>

	<li  style="line-height: normal">
		<a href="{{ url('/institutions/' . Auth::user()->id  . '/edit') }}" class="white-text">
			<i class="fas fa-cog white-text"></i>
			Configuración
		</a>
	</li>

	<li   style="line-height: normal">
		<a href="{{ route('logout') }}"
		    onclick="event.preventDefault();
		    document.getElementById('logout-form').submit();"  

		    class="waves-effect white-text" data-toggle="tooltip" title="Salida segura." >
		      <i class="fas fa-sign-out-alt white-text"></i>
		      {{ __('Salir') }}
		      
		</a>

		<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
		    @csrf
		</form>
	</li>


@section('extra_scripts_DOMContentLoaded')
<script>
	function function_set_icon() {
		console.log("setiando");
		$('#icon_click').toggleClass('fa-angle-down').toggleClass('fa-angle-right');
	}
	/*

	$(document).ready(function(){

		setInterval(function(){
			$.get('api/realtime_followers/'+{{$institution->id}}+'/reporte', function(data) {
	        	if (data.add) {
	        		var value = $("label[for*='label-count']").html();
	        		if (data.list > value) {
						$("label[for*='label-count']").html(data.list);
        				$("i[name*='icon-focus']").addClass('fas fa-lightbulb yellow-text');
        				Number.prototype.format = function(n, x) {
        					var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
        					return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&.');
        				};
        				var new_notify = data.list - value;
        				var toastHTML = 'Tienes '+ new_notify.format(0, 3, '.', ',')+' Notificaciones';
  						M.toast({html: toastHTML, classes: 'rounded'});
	        		}
	        	}
	        });
		}, 5000);

	});
	*/

</script>
@endsection