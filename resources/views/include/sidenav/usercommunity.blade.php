<ul class="sidenav white-text blue-grey darken-4" id="slide-out">
@auth
	
	<li style="height: 200px">
		@include('include.constant_view.card_usercommunity_side')
	</li>

	<li>
		<a  class=" white-text waves-effect waves-light" href="{{ url('/community') }}" >
			<i class="white-text fa fa-home fa-lg"></i>
			Inicio
		</a>
	</li>
	<li>
		<a class="white-text" href="{{ url('/community/' . Auth::user()->id ) }}" >
	      <i class="white-text fas fa-user fa-lg"></i>
	      Perfil
	    </a>
	</li>

	<li>
               <a class="white-text" href="/notificaciones">
			@if(sizeof($notificaciones) > 0)
                <i class="fas fa-lightbulb yellow-text fa-lg pulse"></i>
				<span class="white-text pulse">Notificaciones</span>
              @else
                <i class="fas fa-lightbulb white-text fa-lg"></i>
                Notificaciones
              @endif
		</a>
	</li>

	<li>
		<a class="white-text" href="/comparar-instituciones">
			<i class="fas fa-balance-scale white-text fa-lg"></i>
			Comparar
		</a>
	</li>

	<li>
		<a class="white-text"  href="/descubrir-ofertas-academicas">

			<i class="fas fa-binoculars white-text fa-lg"></i>
			Filtrar
		</a>
	</li>

	<li>
		<a class="white-text" href="/ranking">
			<img align="center" src="{{ asset('images/img_logos_delphos/lgdelphosnotext.png') }}" height="20px" width="20px"/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Descubrir
		</a>
	</li>

	<li>
		<a class="white-text" href="{{ url('/community/' . Auth::user()->id . '/edit') }}" >
	      <i class="white-text fas fa-cog fa-lg"></i>
	      Configuraciones
	    </a>
	</li>

	<li>
		<a href="{{ route('logout') }}"
		    onclick="event.preventDefault();
		    document.getElementById('logout-form').submit();"  

		    class=" white-text">
		      <i class="fas fa-sign-out-alt white-text fa-lg"></i>
		      {{ __('Salir') }}
		      
		</a>

		<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
		      @csrf
		</form>
	</li>

@else
	<li class=" white-text  blue-grey darken-4">
		@include('include.constant_view.sidenav_header_delphos')
	</li>

	<li>
		<a  class=" white-text waves-effect waves-light" ref="{{ url('/login')}}" >
			<i class=" white-text fa fa-user fa-lg"></i>
			Iniciar Sesión
		</a>
	</li>

	<li>
		<hr />
		<a href="{{ url('/community/create')}}" class=" white-text waves-effect waves-light">
			<i class=" white-text fas fa-edit fa-lg"></i>
			Crear cuenta
		</a>
	</li>

	<li>
		<hr />
		<a href="{{ url('/institution/login')}}" class="white-text waves-effect waves-light">
			<i class="white-text fas fa-university fa-lg"></i>
			Institución
		</a>
	</li>
@endauth
</ul>