	<li style="height: 200px">
		<div class="user-view">
			<div class="background" style="height: 200px">
				<img src="{{ asset('images/img_default/background.jpg') }}" height="220"  />
			</div>

			<img class="circle" src="{{ asset($user->avatar) }}" width="64" height="64" />

			<span class="white-text name">
				<i class="fas fa-university white-text"></i>
				<b>{{ $user->name }}</b>
			</span>
			<span class="white-text email">
				<i class="fas fa-envelope white-text"></i>
				{{ $user->email }}
			</span>
		</div>
	</li>

	<li  style="line-height: normal; height: 35px;" class="white-text valign-wrapper  blue-grey lighten-1">
		<span class="p20">
			<b>Clasificación: </b> {{ $type_institutions->name }}
		</span>
	</li>
	
	<li  style="line-height: normal">
		<a href="{{ url('/') }}" class="waves-effect waves-light white-text">
			<i class="fas fa-home white-text" ></i>
			Home
		</a>
	</li>

	<li  style="line-height: normal">
		<a href="{{ url('/presentacion/'.$institution->slug) }}" class="waves-effect waves-teal white-text">
			<i class="fas fa-external-link-alt white-text"></i>
			Comunidad
		</a>
	</li>


	<li style="line-height: normal">
		<a href="{{ url('/programs/ofertas/'.$user->username) }}" class="waves-effect waves-light white-text">
			<i class="fas fa-graduation-cap white-text white-text"></i>
			Programas
		</a>
	</li>

	<li style="line-height: normal">
		<a href="{{ url('/alianzas-estrategicas/'. $user->username) }}" class="waves-effect waves-light white-text">
			<i class="far fa-handshake white-text"></i>
			Financiación
		</a>
	</li>

	<li  style="line-height: normal">
		<a href="{{ url('/institutions-googlemaps/' . $institution->slug) }}" class="waves-effect waves-light white-text">
			<i class="fas fa-map-marker-alt white-text"></i>
			Ubicación
		</a>
	</li>

	{{-- Contacto --}}
	@auth


	<li  style="line-height: normal">
		<a class="waves-effect waves-teal white-text"  id="btn-ws" onclick="send_contact();">
			<i class="fab fa-whatsapp green-text"></i>
			Contactar
		</a>
	</li>

	@endauth

	<li   style="line-height: normal">
		<a href="{{ route('logout') }}"
		    onclick="event.preventDefault();
		    document.getElementById('logout-form').submit();"  

		    class="waves-effect white-text" data-toggle="tooltip" title="Salida segura." >
		      <i class="fas fa-sign-out-alt white-text"></i>
		      {{ __('Salir') }}
		      
		</a>

		<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
		      @csrf
		</form>
	</li>
