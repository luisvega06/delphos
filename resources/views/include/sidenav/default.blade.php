<ul class="sidenav white-text blue-grey darken-4" id="slide-out">
	

	@auth

		<li style="height: 200px">
			@include('include.constant_view.card_usercommunity_side')
		</li>


		@switch(Auth::user()->rols_id)


			@case(1) 
				<!-- Barra usuario Community -->
				<li>
					<a  class=" white-text waves-effect waves-light" href="{{ url('/community') }}" >
						<i class="white-text fa fa-home fa-lg"></i>
						Inicio
					</a>
				</li>
				<li>
					<a class="white-text" href="{{ url('/community/' . Auth::user()->id ) }}" >
				      <i class="white-text fas fa-user fa-lg"></i>
				      Perfil
				    </a>
				</li>
			@break

			<!-- Barra usuario Delphos -->
			@case(2)
				<li>
					<a href="{{ url('/administrativos') }}" >
						Inicio
					</a>
				</li>
				<li>
					<a class="waves-effect waves-light" href="{{ url('/administrativos/' . Auth::user()->username ) }}">
						Perfil
					</a>
				</li>
			@break
			<!-- Barra usuario Institutions -->
			@case(3)

				<li  style="line-height: normal">
					<a href="{{ url('/institutions') }}" class="white-text">
						<i class="fas fa-home white-text" ></i>
						Inicio
					</a>
				</li>

				<li  style="line-height: normal">
					<a href="/institutions/{{Auth::user()->id}}/edit" class="white-text">
						<i class="fas fa-user-alt white-text"></i>
						Perfil
					</a>
				</li>
			@break
			<!-- Barra usuario SuperUser -->
			@case(4)
				<li>
					<a href="">
						Inicio
					</a>
				</li>
				<li>
					<a class="waves-effect waves-light" >
						Perfil
					</a>
				</li>
			@break

			@default
				<span> Soy tipo xX {{Auth::user()->rols_id}}</span>
			@break
		@endswitch
		


		<li>
			<a class="waves-effect waves-light white-text" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form1').submit();" class="white-text">
			    <i class="fas fa-sign-out-alt white-text fa-lg"></i>
				Salir
			</a>

			<form id="logout-form1" action="{{ route('logout') }}" method="POST" style="display: none;">
				@csrf
			</form>
		</li>
	@else

		<li>
			@include('include.constant_view.sidenav_header_delphos')
		</li>

		<li>
			<a  class="waves-effect waves-ligh white-text" href="community">
				<i class="fa fa-user fa-lg  white-text"></i>
				Iniciar Sesion
			</a>
		</li>

		<li>
			
			<a  class="waves-effect waves-light  white-text" href="{{ url('/community/create')}}">
				<i class="fas fa-edit fa-lg  white-text"></i>
				Crear cuenta
			</a>
		</li>
	@endauth
</ul>
