<ul class="sidenav white-text blue-grey darken-4" id="slide-out">
	@auth
		
		<li style="height: 200px">
			@include('include.constant_view.card_usercommunity_side')
		</li>

		<li>
			<a class=" white-text waves-effect waves-light" href="{{ url('/administrativos') }}" >
				<i class="white-text fa fa-home fa-lg"></i>
				Inicio
			</a>
		</li>

		<li>
			<a class="white-text" href="{{ url('/administrativos/' . Auth::user()->username ) }}" >
		      <i class="white-text fas fa-user fa-lg"></i>
		      Perfil
		    </a>
		</li>

		<li>
			<a href="{{ route('logout') }}"
			    onclick="event.preventDefault();
			    document.getElementById('logout-form').submit();"  

			    class=" white-text">
			      <i class="fas fa-sign-out-alt white-text fa-lg"></i>
			      {{ __('Salir') }}
			      
			</a>

			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
			      @csrf
			</form>
		</li>

	@else
		<li class=" white-text  blue-grey darken-4">
			@include('include.constant_view.sidenav_header_delphos')
		</li>

		<li>
			<a class=" white-text waves-effect waves-light" ref="{{ url('/login')}}" >
				<i class=" white-text fa fa-user fa-lg"></i>
				Iniciar Sesión
			</a>
		</li>
	@endauth
</ul>