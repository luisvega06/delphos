<div class="section white">
    <div class="row center">
        <div class="col xl4 l4 m4 s12">
            <!-- Promo Content 1 goes here -->
            <i class="fas fa-globe fa-5x">
            </i>
            <h5>
                Ciudades
            </h5>
            <p>
                Encuentra la ciudad destino de mayor conveniencia en la cual deseas buscar instituciones apropiadas para la selección del programa de educación superior que quieres analizar.
            </p>
            <br/>
            <br/>
        </div>
        <div class="col xl4 l4 m4 s12">
            <!-- Promo Content 2 goes here -->
            <i class="fas fa-university fa-5x">
            </i>
            <h5>
                Universidades
            </h5>
            <p>
                Verifica y compara la información que te brinda cada institución, acerca de su campus, programas de educación, costos de sus programas, mallas curriculares, entre otros.
            </p>
            <br/>
            <br/>
        </div>
        <div class="col xl4 l4 m4 s12">
            <!-- Promo Content 3 goes here -->
            <i class="fas fa-graduation-cap fa-5x">
            </i>
            <h5>
                Programas
            </h5>
            <p>
                Toma la mejor decisión, seleccionando previamente entre las ofertas disponibles, agrega programas a la lista de favoritos y gestiona tu inscripción directamente desde tu equipo o móvil.
            </p>
            <br/>
            <br/>
        </div>
    </div>
</div>

{{--
<div class="parallax-container">
    <div class="parallax">
        <img class="responsive-img" src="{{ asset('images/img_logos_delphos/parallax.jpg') }}">
    </div>
</div>
--}}
<div>
    <img class="responsive-img" src="{{ asset('images/img_logos_delphos/parallax.jpg') }}" style="width: 100%;">
</div>