<section id="section_3">
	<footer class="page-footer grey darken-1">
		<div class="container">
			<div class="row">
				<div class="col l6 s12">
					<h5 class="white-text">
						Contacto
					</h5>
					<p class="grey-text text-lighten-4">
						Comunicate con nosotros por medio de nuestra plataforma
						<br/>
						<b>
							Delphos Académicos
						</b>
						y/o en todas nuestras redes sociales.
					</p>
				</div>
				<div class="col l4 offset-l2 s12">
					<h5 class="white-text">
						Redes Sociales
					</h5>
					<div class="row">

						<div class="col s2">

							{{-- Facebook --}}
							<a class="grey-text text-lighten-3 tooltipped" 
								data-delay="50" data-position="bottom" 
								data-tooltip="@WebDelphos" 
								target="_blank" 
								href="https://www.facebook.com/WebDelphos/">

								<i class="fab fa-facebook-square fa-2x"></i>

							</a>

						</div>

						<div class="col s2">

							{{-- Google+ --}}
							<a class="grey-text text-lighten-3 tooltipped" 
								data-delay="50" data-position="bottom" 
								data-tooltip="@WebDelphos" 
								target="_blank" 
								href="https://plus.google.com/u/0/102579668306025643164">

								<i class="fab fa-google-plus-square fa-2x"></i>

							</a>

						</div>

						<div class="col s2">

							{{-- Instagram --}}
							<a class="grey-text text-lighten-3 tooltipped" 
								data-delay="50" 
								data-position="bottom" 
								data-tooltip="@WebDelphos" 
								
								href="https://www.instagram.com/webdelphos/">
								
								<i class="fab fa-instagram fa-2x"></i>

							</a>

						</div>

						<div class="col s2">
							{{-- Google Play --}}
							<a class="grey-text text-lighten-3 tooltipped" 
								data-delay="50" 
								data-position="bottom" 
								data-tooltip="Delphos Academicos" 
								
								href="#!">
								
								<i class="fab fa-google-play fa-2x"></i>
							</a>

						</div>

						<div class="col s2">
							
							<a class="grey-text text-lighten-3 tooltipped" 
								data-delay="50" 
								data-position="bottom" 
								data-tooltip="@WebDelphos" 
								href="#!">
								<i class="fab fa-app-store-ios fa-2x"></i>

							</a>
						</div>

						<div class="col s2">
							
							<a class="grey-text text-lighten-3 tooltipped" 
							data-delay="50" 
							data-position="bottom" 
							data-tooltip="@WebDelphos" 
							target="_blank" 
							href="https://www.youtube.com/channel/UCD4Nw4GAnO48Z6oKzXM8ATw">

								<i class="fab fa-youtube fa-2x"></i>
							</a>

						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
</section>

<!-- footer -->
<div class="footer-copyright">
	<div class="container">
		© 2018 Copyright Delphos Académico, All rights reserved.
		<a class="grey-text text-black right" href="#!">
			Licencia
		</a>
	</div>
</div>
<!-- fin footer -->