{{-- INICIO: Carrusel ///  custom --}}
    <a href="#modal1" class="modal-trigger">
        
        <img class="responsive-img" src="{{ asset('images/img_default/portada.png') }}"  />
    
        
    </a>

    <div id="modal1" class="modal">
        <div class="modal-content">
            <div class="row">
                <h5 class="center">
                    Inicia tu experiencia
                </h5>
                
            </div>
            <div class="row">
                <div class="col s6">
                    <a href="{{ url('/community/create')}}">
                        <img class="responsive-img" src="{{ asset('images/img_default/registro.png') }}"  />
                    </a>
                </div>
                <div class="col s6">
                    <a href="{{ url('/login')}}">
                        <img class="responsive-img" src="{{ asset('images/img_default/iniciar_sesion.png') }}"  />
                    </a>
                </div>
            </div>
        </div>
  </div>

  <script>
    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems, {});
    });
  </script>

  <style>
    #modal1{
        width:30%
    }
    .col{
        text-align: center;
    }
    i{
        font-size: 3em !important;
    }
  </style>
{{-- FIN: Carrusel 


<a class="carousel-item" href="#four!"><img src="https://lorempixel.com/800/400/food/4"></a>
--}}

