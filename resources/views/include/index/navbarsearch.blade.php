<div class=" hide-on-extra-large-only hide-on-large-only ">
  <nav class="right">
    <div class="nav-wrapper grey darken-4" >
      <nav>
        <div class="nav-wrapper white black-text">
          <form method="GET" action="/panel-buscar/">
              
            <div class="input-field">

              <input id="search2" name="search" type="search" placeholder="Instituciones | Ofertas académicas" required="" maxlength="255">

              <label class="label-icon black-text" for="search2">
                <i class="material-icons black-text">search</i>
              </label>

              <i class="material-icons" onclick="funcion_limpiar()" >close</i>

            </div>
          </form>
        </div>
      </nav>
    </div>
  </nav>
  <br/><br/><br/>
</div>
