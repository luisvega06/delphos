{{-- INICIO: Carrusel ///  custom --}}
<div class="carousel carousel-slider" data-indicators="true" >
    <a class="carousel-item" href="#one!">
        <img class="carousel-img" src="{{ asset('images/img_carrusel_index/1.jpg') }}"  />
    </a>
    <a class="carousel-item" href="#two!">
        <img class="carousel-img" src="{{ asset('images/img_carrusel_index/2.jpg') }}"  />
    </a>
    <a class="carousel-item" href="#three!">
        <img class="carousel-img" src="{{ asset('images/img_carrusel_index/3.jpg') }}"  />
    </a>
    <a class="carousel-item" href="#four!">
        <img class="carousel-img" src="{{ asset('images/img_carrusel_index/4.jpg') }}"  />
    </a>
    <a class="carousel-item" href="#five!">
        <img class="carousel-img" src="{{ asset('images/img_carrusel_index/5.jpg') }}"  />
    </a>
    <a class="carousel-item" href="#six!">
        <img class="carousel-img" src="{{ asset('images/img_carrusel_index/6.jpg') }}"  />
    </a>
</div>
{{-- FIN: Carrusel 


<a class="carousel-item" href="#four!"><img src="https://lorempixel.com/800/400/food/4"></a>
--}}

