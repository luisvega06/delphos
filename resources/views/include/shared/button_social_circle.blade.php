<button class="waves-effect waves-light btn-floating btn-flat blue darken-4" 
		id="share-fb">

		<i class="fab fa-facebook-f"></i>

</button>


<button class="waves-effect waves-light btn-floating btn-flat blue darken-1" 
		id="share-twitter">
		
		<i class="fab fa-twitter"></i>

</button>


<button class="waves-effect waves-light btn-floating btn-flat deep-orange darken-1" 
			id="share-google-plus">
	
			<i class="fab fa-google-plus-g"></i>

</button>






