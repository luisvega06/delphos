@extends('layouts.app')
@section('title', 'Restablecer contraseña')
@section('style_body', 'grey lighten-3')

@section('content')

    @include('include.navbar.default')
    @include('include.sidenav.default')

    
    <br/>
    <div class="row container">

        <div class="center col s12 m6 l6 xl6  offset-m3 offset-l3 offset-xl3 white" style="padding: 20px;">
            
            

            <h4>Restablecer contraseña</h4>

            <form method="POST" action="{{ route('password.email') }}">
                @csrf
                <br/>
                <div class="input-field">
                    <input id="email" type="email" class="validate" name="email" value="{{ old('email') }}" required>
                    <label for="email">Correo electronico</label>
                </div>

                <br/>

                <div>
                    @if ($errors->has('email'))
                        <span>
                            <i><strong>
                                {{ $errors->first('email') }}
                                <br/><br/><br/>
                            </strong></i>
                        </span>
                    @endif
                </div>

                <div class="col s12 m12 l12 xl12"> 
                    <button type="submit" class="waves-effect waves-light btn blue  btn-radius col s12">
                        Enviar
                    </button> 
                </div>

            </form>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            
            <a href="/">Volver al home</a>
            <br/>
            <br/>



        </div>
    </div>

    
    @include('include.index.footer')
@endsection
