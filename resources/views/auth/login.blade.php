@extends('layouts.app')

@section('title', 'Iniciar Sesión')

@section('extra_meta')
	@include('layouts.meta_shared.login')
@endsection

@section('style_body', 'background_register')


@section('content')
	
	{{--
		@include('include.navbar.default')
		@include('include.sidenav.default')
	--}}
	

	<div class="container center row">
		<br/>
			<div class="white col s12 m8 l6 xl6 offset-m2 offset-l3 offset-xl3 hoverable z-depth-5">
				<form class="p20" role="form" method="POST" action="{{route('login')}}">
					@csrf
					@include('auth.form')
				</form>
			</div>
	</div>

	
	@include('include.index.footer')
@endsection


@section('extra_scripts')

	<script type="text/javascript">

		
		var mensaje = "";
		@if ($errors->any())
			@foreach($errors->all() as $error)
			mensaje += '{{ $error }}' + '\n';
			@endforeach

			//"error", "success" and "info"
			swal({
				icon  : 'error',
				title : 'Oups!',
				text  : mensaje,
				button: 'ok',
			});

		@endif

		@if( session()->has('adios') )
			swal({
				icon  : 'info',
				title : 'Aviso',
				text  : '{{ session()->get('message') }}',
				button: 'Aceptar',
			});
		@endif
	</script>

@endsection