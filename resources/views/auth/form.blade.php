<div class="row">

    <div class="col s12 m12 l12 xl12 center">
        <img alt="Logo Delphos Académico" class="img-resposive mt10 mb20" src="{{ asset('images/img_logos_delphos/lgdelphos.png') }}" height="110mm" width="120mm" />
    </div>

    {{-- Datos email --}}
    <div class="input-field col s12 m12 l12 xl12">
        <i class="fas fa-user prefix fa-lg" style="margin-top: 10px;"></i>
        <input id="email" name="email" type="email" class="validate" value="{{ old('email') }}">
        <label for="email">Correo</label>
    </div>

    {{-- Datos Password --}}
    <div class="input-field col s12 m12 l12 xl12 ">
        <i class="fas fa-unlock prefix fa-lg" style="margin-top: 10px;"></i>
        <input id="password" name="password" type="password" class="validate">
        <label for="password">Contraseña</label>
    </div>

    {{-- Datos Password --}}
    <div class="input-field col s12 m12 l12 xl12 center mt20">
        
        <button type="submit" 
                class="waves-effect waves-light btn blue darken-3 z-depth-3 col s12 m12 l12 xl12 btn-radius">
            <b>Entrar</b>
        </button>

        <br/>
        <br>
        <hr class="style6">
        

        <a class="col s12 m12 l12 xl12 waves-effect waves-light btn white black-text capitalize btn-radius z-depth-3 mt10" 
            href="login/google">

            <i class="ico-google-color"></i>
            <span>Inicia sesión con Google</span>

        </a>

    </div>

    <div class="input-field col s12" style="margin-bottom: 0px;">
        <p class="center-align" style="margin-bottom: 0px;">

            <a href="{{ route('password.request') }}">
                {{ __('¿Olvidaste la contraseña?') }}
            </a>
            <br/> ¿No tienes una cuenta?
            <a href="{{ url('/community/create') }}">
                Regístrate aquí
            </a>
        </p>
    </div>

</div>
