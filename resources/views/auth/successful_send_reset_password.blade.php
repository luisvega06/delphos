@extends('layouts.app')
@section('title', 'Resent Password')

@section('style_body', 'grey lighten-3')

@section('content')

    @include('include.navbar.default')
    @include('include.sidenav.default')

    
    <br/><br/><br/>
    <div class="row container center">

        <div class="col s12 m12 l12 xl12 white">
            <h4>Solicitud exitosa!</h4>
            <h6>En breve te enviaremos un correo.</h6>

            <br/>

            <i class="fas fa-check-circle green-text animated rubberBand fa-7x"></i>

            <br/><br/>

            <a href="/">Volver al home</a>

            <br/><br/>
        </div>

    </div>

    
    @include('include.index.footer')
@endsection
