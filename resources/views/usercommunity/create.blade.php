@extends('layouts.app')

@section('title', 'Crear cuenta')

@section('style_body', 'background_register')


@section('content')

@include('include.navbar.default')
@include('include.sidenav.default')

<div class="" id="divbody">
<br/>
<div class="container">
<div class="container white">
	<div class="container center">
		<form class="form-group" action="/community" method="POST" >
			@csrf
			<br/>
			<h4><b>Crear cuenta</b></h4>

			<div class="row">

				<!-- Nombres -->
				<div class="input-field col s12 m12 l12 xl12">
					<input name="name" id="name" type="text" class="validate capitalize" maxlength="254" value="{{ old('name') }}">
					<label for="name">Nombres y apellidos</label>
				</div>

				<!-- Username -->
				<div class="input-field col s12 m6 l6 xl6">
					<input name="username" id="username" type="text" class="validate lowercase" minlength="10" maxlength="30" pattern="^(\w){4,30}$" value="{{ old('username') }}" >
					<label for="username">Username</label>
				</div>

				<!-- Phone -->
				<div class="input-field col s12 m6 l6 xl6">
					<input name="number" id="number" type="text" class="validate" maxlength="11" onKeypress="if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;" value="{{ old('number') }}" >
					<label for="number">Celular</label>
				</div>

				<!-- Email -->
				<div class="input-field col s12 m12 l12 xl12">
					<input name="email" id="email" type="email" class="validate lowercase" maxlength="254" value="{{ old('email') }}">
					<label for="email">Correo</label>
					<span id="noti_email"></span>
				</div>

				<!-- Password -->
				<div class="row">
					<div class="input-field col s12 m6 l6 xl6">
						<input id="password" name="password" type="password" class="validate">
						<label for="password">Password</label>
					</div>
					<div class="input-field col s12 m6 l6 xl6">
						<input id="password_confirmation" name="password_confirmation" type="password" class="validate">
						<label for="password_confirmation">Confirmar password</label>
					</div>
				</div>
	<!-- Ubicacion -->
	<div class="row" style="margin-top: -10px;">
		<div class="input-field col s12 m6 l6 xl6">
			<select id="select-project" name="departamento" >
				<option value="0" disabled selected>Departamento
				</option>
<!-- <-- No Mover porque despues no detecta la tecla -->
				@foreach($states as $state )
<option value="{{ $state->id }}" id="op_state" name="op_state" >{{$state->name}}</option>
				@endforeach
			</select>
			<label>
				Departamento
			</label>
		</div>

		<div class="input-field col s12 m6 l6 xl6">
			<select id="select-level" name="municipio" >
				<option disabled="" selected="" value="0">Municipio</option>
			</select>
			<label>
				Municipio
			</label>
		</div>
	</div>

				<!-- Fecha Nacimiento AND Genero -->
				<div class="row">
					<div class="input-field col s12 m6 l6 xl6" style="margin-top: -8px;">
						<label for="icon_prefix">
							Fecha de nacimiento
						</label>
						<br/>
						<input type="date" id="birthdate" name="birthdate" value="{{ old('birthdate') }}"/>
					</div>
					<div class="input-field col s12 m6 l6 xl6">
						<select id="genero" name="genero">
							<option value="0" disabled selected>Seleccione un genero</option>
				@foreach($genders as $gender )
				<!-- <-- No Mover porque despues no detecta la tecla -->
<option value="{{ $gender->id }}" id="op_state" name="op_state" >{{$gender->name}}</option>
				@endforeach
						</select>
						<label>Genero</label>
					</div>
				</div>

				<!-- Aceptar terminos y condiciones-->
				<div class="col s12 m12 l12 xl12">
					<p class="left">
						<label>
							<input type="checkbox" id="terminos" name="terminos" />
							<span>
								Aceptar terminos y condiciones. 
								<a href="{{ url('/terms_and_conditions') }}" class="tooltipped" data-position="bottom" data-tooltip="Click para ver terminos y condiciones">
									Ver terminos
								</a>
							</span>
						</label>
					</p>
					<br/>
					
				</div>
				<span id="noti_terminos"></span>
				<div class="input-field col s12">
					<br/>
					<button class="btn waves-effect waves-light blue lighten-1 col s12" type="submit">
						Registrar
					</button>
				</div>
			</div>
		</div>
	</div>
</form>
</div>
</div>
</div>

@endsection
</div>

@section('extra_scripts')
<!-- Script listar municipios-->
<script src="{{ asset('js/list_municipios.js') }}"></script>

<script type="text/javascript">
	var mensaje = "";
	@if ($errors->any())
	@foreach($errors->all() as $error)
		console.log('{{ $error }}');
		mensaje += '{{ $error }}' + '\n';
	@endforeach

		//"error", "success" and "info"
		swal({
			icon  : 'error',
			title : 'Oups!',
			text  : mensaje,
			button: 'ok',
		});

		@endif

	@if( session()->has('message') )
		swal({
			icon  : 'success',
			title : 'Message',
			text  : '{{ session()->get('message') }}',
			button: 'Aceptar',
		});
	@endif

</script>

@endsection



