<div class="row">
  
  <div class="col s12">
    <ul class="tabs">
      @foreach( $publicaciones as $publicacion )
        <li class="tab">
          <a class="active" href="{{ '#tab' . $publicacion->idtown}}"> 
            &nbsp; {{ $publicacion->name_town }} &nbsp;
          </a>
        </li>
      @endforeach
    </ul>
  </div>
  
  @foreach( $publicaciones as $publicacion )

    <div id="{{ 'tab' . $publicacion->idtown }}" class="col s12 m12 l12 xl12">
      
      @foreach( $publicacion->contenido as $content )
        
        <div class=" col s12 m6 l4 xl4">

          <div class="card">

            <div class="chip left white black-text ml-mt-mb-10 " id="link_profile_inst">
              <img class="z-depth-2" src="{{ asset( $content->institutions->avatar) }}" alt="Contact Person"/>

              <a class="waves-effect waves-light black-text" href="{{ url('/institutions/'. $content->institutions->username) }}" >
                {{ $content->institutions->name }}
              </a>
            </div>

            <br/><br/><br/>

            <div class="card-image">
              <img class="responsive-img" src="{{ asset( $content->publications->picture) }}" style="max-height: 150px; min-height: 150px;">
            </div>

            <div class="card-content" style="max-height: 150px; min-height: 150px; padding: 0px;">

              <span style="min-width: 270px;">
                <b>{{ substr( $content->publications->title , 0, 100) }}</b>
              </span>
              <p style="max-height: 30px; min-height: 30px;" 
              class="left-align p10">
                {{ substr( $content->publications->description , 0, 160)  }}...
              </p>
            </div>

            <div class="card-action right-align">
              <a class="btn grey " href="/publications/{{ $content->publications->slug }}"> Ver </a>
            </div>
          </div>
        </div>
        
      @endforeach
      
    </div>

  @endforeach
</div>

