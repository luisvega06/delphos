@extends('layouts.app')

@section('title', 'Perfil')

@section('style_body', 'white')


@section('content')

	@include('include.navbar.usercommunity')
	@include('include.sidenav.usercommunity')

	
		
	<div class="row">
		<div class="col s12 m10 l6 xl6 white offset-m1 offset-l3 offset-xl3 hoverable z-depth-5 content-delphos-create center">

			<div class="col s12 mb30">
				<br/>
				<h5 class="center-align"><b> Datos de usuario </b></h5>
			</div>

			<img class="img-facture responsive-img circle center" src="{{ asset(Auth::user()->avatar) }}"  alt="{{ Auth::user()->name }} - Delphos Académico" title="{{ Auth::user()->name }} - Delphos Académico" />
			<br>
				<i>
					<label>
						Actualizado el {{ Auth::user()->getCreate()->format('l j \\, F Y') }} 
					</label>
				</i>

			<br>
			<hr>

			<div class="row center">
				<div class="col s12 m6 l6 xl6">
					<br>
					<label for="name">Nombre Completo</label><br>
					<b><span>{{Auth::user()->name}}</span></b>
					<br>
				</div>
				<div class="col s12 m6 l6 xl6">
					<br>
					<label for="number">Ubicación</label><br>
					<b><span>{{ $departamento->name . ' - ' . $municipio->name }}</span></b>
					<br>
				</div>

				<div class="col s12 m6 l6 xl6">
					<br>
					<label for="email">Correo electrónico </label><br>
					<b><span>{{ Auth::user()->email }}</span></b>
					<br>
				</div>
				<div class="col s12 m6 l6 xl6">
					<br>
					<label for="number">Teléfono</label><br>
					<b><span>{{ $phone->number }}</span></b>
					<br>
				</div>

				<div class="col s12 m6 l6 xl6">
					<br>
					<label for="email">Fecha de nacimiento </label><br>
					<b><span>{{ $usercommunity->getBirthdate()->format('F j \\d\\e Y') }}</span></b>
					<br>
				</div>
				<div class="col s12 m6 l6 xl6">
					<br>
					<label for="number">Genero</label><br>
					<b><span>{{ $genero->name }}</span></b>
					<br>
				</div>

				<div class="col s12 m12 l10 xl10 offset-l1 offset-xl1">
					<br>
					<a href="{{ url('favourites-programs') }}"  class="">
						Me gustan:
						{{ number_format( $ilike,0,",",".") }} 
						<i class="fas fa-graduation-cap tooltipped" data-position="bottom" data-tooltip="Institutciones"></i>
					</a>
					<br>
				</div>
			</div>

		

			<a class="col s12 m10 l6 xl6 offset-m1 offset-l3 offset-xl3 z-depth-2 waves-effect waves-light btn btn-radius blue" href="{{ url('/community/' . Auth::user()->id . '/edit') }}" style="margin-bottom: 30px;">
				Editar
			</a>


		</div>
	</div>
	

	@include('include.index.footer')

@endsection



@section('extra_scripts')

	<script>
		
		var mensaje = "";
		@if ($errors->any())

			@foreach($errors->all() as $error)
				mensaje += '{{ $error }}' + '\n';
			@endforeach

			//"error", "success" and "info"
			swal({
				icon  : 'error',
				title : 'Oups!',
				text  : mensaje,
				button: 'ok',
			});

		@endif


		@if( session()->has('info') )
			swal({
				icon  : 'info',
				title : 'Notificación',
				text  : '{{ session()->get('info') }}',
				button: 'Aceptar',
			});
		@endif


		@if( session()->has('message') )
			swal({
				icon  : 'success',
				title : 'Message',
				text  : '{{ session()->get('message') }}',
				button: 'Aceptar',
			});
		@endif
		
	</script>

@endsection