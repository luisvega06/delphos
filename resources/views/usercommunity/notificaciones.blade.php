@extends('layouts.app')

@section('title', 'Perfil')


@section('extra_style')

	<style type="text/css">
		
	
	</style>

@endsection


@section('style_body', 'grey lighten-3')

@section('content')

	@include('include.navbar.usercommunity')
	@include('include.sidenav.usercommunity')


	<div class="container">
		@if ( sizeof($list_notificaciones) > 0 )
			<table>
				<thead>
					<tr>
						<h3>Notificaciones</h3>
					</tr>
				</thead>

				<tbody>
					@foreach($list_notificaciones as $notificacion )
					<tr>
						<td> {{ $notificacion->created_at }} </td>
						<td class="left-align"> {{ $notificacion->content }} </td>
						<td class="right-align">
							
							<a href="/publications/{{ $notificacion->slug }}/">
								@if(!$notificacion->confirmed)
									<i class="far fa-eye"></i>
								@else
									<i class="far fa-eye-slash"></i>
								@endif
							</a>

						</td>
					</tr>
					@endforeach

				</tbody>
			</table>

			<br/>
			<div class="col s12 center">
				{{ $list_notificaciones->links('vendor.pagination.materialize') }}
			</div>
		@else
			<div class="center">
				<a href="/" class="black-text waves-effect waves-light">
					<h5><i>No tienes notificaciones</i></h5>
					<br/><br/>
					<img class="responsive-img animated bounceInUp" src="{{ asset('images/img_default/nofavoritos.png') }}" style="max-height: 300px;" />
					<br/>
					<span class="center-align">
						<i>
							Agrega instituciones educativas a tu lista de favoritos y contempla toda la información de primera mano relevante para tu futuro académico.
						</i>

						<br/>

						<b>@Delphos Académico</b>
					</span>
				</a>

			</div>
		@endif
	</div>




	@include('include.index.footer')
@endsection

