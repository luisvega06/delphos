@extends('layouts.app')

@section('title', 'Home')

@section('content')

	@include('include.navbar.usercommunity')
	@include('include.sidenav.usercommunity')
	@include('include.carrusel.usercommunity')


	<div class="center">

		@if (sizeof ($publicaciones) == 0)
			<a href="/community" class="black-text waves-effect waves-light">
				<h5><i>Encuentra contenido relevante para tí</i></h5>
				<br/><br/>
				<img class="responsive-img" src="{{ asset('images/img_default/nofavoritos.png') }}" style="max-height: 300px;" />
				<br/>
				<span class="center-align">
					<i>
						Agrega instituciones educativas a tu lista de favoritos y contempla toda la información de primera mano relevante para tu futuro académico.
					</i>

					<br/>

					<b>@Delphos Académico</b>
				</span>
			</a>

			<br/><br/><br/>
		@else
			<div class="row animated bounceInUp">
				@include('usercommunity.include.home_cards')
			</div>
		@endif

	</div>








	@include('include.index.footer')
@endsection



@section('extra_scripts')
	<script type="text/javascript">

		var mensaje = "";
		@if ($errors->any())
		@foreach($errors->all() as $error)
			console.log('{{ $error }}');
			mensaje += '{{ $error }}' + '\n';
		@endforeach

			//"error", "success" and "info"
			swal({
				icon  : 'error',
				title : 'Oups!',
				text  : mensaje,
				button: 'ok',
			});

		@endif

		@if( session()->has('message') )
			swal({
				icon  : 'success',
				title : 'Message',
				text  : '{{ session()->get('message') }}',
				button: 'Aceptar',
			});
		@endif
	</script>
@endsection

