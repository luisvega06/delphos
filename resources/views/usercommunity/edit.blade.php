
@extends('layouts.app')

@section('title', 'Perfil')




@section('style_body', 'grey lighten-3')

@section('content')

	@include('include.navbar.usercommunity')
	@include('include.sidenav.usercommunity')

	<!--
<header class="center-align">
		<div class="background-img" id="background-img"></div>

		<br/><br/>
		<img class="avatar-200" src="{{ asset(Auth::user()->avatar) }}"  alt="{{ Auth::user()->name }} - Delphos Académico" title="{{ Auth::user()->name }} - Delphos Académico"/>
		<br/>
	</header>
	-->

	<div class="center" style="padding-top: 30px;">
		
		<div class="container">
			<div class="container z-depth-5 white back-text animated bounceInLeft">

				<div class=" teal lighten-2 white-text">

				<br/>
				<h4 style="padding-bottom: 100px;"><b> Información <b>Delphos Académico</b> </b></h4>
				<br/>

					
				</div>

				<form action="/community/{{ $usercommunity->slug}}" method="POST" enctype="multipart/form-data">
					@method('PUT')
					@csrf
				
					<table>
						<thead>
							<tr>
								<th>
									<h4 class="center-align">
										
										<img class="avatar-user-style hover-update" src="{{ asset(Auth::user()->avatar) }}"  alt="{{ Auth::user()->name }} - Delphos Académico" title="{{ Auth::user()->name }} - Delphos Académico" style="margin-top: -120px;" id="blah" name="blah"/>
										<br/>
										<a onclick="$('input[type=file]').click()" class="text-cambiar-img waves-effect waves-light">
											Cambiar imagen
										</a>
										<input type="file" class="ocultar-file"  id="avatar" name="avatar" onchange="readURL(this);" />

									</h4>
									<br/>
								</th>
							</tr>
						</thead>

						

						<tbody>
							<tr>
								<td class="center-align capitalize">

									<div class="row">

										<div class="input-field col s12 m12 l6 xl6 p-l-m-10">
											<input id="name" name="name" type="text" class="validate" value="{{ Auth::user()->name }}">
											<label for="name" class="p-l-m-10">Nombre</label>
										</div>


										<!-- Username -->
										<div class="input-field col s12 m12 l6 xl6 p-l-m-10">
											<input name="username" id="username" type="text" class="validate lowercase" maxlength="254" pattern="^(\w){4,30}$" value="{{ Auth::user()->username }}" >
											<label for="username">Username</label>
										</div>

										

										<div class="input-field col s12 m12 l6 xl6 p-l-m-10">
											<input id="email" name="email" type="email" class="validate" value="{{ Auth::user()->email }}">
											<label for="email" class="p-l-m-10">Correo</label>
										</div>

										<div class="input-field col s12 m12 l6 xl6 p-l-m-10">
											<input id="phone" name="phone" type="tel" class="validate" pattern="[0-9]{10}" value="{{ $phone->number  }}">
											<label for="phone" class="p-l-m-10">Celular</label>
										</div>

										<div class="input-field col s12 m6 l6 xl6">
											<input id="password1" name="password1" type="password" class="validate">
											<label for="password1">Antigua contraseña</label>
										</div>
										<div class="input-field col s12 m6 l6 xl6">
											<input id="password2" name="password2" type="password" class="validate">
											<label for="password2">Nueva contraseña</label>
										</div>

										
									</div>

								</td>
							</tr>
							<tr>
								<td class="center-align">
									<button class="waves-effect waves-light btn blue" type="submit">Actualizar <i class="fas fa-edit"></i></button> 
									</a>
								</td>
							</tr>
						</tbody>
						</form>
					</table>
			</div>
		</div>
	</div>


	<br/><br/>





	@include('include.index.footer')
@endsection



@section('extra_scripts')

	<script type="text/javascript">
		var mensaje = "";
		@if ($errors->any())
		@foreach($errors->all() as $error)
			console.log('{{ $error }}');
			mensaje += '{{ $error }}' + '\n';
		@endforeach

			//"error", "success" and "info"
			swal({
				icon  : 'error',
				title : 'Oups!',
				text  : mensaje,
				button: 'ok',
			});

		@endif

		@if( session()->has('message') )
			swal({
				icon  : 'success',
				title : 'Message',
				text  : '{{ session()->get('message') }}',
				button: 'Aceptar',
			});
		@endif


		$("#background-img").addClass("load");
	</script>

	<script type="text/javascript">
		function readURL(input) {
			
			var filePath = input.value;
			var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
			if(!allowedExtensions.exec(filePath)){
				
				swal({icon  : 'error',title : 'Oups!', text : 'Formatos permitidos: .jpeg/.jpg/.png/.gif',button: 'ok',});

				input.value = '';
				$('#blah').attr('src', "{{ asset(Auth::user()->avatar) }}");

			}else{

				if (input.files && input.files[0]) {
					var reader = new FileReader();

					reader.onload = function (e) {
						$('#blah')
						.attr('src', e.target.result);
					};

					reader.readAsDataURL(input.files[0]);
				}
			}
		}
	</script>
@endsection



