
@extends('layouts.app')

@section('title', 'Home')

@section('content')

	@include('include.navbar.usercommunity')
	@include('include.sidenav.usercommunity')
	@include('include.carrusel.usercommunity')


	<div class="center">

		@if (sizeof ($publicaciones) == 0)
		<a href="" class="black-text waves-effect waves-light">
			<h5><i>Encuentra contenido relevante para tí</i></h5>
			<br/><br/>
			<img class="responsive-img" src="{{ asset('images/img_default/nofavoritos.png') }}" style="max-height: 300px;" />
			<br/>
			<span class="center-align">
				<i>
					Agrega instituciones educativas a tu lista de favoritos y contempla toda la información de primera mano relevante para tu futuro académico.
				</i>

				<br/>

				<b>@Delphos Académico</b>
			</span>
		</a>

		<br/><br/><br/>
		@else
			<div class="row">
				@foreach($publicaciones as $publicacion )
				
					<div class=" col s12 m6 l4 xl4 ">

						<div class="card">
							
							<div class="chip left white black-text ml-mt-mb-10 " id="link_profile_inst">
								<img class="z-depth-2" src="{{ asset( $publicacion->uavatar) }}" alt="Contact Person"/>

								<a class="waves-effect waves-light black-text" href="{{ url('/institutions/'.$publicacion->uusername) }}" >
									{{ $publicacion->uname }}
								</a>

							</div>
							<br/><br/><br/>
							
							<div class="card-image">
								<img class="responsive-img" src="{{ asset( $publicacion->ppicture) }}" style="max-height: 150px; min-height: 150px;">
							</div>
							<a href="">
								<p class="left-align teal-text ml10"> 
									<i class="fas fa-map-marker-alt"></i> {{ $publicacion->tname }} 
								</p>
							</a>

							<div class="card-content" style="max-height: 150px; min-height: 150px; padding: 0px;">
								
								<span style="min-width: 270px;">
									<b>{{ substr($publicacion->ptitle, 0, 100) }}</b>
								</span>
								<p style="max-height: 30px; min-height: 30px;">{{ substr($publicacion->pdescription, 0, 100)  }}...
								</p>
							</div>
							<div class="card-action right-align">
								<a class="btn grey " href="/publications/{{ $publicacion->pslug }}"> Ver </a>
							</div>
						</div>
					</div>
				@endforeach
			</div>
		@endif

	


		


	</div>








	@include('include.index.footer')
@endsection



@section('extra_scripts')
	<script type="text/javascript">
		var mensaje = "";
		@if ($errors->any())
		@foreach($errors->all() as $error)
			console.log('{{ $error }}');
			mensaje += '{{ $error }}' + '\n';
		@endforeach

			//"error", "success" and "info"
			swal({
				icon  : 'error',
				title : 'Oups!',
				text  : mensaje,
				button: 'ok',
			});

		@endif

		@if( session()->has('message') )
			swal({
				icon  : 'success',
				title : 'Message',
				text  : '{{ session()->get('message') }}',
				button: 'Aceptar',
			});
		@endif
	</script>
@endsection

@section('extra_scripts_documentready')
	<script type="text/javascript">
		$(document).ready(function(){

		});
	</script>
@endsection


