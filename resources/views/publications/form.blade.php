<div class="row">
	<div class="card">
		<br/>
		<h5>
			<b>Crear publicación</b>
		</h5>
		<div class="card-content">

			<span class="card-title">
				<div class="input-field col s12 m12 l12 xl12">
					<input id="title" name="title" type="text" class="validate" maxlength="255" value="{{ old('title') }}" >
					<label for="title">Título</label>
				</div>
			</span>
			<br/>
			<br/>
			<textarea id="description" name="description" placeholder="¿Quieres contarle algo novedoso a tu comunidad?" style="min-height: 150px; max-width: 100%; min-width: 100%;">{{ old('description') }}</textarea>

			<div class="card-image contenedor" style=" max-height: 200px; max-width: 200px; " >
				<br/>
				
				<img id="blah" class="responsive-img image" src="{{ asset('images/img_default/background.jpg') }}"  style=" max-height: 160px; max-width: 200px; " />

				<div class="middle">
					<div class="text">
						<div class="inputFile">
							<input type="file" class="file" id="picture" name="picture" onchange="readURL(this);"/>
							<div class="fakeFile">
								<img src="{{ asset('images/img_default/upload.png') }}" width="100"/>
								<i>Cambiar</i>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			<div class="card-content">
				<button class="waves-effect waves-light btn blue btn-radius" type="submit">
					Publicar
				</button>
			</div>
		</div>
	</div>
</div>






@section('extra_scripts')
<script type="text/javascript">

	function readURL(input) {
		
		var filePath = input.value;
		var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
		if(!allowedExtensions.exec(filePath)){
			
			swal({icon  : 'error',title : 'Oups!', text : 'Formatos permitidos: .jpeg/.jpg/.png/.gif',button: 'ok',});

			input.value = '';
			$('#blah').attr('src', "{{ asset('images/img_default/background.jpg') }}");

		}else{

			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#blah')
					.attr('src', e.target.result);
				};

				reader.readAsDataURL(input.files[0]);
			}
		}
	}



	var mensaje = "";
	@if ($errors->any())
		@foreach($errors->all() as $error)
			console.log( '{{ $error }}');
			mensaje += '{{ $error }}' + '\n';
		@endforeach

		//"error", "success" and "info"
		swal({
			icon  : 'error',
			title : 'Oups!',
			text  : mensaje,
			button: 'ok',
		});

	@endif
</script>
@endsection