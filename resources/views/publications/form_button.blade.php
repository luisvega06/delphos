<div class="fixed-action-btn">
  <a class="btn-floating btn-large red">
    <i class="large material-icons">add</i>
  </a>
  <ul>
    
    <li>
      <a class="waves-effect waves-light btn-floating btn-flat yellow" 
      href="/publications/{{$publication->institutions_id}}/{{$publication->slug}}/analytics" >

      <i class="fas fa-chart-area"></i>

      </a>
    </li>
    
    <li>
      <a class="waves-effect waves-light btn-floating btn-flat blue" 
          href="/publications/{{$publication->slug}}/edit" >

          <i class="large material-icons">mode_edit</i>

      </a>
    </li>
    
    <li>
      <a class="waves-effect waves-light btn-floating btn-flat red" 
          id="btn-delete2" onclick="fevent(event);">

          <i class="fas fa-trash"></i>

      </a>
    </li>

  </ul>
</div>