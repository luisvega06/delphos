@extends('layouts.app')

@section('title', 'Crear cuenta')

@section('style_body', 'grey lighten-2')



@section('content')

@include('include.navbar.default')



@include('include.sidenav.default')




<br/>
<div class="container  ">
	<br/>
	<div class="container center ">

		<form action="/publications/{{$publication->slug}}" method="POST" enctype="multipart/form-data">
			@method('PUT')
			@csrf

			<!-- New show  -->
			<div class="row">

				<div class="card hoverable">
					<br/>
					<h5>
						<b>Crear publicación</b>
					</h5>
					<div class="card-content">

						<span class="card-title">
							<div class="input-field col s12 m12 l12 xl12">
								<input id="title" name="title" type="text" class="validate" maxlength="255" value="{{$publication->title}}" required="">
								<label for="title">Título</label>
							</div>
						</span>
						<br/>
						<br/>
						<textarea id="description" name="description" placeholder="¿Quieres contarle algo novedoso a tu comunidad?" style="min-height: 150px; max-width: 100%; min-width: 100%;" required=""  value="{{$publication->description}}">{{$publication->description}}</textarea>

						<div class="card-image contenedor" style=" max-height: 200px; max-width: 200px; " >
							<br/>

							<img id="blah" class="responsive-img image" src="{{ asset($publication->picture) }}"  style=" max-height: 160px; max-width: 200px; " />

							<div class="middle">
								<div class="text">
									<div class="inputFile">
										<input type="file" class="file" id="picture" name="picture" onchange="readURL(this);" />
										<div class="fakeFile">
											<img src="{{ asset('images/img_default/upload.png') }}" width="100"/>
											<i>Cambiar</i>
										</div>
									</div>

								</div>
							</div>
						</div>

						<br/><br/>
						<button class="waves-effect waves-light btn blue btn-radius" type="submit">
							Actualizar <i class="fas fa-edit"></i>
						</button>
						<br/>
					</div>
				</div>
			</div>
			<!-- End/ new show-->


			
				
			
			
		</form>


		<br/><br/><br/>
	</div>
</div>




@endsection


@section('extra_scripts')
<script type="text/javascript">

	function readURL(input) {
		
		var filePath = input.value;
		var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
		if(!allowedExtensions.exec(filePath)){
			
			swal({icon  : 'error',title : 'Oups!', text : 'Formatos permitidos: .jpeg/.jpg/.png/.gif',button: 'ok',});

			input.value = '';
			$('#blah').attr('src', "{{ asset($publication->picture) }}");

		}else{

			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#blah')
					.attr('src', e.target.result);
				};

				reader.readAsDataURL(input.files[0]);
			}
		}
	}



	var mensaje = "";
	@if ($errors->any())
	@foreach($errors->all() as $error)
	console.log( '{{ $error }}');
	mensaje += '{{ $error }}' + '\n';
	@endforeach

		//"error", "success" and "info"
		swal({
			icon  : 'error',
			title : 'Oups!',
			text  : mensaje,
			button: 'ok',
		});

		@endif
	</script>
	@endsection