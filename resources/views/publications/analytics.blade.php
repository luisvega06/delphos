@extends('layouts.app')

@section('title', 'Analytics')

@section('script_extras_head')
  <script src="{{ asset('js/Chart.min.js'   ) }}"></script>
  <script src="{{ asset('js/jspdf.min.js'   ) }}"></script>

@endsection

@section('content')

@include('include.navbar.default')
@include('include.sidenav.default')



<div class="row">
  <div class="col s12 m12 l12 xl12">
    <div id="uno" class="section scrollspy">
      <div class="col s12 m12 l12 xl12 center-align grey lighten-3">
        
        <h3><b>Indicadores Delphos Académico</b></h3>
        
        
        <p class="left-align p10">
          <b>Información Basica</b>
          <br/><b>- Nombre: </b>{{ $publication->title}}
          <br/><b>- Fecha Creación: </b>{{ $publication->created_at}}
          <br/><b>- Fecha Actualización: </b>{{ $publication->updated_at}}
        </p>
        <br/><br/><br/>

        <div class="container">
          <canvas class="canvas-report-3 center-align grey lighten-3" id="pie-chart"></canvas>
        </div>

        <br/>
        <br/><br/>
        <div id="div-download-pdf" class="center scale-transition scale-out">
          <p>Descargar listado de usuarios que han visualizado tu publicación  
            <button id="btn-download-pdf" class="waves-effect waves-light  pulse btn-radius grey lighten-4 blue-text" onclick="Download_PDF()">"<i><u>{{ $publication->title}}</u></i>. " <i class="fas fa-download"></i></button>
           </p>
        </div>

        <br/><br/>
        <button class="waves-effect waves-light btn" onclick="load_diagrams_publications({{ $institution->id }}, {{ $publication->id }});">
          Refrescar
        </button>
        <br/><br/><br/><br/>

        

      </div>
    </div>
</div>


@include('include.index.footer')


@endsection

@section('extra_scripts')
  <script src="{{ asset('js/publications/diagram_publications.js'   ) }}"></script>
@endsection