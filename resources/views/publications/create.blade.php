@extends('layouts.app')

@section('title', 'Crear cuenta')

@section('style_body', 'grey lighten-2')

@section('content')

@include('include.navbar.default')
@include('include.sidenav.default')


<div class="container">
	<div class="container center ">
		<form action="/publications" method="POST" enctype="multipart/form-data">
			@csrf
			@include('publications.form')
		</form>
	</div>
</div>



@include('include.index.footer')
@endsection


