@extends('layouts.app')

@section('title', 'Publicaión')

@section('extra_meta')
  @include('layouts.meta_shared.publication')
@endsection

@section('style_body', 'grey lighten-2')

@section('content')

  @include('include.navbar.default')
  @include('include.sidenav.default')


  <div class="container">

    <div class="row">

      <div class="col s12 m12 l12 xl12">

        <div class="card hoverable animated bounceInUp">

          {{-- Chips INSTITUCION --}}
          <div class="grey lighten-5">
            
            <blockquote class="mb-mt-0">

              <div class="chip valign-wrapper transparent">
                
                <img class="circle z-depth-2" src="{{ asset($user_institution->avatar) }}" width="64" height="64"  style="margin-top: 10px;" />

                @auth
                  @if (Auth::user()->rols_id == 3)
                    <a class="waves-effect waves-light black-text" href="{{ url('/institutions') }}" >
                  @else
                    <a class="waves-effect waves-light black-text" href="{{ url('/institutions/'.$user_institution->username) }}" >
                  @endif
                @else
                  <a class="waves-effect waves-light black-text" href="{{ url('/institutions/'.$user_institution->username) }}" >
                @endauth
                
                      <p style="line-height: 15px;">
                        <b>{{ $user_institution->name }}</b> <br>
                        {{ ucwords($publication->getCreate()->format('l j \\, F Y') ) }}
                      </p>

                    </a>
              </div>

            </blockquote>
            
          </div>

          
          {{-- CONTENIDO: TEXTO TITULO--}}
          <h5 class="card-title left-align p15">

            <b><b>Título:</b> {{ $publication->title }} </b>

          </h5>
          

          {{-- CONTENIDO: IMAGEN PUBLICACION--}}
          <div class="card-image">

            <img class="responsive-img materialboxed" src="{{ asset($publication->picture) }}" style="max-height: 350px; min-width: 100%;">

          </div>
          
          
          {{-- CONTENIDO: TEXTO/DESCRIPCION--}}
          <div class="card-content" style="padding: 10px;">
            
            <textarea class="black-text" disabled="true" 
              style="border: none; min-height: 300px; max-width: 100%; min-width: 100%;">{{$publication->description}}</textarea>

          </div>

          {{-- CONTENIDO: BUTTON  --}}
          <div class="hoverable card-action center-align">

            @include('include.shared.button_social_circle')
            
            <a class="waves-effect waves-light btn-floating btn-flat green accent-4"  
                id="share-google-plus"
                target="_blank" 
                href="https://wa.me/?text=*{{$user_institution->name}}* publico a través de Delphos Académico https://delphosacademico.com/publications/{{$publication->slug}}">

                <i class="fab fa-whatsapp"></i>
            </a>

          </div>
          

        </div>

      </div>

    </div>

    {{-- CONTENIDO: BUTTON FLOTANTE --}}

    @auth

      @if (Auth::user()->id == $user_institution->id)

        @include('publications.form_button')

        <form id="form_delete" 
              action="/publications/{{$publication->slug}}" 
              method="POST" hidden="true" style="hidden: true;">

                @method('DELETE')
                @csrf

                <a id="btn-delete" >
                  <i class="fas fa-trash"></i>
                </a>

        </form>


      @endif

    @endauth

  </div>

  

  @include('include.index.footer')
@endsection


@section('extra_scripts_function')
  <script type="text/javascript">


    function fevent(e) {
      e.preventDefault();
    }

    $(document).ready(function(){
      $('#btn-delete2').click(function(){

        swal("Confirmar eliminación","", "warning", {
          
          buttons: {
            cancel: "No eliminar",
            aceptar: {
              text: "Eliminar",
              value: "Eliminar",
              className: "red",
            }, 
          },


        })
        .then((value) => {
          switch (value) {

            case "Eliminar":
            
              var form = document.getElementById("form_delete");
              form.submit();
              //alert("Borrado");

            break;

            default:
            //swal("cancel");
          }
        });
      });
    });
    

    @if ($errors->any())

    @foreach ($errors->all() as $error)
    console.log("{{ $error }}");
    @endforeach


    @endif

  </script>

  <script src="{{ asset('js/social_shared_button/social.js'   ) }}"></script>
@endsection