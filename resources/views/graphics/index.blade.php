@extends('layouts.app')
@section('title', 'Reportes')
@section('script_extras_head')
  <script src="{{ asset('js/Chart.min.js'   ) }}"></script>
  <script src="{{ asset('js/jspdf.min.js'   ) }}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
  
@endsection
@section('content')

  @include('include.navbar.default')
  @include('include.sidenav.default')

  <div class="row container">
      <div class="col s12 m12 l12 xl12 center">
        <h4 class="left-align">Reportes estadistico visitas perfil institucional</h4>
        
        <p class="left-align ">
          Se tendra encuenta para el reporte los siguientes factores: <br/>
           
            &nbsp; - Total general visitas. <br/><br/>
            &nbsp; - Total general visitas fecha actual.  <br/><br/>
            &nbsp; - total de cuentas unicas que visto el perfil. <br/><br/>
            &nbsp; - Total general de usuarios  por sexo ó genero. <br/><br/>
        </p>

      </div>
  </div>

  {{-- Panel grafica vista insititucion --}}
  <div class="row">
    
    <div class="col s12">
      <ul class="tabs">
        <li class="tab col s3"><a class="active" href="#tab1">Diagrama de tora</a></li>
        <li class="tab col s3"><a href="#tab2">Diagrama de barras</a></li>
      </ul>
    </div>

    
      <div id="tab1" class="col s12 container">
        <div class="container">
          <div class="container">
            <canvas id="pie-chart"></canvas>
          </div>
        </div>
      </div>

      <div id="tab2" class="col s12">
        <div class="container">
          <canvas id="bar-chart"></canvas>
        </div>
      </div>
    

  </div>

  <br/>

  <div class="col s12 m12 l12 xl12 center">
    <button class="waves-effect waves-light btn" onclick="load_diagrams_publications( {{ $institution->id }} )">refrescar</button>
  </div>

  <br/><br/>


  <div id="div-download-pdf" class="center scale-transition scale-out">

    <a id="btn-download-pdf" class="waves-effect waves-light  pulse btn-radius grey lighten-4 blue-text" onclick="Download_PDF()">
      Descargar listado de usuarios <i class="fas fa-download"></i>
    </a>

  </div>

  <br/><br/>

  <hr class="style2"/>

  <br/>


  <div id="graphics_programas" class="center">
      
      <h5>Programas Académicos</h5>
      <p class="left-align p20">
        <i>La gráfica denota la acogida del programa por parte de los usuarios (Comunidad Delphos) registrados en la plataforma que a su vez han decidido agregar los siguientes a su lista de ofertas atractivas de estudio (favoritos)</i>.
      </p>

      <div class="container">
        <div class="container">
          <canvas class="grey lighten-3" id="pie-chart-programs"></canvas>
        </div>
      </div>
      
      <br/>
      <button class="waves-effect waves-light btn" id="btn-diagram-program" value="{{ $institution->id }}">refrescar</button>

  </div>
  <div id="div-download-2-pdf" class="center scale-transition scale-out">

    <br/>

    <a id="btn-download-2-pdf" class="waves-effect waves-light  pulse btn-radius grey lighten-4 blue-text" onclick="Download_PDF_2()">
      Descargar listado de usuarios por clasificación<i class="fas fa-download"></i>
    </a>
    <br/>

  </div>


  
  
{{-- INSCRIPCIONES --}}

  <div id="graphics_inscripciones" class="center">

      <h5>Inscripciones</h5>

      <p class="left-align p20">
        <i>La gráfica denota la acogida por parte de los usuarios (Comunidad Delphos) registrados en la plataforma que a su vez han decidido visualizar el formulario de inscripciones accediendo por medio del hipervínculo suministrado por la institución</i>.
      </p>

      <div class="container">
        <div class="container">
          <canvas class="grey lighten-3" id="pie-chart-inscripciones"></canvas>
        </div>
      </div>
      
      <br/>
      <button class="waves-effect waves-light btn" id="btn-diagram-inscripciones" onclick="load_diagrams_inscripciones( {{ $institution->id }} )">refrescar</button>

  </div>
  <div id="div-download-3-pdf" class="center scale-transition scale-out">

    <br/>

    <a id="btn-download-3-pdf" class="waves-effect waves-light  pulse btn-radius grey lighten-4 blue-text" onclick="Download_PDF_3()">
      Descargar listado de usuarios por clasificación<i class="fas fa-download"></i>
    </a>
    <br/><br/>

  </div>






  @include('include.index.footer')
@endsection
@section('extra_scripts')
  <script src="{{ asset('js/graphics/diagram_institutions.js'   ) }}"></script>
  
  <script src="{{ asset('js/graphics/diagram_programs.js'       ) }}"></script>

  <script src="{{ asset('js/graphics/diagram_clickinscripcion.js') }}"></script>
  
@endsection