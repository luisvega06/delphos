@extends('layouts.app')

@section('title', 'Buscar')

@section('style_body', 'grey lighten-3')

@section('content')

    @include('include.navbar.default')
    @include('include.sidenav.default')


    <br/>
    <h5 class="center">Resultados para: <i><u>"{{ $criterio }}"</u></i></h5>
    <br/><br/>
    <div class="white hoverable">
        <div class="center p10">

            @if (sizeof($allprogramas) > 0 )

                
                <div class="col s12 center">
                    {{ $allprogramas->links('vendor.pagination.materialize') }}
                </div>
                <br/>

                <ul class="collection">
                            @foreach($allprogramas as $program )
                            
                            <li class="collection-item avatar">
                                <a href="{{ url('/programs/'.$program->pid) }}" class="title black-text left-align">
                                    <p class="p10">
                                    <img class="responsive-img circle z-depth-2  animated bounceInUp" src="{{ asset( $program->uavatar ) }}" width="35px" height="35px"/>
                                    {{ $program->uname }} <br/><br/>
                                    {{ $program->pname }} <br/><br/>
                                    {{ $program->ptime . ' ' . $program->dname  }} <br/>
                                    </p>
                                </a>
                                <a class=" btn-floating btn waves-effect waves-light transparent right hide-on-med-and-down secondary-content" href="{{ url('/programs/'.$program->pid) }}">

                                        <i class="far fa-eye black-text"></i>

                                    </a>
                            </li>

                            @endforeach
                        </ul>

                <br/>
                <div class="col s12 center">
                    {{ $allprogramas->links('vendor.pagination.materialize') }}
                </div>
            @else

                <a href="/" class="black-text waves-effect waves-light">
                    <h5><i>Encuentra contenido relevante para tí</i></h5>
                    <br/><br/>
                    <img class="responsive-img" src="{{ asset('images/img_default/nofavoritos.png') }}" style="max-height: 300px;" />
                    <br/>
                    <span class="center-align">
                        <i>
                            Agrega instituciones educativas a tu lista de favoritos y contempla toda la información de primera mano relevante para tu futuro académico.
                        </i>

                        <br/>

                        <b>@Delphos Académico</b>
                    </span>
                </a>
            @endif

        </div>
    </div>

    <br/>
    <br/>
    
    @include('include.index.footer')
@endsection



@section('extra_scripts')
    <script type="text/javascript">
        var mensaje = "";
        @if ($errors->any())
        @foreach($errors->all() as $error)
            console.log('{{ $error }}');
            mensaje += '{{ $error }}' + '\n';
        @endforeach

            //"error", "success" and "info"
            swal({
                icon  : 'error',
                title : 'Oups!',
                text  : mensaje,
                button: 'ok',
            });

        @endif

        @if( session()->has('message') )
            swal({
                icon  : 'success',
                title : 'Message',
                text  : '{{ session()->get('message') }}',
                button: 'Aceptar',
            });
        @endif


        $("#background-img").addClass("load");
    </script>
@endsection
