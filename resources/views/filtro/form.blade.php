<br>

    {{--Tipo de Region, ciudad, municipio y etc --}}
    <div class="input-field col s12 m6 l6 xl6 offset-l3 offset-xl3">
        <select id="towns" name="towns" >
            <option value="0" disabled selected>Seleccionar</option>
            @foreach($towns as $obj )
<option value="{{ $obj->id }}">{{$obj->name}}</option>
            @endforeach
        </select>
        <label>
            Ciudad (*)
        </label>
    </div>

    {{--Tipo de Nivel de estudio --}}
    <div class="input-field col s12 m6 l6 xl6 offset-l3 offset-xl3">
        <select id="level_of_educations" name="level_of_educations" >
            <option value="0" disabled selected>Seleccionar</option>
            @foreach($level_of_educations as $obj )
<option value="{{ $obj->id }}">{{$obj->name}}</option>
            @endforeach
        </select>
        <label>
            Formación (*)
        </label>
    </div>

    {{--Tipo de Modalidad --}}
    <div class="input-field col s12 m6 l6 xl6 offset-l3 offset-xl3">
        <select id="modalities" name="modalities" >
            <option value="0" disabled selected>Seleccionar</option>
            @foreach($modalities as $obj )
<option value="{{ $obj->id }}">{{$obj->name}}</option>
            @endforeach
        </select>
        <label>
            Modalidad de estudio
        </label>
    </div>
<br>