@extends('layouts.app')

@section('title', 'Buscar')

@section('style_body', 'grey lighten-3')

@section('content')

    @include('include.navbar.default')
    @include('include.sidenav.default')

    <div class="row container center">
        <div class="col s12 m12 l12 xl12">

            <form action="/descubrir" method="POST" >

                @csrf
                @include('filtro.form')
                
                <div class="col s12">

                    <p><i>Todos los campos son requeridos*</i></p>
                    

                    <button class="waves-effect waves-light btn blue mt15 mb15 btn-radius" type="submit">Filtrar 
                    </button>
                    <br><br>
                </div>

            </form>


        </div>
    </div>

    <br/><br/>
    <div class="white">
        <div class="center container">

            
            @if (sizeof($allprogramas) > 0 )

                <br>
                <h5>Ofertas Acdémicas Encontradas</h5>
                
                <a href="/descubrir-ofertas-academicas"><i>¡Hacer una nueva busqueda!</i></a>

                <br/><br/>

                <ul class="collection">
                            @foreach($allprogramas as $program )
                            
                            <li class="collection-item avatar">
                                <a href="{{ url('/programs/'.$program->pid) }}" class="title black-text left-align">
                                    <p class="p10">
                                        <img class="responsive-img circle z-depth-2  animated bounceInUp" src="{{ asset( $program->uavatar ) }}" width="35px" height="35px"/>
                                        
                                        {{ $program->uname }} <br/>

                                        <label class="black-text">
                                            <b>{{ $program->pname }}</b>
                                        </label>
                                        <br/>

                                        <label>
                                            {{ $program->ptime . ' ' . $program->dname  }}
                                        </label>
                                        <br/>
                                    </p>
                                </a>
                                <a class=" btn-floating btn waves-effect waves-light transparent right hide-on-med-and-down secondary-content" href="{{ url('/programs/'.$program->pid) }}">

                                        <i class="far fa-eye black-text"></i>

                                    </a>
                            </li>

                            @endforeach
                        </ul>

                <br/>
            @else

                <a href="/descubrir-ofertas-academicas" class="black-text waves-effect waves-light">
                    <h5><i>La busqueda no arrojo resultados</i></h5>
                    <br/><br/>
                    <img class="responsive-img" src="{{ asset('images/img_default/nofavoritos.png') }}" style="max-height: 300px;" />
                </a>
                
            @endif
            

        </div>
    </div>

    <br/>
    <br/>
    
    @include('include.index.footer')
@endsection



@section('extra_scripts')
    <script type="text/javascript">
        var mensaje = "";
        @if ($errors->any())
        @foreach($errors->all() as $error)
            console.log('{{ $error }}');
            mensaje += '{{ $error }}' + '\n';
        @endforeach

            //"error", "success" and "info"
            swal({
                icon  : 'error',
                title : 'Oups!',
                text  : mensaje,
                button: 'ok',
            });

        @endif

        @if( session()->has('message') )
            swal({
                icon  : 'success',
                title : 'Message',
                text  : '{{ session()->get('message') }}',
                button: 'Aceptar',
            });
        @endif


        $("#background-img").addClass("load");
    </script>
@endsection
