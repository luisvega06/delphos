@extends('layouts.app')

@section('title', 'Buscar')


@section('style_body', 'grey lighten-3')

@section('content')

    @include('include.navbar.default')
    @include('include.sidenav.default')

    <br>
    
    <h5 class="center">Filtrar Ofertas Académicas</h5>

    <br>


    <div class="container center row white">

        <div class="col s12 m12 l12 xl12">

            <form action="/descubrir" method="POST" >
                
                @csrf
                @include('filtro.form')
                
                <div class="col s12">
                    
                    <p><i>Todos los campos son requeridos*</i></p>
                    

                    <button class="waves-effect waves-light btn blue mt15 mb15 btn-radius" type="submit">Filtrar 
                    </button>
                    <br><br><br>
                </div>

            </form>


        </div>



    </div>
    
    @include('include.index.footer')
@endsection



@section('extra_scripts')

    <script type="text/javascript">
        var mensaje = "";
        @if ($errors->any())
        @foreach($errors->all() as $error)
            console.log('{{ $error }}');
            mensaje += '{{ $error }}' + '\n';
        @endforeach

            //"error", "success" and "info"
            swal({
                icon  : 'error',
                title : 'Oups!',
                text  : mensaje,
                button: 'ok',
            });

        @endif

        @if( session()->has('message') )
            swal({
                icon  : 'success',
                title : 'Message',
                text  : '{{ session()->get('message') }}',
                button: 'Aceptar',
            });
        @endif


        $("#background-img").addClass("load");
    </script>
@endsection
