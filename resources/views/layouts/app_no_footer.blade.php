<!DOCTYPE html>
<html lang="es">
<head>

    <link rel="shortcut icon" href="{{ asset('images/img_logos_delphos/Favicon/favicon.ico')}}" type="image/x-icon">
    <link rel="icon"          href="{{ asset('images/img_logos_delphos/Favicon/favicon.ico')}}" type="image/x-icon">
    

    <title>Delphos Académico - @yield('title')</title>


    
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta content="IE=edge" http-equiv="X-UA-Compatible"></meta>
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="cache-control" content="no-store" />
    <meta http-equiv="cache-control" content="must-revalidate" />
    <meta http-equiv="expires"       content="0" />
    <meta http-equiv="expires"       content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma"        content="no-cache" />
    
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @yield('extra_meta')

	<!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>

    
	<link rel="stylesheet" href="{{ asset('css/materialize.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}" />
    <link href="{{ asset('css/fontawesome-all.css') }}"  rel="stylesheet"/>

    <style>
        .dropdowncontent {
        	top: -55px;
            margin-top: 63px;
            min-width: 230px;
            max-width: 350px;
        }
        .dropdowncontent_notice{
        	top: -55px;
            border-radius: 5px;
            margin-top: 63px;
            min-width: 430px;
        }
    </style>
    
    @yield('extra_style')

    
</head>

	<body class="@yield('style_body')">


		<!--Agrega contenido  -->
		@yield('content')
		
		<!-- script -->
		<script src="{{ asset('js/jquery-3.3.1.min.js' ) }}"></script>
		<script src="{{ asset('js/materialize.min.js'  ) }}"></script>
		<script src="{{ asset('js/sweetalert.min.js'   ) }}"></script>
        <script src="{{ asset('js/push/push.min.js' ) }}"></script>

        <script>
        
			$(document).ready(function(){
				$('.sidenav').sidenav();
				$('.scrollspy').scrollSpy();
				M.updateTextFields();
				$('select').formSelect();
				$(".dropdown-trigger").dropdown();
				$('.tooltipped').tooltip();
				$('.datepicker').datepicker();
				$('.tap-target').tapTarget();
				$('.modal').modal();
				$('.fixed-action-btn').floatingActionButton();
				$('.parallax').parallax();
                $('.collapsible').collapsible();
                $('.tabs').tabs();
                $('.materialboxed').materialbox();
				$('.carousel.carousel-slider').carousel({
                    fullWidth: true,
                    indicators: true
                });
			});

			function funcion_limpiar(){
            	

            	if($('#search').val() != "" ){
            		$('#search').val("");
            		console.log("Limpiando input search 1");
            	}else{
            		if($('#search2').val() != "" ){
            			$('#search2').val("");
            			console.log("Limpiando input search 2");
            		}else{
            			console.log("Input vacios");
            		}
            	}
            }
        
		</script>
        

        

		@yield('extra_scripts')

		@yield('extra_scripts_function')

		@yield('extra_scripts_documentready')
        
        @yield('extra_scripts_DOMContentLoaded')

	</body>
</html>
