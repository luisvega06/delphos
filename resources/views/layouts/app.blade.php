<!DOCTYPE html>
<html lang="es">
<head>
	<!-- Hoja de estilos inicial -->
	<meta charset="UTF-8">
	<!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta content="IE=edge" http-equiv="X-UA-Compatible"></meta>

    @yield('extra_meta')
    

	<link rel="icon" type="image/png" href="{{ asset('images/img_logos_delphos/lgdelphosnotext.png') }}"/>

    <link rel="shortcut icon" href="{{ asset('images/img_logos_delphos/Favicon/favicon.ico')}}" type="image/x-icon">
    <link rel="icon"          href="{{ asset('images/img_logos_delphos/Favicon/favicon.ico')}}" type="image/x-icon">
    
	
	<!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>

    <title>@yield('title') - Delphos Académico</title>
	<link rel="stylesheet" href="{{ asset('css/materialize.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}" />
    <link href="{{ asset('css/fontawesome-all.css') }}"  rel="stylesheet"/>

    {{-- <script src="{{ asset('js/push/push.min.js'   ) }}"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/push.js/1.0.5/push.js"></script>



    @yield('extra_style')

    <style>
        .dropdowncontent {
        	top: -55px;
            margin-top: 63px;
            min-width: 230px;
            max-width: 350px;
        }
        .dropdowncontent_notice{
        	top: -55px;
            border-radius: 5px;
            margin-top: 63px;
            min-width: 430px;
        }

        .carousel {
            max-height: 450px !important;
        }
        .carousel-item {
            max-height: 400px !important;
        }
        .carousel-img {
            max-height: 400px !important;
        }
    </style>

    <script type="text/javascript">
        
    </script>

    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="cache-control" content="no-store" />
    <meta http-equiv="cache-control" content="must-revalidate" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />

    <meta name="csrf-token" content="{{csrf_token()}}">

    @yield('script_extras_head')
    
</head>

	<body class="@yield('style_body')">


		<!--Agrega contenido  -->
		@yield('content')
		
		<!-- script -->
        <script src="{{ asset('js/jquery-3.3.1.min.js' ) }}"></script>
		<script src="{{ asset('js/materialize.min.js'  ) }}"></script>
        <script src="{{ asset('js/sweetalert.min.js'   ) }}"></script>



		
        
        @yield('extra_links')

		<script>
			$(document).ready(function(){

                
                $('.sidenav').sidenav();
                
                M.updateTextFields();
                $('select').formSelect();
                $(".dropdown-trigger").dropdown();
                $('.tooltipped').tooltip();
                $('.datepicker').datepicker();
                $('.tap-target').tapTarget();
                $('.modal').modal();
                $('.fixed-action-btn').floatingActionButton();
                $('.parallax').parallax();
                $('.collapsible').collapsible();
                $('.tabs').tabs();
                $('input#input_text, textarea#textarea2').characterCounter();
                $('.materialboxed').materialbox();
                $('.scrollspy').scrollSpy();


                $('.carousel.carousel-slider').carousel({
                    fullWidth: true,
                    indicators: true,
                });

                $('.carousel').carousel({
                    fullWidth: true,
                    indicators: true,
                });
                    setInterval(function(){
                        $('.carousel').carousel('next');
                    }, 2000);

                

			function funcion_limpiar(){
            	

            	if($('#search').val() != "" ){
            		$('#search').val("");
            		console.log("Limpiando input search 1");
            	}else{
            		if($('#search2').val() != "" ){
            			$('#search2').val("");
            			console.log("Limpiando input search 2");
            		}else{
            			console.log("Input vacios");
            		}
            	}
            }
        });
		</script>

		@yield('extra_scripts')

		@yield('extra_scripts_function')

		


	</body>
</html>
