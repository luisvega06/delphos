<meta property="og:url"                content="https://www.delphosacademico.com/institutions/{{ $user->username}}" />
<meta property="og:type"               content="article" />
<meta property="og:title"              content="Delphos Académico - {{ $user->name}}" />
<meta property="og:description"        content="{{ $institution->description}}" />
<meta property="og:image"              content="https://www.delphosacademico.com{{$user->avatar}}" />