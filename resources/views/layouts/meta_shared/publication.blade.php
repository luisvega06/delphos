<meta property="og:url"          content="https://www.delphosacademico.com/publications/{{$publication->slug}}" />
<meta property="og:type"         content="article" />
<meta property="og:title"        content="{{ $publication->title}}" />
<meta property="og:description"  content="{{ $publication->description}}" />
<meta property="og:image"        content="https://www.delphosacademico.com{{$publication->picture}}" />

<meta name="twitter:title"       content="{{ $publication->title}}"/>
<meta name="twitter:description" content="{{ $publication->description}}"/>
<meta name="twitter:image"       content="https://www.delphosacademico.com{{$publication->picture}}"/>
<meta name="twitter:card"        content="photo"/>
<meta name="twitter:url"         content="https://www.delphosacademico.com/publications/{{$publication->slug}}"/>

