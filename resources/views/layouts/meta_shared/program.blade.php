<meta property="og:url"          content="https://www.delphosacademico.com/programs/{{$program->id}}"/>
<meta property="og:type"         content="article" />
<meta property="og:title"        content="{{$perfil->name . ' - ' . $program->name}}" />
<meta property="og:description"  content="{{$program->description}}" />
<meta property="og:image"        content="https://www.delphosacademico.com/{{$program->avatar}}" />