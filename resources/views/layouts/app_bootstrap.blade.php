<!DOCTYPE html>
<html lang="es">
<head>

	<!-- Hoja de estilos inicial -->
	<meta charset="UTF-8">
	<!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    

    <meta content="0" http-equiv="Expires"/>
    <meta content="no-cache" http-equiv="Pragma"/>


	<link rel="shortcut icon" href="{{ asset('images/img_logos_delphos/Favicon/favicon.ico')}}" type="image/x-icon">
    <link rel="icon"          href="{{ asset('images/img_logos_delphos/Favicon/favicon.ico')}}" type="image/x-icon">
    
	
	<!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>

    <title>Delphos Académico - @yield('title')</title>
	<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" />
    @yield('extra_style')
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}" />
    <link href="{{ asset('css/fontawesome-all.css') }}"  rel="stylesheet"/>



    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="cache-control" content="no-store" />
    <meta http-equiv="cache-control" content="must-revalidate" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />

    <meta name="csrf-token" content="{{ csrf_token() }}">

    
    
</head>

	<body class="@yield('style_body')">


		<!--Agrega contenido  -->
		@yield('content')
		
		<!-- script -->
        <script src="{{ asset('js/jquery-3.3.1.min.js' ) }}"></script>
		<script src="{{ asset('js/bootstrap.min.js'  ) }}"></script>
        <script src="{{ asset('js/sweetalert.min.js'   ) }}"></script>
		
        @yield('extra_scripts')

	</body>
</html>
