@extends('layouts.app')

@section('extra_meta')
  @include('layouts.meta_shared.institucion')
@endsection

@section('title', 'Conocer')

@section('style_body', 'background_two')

@section('content')

	@include('include.navbar.default')
	@include('include.sidenav.default')

	<div class="center">

		<h5 class="p10 white-text">

			<b>Delphos Académico</b> presenta <br>

			
			@auth
              @if (Auth::user()->rols_id == 3)
              <a class="waves-effect waves-light white-text" href="{{ url('/institutions') }}" >
              	<u>{{$user->name}}</u>
              </a>

              @else
              <a class="waves-effect waves-light white-text" href="{{ url('/institutions/'.$user->username) }}" >
              	<u>{{$user->name}}</u>
              </a>
              @endif
            @else
              <a class="waves-effect waves-light white-text" href="{{ url('/institutions/'.$user->username) }}" >
              	<u>{{$user->name}}</u>
              </a>
            @endauth	
			

		</h5>



		<div class="row animated bounceInRight">

			<div class="col s10 m10 l10 xl10 offset-s1 offset-m1 offset-l1 offset-xl1 white">
				
			</div>
			
			{{-- Video Clip Instalaciones --}}
			<div class="col s10 m10 l10 xl10 offset-s1 offset-m1 offset-l1 offset-xl1 white hoverable" style="padding-left: 0px; padding-right: 0px;">

				<div class="col s12 m12 l7 xl7" style="padding-left: 0px; padding-right: 0px;">

					<div class="card mt0 mb0">
						<div class="card-image waves-effect waves-block waves-light">

							<div class="video-container">

								<iframe width="560" height="315" src="https://www.youtube.com/embed/{{$institution->URL_video_present}}?rel=0&autoplay=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

							</div>

						</div>
					</div>

				</div>

				<div class="col s12 m12 l5 xl5 " style="padding: 0px;">
					<p><b>Quienes somos</b></p>
					<textarea class="black-text p10" disabled="true" 
					style="border: none; height: 215px; max-width: 100%; min-width: 100%;">{{$institution->description}}
					</textarea>	
					<br><br>

				</div>
				
			</div>

		</div>


		<br>
		<div class="white-text">		

			<div class="col s10 m10 l10 xl10 offset-s1 offset-m1 offset-l1 offset-xl1" style="margin-bottom: 0px;">

				<div class="row grey darken-1 mb0">

					<a target="_blank" 
						href="{{ url('/programs/ofertas/'.$user->username) }}" 
						class="white-text">

						<div class="col s12  m6 l3 xl3 z-depth-1 grey darken-1 animated zoomIn">
							<br>
							<i class="fas fa-graduation-cap fa-3x"></i>
							<br>
							<span><b>Programas</b></span>
							<br>
							<label class="white-text">
								<i>
									<b>{{ $programs }}</b> Ofertas Académicos
								</i>
							</label>
							<br><br>
						</div>
					</a>
					
					<a target="_blank" 
						href="{{ url('//'.$institution->ulr_inscriptions) }}" 
						class="white-text">

						<div class="col s12  m6 l3 xl3 z-depth-1 grey darken-1 animated zoomIn">
							<br>
							<i class="fas fa-link fa-3x"></i>
							<br>
							<span><b>Inscripción</b></span><br>
							<label class="white-text">
								<i>URL Formulario de inscripciones</i>
							</label>
							<br><br>
						</div>
					</a>	
					
					
					<div class="col s12  m6 l3 xl3 z-depth-1 grey darken-1 white-text animated zoomIn">
						<br>
						<i class="fas fa-mobile-alt fa-3x"></i>
						<br>
						<span><b>Teléfono</b></span><br>
						<label class="white-text">
							<i>{{ $phone->number }}</i>
						</label>
						<br><br>
					</div>
					

					<a target="_blank" 
						href="{{ url('/institutions-googlemaps/'.$user->username) }}" 
						class="white-text">

						<div class="col s12  m6 l3 xl3 z-depth-1 grey darken-1 animated zoomIn">
							<br>
							<i class="fas fa-map-marker-alt fa-3x"></i>
							<br>
							<span><b>Dirección</b></span><br>

							<div style="line-height: 14px;">
								
								<i>
									<label class="white-text">{{ $state->name . ', ' .$town->name}}</label>
									<br>
									<label class="white-text">{{ $institution->address }}</label>
									<br><br>
								</i>

							</div>
						</div>
					</a>
				</div>

				

			</div>
		</div>
		
	</div>


	@include('include.index.footer')
@endsection


@section('extra_scripts')
	<script type="text/javascript">

		function delete_function(argument) {
			var form = document.getElementById("form_delete"+argument);
	    	form.submit();
		}


		var mensaje = "";
		@if ($errors->any())
			@foreach($errors->all() as $error)
				mensaje += '{{ $error }}' + '\n';
			@endforeach

			swal({
				icon  : 'error',
				title : 'Oups!',
				text  : mensaje,
				button: 'ok',
			});

		@endif

		@if( session()->has('info') )
			swal({
				icon  : 'info',
				title : 'Message',
				text  : '{{ session()->get('errors') }}',
				button: 'Aceptar',
			});
		@endif

		@if( session()->has('message') )
			swal({
				icon  : 'success',
				title : 'Message',
				text  : '{{ session()->get('message') }}',
				button: 'Aceptar',
			});
		@endif
	</script>
@endsection



