@extends('layouts.app')
@section('title', 'Delphos Académico')

@section('extra_meta')
	@include('layouts.meta_shared.basic')
@endsection

@section('content')

	@include('include.navbar.default')
	@include('include.sidenav.default')

	@if (false)
		@include('include.index.carrusel')
	@endif
	@include('include.index.imagen')

	<br/><br/>
	@include('include.index.descriptions')



	@include('include.index.footer')
@endsection

@section('extra_scripts')
	<script type="text/javascript">

		var mensaje = "";
			@if ($errors->any())
			@foreach($errors->all() as $error)
				console.log('{{ $error }}');
				mensaje += '{{ $error }}' + '\n';
			@endforeach

				//"error", "success" and "info"
				swal({
					icon  : 'error',
					title : 'Oups!',
					text  : mensaje,
					button: 'ok',
				});

			@endif

		
		mensaje = "";
		@if ($errors->any())
		@foreach($errors->all() as $error)
		console.log('{{ $error }}');
		mensaje += '{{ $error }}' + '\n';
		@endforeach

			//"error", "success" and "info"
			swal({
				icon  : 'warning',
				title : 'Oups!',
				text  : mensaje,
				button: 'ok',
			});

			@endif
	</script>

@endsection

