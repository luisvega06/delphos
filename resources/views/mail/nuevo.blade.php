<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/materialize.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('css/materialize.css') }}" />

    
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}" />
    <style>
            body { background-color: #e3f2fd; }
            .tamaño{
                width:  1150px;
                height: 400px;
                margin: 6rem;
                padding: 1rem;
                border: 2px;
                /* IMPORTANTE */
                text-align: center;
            }
            .tamimg{
                width:  900px;
                height: 400px;
            }

            
    </style>
    <title>Nuevo Email</title>
</head>
@include('include.navbar.default')
<body>

    
    
    <form action="/email" method="POST">
    {{ csrf_field() }}

    <div class="center white tamaño  " style="padding-right: 5%; padding-left: 5%;">


            
        
                <br/><br/>
        
                <div class="row ">
                    <div class="col s12">
                            <div class="col s12 m12 l6 xl6 hide-on-med-and-down tamimg" >
        
                                    <img class="responsive-img" style="height: 310.219px; min-width: 380px; max-width: 300.453px;" src="{{ asset('images/img_logos_delphos/lgdelphos.png') }}" >
                                </div>
                                
                    
                                <br>
                                
                                <!-- form right-->
                                <div class="col s12 m12 l6 xl6 white" style="padding: 20px; min-height:290px;">
                    
                                    <div class="center">
                                        <div class="col s12 center-align ">
                                            <h5>
                                                <b>
                                                    Quiero enviar esta información a:
                                                </b>
                                            </h5>
                                            <br/><br/>
                    
                                        </div> 
                                                               
                                            <div class="input-field col s12 m12 l12 xl12">
                                                <input id="correo" name="correo" type="text" class="validate" value="" placeholder="ingresa un correo">
                                                
                                            </div>
                                        
                                            
                                        <button class="col s12 waves-effect waves-light btn btn-radius blue lighten-1 mt10" type="submit" >
                                            Enviar
                                        </button>
                                        <a href="/programs/{{$id}}" class="col s12 waves-effect waves-light btn btn-radius white mt10 black-text">Omitir</a>
                                    </div>
                    
                    
                                    
                                </div>
                            </div>
                    
                    </div>
                    <!-- Imagen left -->
                    
                </div>
                
        
        
            
            
        </div>
        <input type="text" name="id" value="{{$id}}" hidden>

    </form>
        
    <br />
    
    
</body>
</html>