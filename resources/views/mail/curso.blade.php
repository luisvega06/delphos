<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Curso</title>
</head>
<body>
    <p>Hola! Este es tu curso.</p>
    <ul>
        <li>Nombre: {{$programa->name}}</li>
    </ul>
    <p>Para más información:</p>
    <ul>
        <li>
            <a href="http://127.0.0.1:8000/programs/{{$programa->id}}">
                Ver Curso
            </a>
        </li>
    </ul>
</body>
</html>