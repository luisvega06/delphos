@extends('layouts.app')

@section('title', 'Administrativo')

@section('style_body', 'white')


@section('content')

	@include('include.navbar.logo_center')
	@include('include.sidenav.administrativos')

	<form action="/administrativos" method="POST" >
		@csrf
		<div class="row">
			<div class="col s12 m10 l6 xl6 white offset-m1 offset-l3 offset-xl3 hoverable z-depth-5 content-delphos-create">

				<div class="col s12 mb30">
					<br/>
					<h5 class="center-align"><b>Crear Administrativo Delphos</b></h5>
				</div>

				<div class="input-field col s12 m6 l5 xl5 offset-l1 offset-xl1">
					<input id="name" name="name" type="text" class="validate capitalize" value="{{ old('name') }}">
					<label for="name">Nombre Completo</label>
				</div>

				<div class="input-field col s12 m6 l5 xl5">
					<input id="number" name="number" type="tel" pattern="[0-9]{3}[0-9]{3}[0-9]{4}" class="validate" onKeypress="if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;" value="{{ old('number') }}">
					<label for="number">Celular</label>
				</div>

				<div class="input-field col s12 m12 l10 xl10 offset-l1 offset-xl1">
					<input id="email" name="email" type="email" class="validate lowercase"  value="{{ old('email') }}">
					<label for="email">Email</label>
				</div>

			
				<div class="input-field col s12 m6 l5 xl5 offset-l1 offset-xl1">
					<input id="password" name="password" type="password" class="validate">
					<label for="password">Password</label>
				</div>
				<div class="input-field col s12 m6 l5 xl5">
					<input id="password_confirmation" name="password_confirmation" type="password" class="validate">
					<label for="password_confirmation">Confirmar password</label>
				</div>
			

				<button class="col s12 m10 l6 xl6 offset-m1 offset-l3 offset-xl3 z-depth-2 waves-effect waves-light btn btn-radius" style="margin-bottom: 30px;">
					Registrar
				</button>


			</div>
		</div>
	</form>

	@include('include.index.footer')

@endsection



@section('extra_scripts')

	<script>
		
		var mensaje = "";
		@if ($errors->any())

			@foreach($errors->all() as $error)
				mensaje += '{{ $error }}' + '\n';
			@endforeach

			//"error", "success" and "info"
			swal({
				icon  : 'error',
				title : 'Oups!',
				text  : mensaje,
				button: 'ok',
			});

		@endif


		@if( session()->has('info') )
			swal({
				icon  : 'info',
				title : 'Notificación',
				text  : '{{ session()->get('info') }}',
				button: 'Aceptar',
			});
		@endif


		@if( session()->has('message') )
			swal({
				icon  : 'success',
				title : 'Message',
				text  : '{{ session()->get('message') }}',
				button: 'Aceptar',
			});
		@endif
		
	</script>

@endsection



