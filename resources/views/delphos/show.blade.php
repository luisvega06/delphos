@extends('layouts.app')

@section('title', 'Administrativo')

@section('style_body', 'white')


@section('content')

	@include('include.navbar.logo_center')
	@include('include.sidenav.administrativos')

	
		
	<div class="row">
		<div class="col s12 m10 l6 xl6 white offset-m1 offset-l3 offset-xl3 hoverable z-depth-5 content-delphos-create center">

			<div class="col s12 mb30">
				<br/>
				<h5 class="center-align"><b>Administrativo Delphos</b></h5>
			</div>

			<img class="img-facture responsive-img circle center" src="{{ asset($user->avatar) }}"  alt="{{ $user->name }} - Delphos Académico" title="{{ $user->name }} - Delphos Académico" />
			<br>

			<hr>

			<div class="col s12 m6 l5 xl5 offset-l1 offset-xl1">
				<br>
				<label for="name">Nombre Completo</label><br>
				<b><span>{{$user->name}}</span></b>
				<br>
			</div>

			<div class="col s12 m6 l5 xl5">
				<br>
				<label for="number">Celular</label><br>
				<b><span>{{$phone->number}}</span></b>
				<br>
			</div>

			<div class="col s12 m12 l10 xl10 offset-l1 offset-xl1">
				<br>
				<label for="email">Correo electrónico </label><br>
				<b><span>{{$user->email}}</span></b>
				<br><br>
			</div>

		

			@if ($user->id == Auth::user()->id)
				<a class="col s12 m10 l6 xl6 offset-m1 offset-l3 offset-xl3 z-depth-2 waves-effect waves-light btn btn-radius blue" href="/administrativos/{{$user->username}}/edit" style="margin-bottom: 30px;">
					Editar
				</a>
			@endif


		</div>
	</div>
	

	@include('include.index.footer')

@endsection



@section('extra_scripts')

	<script>
		
		var mensaje = "";
		@if ($errors->any())

			@foreach($errors->all() as $error)
				mensaje += '{{ $error }}' + '\n';
			@endforeach

			//"error", "success" and "info"
			swal({
				icon  : 'error',
				title : 'Oups!',
				text  : mensaje,
				button: 'ok',
			});

		@endif


		@if( session()->has('info') )
			swal({
				icon  : 'info',
				title : 'Notificación',
				text  : '{{ session()->get('info') }}',
				button: 'Aceptar',
			});
		@endif


		@if( session()->has('message') )
			swal({
				icon  : 'success',
				title : 'Message',
				text  : '{{ session()->get('message') }}',
				button: 'Aceptar',
			});
		@endif
		
	</script>

@endsection



