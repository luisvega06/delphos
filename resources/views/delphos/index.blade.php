@extends('layouts.app')

@section('title', 'Home')

@section('content')

	
	@include('include.navbar.logo_center')
	@include('include.sidenav.administrativos')


	<br><br>
	<div class="row">
		
		<div class="input-field col s10 m8 l8 xl8 offset-s1 offset-m1 offset-l1 offset-xl1">
			<i class="material-icons prefix">textsms</i>
			<input type="text" id="autocomplete-input" class="autocomplete">
			<label for="autocomplete-input">Institución</label>
		</div>

		<button class="col s10 m2 l2 xl2 offset-s1 waves-effect waves-light btn mt15" id="btn-buscar">
			Buscar
		</button>
		
		
	</div>

	<div class="row mt30">

		<div class="col s12 m12 l10 xl10 offset-l1 offset-xl1">
			<table class="highlight">
				<thead>
					<tr>
						<th>No.</th>
						<th>Institución</th>
						<th>Fecha</th>
						<th class="center">Acciones</th>
					</tr>
				</thead>

				<tbody>
					<?php $cont = 0;?>
					@foreach($instituciones as $institucion )
					<tr>
						<td> {{ $cont+=1 }} </td>
						<td> {{ $institucion->name }} </td>
						<td> {{ $institucion->created_at }} </td>
						<td class="center"> 
							
							<a class="btn-floating tooltipped transparent" 
								data-position="bottom" 
								data-tooltip="Detalles" 
								onclick="info('{{$institucion->username}}')">

									<i class="fas fa-info  grey-text"></i>

							</a>

							<a class="btn-floating tooltipped transparent" 
								data-position="bottom" 
								data-tooltip="Reporte"
								target="_blank" 
								href="/consultar-reporte-mensual-institucion/{{$institucion->username}}" 
								>

									<i class="fas fa-chart-line  grey-text"></i>

							</a>

							<a class="btn-floating tooltipped transparent" data-position="bottom" data-tooltip="Contact" target="_blank" href="https://wa.me/57{{$institucion->number}}">
								<i class="fab fa-whatsapp  grey-text"></i>
							</a>

						</td>
					</tr>
					@endforeach
				</tbody>
			</table>

			<br/>
			<div class="col s12 center">
				{{ $instituciones->links('vendor.pagination.materialize') }}
			</div>

		</div>
	</div>



	{{-- Detalles --}}
	<div id="modal1" class="modal modal-fixed-footer">
		<div class="modal-content">
			

			<div class="row center">
				<div class="col s12">
					<h5>Información</h5>
				</div>
				<div class="col s12">
					<img class="circle" 
						id="avatar" 
						src="{{ asset('images/img_default/default.jpg') }}"  
						width="120px" 
						height="120px" 
						 />
				</div>
				<div class="col s12">
					<table class="striped centered">
						<tbody>
							<tr>
								<td>
									<label>Nombre: </label>
									<br> 
									<span id="nombre">|||||||||||||||||||</span>
								</td>
            					
							</tr>
							<tr>
								<td>
									<label>Email: </label>
									<br> 
									<span id="email">|||||||||||||||||||</span>
								</td>
            					
							</tr>
							<tr>
								<td>
									<label>Celular: </label>
									<br> 
									<span id="celular">|||||||||||||||||||</span>
								</td>
            					
							</tr>
							<tr>
								<td>
									<label>Departamento: </label>
									<br> 
									<span id="departamento">|||||||||||||||||||</span></
            					
							</tr>
							<tr>
								<td>
									<label>Ciudad: </label>
									<br> 
									<span id="ciudad">|||||||||||||||||||</span>
								</td>
            					
							</tr>
							<tr>
								<td>
									<label>Dirección: </label>
									<br> 
									<span id="dirección">|||||||||||||||||||</span>
								</td>
            					
							</tr>
							<tr>
								<td>
									<label>Fecha Reg.: </label>
									<br> 
									<span id="fecha">|||||||||||||||||||</span>
								</td>
            					
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			
		</div>
		<div class="modal-header">
			<a href="#!" class="modal-close black-text waves-effect waves-red z-depth-5 btn-flat right">
				<i class="fas fa-times-circle"></i>
			</a>
		</div>
	</div>


	@include('include.index.footer')
@endsection


@section('extra_scripts')

	<script>
		
		var mensaje = "";
		@if ($errors->any())

			@foreach($errors->all() as $error)
				mensaje += '{{ $error }}' + '\n';
			@endforeach

			//"error", "success" and "info"
			swal({
				icon  : 'error',
				title : 'Oups!',
				text  : mensaje,
				button: 'ok',
			});

		@endif


		@if( session()->has('info') )
			swal({
				icon  : 'info',
				title : 'Notificación',
				text  : '{{ session()->get('info') }}',
				button: 'Aceptar',
			});
		@endif


		@if( session()->has('message') )
			swal({
				icon  : 'success',
				title : 'Message',
				text  : '{{ session()->get('message') }}',
				button: 'Aceptar',
			});
		@endif
		
	</script>

	<script src="{{ asset('js/administrativos/listar_instituciones.js'   ) }}"></script>
	<script src="{{ asset('js/administrativos/getDetalles.js'   ) }}"></script>
	<script src="{{ asset('js/administrativos/show_factura.js'   ) }}"></script>


@endsection

