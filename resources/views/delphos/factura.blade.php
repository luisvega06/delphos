@extends('layouts.app')

@section('title', 'Home')

@section('content')

	
	@include('include.navbar.logo_center')
	@include('include.sidenav.administrativos')

	<h5 class="center-align">
		<b>ESTADISTICAS DELHPOS ACADÉMICO</b>
		<br>
		<span><i>Panel Administrador</i></span>
	</h5>

	{{-- Perfil --}}
	<br>	
	<div class="row">
		<div class="col s10 m8 l8 xl8 offset-s1 offset-m2 offset-l2 offset-xl2 center">
			
			<img class="img-facture responsive-img circle" src="{{ asset($user_inst->avatar) }}"  alt="{{ $user_inst->name }} - Delphos Académico" title="{{ $user_inst->name }} - Delphos Académico" />


			<div class="row">
				<div class="col s12 m12 l12 xl12 white">

					<table>

						<thead class="teal white-text">
							<tr>
								<td>
									<span class="center-align">
										<b>Datos de usuario Institucional</b>
									</span>
								</td>
								<td></td>
							</tr>
						</thead>

						<tbody>
							<tr>
								<td><b>Fecha Facturación </b></td>
								<td><b>{{ $fecha_facturada }} </b></td>
							</tr>
							<tr>
								<td>Titular</td>
								<td>Dirección</td>
							</tr>
							<tr>
								<td><b> {{ $user_inst->name }}      </b></td>
								<td><b> {{ $institucion->address }} </b></td>
							</tr>
							<tr>
								<td>Ciudad</td>
								<td>Departamento</td>
							</tr>
							<tr>
								<td><b> {{ $town->name }}           </b></td>
								<td><b> {{ $state->name }}          </b></td>
							</tr>
						</tbody>
					</table>


					
				</div>
			</div>
		</div>
	</div>


	{{-- Reporte --}}

	<div class="row container">
		<div class="col s6 m3 l3 xl3 center-align z-depth-1">
			<br>
			<i class="fas fa-chalkboard-teacher fa-5x"></i>
			<br>
			<label>{{ $contenedor[0]->label }}</label>
			<br>
			{{ number_format($contenedor[0]->count,0,",",".")}}
			<label><i>(users)</i></label>
			<br><br>
		</div>
		<div class="col s6 m3 l3 xl3 center-align z-depth-1">
			<br>
			<i class="fas fa-heart fa-5x"></i>
			<br>
			<label>{{ $contenedor[1]->label }}</label>
			<br>
			{{ number_format($contenedor[1]->count,0,",",".")}}
			<label><i>(users)</i></label>
			<br><br>
		</div>
		<div class="col s6 m3 l3 xl3 center-align z-depth-1">
			<br>
			<i class="fas fa-university fa-5x"></i>
			<br>
			<label>{{ $contenedor[2]->label }}</label>
			<br>
			{{ number_format($contenedor[2]->count,0,",",".")}}
			<label><i>(users)</i></label>
			<br><br>
		</div>
		<div class="col s6 m3 l3 xl3 center-align z-depth-1">
			<br>
			<i class="far fa-address-card fa-5x"></i>
			<br>
			<label>{{ $contenedor[3]->label }}</label>
			<br>
			{{ number_format($contenedor[3]->count,0,",",".")}}
			<label><i>(users)</i></label>
			<br><br>
			
		</div>
	</div>





	@include('include.index.footer')
@endsection


@section('extra_scripts')

	<script>
		
		var mensaje = "";
		@if ($errors->any())

			@foreach($errors->all() as $error)
				mensaje += '{{ $error }}' + '\n';
			@endforeach

			//"error", "success" and "info"
			swal({
				icon  : 'error',
				title : 'Oups!',
				text  : mensaje,
				button: 'ok',
			});

		@endif


		@if( session()->has('info') )
			swal({
				icon  : 'info',
				title : 'Notificación',
				text  : '{{ session()->get('info') }}',
				button: 'Aceptar',
			});
		@endif


		@if( session()->has('message') )
			swal({
				icon  : 'success',
				title : 'Message',
				text  : '{{ session()->get('message') }}',
				button: 'Aceptar',
			});
		@endif
		
	</script>



@endsection

