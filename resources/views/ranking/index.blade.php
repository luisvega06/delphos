@extends('layouts.app')

@section('extra_meta')
  @include('layouts.meta_shared.ranking')
@endsection

@section('title', 'Ranking Instituciones')
@section('style_body', 'white')

@section('content')

	@include('include.navbar.default')
	@include('include.sidenav.default')

	<div class="container" style="min-height: 350px;">
		
		<h5 class="center-align">
			<b>
				Ranking Institucional 
			</b>
		</h5>

		<br/>

		<div class="row center">
			@php ($index = 0)
			@if(sizeof ($contenedor ) != 0 )
				@foreach($contenedor as $content)
					<div class="col s6 m4 l2 xl2">

						<a class="waves-effect waves-light" href="{{ url('/institutions/'. $content->username) }}">
							
							<img align="center" class="circle z-depth-2" src="{{ asset($content->avatar) }}" width="103px" height="103px" style="max-height: 103px; min-width: 103px;" />

							<br/>

							@php ($index++)  
							<label>
								<b> {{ '#' . number_format( $index ,0,",",".")  }} </b>
							</label>

							<br>

							<label > 
								<b class="black-text">{{ $content->username}}</b>
							</label>
							
						</a>

					</div>
				@endforeach
			@else
				<a href="/" class="black-text waves-effect waves-light">
					<br/>
					<img class="responsive-img animated bounceInUp" src="{{ asset('images/img_default/nofavoritos.png') }}" style="max-height: 300px;" />
					<br/>
					<span class="center-align">
						<i>
							Agrega instituciones educativas a tu lista de favoritos y contempla toda la información de primera mano relevante para tu futuro académico.
						</i>

						<br/>

						<b>@Delphos Académico</b>
					</span>
				</a>
			@endif
			
		</div>

	</div>




	@include('include.index.footer')
@endsection


@section('extra_scripts')
	<script type="text/javascript">

		function delete_function(argument) {
			var form = document.getElementById("form_delete"+argument);
	    	form.submit();
		}


		var mensaje = "";
		@if ($errors->any())
			@foreach($errors->all() as $error)
				mensaje += '{{ $error }}' + '\n';
			@endforeach

			swal({
				icon  : 'error',
				title : 'Oups!',
				text  : mensaje,
				button: 'ok',
			});

		@endif

		@if( session()->has('info') )
			swal({
				icon  : 'info',
				title : 'Message',
				text  : '{{ session()->get('errors') }}',
				button: 'Aceptar',
			});
		@endif

		@if( session()->has('message') )
			swal({
				icon  : 'success',
				title : 'Message',
				text  : '{{ session()->get('message') }}',
				button: 'Aceptar',
			});
		@endif
	</script>
@endsection



