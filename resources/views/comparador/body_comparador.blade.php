<div class="row">

	{{-- CONTENIDO A --}}
	<div class="col s10 m10 l5 xl5 offset-s1 offset-m1 offset-l1 offset-xl1">
	
		<div class="row">
			{{-- Titulo: Datos Institución A --}}
			<div class="col s12 left-align">
				<span id="title-name-program-a">Datos Institución A</span> 
			</div>

			{{-- Lado A --}}
			<div class="col s12 right-align z-depth-2">
				<br>
				<img id="avatar-a" class="circle" src="{{ asset('/images/img_default/default.jpg') }}" width="100" height="100" />

				<br/>
				<b><label id="name-a" class="black-text"></label></b>
				<br>

				<div class="input-field col s12 m12 l12 xl12" style="line-height: 0.9; font-size: 1em;">
					<span>
						<b>Tipo Institución</b>
						<br>
						<label id="type-institution-a">||||||||||||||||||||||||||||||||||||||||</label>
					</span>
					<br/><br>

					<span>
						<b>Ubicación</b>
						<br>
						<label id="ubicacion-a">|||||||||||||||||||||||||||||||||||||||||||||||||||||||||</label>
					</span>
					<br/><br>

					<span>
						<b>Dirección</b>
						<br>
						<label id="address-a">|||||||||||||||||||||||||||||||||||||||||||||||||||||||||</label>
					</span>
					<br/><br>



					<span>
						<b>Formación</b>
						<br>
						<label id="formacion-a">|||||||||||||||||||||||||||||||||||||||||||||||||||||||||</label>
					</span>
					<br/><br>



					<span>
						<b>Modalidad</b>
						<br>
						<label id="modalidad-a">|||||||||||||||||||||||||||||||||||||||||||||||||||||||||</label>
					</span>
					<br/><br>

					<span>
						<b>Carrera / Programa</b>
						<br>
						<label id="programs_name-a">|||||||||||||||||||||||||||||||||||||||||||||||||||||||||</label>
					</span>
					<br/><br>

					<span>
						<b>Duración</b>
						<br>
						<label id="duracion-a">|||||||||||||||||||||||||||||||||||||||||||||||||||||||||</label>
					</span>
					<br/><br>

					<span>
						<b>Creditos</b>
						<br>
						<label id="program_credits-a">|||||||||||||||||||||||||||||||||||||||||||||||||||||||||</label>
					</span>
					<br/><br>

					<span>
						<b>Reg. SNIES</b>
						<br>
						<label id="program_codigo_snies-a">|||||||||||||||||||||||||||||||||||||||||||||||||||||||||</label>
					</span>
					<br/><br>

					<span>
						<b>Ultima Certificación</b>
						<br>
						<label id="program_last_accreditation-a">|||||||||||||||||||||||||||||||||||||||||||||||||||||||||</label>
					</span>
					<br/><br>

					<span>
						<b>Costo:</b>
						<label id="costo-a">COP $  |||||||||||||||||||||||| </label>
					</span>
					<br/><br>

					<span>
						<b>Inscripción:</b>
						<label id="costo_inscripcion-a">COP $  |||||||||||||||||||||||</label>
					</span>
					<br/><br>

					<span>
						<b>Pensum:</b>
						<label><a id="pensum-a" href='//www.delphosacademico.com' target="_blank"> Visualizar <i class="fas fa-link"></i></a></label>
					</span>
					<br/><br>

		            <span>
		              <b>Materias Afines: </b>
		              <br>
		              <div id="chips-a"></div>
		            </span>
		            <br/><br>

				</div>

			</div>
			
		</div>
	
	</div>

	<div class="col s10 m10 l5 xl5 offset-s1 offset-m1">
		
		<div class="row">
			{{-- Titulo: Datos Institución B --}}
			<div class="col s12 right-align">
				<span id="title-name-program-b">Datos Institución B</span> 
			</div>

			{{-- Lado B --}}
			<div class="col s12 left-align z-depth-2">
				
				<br>

				<img id="avatar-b" class="circle" src="{{ asset('/images/img_default/default.jpg') }}" width="100" height="100" />

				<br/>
				<b><label id="name-b" class="black-text"></label></b>
				<br>

				<div class="input-field col s12 m12 l12 xl12" style="line-height: 0.9; font-size: 1em;">
					<span>
						<b>Tipo Institución</b>
						<br>
						<label id="type-institution-b">||||||||||||||||||||||||||||||||||||||||</label>
					</span>
					<br/><br/>

					<span>
						<b>Ubicación</b>
						<br>
						<label id="ubicacion-b">|||||||||||||||||||||||||||||||||||||||||||||||||||||||||</label>
					</span>
					<br/><br/>

					<span>
						<b>Dirección</b>
						<br>
						<label id="address-b">|||||||||||||||||||||||||||||||||||||||||||||||||||||||||</label>
					</span>
					<br/><br/>



					<span>
						<b>Formación</b>
						<br>
						<label id="formacion-b">|||||||||||||||||||||||||||||||||||||||||||||||||||||||||</label>
					</span>
					<br/><br>


					<span>
						<b>Modalidad</b>
						<br>
						<label id="modalidad-b">|||||||||||||||||||||||||||||||||||||||||||||||||||||||||</label>
					</span>
					<br/><br>

					<span>
						<b>Carrera / Programa</b>
						<br>
						<label id="programs_name-b">|||||||||||||||||||||||||||||||||||||||||||||||||||||||||</label>
					</span>
					<br/><br>

					<span>
						<b>Duración</b>
						<br>
						<label id="duracion-b">|||||||||||||||||||||||||||||||||||||||||||||||||||||||||</label>
					</span>
					<br/><br>

					<span>
						<b>Creditos</b>
						<br>
						<label id="program_credits-b">|||||||||||||||||||||||||||||||||||||||||||||||||||||||||</label>
					</span>
					<br/><br>

					<span>
						<b>Reg. SNIES</b>
						<br>
						<label id="program_codigo_snies-b">|||||||||||||||||||||||||||||||||||||||||||||||||||||||||</label>
					</span>
					<br/><br>

					<span>
						<b>Ultima Certificación</b>
						<br>
						<label id="program_last_accreditation-b">|||||||||||||||||||||||||||||||||||||||||||||||||||||||||</label>
					</span>
					<br/><br>

					<span>
						<b>Costo</b>
						<label id="costo-b">COP $  |||||||||||||||||||||||| </label>
					</span>
					<br/><br>

					<span>
						<b>Inscripción</b>
						<label id="costo_inscripcion-b">COP $  |||||||||||||||||||||||</label>
					</span>
					<br/><br>

					<span>
						<b>Pensum</b>
						<label><a id="pensum-b" href='//www.delphosacademico.com' target="_blank""> Visualizar <i class="fas fa-link"></i></a></label>
					</span>
					<br/><br>

		            <span>
		              <b>Materias Afines: </b>
		              <br>
		              <div id="chips-b"></div>
		            </span>
		            <br/><br>

				</div>
			</div>
			
		</div>

	</div>

</div>