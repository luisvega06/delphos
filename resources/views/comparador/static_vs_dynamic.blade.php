@extends('layouts.app')

@section('title', 'Comparar')

@section('extra_style')
	<style type="text/css">
		.select-wrapper input.select-dropdown {
			font-size: 12px;
		}
	</style>
@endsection

@section('style_body', 'white')

@section('content')

	@include('include.navbar.default')
	@include('include.sidenav.default')

	
	<div class="row">

		<div class="col s12">
			<h3 class="center-align"><i><b>Comparar</b></i></h3>
			<br/><br/>
		</div>


		{{-- Header select --}}
		
		<div class="col s10 m10 l4 xl4 offset-s1 offset-m1 offset-l1 offset-xl1">

			<h5 class="center-align"><i><b>Institución A</b></i></h5>

			<div class="input-field col s12 m12 l12 xl12">
				<select class="icons" id="institucion_a" name="institucion_a">
					<option value="{{ $pivot->slug }}" data-icon="{{ asset($pivot->avatar) }}" class="right">{{ $pivot->name . ', ' . $pivot->program}}</option>
				</select>
				<label>Programas académicos</label>
			</div>

		</div>

		<div class="col s12 m12 l2 xl2 center">
			<h3><b><i> Vs </i></b></h3>
		</div>

		<div class="col s10 m10 l4 xl4 offset-s1 offset-m1">

			<h5 class="center-align"><i><b>Institución B</b></i></h5>

			<div class="input-field col s12">
				<select class="icons" id="institucion_b" name="institucion_b">
					<option value="0" disabled selected>Elige tu opción</option>
					@foreach ($contents as $content)
					<option value="{{ $content->slug }}" data-icon="{{ asset($content->avatar) }}" class="right">{{ $content->name . ', ' . $content->program}}</option>    
					@endforeach
				</select>
				<label>Programas académicos</label>
			</div>

		</div>

		<div class="col s12 m12 l12 xl12 center">

			<button class="waves-effect waves-light btn blue capitalize w-100 mt50 mb25" id="btn-compare" name="btn-compare">
				Aceptar
			</button>
		</div>
		

		{{-- linea --}}
		<div class="col s12 m12 l12 xl12">
			<hr class="style6">
		</div> 

	</div>

	{{-- Content instituciones A Vs B--}}
	@include('comparador.body_comparador')
	


	@include('include.index.footer')
@endsection



@section('extra_scripts')
	<script type="text/javascript">
		
		var mensaje = "";
		@if ($errors->any())
		@foreach($errors->all() as $error)
		console.log('{{ $error }}');
		mensaje += '{{ $error }}' + '\n';
		@endforeach

			//"error", "success" and "info"
			swal({
				icon  : 'error',
				title : 'Oups!',
				text  : mensaje,
				button: 'ok',
			});

			@endif

			@if( session()->has('message') )
			swal({
				icon  : 'success',
				title : 'Message',
				text  : '{{ session()->get('message') }}',
				button: 'Aceptar',
			});
			@endif

			@if( session()->has('adv-message') )
			swal({
				icon  : 'info',
				title : 'Información',
				text  : '{{ session()->get('message') }}',
				button: 'Aceptar',
			});
			@endif
	</script>

	<script src="{{ asset('js/community/comparador.js'   ) }}"></script>

@endsection




