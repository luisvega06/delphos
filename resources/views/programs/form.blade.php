<div class="input-field col s12 m6 l6 xl6">
	<select id="level_of_educations_id" name="level_of_educations_id">
		<option value="0" disabled selected>Seleccionar</option>
		@foreach($levelofeducations as $levelofeducation )
<!-- No Mover porque despues no detecta la tecla -->
<option value="{{ $levelofeducation->id }}" >{{$levelofeducation->name}}</option>
		@endforeach
	</select>
	<label>Nivel de formación</label>
</div>

<div class="input-field col s12 m6 l6 xl6">
	<select id="modalities_id" name="modalities_id" required="">
		<option value="0" disabled selected>Seleccionar</option>
		@foreach($modalities as $modality )
<!-- <-- No Mover porque despues no detecta la tecla -->
<option value="{{ $modality->id }}" >{{$modality->name}}</option>
		@endforeach
	</select>
	<label>Modalidad</label>
</div>


<div class="input-field col s12 m12 l12 xl12">
	<input id="name" name="name" type="text" class="validate capitalize" value="{{ old('name') }}">
	<label for="name">Nombre</label>
</div>

<div class="input-field col s12 m12 l6 xl6">
	<input id="codigo_snies" name="codigo_snies" type="text" class="validate capitalize" value="{{ old('codigo_snies') }}">
	<label for="codigo_snies">COD. SNIES</label>
</div>

<div class="input-field col s12 m12 l6 xl6">
	<input id="last_accreditation" name="last_accreditation" type="text" class="validate capitalize" placeholder="No. #### de ## (mes) de (año). Vigencia" value="{{ old('last_accreditation') }}">
	<label for="last_accreditation">No. Resolución ultima certificación</label>
</div>

<div class="input-field col s12 m6 l4 xl4">
	<input id="tiempo" name="tiempo" type="number" class="validate" placeholder="10" onKeypress="if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;"  value="{{ old('tiempo') }}" >
	<label for="tiempo">Duración</label>
</div>
<div class="input-field col s12 m6 l4 xl4">
    <select id="duracions_id" name="duracions_id">
@foreach($duracions as $duracion )
<!-- <-- No Mover porque despues no detecta la tecla -->
<option value="{{ $duracion->id }}" >{{$duracion->name}}</option>
		@endforeach
    </select>
    <label>Unidad de tiempo</label>
  </div>

<div class="input-field col s12 m12 l4 xl4">
	<input id="credits" name="credits" type="number" class="validate" placeholder="10" onKeypress="if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;" value="{{ old('credits') }}" >
	<label for="credits">Creditos</label>
</div>


<div class="col s12 m12 l12 xl12">
	
	<div class="input-field col s12 m12 l12 xl12">
		<span>Materias Afines</span>
	</div>

	<div class="input-field col s12 m4 l4 xl4">
	    <select id="select-materia-1" name="select-materia-1">
			@foreach($materias as $materia )
				<option value="{{ $materia->id }}">{{$materia->nombre}}</option>
			@endforeach
	    </select>
	    <label>Materia #1</label>
	</div>

	<div class="input-field col s12 m4 l4 xl4">
	    <select id="select-materia-2" name="select-materia-2">
			@foreach($materias as $materia )
				<option value="{{ $materia->id }}">{{$materia->nombre}}</option>
			@endforeach
	    </select>
	    <label>Materia #2</label>
	</div>

	<div class="input-field col s12 m4 l4 xl4">
	    <select id="select-materia-3" name="select-materia-3">
			@foreach($materias as $materia )
				<option value="{{ $materia->id }}">{{$materia->nombre}}</option>
			@endforeach
	    </select>
	    <label>Materia #3</label>
	</div>
	
</div>



<div class="row">

	<div class="col s12 m12 l12 xl12">

		<label class="left input-field"><h6>Descripción</h6></label>

		<textarea id="description" name="description" style="min-width: 100%; max-width: 100%; min-height: 150px;" placeholder="Información acerca del programa">{{ old('description') }}</textarea>

	</div>

</div>

<div class="input-field col s12 m12 l6 xl6">
	<input id="price" name="price" type="number" onKeypress="if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;" class="validate" value="{{ old('price') }}">
	<label for="price">Precio (COP)</label>
</div>

<div class="input-field col s12 m12 l6 xl6">
	<input id="inscription_price" name="inscription_price" type="number" onKeypress="if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;" class="validate" value="{{ old('inscription_price') }}">
	<label for="inscription_price">Precio Inscripción (COP)</label>
</div>

<div class="row">
	<div class="col s12 m12 l12 xl12">


		<label for="textarea1" class="left input-field"><h6>Intercambios</h6></label>
		
		<textarea id="info_intercambios" name="info_intercambios" style="min-width: 100%; max-width: 100%; min-height: 150px;" placeholder="Información relevante del programa (intercambios, oportunidades de practica entre otras cosas).">{{ old('info_intercambios') }}</textarea>

	</div>
</div>

<div class="row">
	<div class="col s12 m12 l12 xl12">

		<label for="textarea1" class="left input-field"><h6>Información para estudiantes extranjeros</h6></label>

		<textarea id="info_extranjero" name="info_extranjero" style="min-width: 100%; max-width: 100%; min-height: 150px;" placeholder="Soy de otro pais, ¿Que debo hacer para ingresar a la institución?">{{ old('info_extranjero') }}</textarea>

	</div>
</div>

<div class="input-field col s12 m12 l12 xl12">
	<input id="url_pensum" name="url_pensum" type="text" class="validate" value="{{ old('url_pensum') }}">
	<label for="url_pensum">URL Pensum</label>
</div>

<div class="col s12" style="padding-bottom: 50px;">
	<label for="img_programa" class="left input-field"><h6>Imagen del programa</h6></label>
	<div  style=" max-height: 200px; max-width: 200px; " >
		<img id="blah" class="responsive-img image" src="{{ asset('images/img_default/background.jpg') }}"  style=" max-height: 160px; max-width: 200px; margin-bottom: 20px; " />
		<span class="valign-wrapper">
			<input type="file" id="avatar" name="avatar" onchange="readURL(this);"  value="{{ old('avatar') }}" />
		</span>
	</div>
	<br/>
</div>





@section('extra_scripts_function')
	<script type="text/javascript">
		function readURL(input) {
			
			var filePath = input.value;
			var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
			if(!allowedExtensions.exec(filePath)){
				
				swal({icon  : 'error',title : 'Oups!', text : 'Formatos permitidos: .jpeg/.jpg/.png/.gif',button: 'ok',});

				input.value = '';
				$('#blah').attr('src', "{{ asset('images/img_default/background.jpg') }}");

			}else{

				if (input.files && input.files[0]) {
					var reader = new FileReader();

					reader.onload = function (e) {
						$('#blah')
						.attr('src', e.target.result);
					};

					reader.readAsDataURL(input.files[0]);
				}
			}
		}
	</script>
@endsection