@extends('layouts.app')

@section('title', 'Crear programa')

@section('style_body', 'grey lighten-2')

@section('content')

@include('include.navbar.default')


@include('include.sidenav.default')

<div class="container white hoverable">
	<div class="container center ">
		<br/><br/>

		<h5><b>Crear programa</b></h5>

		<br/><br/>

		<form action="/programs" method="POST" enctype="multipart/form-data">
			@csrf

			<div class="row">

				@include('programs.form')

				<input  type="text" name="institutions_id" hidden="true" value="{{ $institutions->id}}" />

				<button class="waves-effect waves-light btn" type="submit">Registrar</button>
			</div>
			
		</form>


		<br/><br/><br/>
	</div>
</div>




@endsection


@section('extra_scripts')

	<script>
	
		var mensaje = "";

		@if ($errors->any())
			@foreach($errors->all() as $error)
				console.log( '{{ $error }}');
				mensaje += '{{ $error }}' + '\n';
			@endforeach

			//"error", "success" and "info"
			swal({
				icon  : 'error',
				title : 'Oups!',
				text  : mensaje,
				button: 'ok',
			});

		@endif

		@if( session()->has('message') )
			swal({
				icon  : 'success',
				title : 'Message',
				text  : '{{ session()->get('message') }}',
				button: 'Aceptar',
			});
		@endif


	</script>

@endsection



