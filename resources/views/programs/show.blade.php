@extends('layouts.app')

@section('title', 'Detalles del Programa')

@section('extra_meta')
  @include('layouts.meta_shared.program')
@endsection



{{-- 

  
  @section('style_body', 'background_present_program') 
--}}
@section('style_body', 'grey lighten-2')


@section('content')

  @include('include.navbar.default')
  @include('include.sidenav.default')

  <div class="row">

    {{-- Cards Modalidad, Costo... --}}
    <div class="offset-m1 offset-l2 offset-xl2 col s12 m10 l8 xl8">

      <div class="col s12 center ">

        <h4 class="mt30 mb30 black-text darken-1">
          @auth
            @if (auth::user()->rols_id == 3)
              <a class="waves-effect waves-light" 
                  href="/institutions/">
                    <b>{{ $perfil->name }}</b>
              </a>
            @else
              <a class="waves-effect waves-light"
                  href="/institutions/{{$perfil->username}}">
                    <b>{{ $perfil->name }}</b>
              </a>
            @endif
          @endauth
          <br>
          {{ $program->name }}
        </h4>
        

      </div>

      {{-- Cabecera --}}
      <div class="row p10">

        <div style="min-height: 120px;" class="col s12 m4 l4 xl4 z-depth-1 white center">
          
          <i class="fas fa-donate fa-4x mt15 mb10"></i>
          <br>
          <label class="mt50"><b>{{ 'COP $' . number_format($program->price,0,",",".") }}</b></label>
          
        </div>

        <div style="min-height: 120px;" class="col s6 m4 l4 xl4 z-depth-1 white center">
          
          <i class="fas fa-graduation-cap fa-4x mt15 mb10"></i>
          <br>
          <label class="mt50"><b>{{  $levelofeducation->name }}</b></label>
          
        </div>

        <div style="min-height: 120px;" class="col s6 m4 l4 xl4 z-depth-1 white center">
          
          <i class="fas fa-diagnoses fa-4x mt15 mb10"></i>
          <br>
          <label class="mt50"><b>{{  $modality->name }}</b></label>
          
        </div>

      </div>

      {{-- Tabs --}}
      <div class="row p10">

        {{-- Tabs titulos--}}
        <div class="col s12 m12 l12 xl12" style="padding: 0px;">
          <ul class="tabs tabs-fixed-width">
            <li class="tab z-depth-1">
              <a class="active waves-effect waves-light w-full h-full mt-5" href="#tab0">
              Información
              </a>
            </li>

            <li class="tab z-depth-1">
              <a class="waves-effect waves-light w-full h-full mt-5" href="#tab1">
              Descripción
              </a>
            </li>

            <li class="tab z-depth-1">
              <a class="waves-effect waves-light w-full h-full mt-5" href="#tab2">
              Intercambios
              </a>
            </li>

            <li class="tab z-depth-1">
              <a class="waves-effect waves-light w-full h-full mt-5" href="#tab3">
              Extranjero
              </a>
            </li>

          </ul>
        </div>

        {{-- Tabs Contents --}}
        <div id="tab0" class="white left-align p20 z-depth-1" style="line-height: 180%; font-size: 1em ">
          <br><br>
          <span> <b>Carrera:  </b> {{  $program->name }} </span>
          <br/>

          <span> <b>Creditos: </b> {{  $program->credits }} </span>
          <br/>

          <span> <b>Duración: </b> {{  $program->tiempo . ' ' . $duracion->name }} </span>
          <br/>

          <span> <b>Inscripción: </b> {{ 'COP $' . number_format($program->inscription_price,0,",",".") }} </span>
          <br/>

          <span> <b>Código SNIES: </b> {{  $program->codigo_snies }} </span>
          <br/>

          <span> <b>Acreditación: </b> <i>{{  $program->last_accreditation }}</i> </span>
          <br/>

          <span> <b>Dirección:</b> {{  $institution->address . ', ' . $town->name . ', ' . $state->name . '.'}} </span>
          <br/>

          <span> 
            <b>Pensum: </b> 
            <a href="{{ url( $program->url_pensum)}}" target="_blank">
              Ver <i class="fas fa-link"></i>
            </a>
          </span>
          <br/>

          @if ( sizeof($list_materias_afines) > 0 )
            <span>
              
              <b>Materias Afines: </b>

              @foreach( $list_materias_afines as $materias_afines )

                <div class="chip">
                  {{ $materias_afines->nombre }}
                </div>

              @endforeach
            </span>
            <br>

          @endif

          
          @if ( $totalprograms > 0 )
            <span>
              <a href="{{ url( '/programs/ofertas/' . $institution->slug ) }}" >
                <span style="text-decoration-line: underline">Más de <b> {{ $totalprograms }} ofertas académicas </b> para tí.</span>
                <i class="far fa-arrow-alt-circle-right blue-text fa-sm"></i>

              </a>
            </span>
            <br/>
          @endif
          <br>

          {{-- 
            Devuelve FALSE si var existe y tiene un valor no vacío, distinto de cero. 
            De otro modo devuelve TRUE.
          --}}

          @if ( !empty($contact) )
            
            <button class="waves-effect waves-light btn blue" onclick="send_contact();">
              <b>Saber más</b> <i class="fas fa-question-circle"></i>
            </button>
            <br>

          @endif
        </div>

        <div id="tab1" class="white p20 z-depth-1">
          <br><br>
          <textarea class="black-text" disabled="true" style="border: none; min-height: 300px; max-width: 100%; min-width: 100%;">{{ $program->description }}</textarea>
        </div>

        <div id="tab2" class="white p20 z-depth-1">
          <br><br>
          <textarea class="black-text" disabled="true" style="border: none; min-height: 300px; max-width: 100%; min-width: 100%;">{{ $program->info_intercambios }}</textarea>
        </div>

        <div id="tab3" class="white p20 z-depth-1">
          <br><br>
          <textarea class="black-text" disabled="true" style="border: none; min-height: 300px; max-width: 100%; min-width: 100%;">{{ $program->info_extranjero }}</textarea>
        </div>


      </div>

      

      {{-- Options --}}
      @auth
        
        {{-- Comunidad --}}
        @if (Auth::user()->rols_id == 1)
          
          @include('programs.form_buttons')
          <div class="col s12 m12 l12 xl12 mt-10 center-align " style="padding-left: 0px; padding-right: 0px; margin-top: -15px;">
            
            {{-- Like --}}
            @if ($favourite)
              <a class="btn-floating btn-large waves-effect waves-light white"
                  id="btn-like1" 
                  onclick="flinke('btn-like1', 'icon-1', '{{ $institution->id }}', '{{ $program->id }}', '{{ Auth::user()->id }}', '{{ csrf_token() }}')">

                <i class="fas fa-bookmark fa-2x orange-text" id="icon-1"></i> 

              </a>
            @else
              <a class="btn-floating btn-large waves-effect waves-light white"
                  id="btn-like2" 
                  onclick="flinke('btn-like2', 'icon-2','{{ $institution->id }}', '{{ $program->id }}', '{{ Auth::user()->id }}', '{{ csrf_token() }}')">

                <i class="far fa-bookmark fa-2x orange-text" id="icon-2"></i> 

              </a>
            @endif

            {{-- Compartir: Comparador --}}
            <a href="/comparar-oferta-academica/{{$program->id}}/" class="btn-floating btn-large waves-effect waves-light white">

              <i class="fas fa-balance-scale fa-2x orange-text"></i> 

            </a>

            {{-- Inscripciones --}}
            <a  class="btn-floating btn-large waves-effect waves-light white "
                href="{{ url( '//' . $institution->ulr_inscriptions)}}" 
                target="_blank" 
                onclick="clickcount('{{ csrf_token() }}', '{{ Auth::user()->id }}', '{{ $institution->id }}', '{{ $program->id }}')">

              {{-- <i class="fas fa-user-plus fa-2x oro-text"></i> --}}
              <i class="fab fa-wpforms fa-3x orange-text"></i>

            </a>

            <a  class="btn-floating btn-large waves-effect waves-light white "
                href="/enviarEmail/{{$program->id}}" 
                target="_blank" 
                onclick="clickcount('{{ csrf_token() }}', '{{ Auth::user()->id }}', '{{ $institution->id }}', '{{ $program->id }}')">

              {{-- <i class="fas fa-user-plus fa-2x oro-text"></i> --}}
              <i class="fa fa-envelope fa-3x orange-text"></i>

            </a>

            
            
            

          </div>
          

        @else
          @if (Auth::user()->id == $perfil->id)

          <div class="col s12 m12 l12 xl12 mt-10 center-align " style="padding-left: 0px; padding-right: 0px;">

            @include('include.shared.button_social_circle')

            
            <a class="waves-effect waves-light btn-floating btn-flat green accent-4"  
                target="_blank" 
                href="https://wa.me/?text=Estudia {{$program->name}} en *{{$perfil->name}}*  https://delphosacademico.com/programs/{{$program->id}}">

                <i class="fab fa-whatsapp"></i>
            </a>
            
            
          </div>

          {{-- Propietario --}}
          <br><br><br><br>
          <div class="col s12 m12 l12 xl12 red mt-30 z-depth-1" style="padding-left: 0px; padding-right: 0px;">

            <a id="btn-delete2" class="col s6 m6 l6 xl6 waves-effect waves-light btn red" onclick="delete_function({{$program->id}})">
              <span>
                eliminar
                <i class="fas fa-trash"></i>
              </span>
            </a>

            <a href="/programs/{{$program->slug}}/edit" class="col s6 m6 l6 xl6 waves-effect waves-light btn blue">
              editar
              <i class="far fa-edit"></i>
            </a>

          </div>

          @endif  
        @endif

      @else

        <div class="col s12 m12 l12 xl12 mt-10 center-align " style="padding-left: 0px; padding-right: 0px;">

          @include('include.shared.button_social_circle')
          

          <a class="waves-effect waves-light btn-floating btn-flat green accent-4"  
                target="_blank" 
                href="https://wa.me/?text=Estudia {{$program->name}} en *{{$perfil->name}}*  https://delphosacademico.com/programs/{{$program->id}}">

                <i class="fab fa-whatsapp"></i>
            </a>

        </div>

      @endauth

    </div>

    <a  class="  right "
                href="/presentacion/{{$program->institution->user->username}}/" 
                 
                >

              {{-- <i class="fas fa-user-plus fa-2x oro-text"></i> --}}
              <img  src="{{ asset('images/img_default/siguiente.png') }}"/>

            </a>

  </div>
           

  <br>
  <br>

  {{-- Form Delete Program --}}
  <form id="form_delete{{$program->id}}" action="/programs/{{$program->id}}" method="POST" hidden="true" style="hidden: true;">
    @method('DELETE')
    @csrf

    <a id="btn-delete" >
      <i class="fas fa-trash"></i>
    </a>
  </form>
{{--
<div class="container">
    <form id="formulario-test" action="/programs/like-program" method="POST" h>
    @csrf

        <input type="text" name="idinstitucion" value="{{ Auth::user()->id }}">
        <input type="text" name="idprograma" value="{{ $institution->id }}">
        <input type="text" name="iduser" value="{{ $program->id }}">
        <button type="submit">enviar</button>
  </form>
</div>
--}}

  @include('include.index.footer')
@endsection

@section('extra_links')
  <script src="{{ asset('js/programs/show.js') }}"></script>
  <script src="{{ asset('js/programs/countclick.js') }}"></script>
  <script src="{{ asset('js/social_shared_button/social.js'   ) }}"></script>

  <script type="text/javascript">

    function send_contact() {
      @auth
      
        var mensaje = '57{{$contact->number}}'
        mensaje += '?text=User: {{Auth::user()->name}}, Email: {{Auth::user()->email}}. Via *Delphos Académico*';

        window.open('https://wa.me/'+mensaje, '_blank');
      @endauth
      
    }
    
  </script>
@endsection
