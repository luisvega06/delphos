@extends('layouts.app')

@section('title', 'Programas académicos')

@section('style_body', 'grey lighten-2')

@section('content')

@include('include.navbar.default')



@include('include.sidenav.default')



<div class="white p20">
	<br/>
	<div class=" center ">
		@if(sizeof ($programs ) != 0 )

		<h4>Ofertas académicas {{ $user->name }}</h4>
		<br/>

		@if( sizeof ($ofertasacademicas) != 0 )
		<ul id="tabs-swipe" class="tabs">
			@foreach($ofertasacademicas as $oferta)
			<li class="tab col">
				<a class="active black-text" href="#swipe-{{$oferta->id}}">
					&nbsp;&nbsp; {{ $oferta->name }} &nbsp;&nbsp; 
				</a>
			</li>
			@endforeach
		</ul>

		@foreach($ofertasacademicas as $oferta)

		<div id="swipe-{{ $oferta->id }}" class="col s12">
			<div class="row">


				@foreach($programs as $program)
				@if ($program->level_of_educations_id == $oferta->id)

				<div class="col s12 m6 l3 xl3">

					<div class="card hoverable">
						<div class="card-image">
							<img class="responsive-img materialboxed" data-caption=" {{$oferta->name . ' - ' . $program->name }}" src="{{ asset($program->avatar) }}" style="max-height: 150px; min-height: 150px;">
						</div>
						<div class="card-content" style="min-height: 180px; max-height: 180px;">


							<p  style="min-width: 150px; min-height: 70px;">
								{{ $program->name }}
							</p>


							<p>
								@foreach($levelofeducations as $levelofeducation)
								@if ($program->level_of_educations_id == $levelofeducation->id)
								<b>{{$levelofeducation->name}}</b>
								@break
								@endif
								@endforeach
								<br/>

								@foreach($modalities as $modality)
								@if ($program->modalities_id == $modality->id)
								<b>( {{$modality->name}} )</b>
								@break
								@endif
								@endforeach
							</p>
						</div>
						<div class="card-action right-align">
							<a class="btn grey " href="/programs/{{$program->id}}">info</a>

							@auth

							@if ( $institutions->users_id == Auth::user()->id)
								<a id="btn-delete2" class="waves-effect waves-light btn red accent-4" onclick="delete_function({{$program->id}})">
									<i class="fas fa-trash"></i>
								</a>

								<form id="form_delete{{$program->id}}" action="/programs/{{$program->id}}" method="POST" hidden="true" style="hidden: true;">
									@method('DELETE')
									@csrf

									<a id="btn-delete" >
										<i class="fas fa-trash"></i>
									</a>
								</form>
							@endif

							@endauth

						</div>
					</div>

					
				</div>

				@endif
				@endforeach
			</div>


		</div>

		@endforeach



		@endif

		@else
		<h5 class="black-text"><b>No se encontrarón elementos</b></h5>
		<br/><br/>
		<img class="responsive-img" src="{{ asset('images/img_default/nofavoritos.png') }}" style="max-height: 300px;" />
		@endif
	</div>
</div>


	@auth

		@if ( $institutions->users_id == Auth::user()->id)
			<div class="fixed-action-btn">
				<a class="btn-floating btn-large red" href="{{ url('/programs/create') }}">
					<i class="fas fa-plus"></i>
				</a>
			</div>
		@endif

	@endauth


@include('include.index.footer')
@endsection


@section('extra_scripts')
<!-- Script listar municipios-->

<script type="text/javascript">

	function delete_function(argument) {
		var form = document.getElementById("form_delete"+argument);
    	form.submit();
	}


	var mensaje = "";
	@if ($errors->any())
		@foreach($errors->all() as $error)
			mensaje += '{{ $error }}' + '\n';
		@endforeach

		swal({
			icon  : 'error',
			title : 'Oups!',
			text  : mensaje,
			button: 'ok',
		});

	@endif

	@if( session()->has('info') )
		swal({
			icon  : 'info',
			title : 'Message',
			text  : '{{ session()->get('errors') }}',
			button: 'Aceptar',
		});
	@endif

	@if( session()->has('message') )
		swal({
			icon  : 'success',
			title : 'Message',
			text  : '{{ session()->get('message') }}',
			button: 'Aceptar',
		});
	@endif


</script>
@endsection



