<div class="fixed-action-btn">
  
  <a class="btn-floating btn-large blue waves-effect waves-light ">
    <i class="fas fa-share-alt"></i>
  </a>

  <ul>

    {{-- Compartir: Whatsapp --}}
    <li>

      <a class="waves-effect waves-light btn-floating btn-flat green accent-4"
          target="_blank" 
          href="https://wa.me/?text=Estudia {{$program->name}} en *{{$perfil->name}}*  https://delphosacademico.com/programs/{{$program->id}}">

          <i class="fab fa-whatsapp"></i>
      </a>
      
    </li>

    {{-- Compartir: google-plus --}}
    <li>

      <button class="waves-effect waves-light btn-floating btn-flat deep-orange darken-1" id="share-google-plus">
        <i class="fab fa-google-plus-g"></i>
      </button>
      
    </li>

    {{-- Compartir: twitter --}}
    <li>

      <button class="waves-effect waves-light btn-floating btn-flat blue darken-1" id="share-twitter">
        <i class="fab fa-twitter"></i>
      </button>
      
    </li>

    {{-- Compartir: facebook --}}
    <li>

      <button class="waves-effect waves-light btn-floating btn-flat blue darken-4" id="share-fb">
        <i class="fab fa-facebook-f"></i>
      </button>
      
    </li>

  </ul>
</div>