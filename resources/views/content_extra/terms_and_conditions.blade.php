<!DOCTYPE html>
<html lang="es">
<head>
	<!-- Hoja de estilos inicial -->
	<meta charset="UTF-8">
	<!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta content="IE=edge" http-equiv="X-UA-Compatible"></meta>
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="cache-control" content="no-store" />
    <meta http-equiv="cache-control" content="must-revalidate" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    

	<link rel="shortcut icon" href="{{ asset('images/img_logos_delphos/lgdelphosnotext.png') }}"/>
	
	<!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>

    <title>Legal- Delphos Académico</title>
    <link rel="stylesheet" href="{{ asset('css/materialize.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}" />
	<link href="{{ asset('css/terms_and_conditions_style.css') }}" rel="stylesheet" type="text/css" media="print">

	


</head>
<body class="white bloque" ondragstart="return false" onselectstart="return false" oncontextmenu="return false">

	<div class="WordSection1 container animated fadeInLeftBig">

		<p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:.75pt;
		margin-left:0cm;text-align:justify;line-height:normal;mso-outline-level:1'><span
		style='font-size:22.5pt;font-family:"interstate-Bold",serif;mso-fareast-font-family:
		"Times New Roman";mso-bidi-font-family:"Times New Roman";mso-font-kerning:18.0pt;
		mso-fareast-language:ES-CO'>Términos y condiciones<o:p></o:p></span></p>

		<p class=MsoNormal style='margin-top:15.0pt;margin-right:0cm;margin-bottom:
		7.5pt;margin-left:0cm;text-align:justify;line-height:normal;mso-outline-level:
		2;background:white'><span style='font-size:18.0pt;font-family:"interstate-Bold",serif;
		mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:Arial;
		mso-fareast-language:ES-CO'>Derechos de autor y marcas registradas<o:p></o:p></span></p>

		<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
		justify;line-height:normal;background:white'><span style='font-size:12.0pt;
		font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
		mso-fareast-language:ES-CO'>Todo el contenido es propiedad de DELPHOS S.A.S.<br>
		Todos los derechos de autor y marcas registradas reservados.<o:p></o:p></span></p>

		<p class=MsoNormal style='margin-top:15.0pt;margin-right:0cm;margin-bottom:
		7.5pt;margin-left:0cm;text-align:justify;line-height:normal;mso-outline-level:
		2;background:white'><span style='font-size:18.0pt;font-family:"interstate-Bold",serif;
		mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:Arial;
		mso-fareast-language:ES-CO'>Contenido<o:p></o:p></span></p>

		<p class=MsoNormal style='margin-bottom:7.5pt;text-align:justify;line-height:
		normal;background:white'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
		mso-fareast-font-family:"Times New Roman";mso-fareast-language:ES-CO'>Todo
		texto, información, datos, fotografías, gráficos, código <span class=SpellE>html</span>,
		software informático, código fuente y código objeto, muestras de audio y video,
		marcas y logotipos, y similares (en adelante el &quot;Contenido&quot;) que
		aparezcan en este sitio web optimizado para dispositivos móviles, servicios de
		dispositivos móviles o aplicaciones de dispositivos móviles (en conjunto
		&quot;Aplicación Móvil&quot;) pertenecen a DELPHOS S.A.S. o sus empresas
		afiliadas, licenciatarios o proveedores, salvo que quede expresamente
		especificado en esta Aplicación Móvil. Los Usuarios (como se definen en el
		presente documento) solo pueden utilizar el Contenido en un dispositivo móvil
		(por ejemplo, Apple iPhone o iPad, dispositivo Android o dispositivo móvil con
		Microsoft Windows) que posean o controlen, y solamente por motivos personales,
		internos y sin fines comerciales. Todos los derechos reservados.” Ningún otro
		uso del Contenido, incluyendo, entre otras, cualquier clase de reedición del
		mismo, está permitido sin previa autorización por escrito otorgada por la
		Compañía. Todo Usuario que haya violado comprobadamente la propiedad
		intelectual de un tercero mediante la retransmisión o publicación de material
		vinculado con esta Aplicación Móvil que infrinja los derechos de autor u otros
		derechos legales de dicho tercero será excluido de esta Aplicación Móvil.<o:p></o:p></span></p>

		<p class=MsoNormal style='margin-bottom:7.5pt;text-align:justify;line-height:
		normal;background:white'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
		mso-fareast-font-family:"Times New Roman";mso-fareast-language:ES-CO'>Todas las
		marcas registradas utilizadas en esta Aplicación Móvil son propiedad de la
		Compañía o, en unos pocos casos, utilizadas con la autorización de sus
		respectivos titulares. Ningún tercero puede utilizar ni reproducir ninguna
		marca registrada que incluya, entre otros, logotipos y dominios de Internet que
		utilicen las marca registrada DELPHOS S.A.S. (ya sea que se usen o no con
		letras mayúsculas o espacios) sin el previo consentimiento por escrito de la
		Compañía o del propietario de la marca registrada.<o:p></o:p></span></p>

		<p class=MsoNormal style='margin-bottom:7.5pt;text-align:justify;line-height:
		normal;background:white'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
		mso-fareast-font-family:"Times New Roman";mso-fareast-language:ES-CO'>Aparte del
		derecho de uso no exclusivo, no <span class=SpellE>sublicenciable</span>,
		intransferible, personal y limitado de los Usuarios tal como se especifica en
		la presente, no se confiere ningún derecho a dicho Contenido ni a porciones del
		mismo, sin importar la forma en que aparezca, mediante su inclusión en esta
		Aplicación Móvil o mediante el acceso al mismo por parte del Usuario. Un
		Usuario no puede: (a) separar ningún Contenido individual o componente de la
		Aplicación Móvil para otro uso que el indicado en relación a la Aplicación Móvil;
		(b) incorporar una porción del mismo a los propios programas del Usuario o
		recopilar cualquier porción en combinación con los propios programas del
		Usuario; (c) transferirlo para ser utilizado por otro servicio; o (d) vender,
		arrendar, ceder, prestar, distribuir, comunicar públicamente, transformar o
		sublicenciar la Aplicación Móvil o conceder en modo alguno cualquier derecho a
		la Aplicación Móvil absoluta o parcialmente. La Compañía será responsable de
		todo mantenimiento o soporte técnico de la Aplicación Móvil; ningún Tercero
		(como se define en el presente documento) será responsable de brindar servicios
		de mantenimiento o soporte técnico para la Aplicación Móvil.<o:p></o:p></span></p>

		<p class=MsoNormal style='margin-bottom:7.5pt;text-align:justify;line-height:
		normal;background:white'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
		mso-fareast-font-family:"Times New Roman";mso-fareast-language:ES-CO'>La
		Compañía se reserva el derecho de enmendar, complementar o suspender total o
		parcialmente la Aplicación Móvil en forma ocasional. Asimismo, la Compañía se
		reserva el derecho de cambiar los Términos y Condiciones en cualquier momento,
		con vigencia inmediata a partir del momento que se actualiza la Aplicación
		Móvil. Los términos “Usuario” y “Usuarios” se refieren a todo individuo o
		entidad que use, acceda, descargue, instale, obtenga o brinde información desde
		y hacia esta Aplicación Móvil. Todas las referencias al plural en la presente
		incluirán al singular y las referencias al singular incluirán al plural salvo
		que se desprenda otra interpretación del contexto.<o:p></o:p></span></p>

		<p class=MsoNormal style='margin-top:15.0pt;margin-right:0cm;margin-bottom:
		7.5pt;margin-left:0cm;text-align:justify;line-height:normal;mso-outline-level:
		2;background:white'><span style='font-size:18.0pt;font-family:"interstate-Bold",serif;
		mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:Arial;
		mso-fareast-language:ES-CO'>Reclamos por violación de derechos en la aplicación
		móvil<o:p></o:p></span></p>

		<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
		justify;line-height:normal;background:white'><span style='font-size:12.0pt;
		font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
		mso-fareast-language:ES-CO'>En caso de que un visitante crea que sus derechos
		de propiedad intelectual o marcas registradas puedan estar siendo violados por
		materiales publicados o almacenados en esta Aplicación Móvil, deberá completar
		la “<a href="http://www2.socalgas.com/espanol/terms-and-conditions/form.shtml"><span
			style='color:windowtext;text-decoration:none;text-underline:none'>Notificación
		de violación de derechos</span></a>” del enlace y enviar un correo electrónico
		a nuestro <span class=SpellE>webmaster</span>:&nbsp;___________________&nbsp;con
		una copia de confirmación enviada a:<o:p></o:p></span></p>

		<p class=MsoNormal style='text-align:justify;line-height:normal;background:
		white'><span style='font-size:13.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
		"Times New Roman";mso-fareast-language:ES-CO'>Dirección:&nbsp;________________________<o:p></o:p></span></p>

		<p class=MsoNormal style='margin-bottom:7.5pt;text-align:justify;line-height:
		normal;background:white'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
		mso-fareast-font-family:"Times New Roman";mso-fareast-language:ES-CO'>Dicha
		Notificación debe brindar la información requerida en cumplimiento de las
		cláusulas aplicables de la Ley 1403 de 2010 (y todas sus modificatorias). Por
		favor, envíe una Notificación por separado para cada caso que desee informar
		como un caso supuesto de violación de derechos de autor.<o:p></o:p></span></p>

		<p class=MsoNormal style='margin-top:15.0pt;margin-right:0cm;margin-bottom:
		7.5pt;margin-left:0cm;text-align:justify;line-height:normal;mso-outline-level:
		2;background:white'><span style='font-size:18.0pt;font-family:"interstate-Bold",serif;
		mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:Arial;
		mso-fareast-language:ES-CO'>Uso<o:p></o:p></span></p>

		<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
		justify;line-height:normal;background:white'><span style='font-size:12.0pt;
		font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
		mso-fareast-language:ES-CO'>Al usar, acceder, descargar, instalar, obtener o
		brindar información desde y hacia esta Aplicación Móvil, se considerará que los
		Usuarios han leído y aceptado estos Términos y Condiciones (incluyendo
		nuestra&nbsp;<a href="https://www.socalgas.com/es/privacy-policy"><span
			style='color:windowtext;text-decoration:none;text-underline:none'>Política de
		privacidad</span></a>), que se incorpora al presente documento en virtud de
		esta referencia. El uso de esta Aplicación Móvil también estará sujeto a los
		Términos y Condiciones de (página web de DELPHOS) y, si correspondiera, a los
		Términos y condiciones de <span class=SpellE>My</span> <span class=SpellE>Account</span>.
		Tenga presente que todas las referencias al &quot;sitio web&quot; en nuestra
		Política de privacidad también se consideran válidas para esta Aplicación
		Móvil, ya sea que se utilice en forma conjunta o independientemente del Sitio
		Web de la Compañía en (página web de DELPHOS). <o:p></o:p></span></p>

		<p class=MsoNormal style='margin-bottom:7.5pt;text-align:justify;line-height:
		normal;background:white'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
		mso-fareast-font-family:"Times New Roman";mso-fareast-language:ES-CO'>Los
		Usuarios deben suspender el uso de esta Aplicación Móvil inmediatamente si no
		están de acuerdo o no aceptan todos estos Términos y condiciones. La Compañía
		se reserva el derecho de eliminar o prohibir a cualquier Usuario la utilización
		de esta Aplicación Móvil a su sola discreción.<o:p></o:p></span></p>

		<p class=MsoNormal style='margin-top:15.0pt;margin-right:0cm;margin-bottom:
		7.5pt;margin-left:0cm;text-align:justify;line-height:normal;mso-outline-level:
		2;background:white'><span style='font-size:18.0pt;font-family:"interstate-Bold",serif;
		mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:Arial;
		mso-fareast-language:ES-CO'>Cuentas de usuario<o:p></o:p></span></p>

		<p class=MsoNormal style='margin-bottom:7.5pt;text-align:justify;line-height:
		normal;background:white'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
		mso-fareast-font-family:"Times New Roman";mso-fareast-language:ES-CO'>La
		Compañía puede, a su sola discreción, brindar acceso a los Usuarios a porciones
		restringidas de esta Aplicación Móvil, incluyendo, entre otros, una o más
		cuentas de Usuario donde pueda brindarse y/u obtenerse la información y
		servicios específicos de los clientes.<o:p></o:p></span></p>

		<p class=MsoNormal style='margin-bottom:7.5pt;text-align:justify;line-height:
		normal;background:white'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
		mso-fareast-font-family:"Times New Roman";mso-fareast-language:ES-CO'>Los
		Usuarios que entren a dichas ubicaciones podrían estar sujetos a Términos y
		Condiciones adicionales según se especifique en relación a los servicios
		proporcionados. Los Usuarios con cuentas de servicio son responsables
		exclusivos de preservar la confidencialidad de toda información de acceso, la
		información de la cuenta del Usuario y todas las acciones u omisiones
		vinculadas con dicha cuenta.<o:p></o:p></span></p>

		<p class=MsoNormal style='margin-top:15.0pt;margin-right:0cm;margin-bottom:
		7.5pt;margin-left:0cm;text-align:justify;line-height:normal;mso-outline-level:
		2;background:white'><span style='font-size:18.0pt;font-family:"interstate-Bold",serif;
		mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:Arial;
		mso-fareast-language:ES-CO'>Envío de contenidos<o:p></o:p></span></p>

		<p class=MsoNormal style='margin-bottom:7.5pt;text-align:justify;line-height:
		normal;background:white'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
		mso-fareast-font-family:"Times New Roman";mso-fareast-language:ES-CO'>En caso
		de que un Usuario envíe imágenes digitales u otro contenido, incluidas todas las
		fotografías, ilustraciones, gráficos y texto (en forma conjunta, “Materiales”)
		a la Compañía a través de la Aplicación Móvil, tendrán validez los siguientes
		términos:<o:p></o:p></span></p>

		<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
		margin-left:18.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;
		line-height:normal;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt;background:
		white'><![if !supportLists]><span style='font-size:10.0pt;mso-bidi-font-size:
		12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
		Symbol;mso-fareast-language:ES-CO'><span style='mso-list:Ignore'>·<span
			style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</span></span></span><![endif]><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
			mso-fareast-font-family:"Times New Roman";mso-fareast-language:ES-CO'>El
			Usuario solo podrá enviar a la Compañía, a través de la Aplicación Móvil, Materiales
			de los cuales posea todos los derechos de propiedad intelectual. Dicho de otro
			modo, si un Usuario envía una imagen digital a la Compañía, el Usuario debe
			poseer todos los derechos sobre dicha imagen o el Usuario debe tener la
			autorización de la persona propietaria de tales derechos. Los menores de edad
			no pueden enviar Materiales a la Compañía a través de la Aplicación Móvil.
			Asimismo, un Usuario no puede enviar ninguna información susceptible de
			identificación personal sobre un niño menor de 13 años de edad.<o:p></o:p></span></p>

			<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
			margin-left:18.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;
			line-height:normal;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt;background:
			white'><![if !supportLists]><span style='font-size:10.0pt;mso-bidi-font-size:
			12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
			Symbol;mso-fareast-language:ES-CO'><span style='mso-list:Ignore'>·<span
				style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</span></span></span><![endif]><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
				mso-fareast-font-family:"Times New Roman";mso-fareast-language:ES-CO'>Por el
				presente el Usuario cede a la Compañía los derechos y licencias mundiales, sin
				exclusividad, libres de regalías y a perpetuidad para (a) reproducir,
				distribuir, transmitir, representar y exhibir públicamente los Materiales,
				total o parcialmente, de cualquier manera y en cualquier medio para transmitir
				información, existente en la actualidad o que se cree en el futuro (“Medios”),
				(b) modificar, adaptar, traducir y crear trabajos derivados de los Materiales,
				total o parcialmente, de cualquier manera y en cualesquiera Medios, y (c)
				otorgar sublicencias por los derechos antedichos, total o parcialmente, a
				terceros con o sin canon de concesión.<o:p></o:p></span></p>

				<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
				margin-left:18.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;
				line-height:normal;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt;background:
				white'><![if !supportLists]><span style='font-size:10.0pt;mso-bidi-font-size:
				12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
				Symbol;mso-fareast-language:ES-CO'><span style='mso-list:Ignore'>·<span
					style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</span></span></span><![endif]><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
					mso-fareast-font-family:"Times New Roman";mso-fareast-language:ES-CO'>Por el
					presente el Usuario otorga a la Compañía y sus <span class=SpellE>sublicenciatarios</span>
					una licencia sin exclusividad, mundial y libre de regalías para usar todas las
					marcas registradas, nombres comerciales y los nombres e imágenes de todo
					individuo que aparezca en los Materiales. El Usuario otorga a la Compañía y sus
					<span class=SpellE>sublicenciatarios</span> el derecho a usar el nombre que el
					Usuario envíe en relación con los Materiales.<o:p></o:p></span></p>

					<p class=MsoNormal style='margin-top:15.0pt;margin-right:0cm;margin-bottom:
					7.5pt;margin-left:0cm;text-align:justify;line-height:normal;mso-outline-level:
					2;background:white'><span style='font-size:18.0pt;font-family:"interstate-Bold",serif;
					mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:Arial;
					mso-fareast-language:ES-CO'>Responsabilidad limitada<o:p></o:p></span></p>

					<p class=MsoNormal style='margin-bottom:7.5pt;text-align:justify;line-height:
					normal;background:white'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
					mso-fareast-font-family:"Times New Roman";mso-fareast-language:ES-CO'>LOS
					TERCEROS, LA COMPAÑÍA Y SUS EMPRESAS MATRICES Y AFILIADAS, JUNTO CON LOS
					RESPECTIVOS DIRECTIVOS, DIRECTORES, PERSONAL, EMPLEADOS Y REPRESENTANTES (EN
					CONJUNTO REFERIDOS COMO LAS “PARTES EXENTAS”) NO SERÁN RESPONSABLES NI ESTARÁN
					SUJETOS A ACCIONES LEGALES, Y POR LA PRESENTE EL USUARIO RENUNCIA A TODO
					RECLAMO, DEMANDA, IMPUTACIÓN DE RESPONSABILIDADES, CAUSA LEGAL, QUERELLA,
					RECLAMACIÓN DE DAÑOS Y PERJUICIOS, POR RAZÓN DE, ENTRE OTROS, DAÑOS DIRECTOS,
					INDIRECTOS, ACCIDENTALES, INCIDENTALES, DERIVADOS, CIRCUNSTANCIALES,
					EXTRAORDINARIOS, ESPECIALES O PUNITIVOS DE CUALQUIER NATURALEZA CON RESPECTO A
					ESTA APLICACIÓN MÓVIL (INCLUYENDO LOS PRODUCTOS, SERVICIOS Y CONTENIDOS DE LAS
					PARTES EXENTAS), AÚN CUANDO LAS PARTES EXENTAS HUBIERAN SIDO ADVERTIDAS DE LA
					POSIBILIDAD DE DICHOS DAÑOS. EL ÚNICO RECURSO DE LOS USUARIOS ANTE TALES
					RECLAMOS, DEMANDAS, IMPUTACIÓN DE RESPONSABILIDADES, CAUSAS LEGALES, QUERELLAS
					O RECLAMOS DE DAÑOS Y PERJUICIOS ES PONER FIN AL USO DE ESTA APLICACIÓN MÓVIL.<o:p></o:p></span></p>

					<p class=MsoNormal style='margin-top:15.0pt;margin-right:0cm;margin-bottom:
					7.5pt;margin-left:0cm;text-align:justify;line-height:normal;mso-outline-level:
					2;background:white'><span style='font-size:18.0pt;font-family:"interstate-Bold",serif;
					mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:Arial;
					mso-fareast-language:ES-CO'>Privacidad<o:p></o:p></span></p>

					<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
					justify;line-height:normal;background:white'><span style='font-size:12.0pt;
					font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
					mso-fareast-language:ES-CO'>Nuestra política de privacidad en relación a
					cualquier información obtenida por la Compañía a través de esta Aplicación
					Móvil puede consultarse en la sección&nbsp;<a
					href="https://www.socalgas.com/es/privacy-policy"><span style='color:windowtext;
					text-decoration:none;text-underline:none'>Política de privacidad</span></a>&nbsp;del
					Sitio Web de la Compañía. Pueden tener validez algunas reglas adicionales en
					materia de privacidad según se establece en las funciones de esta Aplicación
					Móvil restringidas para servicios específicos del Usuario.<o:p></o:p></span></p>

					<p class=MsoNormal style='margin-bottom:7.5pt;text-align:justify;line-height:
					normal;background:white'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
					mso-fareast-font-family:"Times New Roman";mso-fareast-language:ES-CO'>.<o:p></o:p></span></p>

					<p class=MsoNormal style='margin-top:15.0pt;margin-right:0cm;margin-bottom:
					7.5pt;margin-left:0cm;text-align:justify;line-height:normal;mso-outline-level:
					2;background:white'><span style='font-size:18.0pt;font-family:"interstate-Bold",serif;
					mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:Arial;
					mso-fareast-language:ES-CO'>Patrocinio<o:p></o:p></span></p>

					<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
					justify;line-height:normal;background:white'><span style='font-size:12.0pt;
					font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
					mso-fareast-language:ES-CO'>La Compañía no recomienda, patrocina ni promociona
					negocios, servicios o productos de terceros, a excepción de manifestaciones de
					recomendación o patrocinio que la Compañía realice de manera expresa, si las
					hubiere, en esta Aplicación Móvil. Si esta Aplicación Móvil proporciona
					información sobre terceros o proporciona contenido de terceros, e incluso
					enlaces a sitios web de terceros, la Compañía no será responsable de ningún
					daño o perjuicio relacionado con cualquier información de terceros, aunque ésta
					contenga errores o equivocaciones. Asimismo, la Compañía no será responsable ni
					estará sujeta a acción legal por los servicios o productos de terceros.
					Consulte también la sección en materia de enlaces de nuestra&nbsp;<a
					href="https://www.socalgas.com/es/privacy-policy"><span style='color:windowtext;
					text-decoration:none;text-underline:none'>Política de privacidad</span></a>,
					incluido el descargo de responsabilidad.<o:p></o:p></span></p>

					<p class=MsoNormal style='margin-top:15.0pt;margin-right:0cm;margin-bottom:
					7.5pt;margin-left:0cm;text-align:justify;line-height:normal;mso-outline-level:
					2;background:white'><span style='font-size:18.0pt;font-family:"interstate-Bold",serif;
					mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:Arial;
					mso-fareast-language:ES-CO'>Indemnización<o:p></o:p></span></p>

					<p class=MsoNormal style='margin-bottom:7.5pt;text-align:justify;line-height:
					normal;background:white'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
					mso-fareast-font-family:"Times New Roman";mso-fareast-language:ES-CO'>Los
					Usuarios liberarán de toda responsabilidad y exonerarán a las Partes Exentas de
					todo reclamo, demanda, responsabilidad civil, causa legal, querella o daños y
					perjuicios (incluidos los honorarios y los gastos razonables de abogados) que
					surjan como consecuencia del uso que dichos Usuarios hagan de la Aplicación
					Móvil (incluidos nuestros productos, servicios y Contenido), incluyendo, entre
					otros, la información, contenido o entrega incorrectos de la Aplicación Móvil,
					o de los productos y servicios de la Compañía o de terceros. La Compañía se
					reserva el derecho, por cuenta propia, de asumir la defensa y el control
					exclusivos de cualquier asunto sujeto a exoneración por parte de los Usuarios,
					pero el hacerlo no exime a los Usuarios de sus obligaciones de exoneración.<o:p></o:p></span></p>

					<p class=MsoNormal style='margin-top:15.0pt;margin-right:0cm;margin-bottom:
					7.5pt;margin-left:0cm;text-align:justify;line-height:normal;mso-outline-level:
					2;background:white'><span style='font-size:18.0pt;font-family:"interstate-Bold",serif;
					mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:Arial;
					mso-fareast-language:ES-CO'>Resolución de disputas<o:p></o:p></span></p>

					<p class=MsoNormal style='margin-top:15.0pt;margin-right:0cm;margin-bottom:
					7.5pt;margin-left:0cm;text-align:justify;line-height:normal;mso-outline-level:
					3;background:white'><span style='font-size:15.0pt;font-family:"interstate-Bold",serif;
					mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:Arial;
					mso-fareast-language:ES-CO'>Acuerdo sobre el arbitraje de disputas<o:p></o:p></span></p>

					<p class=MsoNormal style='margin-bottom:7.5pt;text-align:justify;line-height:
					normal;background:white'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
					mso-fareast-font-family:"Times New Roman";mso-fareast-language:ES-CO'>MEDIANTE
					EL USO, ACCESO, DESCARGA, INSTALACIÓN, OBTENCIÓN O PROVISIÓN DE INFORMACIÓN
					DESDE Y HACIA ESTA APLICACIÓN MÓVIL, LOS USUARIOS ACUERDAN EXPRESAMENTE QUE
					TODO RECLAMO, DISPUTA O CONTROVERSIA DE ÍNDOLE LEGAL ENTRE LOS USUARIOS Y LA
					COMPAÑÍA QUE SURJAN SURGIDOS O ESTÉN VINCULADOS DE CUALQUIER MODO CON LA
					APLICACIÓN MÓVIL, INCLUIDAS LAS CONTROVERSIAS VINCULADAS CON LA APLICABILIDAD,
					ALCANCE O VALIDEZ DE CUALQUIERA DE LAS CLÁUSULAS DE ESTOS TÉRMINOS Y
					CONDICIONES O DE NUESTRA POLÍTICA DE PRIVACIDAD (EN FORMA CONJUNTA, “DISPUTAS”)
					SERÁN RESUELTAS DE ACUERDO CON LOS PROCEDIMIENTOS AQUÍ ESTABLECIDOS. CUALQUIERA
					DE LAS PARTES PUEDE PRESENTAR UN RECLAMO CONTRA LA COMPAÑÍA HACIENDO CLIC AQUÍ.
					EN CASO DE QUE EL RECLAMO NO SE RESUELVA INFORMALMENTE, EL USUARIO ACEPTA POR
					LA PRESENTE RESOLVER TODAS LAS DISPUTAS MEDIANTE ARBITRAJE VINCULANTE
					CONFIDENCIAL, SALVO POR LA ÚNICA EXCEPCIÓN QUE SE ESTABLECE MÁS ADELANTE. TODAS
				LAS DISPUTAS SERÁN RESUELTAS POR UN ÁRBITRO, QUE SERÁ UN </span><span
				style='font-size:12.0pt;font-family:"Arial",sans-serif;background:white'>ÁRBITRO
			NEUTRAL DESIGNADO POR ACUERDO DE AMBAS PARTES DE LA DISPUTA.&nbsp;</span><span
			style='font-size:12.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
			"Times New Roman";mso-fareast-language:ES-CO'><o:p></o:p></span></p>

			<p class=MsoNormal style='margin-top:15.0pt;margin-right:0cm;margin-bottom:
			7.5pt;margin-left:0cm;text-align:justify;line-height:normal;mso-outline-level:
			3;background:white'><span style='font-size:15.0pt;font-family:"interstate-Bold",serif;
			mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
			mso-fareast-language:ES-CO'>Renuncia a arbitraje colectivo<o:p></o:p></span></p>

			<p class=MsoNormal style='margin-bottom:7.5pt;text-align:justify;line-height:
			normal;background:white'><span style='font-size:10.5pt;font-family:"Arial",sans-serif;
			mso-fareast-font-family:"Times New Roman";mso-fareast-language:ES-CO'>En la
			medida que la plena aplicación de las leyes lo permita, todas las Disputas se
			resolverán por arbitraje vinculante confidencial en forma individual según la
			capacidad individual de cada parte, y no como parte integrante de una supuesta
			demanda colectiva de ninguna acción legal colectiva. El árbitro no tiene la
			capacidad legal de resolver más que el reclamo de una sola persona, y de
			ninguna manera puede dirimir ninguna forma de acción legal representativa o
			colectiva. Los Usuarios aceptan expresamente que ninguna otra Disputa será
			unificada ni anexada a su Disputa, ya sea mediante acciones legales de
			arbitraje colectivo u otros procedimientos. Mediante el uso, acceso, descarga,
			instalación, obtención o provisión de información desde o hacia esta Aplicación
			Móvil, los Usuarios aceptan que renuncian en forma voluntaria y deliberada a
			cualquier derecho a participar como representante o miembro de cualquier grupo
			o colectivo de demandantes de cualquier Disputa.<o:p></o:p></span></p>

			<p class=MsoNormal style='margin-top:15.0pt;margin-right:0cm;margin-bottom:
			7.5pt;margin-left:0cm;text-align:justify;line-height:normal;mso-outline-level:
			3;background:white'><span style='font-size:15.0pt;font-family:"interstate-Bold",serif;
			mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
			mso-fareast-language:ES-CO'>Cumplimiento de la decisión del arbitraje<o:p></o:p></span></p>

			<p class=MsoNormal style='margin-bottom:7.5pt;text-align:justify;line-height:
			normal;background:white'><span style='font-size:10.5pt;font-family:"Arial",sans-serif;
			mso-fareast-font-family:"Times New Roman";mso-fareast-language:ES-CO'>La
			decisión del árbitro será final y vinculante para todas las partes sujetas a
			estos Términos y condiciones, y puede ser aplicada como precedente en cualquier
			juzgado de jurisdicción competente.<o:p></o:p></span></p>

			<p class=MsoNormal style='margin-top:15.0pt;margin-right:0cm;margin-bottom:
			7.5pt;margin-left:0cm;text-align:justify;line-height:normal;mso-outline-level:
			2;background:white'><span style='font-size:18.0pt;font-family:"interstate-Bold",serif;
			mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
			mso-fareast-language:ES-CO'>Divisibilidad<o:p></o:p></span></p>

			<p class=MsoNormal style='margin-bottom:7.5pt;text-align:justify;line-height:
			normal;background:white'><span style='font-size:10.5pt;font-family:"Arial",sans-serif;
			mso-fareast-font-family:"Times New Roman";mso-fareast-language:ES-CO'>Si
			cualquier cláusula de estos Términos y condiciones resultara inválida, nula o
			inaplicable, las cláusulas restantes conservarán de todos modos su total
			validez y la cláusula inválida, nula o inaplicable será considerada como
			modificada de modo tal que sea válida y aplicable hasta el máximo alcance
			permitido por la ley.<o:p></o:p></span></p>

			<p class=MsoNormal style='margin-top:15.0pt;margin-right:0cm;margin-bottom:
			7.5pt;margin-left:0cm;text-align:justify;line-height:normal;mso-outline-level:
			2;background:white'><span style='font-size:18.0pt;font-family:"interstate-Bold",serif;
			mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
			mso-fareast-language:ES-CO'>Idioma prevaleciente<o:p></o:p></span></p>

			<p class=MsoNormal style='margin-bottom:7.5pt;text-align:justify;line-height:
			normal;background:white'><span style='font-size:10.5pt;font-family:"Arial",sans-serif;
			mso-fareast-font-family:"Times New Roman";mso-fareast-language:ES-CO'>En el
			caso de que existiera alguna inconsistencia, ambigüedad o conflicto entre la
			versión en español de estos Términos y condiciones y las traducidas a otros
			idiomas, la versión en español prevalecerá sobre el resto.<o:p></o:p></span></p>

			<p class=MsoNormal style='margin-top:15.0pt;margin-right:0cm;margin-bottom:
			7.5pt;margin-left:0cm;text-align:justify;line-height:normal;mso-outline-level:
			2;background:white'><span style='font-size:18.0pt;font-family:"interstate-Bold",serif;
			mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
			mso-fareast-language:ES-CO'>Preguntas y comentarios<o:p></o:p></span></p>

			<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
			justify;line-height:normal;background:white'><span style='font-size:10.5pt;
			font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
			mso-fareast-language:ES-CO'>Los Usuarios que tengan alguna pregunta o duda
			sobre los Términos y condiciones para Aplicaciones Móviles pueden ponerse en
			contacto con el <span class=SpellE>webmaster</span> en relación con esta
			Aplicación Móvil escribiendo al siguiente correo electrónico:<br>
			<u><a href="mailto:webmaster@socalgas.com"><span style='color:windowtext'>(Dirección
			de DELPHOS)</span></a></u><br>
			Los Usuarios también pueden enviar sus preguntas y comentarios por correo
			postal:<o:p></o:p></span></p>

			<p class=MsoNormal style='text-align:justify'><span style='font-size:10.5pt;
			line-height:107%;font-family:"Arial",sans-serif;mso-fareast-font-family:"Times New Roman";
			mso-fareast-language:ES-CO'>________________________________________________________________________________________________________________________</span></p>


		</div>




	@include('include.index.footer')


	<!-- script -->
    <script src="{{ asset('js/jquery-3.3.1.min.js' ) }}"></script>
	<script src="{{ asset('js/materialize.min.js'  ) }}"></script>
</body>
</html>