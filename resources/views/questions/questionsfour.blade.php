@extends('layouts.app_bootstrap')

@section('title', 'Pregunta #1')

@section('extra_style')
    <link rel="stylesheet" href="{{ asset('css/community_questions/questions2.css') }}" />
@endsection

@section('content')

    <style>
            .tamaño{
               width: 150px;
            }
            .tamano{
                width: 90px;
                height: 80px;
            }
            

            
    </style>

    <nav class="navbar navbar-inverse bg-inverse sticky-top">
        <div class="d-flex justify-content-between">
            <a alt="Logo Delphos" class="navbar-brand" href="/">
                <img  class="logo-navbar" src="{{ asset('images/img_logos_delphos/logo.png') }}" alt="logo delphos" width="34px" height="20px" />
            </a>
           
            <a href="/community" class="btn btn-outline-success btn-xs ml-auto">
                    Omitir
            </a>
            

        <!-- <button type="button" class="btn btn-outline-success btn-xs ml-auto" onclick="return verificar();">Siguiente</button> -->
        </div>
    </nav>


    <br>

    <h2 class="container">Resultados de tu orientacion</h2>
    <br><br>

    <div class="container text-center">
        <table class="table table table-hover">
        <thead class="thead-dark ">
            <tr>
            <td><img  src="{{ asset('images/img_questions/imgQ3/programa.png') }}"/></td>
            <td><img  src="{{ asset('images/img_questions/imgQ3/universidad.png') }}"/></td>
            <td><img  src="{{ asset('images/img_questions/imgQ3/ubicacion.png') }}"/></td>
            </tr>
        </thead>
        <tbody>
            @foreach($programas as $programa)
                <tr>
                    <td><div class="container tamaño"><a href="/programs/{{$programa->id}}">{{$programa->name}}</a></div></td>
                    <td >{{$programa->institution->user->name}}</td>
                    <td >{{$programa->institution->town->name}}</td>
                </tr>
            @endforeach
        </tbody>
        </table>
    </div>

    
    <!-- Footer -->
    <footer class="py-5 bg-black">
            
        <div class="container">
            <p class="m-0 text-center text-black small">
                <b>
                    DELPHOS 2018
                </b>
                <i aria-hidden="true" class="fa fa-copyright">
                </i>
                · © 2018 Copyright Delphos Académico, All rights reserved.
            </p>
        </div>
        
    </footer>

    <style>
        img{
            height: 50px;
        }
    </style>

@endsection