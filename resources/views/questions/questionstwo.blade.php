@extends('layouts.app_bootstrap')

@section('title', 'Pregunta #1')

@section('extra_style')
    <link rel="stylesheet" href="{{ asset('css/community_questions/questions2.css') }}" />
@endsection


  
@section('content')

    <nav class="navbar navbar-inverse bg-inverse sticky-top">
        <div class="d-flex justify-content-between">
            <a alt="Logo Delphos" class="navbar-brand" href="/">
                <img  class="logo-navbar" src="{{ asset('images/img_logos_delphos/logo.png') }}"  alt="logo delphos" width="34px" height="20px"/>
            </a>

            <button type="button" class="btn btn-outline-success btn-xs ml-auto" onclick="return verificar();">Siguiente</button>
        </div>
    </nav>


    <br>
    <h2 class="text-center">¿Cúales son tus materias favoritas?</h2>
    <h6 class="text-center"> Usted puede elegir tres (3) opción</h6>
    <br>

    
    <div class="container">
        <div class="row justify-content-center">
            <ul>
                <li>
                    <input class="chk" id="cb1" name="Historia" type="checkbox" />
                    <label for="cb1">
                        <img  src="{{ asset('images/img_questions/imgQ2/Historia.png') }}" />
                        <h6 class="text-center">Historia</h6>
                    </label>
                </li>
                <li>
                    <input class="chk" id="cb2" name="Politica" type="checkbox"/>
                    <label for="cb2">
                        <img  src="{{ asset('images/img_questions/imgQ2/Politica.png') }}" />
                        <h6 class="text-center">Politica</h6>
                    </label>
                </li>
                <li>
                    <input class="chk" id="cb3" name="Naturaleza" type="checkbox"/>
                    <label for="cb3">
                        <img  src="{{ asset('images/img_questions/imgQ2/naturaleza.png') }}" />
                        <h6 class="text-center">Naturaleza</h6>
                    </label>
                </li>
                <li>
                    <input class="chk" id="cb4" name="Geografia" type="checkbox"/>
                    <label for="cb4">
                        <img  src="{{ asset('images/img_questions/imgQ2/Geografia.png') }}" />
                        <h6 class="text-center">Geografia</h6>
                    </label>
                </li>
                <li>
                    <input class="chk" id="cb5" name="Música" type="checkbox"/>
                    <label for="cb5">
                        <img  src="{{ asset('images/img_questions/imgQ2/Musica.png') }}" />
                        <h6 class="text-center">Música</h6>
                    </label>
                </li>
                <li>
                    <input class="chk" id="cb6" name="Economía" type="checkbox"/>
                    <label for="cb6">
                        <img  src="{{ asset('images/img_questions/imgQ2/Economia.png') }}" />
                        <h6 class="text-center">Economía</h6>
                    </label>
                </li>
                <br>
            </ul>

            <ul>
                <li>
                    <input class="chk" id="cb7" name="Deporte" type="checkbox"/>
                    <label for="cb7">
                        <img  src="{{ asset('images/img_questions/imgQ2/Deporte.png') }}" />
                        <h6 class="text-center">Deporte</h6>
                    </label>
                </li>
                <li>
                    <input class="chk" id="cb8" name="Física" type="checkbox" value="Física" />
                    <label for="cb8">
                        <img  src="{{ asset('images/img_questions/imgQ2/Fisica.png') }}" />
                        <h6 class="text-center">Física</h6>
                    </label>
                </li>
                <li>
                    <input class="chk" id="cb9" name="Química" type="checkbox"/>
                    <label for="cb9">
                        <img  src="{{ asset('images/img_questions/imgQ2/Quimica.png') }}" />
                        <h6 class="text-center">Química</h6>
                    </label>
                </li>
                <li>
                    <input class="chk" id="cb10" name="Matemática"  type="checkbox"/>
                    <label for="cb10">
                        <img  src="{{ asset('images/img_questions/imgQ2/Matematica.png') }}" />
                        <h6 class="text-center">Matemática</h6>
                    </label>
                </li>
                <li>
                    <input class="chk" id="cb11" name="Artística"  type="checkbox"/>
                    <label for="cb11">
                        <img  src="{{ asset('images/img_questions/imgQ2/Artistica.png') }}" />
                        <h6 class="text-center">Artística</h6>
                    </label>
                </li>
                <li>
                    <input class="chk" id="cb12" name="Biología"  type="checkbox"/>
                    <label for="cb12">
                        <img  src="{{ asset('images/img_questions/imgQ2/Biologia.png') }}" />
                        <h6 class="text-center">Biología</h6>
                    </label>
                </li>
            </ul>

            <ul>
                <li>
                    <input class="chk" id="cb13" name="Ética" type="checkbox"/>
                    <label for="cb13">
                        <img  src="{{ asset('images/img_questions/imgQ2/Etica.png') }}" />
                        <h6 class="text-center">Ética</h6>
                    </label>
                </li>
                <li>
                    <input class="chk" id="cb14" name="Humanidades" type="checkbox"/>
                    <label for="cb14">
                        <img  src="{{ asset('images/img_questions/imgQ2/Humanidades.png') }}" />
                        <h6 class="text-center">Humanidades</h6>
                    </label>
                </li>
                <li>
                    <input class="chk" id="cb15" name="Religión" type="checkbox"/>
                    <label for="cb15">
                        <img  src="{{ asset('images/img_questions/imgQ2/Religion.png') }}" />
                        <h6 class="text-center">Religión</h6>
                    </label>
                </li>
                <li>
                    <input class="chk" id="cb16" name="Castellano" type="checkbox"/>
                    <label for="cb16">
                        <img  src="{{ asset('images/img_questions/imgQ2/Castellano.png') }}" />
                        <h6 class="text-center">Castellano</h6>
                    </label>
                </li>
                <li>
                    <input class="chk" id="cb17" name="Inglés" type="checkbox"/>
                    <label for="cb17">
                        <img  src="{{ asset('images/img_questions/imgQ2/Ingles.png') }}" />
                        <h6 class="text-center">Inglés</h6>
                    </label>
                </li>
                <li>
                    <input class="chk" id="cb18" name="Informática" type="checkbox"/>
                    <label for="cb18">
                        <img  src="{{ asset('images/img_questions/imgQ2/Informatica.png') }}" />
                        <h6 class="text-center">Informática</h6>
                    </label>
                </li>
            </ul>
        </div>

    </div>


    <!-- Footer -->
    <footer class="py-5 bg-black">
        <div class="container">
            <p class="m-0 text-center text-black small">
                <b>
                    DELPHOS 2018
                </b>
                <i aria-hidden="true" class="fa fa-copyright">
                </i>
                · © 2018 Copyright Delphos Académico, All rights reserved.
            </p>
        </div>
    </footer>



@endsection

@section('extra_scripts')
    <script src="{{ asset('js/questions/questions_two.js'   ) }}"></script>
@endsection