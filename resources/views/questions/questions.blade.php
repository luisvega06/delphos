@extends('layouts.app_bootstrap')

@section('title', 'Pregunta #1')

@section('extra_style')
    <link rel="stylesheet" href="{{ asset('css/community_questions/question1.css') }}" />
@endsection


  
@section('content')

    <nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse sticky-top">
        <a alt="Logo Delphos" class="navbar-brand" href="/">
            <img alt="logo delphos" class="d-inline-block align-top logo-navbar" src="{{ asset('images/img_logos_delphos/logo.png') }}"  alt="logo delphos" width="34px" height="20px" />
        </a>
    </nav>


    <br>
    <br>
    <h2 class="text-center">¿Cúal es tu nivel de estudios?</h2>
    <h6 class="text-center"> Usted puede elegir una (1) opción.</h6>
    <br>
    <div class="container">
        <div class="row justify-content-center">
            <form method="get">
                <ul>
                    <li>
                        <input class="chk" id="cb1" name="Estudiante" type="checkbox"/>
                        <label for="cb1">
                            <img src="{{ asset('images/img_questions/imgQ1/Estudiante.png') }}" />
                            <h6 class="text-center">Estudiante</h6>
                        </label>
                    </li>
                    <li>
                        <input class="chk" id="cb2" name="Bachiller" type="checkbox" />
                        <label for="cb2">
                            <img  src="{{ asset('images/img_questions/imgQ1/Bachiller.png') }}" />
                            <h6 class="text-center">Bachiller</h6>
                        </label>
                    </li>
                    <li>
                        <input class="chk" id="cb3" name="Tecnico" type="checkbox"/>
                        <label for="cb3">
                            <img  src="{{ asset('images/img_questions/imgQ1/Tecnico.png') }}"/>
                            <h6 class="text-center">Tecnico</h6>
                        </label>
                    </li>
                    <li>
                        <input class="chk" id="cb4" name="Tecnologo" type="checkbox" />
                        <label for="cb4">
                            <img  src="{{ asset('images/img_questions/imgQ1/Tecnologo.png') }}"/>
                            <h6 class="text-center">Tecnologo</h6>
                        </label>
                    </li>
                    <br>



                </ul>
            </form>
        </div>


        <div class="row justify-content-center">
            <form method="get">
                <ul>
                    <li>
                        <input class="chk" id="cb5" name="Profesional" type="checkbox" />
                        <label for="cb5">
                            <img  src="{{ asset('images/img_questions/imgQ1/Profesional.png') }}"/>
                            <h6 class="text-center">Profesional</h6>
                        </label>
                    </li>
                    <li>
                        <input class="chk" id="cb6" name="Especialista" type="checkbox" />
                        <label for="cb6">
                            <img  src="{{ asset('images/img_questions/imgQ1/Especialista.png') }}"/>
                            <h6 class="text-center">Especialista</h6>
                        </label>
                    </li>
                    <li>
                        <input class="chk" id="cb7" name="Master" type="checkbox" />
                        <label for="cb7">
                            <img  src="{{ asset('images/img_questions/imgQ1/Master.png') }}"/>
                            <h6 class="text-center">Master</h6>
                        </label>
                    </li>
                    <li>
                        <input class="chk" id="cb8" name="Doctorado" type="checkbox" />
                        <label for="cb8">
                            <img  src="{{ asset('images/img_questions/imgQ1/Doctorado.png') }}"/>
                            <h6 class="text-center">Doctorado</h6>
                        </label>
                    </li>
                </ul>
            </form>
        </div>
    </div>

    <!-- Footer -->
    <footer class="py-5 bg-black">
        <div class="container">
            <p class="m-0 text-center text-black small">
                <b>
                    DELPHOS 2018
                </b>
                <i aria-hidden="true" class="fa fa-copyright">
                </i>
                · © 2018 Copyright Delphos Académico, All rights reserved.
            </p>
        </div>
        <!-- /.container -->
    </footer>



@endsection

@section('extra_scripts')
    <script src="{{ asset('js/questions/questions_one.js'   ) }}"></script>
@endsection
