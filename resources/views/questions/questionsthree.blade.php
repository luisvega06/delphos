@extends('layouts.app_bootstrap')

@section('title', 'Pregunta #1')

@section('extra_style')
    <link rel="stylesheet" href="{{ asset('css/community_questions/questions2.css') }}" />
@endsection


  
@section('content')

    <nav class="navbar navbar-inverse bg-inverse sticky-top">
        <div class="d-flex justify-content-between">
            <a alt="Logo Delphos" class="navbar-brand" href="/">
                <img  class="logo-navbar" src="{{ asset('images/img_logos_delphos/logo.png') }}" alt="logo delphos" width="34px" height="20px" />
            </a>

        <button type="button" class="btn btn-outline-success btn-xs ml-auto" onclick="return verificar();">Siguiente</button>
        </div>
    </nav>


    <br>

    <h2 class="text-center">¿Como te ves en el futuro?</h2>
    <h6 class="text-center"> Usted puede elegir una (1) opción</h6>
    <br><br>

    <div class="container">
        
        <div class="row justify-content-center">
            <ul>
                <li>
                    <input class="chk" id="cb17" type="checkbox" name="chk[]" value="1"/>
                    <label for="cb17">
                        <img  src="{{ asset('images/img_questions/imgQ3/Gestion Administrativo.png') }}"/>
                        <h6 class="text-center">Gestión</h6>
                        <h6 class="text-center">administrativo</h6>
                    </label>
                </li>

                <li>
                    <input class="chk" id="cb1" type="checkbox" name="chk[]" value="2"/>
                    <label for="cb1">
                        <img  src="{{ asset('images/img_questions/imgQ3/cientificoexperimental.png') }}"/>
                        <h6 class="text-center">Científico</h6>
                        <h6 class="text-center">experimental</h6>
                    </label>
                </li>
                <li>
                    <input class="chk" id="cb2" type="checkbox" name="chk[]" value="3"/>
                    <label for="cb2">
                        <img  src="{{ asset('images/img_questions/imgQ3/cientifico tecnico.png') }}"/>
                        <h6 class="text-center">Científico</h6>
                        <h6 class="text-center">tecnico</h6>

                    </label>
                </li>
                <li>
                    <input class="chk" id="cb4" type="checkbox" name="chk[]" value="4"/>
                    <label for="cb4">
                        <img  src="{{ asset('images/img_questions/imgQ3/cientifico aplicado.png') }}"/>
                        <h6 class="text-center">Rama de</h6>
                        <h6 class="text-center">la salud</h6>
                    </label>
                </li>
                
                
                
                <br>
            </ul>
        </div>


        <div class="row justify-content-center">
            <ul>
            <li>
                <input class="chk" id="cb5" type="checkbox" name="chk[]" value="5"/>
                <label for="cb5">
                    <img  src="{{ asset('images/img_questions/imgQ3/jutidico social.png') }}"/>
                    <h6 class="text-center">Jurídico</h6>
                    <h6 class="text-center">social</h6>
                </label>
            </li>
            <li>
                <input class="chk" id="cb6" type="checkbox" name="chk[]" value="6"/>
                <label for="cb6">
                    <img  src="{{ asset('images/img_questions/imgQ3/comunicacion publicidad.png') }}"/>
                    <h6 class="text-center">comunicacón</h6>
                    <h6 class="text-center">publicidad</h6>

                </label>
            </li>
            <li>
                <input class="chk" id="cb15" type="checkbox" name="chk[]" value="7"/>
                <label for="cb15">
                    <img  src="{{ asset('images/img_questions/imgQ3/Turismo hoteleria.png') }}"/>
                    <h6 class="text-center">Turismo</h6>
                    <h6 class="text-center">hotelería</h6>
                </label>
            </li>
            <li>
                <input class="chk" id="cb16" type="checkbox" name="chk[]" value="8"/>
                <label for="cb16">
                    <img  src="{{ asset('images/img_questions/imgQ3/Informatico TICS.png') }}"/>
                    <h6 class="text-center">Informático</h6>
                    <h6 class="text-center">TICS</h6>
                </label>
            </li>
            </ul>
        </div>

    </div>

    <!-- Footer -->
    <footer class="py-5 bg-black">
        <div class="container">
            <p class="m-0 text-center text-black small">
                <b>
                    DELPHOS 2018
                </b>
                <i aria-hidden="true" class="fa fa-copyright">
                </i>
                · © 2018 Copyright Delphos Académico, All rights reserved.
            </p>
        </div>
    </footer>



@endsection

@section('extra_scripts')
    <script type="text/javascript">
        //<![CDATA[
        function verificar(){
            var suma = 0;
            var los_cboxes = document.getElementsByName('chk[]');
            var select;
            for (var i = 0, j = los_cboxes.length; i < j; i++) {
                if(los_cboxes[i].checked == true){
                    select = los_cboxes[i].value;
                    suma++;
                }
            }
            if(suma == 0){
                swal ( "Oops" ,  'Debe seleccionar por lo menos [1] opción.' ,  "error" );
                return false;
            }else{
                if(suma > 2){
                        swal ( "Oops" ,  'Debe seleccionar maximo 2 opciones.' ,  "error" );
                        return false;
                    }else {
                        location.href ="/question-four/"+select;
                    }
                }
            }
            //]]>
    </script>
@endsection
