@extends('layouts.app')

@section('title', 'Alianzas')

@section('style_body', '')


@section('content')

	@include('include.navbar.default')
	@include('include.sidenav.default')


	
	<div class="row container center">
		<br><br>
		
		<h4><b>{{$user_insti->name}}</b></h4>
		<h5><i>Financiación</i></h5>
		<br>

		<div class="col s12 m12 l12 xl12">
			<div class="card">
				<div class="card-image">
					<img class="responsive-img materialboxed img-alliances z-depth-2 animated bounceInDown" src="{{ asset( $alliances->avatar ) }}" >
					
					@auth
	              		@if ( $user->id ==  $user_insti->id )

							<a href="{{ url('/alianzas-estrategicas/' . $alliances->id . '/edit' )}}" class="btn-floating halfway-fab waves-effect waves-light blue animated bounceInDown">

								<i class="fas fa-cog"></i>

							</a>

						@endif
	            	@endauth

				</div>
				<div class="card-content">
					<textarea disabled="true" class="black-text parrafo-descripcion-style1 p10">{{$alliances->description}}</textarea>
				</div>
			</div>
		</div>
	</div>

	<br/><br/>
	@include('include.index.footer')

@endsection


@section('extra_scripts')

<script type="text/javascript">

	var mensaje = "";
		@if ($errors->any())

			//alert('mensaje: ' + {{ $errors->any() }});
			
			@foreach($errors->all() as $error)
				//console.log('{{ $error }}');

				mensaje += '{{ $error }}' + '\n';
			@endforeach

			//"error", "success" and "info"
			swal({
				icon  : 'error',
				title : 'Oups!',
				text  : mensaje,
				button: 'ok',
			});
			



		@endif


		@if( session()->has('message') )
		swal({
			icon  : 'success',
			title : 'Message',
			text  : '{{ session()->get('message') }}',
			button: 'Aceptar',
		});
		@endif
</script>

@endsection