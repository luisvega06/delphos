<div class="row">
	<div class="card">
		
		<br/>
		
		
		<div class="card-content">

			<h4><b>{{$user->name}}</b></h4>
			<h5>Aliados <i class="fas fa-hands-helping"></i> estratégicos</h5>

			<br/>
			<br/>

			<textarea id="description" name="description" placeholder="¿Tienes algo que contar de tus aliados estratégicos?" style="min-height: 150px; max-width: 100%; min-width: 100%;">{{ $alliances->description }}</textarea>

			<div class="card-image contenedor" style=" max-height: 200px; max-width: 200px; " >
				<br/>
				
				<img id="blah" class="responsive-img image" src="{{ asset($alliances->avatar ) }}"  style=" max-height: 160px; max-width: 200px; " />

				<div class="middle">
					<div class="text">
						<div class="inputFile">
							<input type="file" class="file" id="avatar" name="avatar" onchange="readURL(this);"/>
							<div class="fakeFile">
								<img src="{{ asset('images/img_default/upload.png') }}" width="100"/>
								
							</div>
						</div>
					</div>
				</div>
				<label>Agregar imagen</label>
				
			</div>

			<div class="card-content">
				<button class="waves-effect waves-light btn blue btn-radius" type="submit">
					Guardar cambios
				</button>
			</div>

		</div>
	</div>
</div>



@section('extra_scripts')
	<script type="text/javascript">

		function readURL(input) {
			
			var filePath = input.value;
			var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
			if(!allowedExtensions.exec(filePath)){
				
				swal({icon  : 'error',title : 'Oups!', text : 'Formatos permitidos: .jpeg/.jpg/.png/.gif',button: 'ok',});

				input.value = '';
				$('#blah').attr('src', "{{ asset('images/img_default/background.jpg') }}");

			}else{

				if (input.files && input.files[0]) {
					var reader = new FileReader();

					reader.onload = function (e) {
						$('#blah')
						.attr('src', e.target.result);
					};

					reader.readAsDataURL(input.files[0]);
				}
			}
		}



		var mensaje = "";
		@if ($errors->any())
			@foreach($errors->all() as $error)
				console.log( '{{ $error }}');
				mensaje += '{{ $error }}' + '\n';
			@endforeach

			//"error", "success" and "info"
			swal({
				icon  : 'error',
				title : 'Oups!',
				text  : mensaje,
				button: 'ok',
			});

		@endif
	</script>
@endsection