@extends('layouts.app')

@section('title', 'Edit Alianzas')

@section('style_body', 'grey lighten-2')

@section('content')

@include('include.navbar.default')
@include('include.sidenav.default')


<div class="container">
	<div class="container center ">
		<form action="/alianzas-estrategicas/{{ $alliances->id }}" method="POST" enctype="multipart/form-data">
			
			@method('PUT')
			@csrf

			@include('institutions.alianzas.form')

		</form>
	</div>
</div>



@include('include.index.footer')
@endsection