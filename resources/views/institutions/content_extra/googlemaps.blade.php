<!DOCTYPE html>
<html>
  <head>

    <link rel="shortcut icon" href="{{ asset('images/img_logos_delphos/lgdelphosnotext.png') }}"/>
    <title>Delphos Académico</title>
    <!-- Hoja de estilos inicial -->
    <meta charset="UTF-8">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta content="IE=edge" http-equiv="X-UA-Compatible"></meta>
    
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
    <link href="{{ asset('css/fontawesome-all.css') }}"  rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('css/materialize.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}" />

    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="cache-control" content="no-store" />
    <meta http-equiv="cache-control" content="must-revalidate" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    
    
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      .map-control {
        background-color: #fff;
        border: 1px solid #ccc;
        box-shadow: 0 2px 2px rgba(33, 33, 33, 0.4);
        font-family: 'Roboto','sans-serif';
        margin: 10px;
        /* Hide the control initially, to prevent it from appearing
           before the map loads. */
        display: none;
      }
      /* Display the control once it is inside the map. */
      #map .map-control { display: block; }

      .selector-control {
        font-size: 14px;
        line-height: 30px;
        padding-left: 5px;
        padding-right: 5px;
      }
    </style>

  </head>

  <body>
      
  @include('include.navbar.default')
  @include('include.sidenav.default')

  <div class="row center">

    <div class="col s12 m12 l12 xl12 teal lighten-3">

      <br>
      <img class="circle" src="{{ asset( $user->avatar) }}" width="150" height="150"/>
        
      <div class="mt15 mb30" style="line-height: 10px">
        <h4 class="black-text darken-1">
          
          <b>{{ $user->name }}</b>

        </h4>
        <p><b>Dirección: </b>{{ $institution->address . ', ' . $town->name . ' - ' . $state->name }}.</p>
        
      </div>

    </div>

  </div>

  {{-- Component Google Maps --}}

  <div id="style-selector-control"  class="map-control">
    <select id="style-selector" class="selector-control">
      <option value="default">Default</option>
      <option value="silver">Silver</option>
      <option value="night">Night mode</option>
      <option value="retro" selected="selected">Retro</option>
      <option value="hiding">Hide features</option>
    </select>
  </div>

  <div style="height: 0px;">
    <input type="text" id="avatar" name="avatar" style="visibility: hidden" value="{{ $user->avatar }}">
    <input type="text" id="lat"    name="lat"    style="visibility: hidden" value="{{ $institution->latitude }}">
    <input type="text" id="lng"    name="lng"    style="visibility: hidden" value="{{ $institution->longitude }}">
    <input type="text" id="name-insti"    name="name-insti"    style="visibility: hidden" value="{{ $user->name }}">

    <input type="text" id="avatar-user" name="avatar-user" style="visibility: hidden" value="{{ Auth::user()->avatar }}">
    <input type="text" id="name-user"    name="name-user"    style="visibility: hidden" value="{{ Auth::user()->name }}">
  </div>

  

  {{-- Google Maps --}}
  <div class="container" id="map"></div>

  {{-- Button --}}

  <div class="fixed-action-btn">
    <a class="btn-floating btn-large red" onclick="add_markers()">
      <i class="fas fa-location-arrow"></i>
    </a>
  </div>

  <br><br>
  
  @include('include.index.footer')

  <script>
      // Note: This example requires that you consent to location sharing when
      // prompted by your browser. If you see the error "The Geolocation service
      // failed.", it means you probably did not give permission for the browser to
      // locate you.
      var map, infoWindow;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -34.397, lng: 150.644},
          zoom: 6
        });
        infoWindow = new google.maps.InfoWindow;

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('Location found.');
            infoWindow.open(map);
            map.setCenter(pos);
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }
      }

      function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                              'Error: The Geolocation service failed.' :
                              'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);
      }
    </script>
    
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8VMaKPG3QnhPfTIA0vHPueKPz95aF5ew&callback=initMap">
    </script>

  <script src="{{ asset('js/maps/googlemaps.js' ) }}"></script>



  <script src="{{ asset('js/jquery-3.3.1.min.js' ) }}"></script>
  <script src="{{ asset('js/materialize.min.js'  ) }}"></script>
  <script src="{{ asset('js/sweetalert.min.js'   ) }}"></script>
  <script src="{{ asset('js/include_materialize/inicializadores.js'   ) }}"></script>



  </body>
</html>