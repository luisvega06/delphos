@extends('layouts.app')

@section('title', 'Followers')

@section('style_body', 'white')


@section('content')

	@include('include.navbar.institucion')
	
	<ul class="sidenav grey darken-3 collapsible" id="slide-out">
		@include('include.sidenav.institucion')
	</ul>

	

	<div class="p20">
		
		<br>
		<h5 class="center"><b>Historial de usuarios</b></h5>
		<br><br>
		
		@if (sizeof ($ListPersons) != 0)
			
				<table class="highlight">
					<thead>
						<tr>
							<th>No. </th>
							<th>Carrera</th>
							<th>Departamento</th>
							<th>Municipio</th>
							<th>Nombre</th>
							<th>Fecha Nac.</th>
							<th>Fecha Visita</th>
						</tr>
					</thead>

					<tbody>
						<?php 
							$index = 0;
						?>
						@foreach($ListPersons as $notification)
						<tr>
							<td> {{ $index += 1}} </td>
							<td> {{ $notification->carrera }} </td>
							<td> {{ $notification->state   }} </td>
							<td> {{ $notification->town    }} </td>
							<td> {{ $notification->name    }} </td>
							<td> {{ $notification->date    }} </td>
							<td> {{ $notification->visito  }} </td>
						</tr>
						
						@endforeach

					</tbody>
				</table>

				<br/>
				<div class="col s12 center">
					{{ $ListPersons->links('vendor.pagination.materialize') }}
				</div>
		@else
			<div class="center">
				<a href="/institutions" class="black-text waves-effect waves-light" style="opacity: 0.5; filter: alpha(opacity=50);">
					
					<br/><br/>
					<img class="responsive-img" src="{{ asset('images/img_default/nofavoritos.png') }}" style="max-height: 300px;" />
				</a>
			</div>

			<br/><br/><br/>
			
		@endif
	</div>
	
	
	<div class="fixed-action-btn">
		<a class="btn-floating btn-large red"  onClick="window.location.reload()">
			<i class="fas fa-sync"></i>
		</a>
	</div>


<br/><br/>
@include('include.index.footer')

@endsection


@section('extra_scripts')

	<script type="text/javascript">

			var mensaje = "";
			@if ($errors->any())
				@foreach($errors->all() as $error)
					mensaje += '{{ $error }}' + '\n';
				@endforeach

				//"error", "success" and "info"
				swal({
					icon  : 'error',
					title : 'Oups!',
					text  : mensaje,
					button: 'ok',
				});
			@endif


			@if( session()->has('message') )
				swal({
					icon  : 'success',
					title : 'Message',
					text  : '{{ session()->get('message') }}',
					button: 'Aceptar',
				});
			@endif

	</script>

	@include('include.pusher.chanel_followers')

@endsection