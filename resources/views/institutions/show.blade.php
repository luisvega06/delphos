@extends('layouts.app_no_footer')
@section('title', 'Institucion')

@section('extra_meta')
  @include('layouts.meta_shared.institucion')
@endsection


@section('content')
	@include('include.navbar.default_no_logo')

	<ul class="sidenav grey darken-3 collapsible" id="slide-out">
		@include('include.sidenav.institucion_public')
	</ul>


	<div class="row grey lighten-5">
		<div class="col s12 l3 xl3 left grey darken-3">
			<ul id="slide-out" class="sidenav sidenav-fixed grey darken-3 col l3 xl3 left collapsible " style="padding: 0px;left: 0px;">
				@include('include.sidenav.institucion_public')
			</ul>
		</div>

		<div class="col s12 m12 l9 xl9">

			<div class="row">

				{{-- INICIO: Banner --}}
				<div class="carousel carousel-slider" style="max-height: 250px;">
					<div class="carousel-fixed-item center white-text">
						
						<a class="waves-effect waves-blue btn btn-radius grey darken-3 capitalize" 
							href="{{ url('/presentacion/'.$institution->slug) }}">

								<b>Comunidad <i class="fas fa-external-link-alt white-text"></i></b>
						</a>

					</div>

					<a class="carousel-item">
						<img src="{{ asset($institution->avatar_cover) }}" style="max-height: 250px;">
					</a>

				</div>
				{{-- FIN: Banner --}}
			</div>


			<div class="row">
				@if (sizeof ($publications) == 0)
					
				<div class="col s12 animated bounceInUp">

					<a class="white-text">
						<div class="card blue-grey darken-1" style="padding-bottom: 10px; min-height: 170px;">
							<div class="card-image center">
								<br/>
								<i class="fas fa-exclamation-circle fa-4x white-text"></i>
							</div>
							<div class="card-content center">
								<span class="card-title"><b><b>DELPHOS ACADEMICO</b></b></span>
								<p>Sin contenido para mostrar.</p>
							</div>
						</div>
					</a>
				</div>

				@else

					@foreach($publications as $publication)
					<div class=" col s12 m6 l4 xl4">

						<div class="card">
							<div class="card-image">
								<img class="responsive-img materialboxed" src="{{ asset($publication->picture) }}" style="max-height: 150px; min-height: 150px;">
							</div>
							
							<div class="card-content" style="max-height: 150px; min-height: 100px;">
								<span class="truncate" style="min-width: 270px;">
									<b>{{ $publication->title }}</b>
								</span>
								<p style="max-height: 30px; min-height: 30px;">{{ substr($publication->description, 0, 70)  }}...</p>
							</div>

							<div class="card-action right-align">
								<a class="btn grey " href="/publications/{{$publication->slug}}"> Ver </a>
							</div>
						</div>
					</div>
					@endforeach
					<br/>
					<div class="col s12 center">
						{{ $publications->links('vendor.pagination.materialize') }}
					</div>
				@endif
			</div>

  


			@include('include.index.footer')
		</div>
	</div>

@endsection


@section('extra_scripts')

	<script type="text/javascript">

		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#picture_preview')
					.attr('src', e.target.result);
				};

				reader.readAsDataURL(input.files[0]);
			}
		}


		
		var mensaje = "";
		@if ($errors->any())

			//alert('mensaje: ' + {{ $errors->any() }});
			
			@foreach($errors->all() as $error)
				//console.log('{{ $error }}');

				mensaje += '{{ $error }}' + '\n';
			@endforeach

			//"error", "success" and "info"
			swal({
				icon  : 'error',
				title : 'Oups!',
				text  : mensaje,
				button: 'ok',
			});
			



		@endif


		@if( session()->has('message') )
		swal({
			icon  : 'success',
			title : 'Message',
			text  : '{{ session()->get('message') }}',
			button: 'Aceptar',
		});
		@endif
		
	</script>


	<script type="text/javascript">
		
		$(document).ready(function(){
			$('#btn-ws').click(function(event){
				event.preventDefault();

				send_contact(); 

			});
		});

		function send_contact() {
			@auth
			
				var mensaje = '57{{$contact->number}}'
				mensaje += '?text=User: {{Auth::user()->name}}, Email: {{Auth::user()->email}}. Via *Delphos Académico*';

				window.open('https://wa.me/'+mensaje, '_blank');
			@endauth
			
		}

	</script>
	
@endsection

