
	<!-- Tipo de institución -->
	<div class="input-field col s12 m12 l12 xl12">
		<select id="select-typeinstitutions" name="Tipo" >
			<option value="0" disabled selected>Seleccionar
			</option>
			@foreach($typeinstitutions as $obj )
			<!-- <-- No Mover porque despues no detecta la tecla -->
<option value="{{ $obj->id }}" id="typeinstitution" name="typeinstitution" >{{$obj->name}}</option>
			@endforeach
		</select>
		<label>
			Tipo institución
		</label>
	</div>



	<div class="input-field col s12 m12 l12 xl12">
		<input id="name" name="name" type="text" class="validate" value="{{ old('name') }}">
		<label for="name">Nombre de la institución</label>
	</div>


	<div class="input-field col s12 m12 l12 xl12">
		<input id="email" name="email" type="email" class="validate" value="{{ old('email') }}">
		<label for="email">Correo electrónico</label>
	</div>




	<!-- Phone -->
	<div class="input-field col s12 m6 l6 xl6">
		<input name="number" id="number" type="text" class="validate" maxlength="11" onKeypress="if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;" value="{{ old('number') }}" placeholder="3XXXXXXXX">
		<label for="number">Business WhatsApp</label>
	</div>



	{{-- Username --}}
	<div class="input-field col s12 m6 l6 xl6">
		<input name="username" id="username" type="text" class="validate lowercase" minlength="10" maxlength="254" pattern="^(\w){4,30}$" value="{{ old('username') }}" >
		<label for="username">Username</label>
	</div>



	<div class="row">

		<div class="input-field col s12 m6 l6 xl6">
			<input id="password" name="password" type="password" class="validate" required="">
			<label for="password">Contraseña</label>
		</div>
		<div class="input-field col s12 m6 l6 xl6">
			<input id="password_confirmation" name="password_confirmation" type="password" class= "validate" required="">
			<label for="password_confirmation">Confirmar contraseña</label>
		</div>
	</div>


	{{-- Ubicacion --}}
	<div class="row" style="margin-top: -10px; padding: 0px; margin-bottom: -10px;">
		<div class="input-field col s12 m12 l6 xl6">
			<select id="select-project" name="Departamento" >
				<option value="0" disabled selected>Departamento
				</option>
{{-- No Mover porque despues no detecta la tecla --}}
				@foreach($states as $state )
<option value="{{ $state->id }}" id="op_state" name="op_state" >{{$state->name}}</option>
				@endforeach
			</select>
			<label>
				Departamento
			</label>
		</div>

		<div class="input-field col s12 m12 l6 xl6">
			<select id="select-level" name="Municipio" >
<option disabled="" selected="" value="0">Municipio</option>
			</select>
			<label>
				Municipio
			</label>
		</div>
	</div>


