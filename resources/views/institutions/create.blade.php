@extends('layouts.app')

@section('title', 'Crear cuenta')

@section('style_body', 'background_register')


@section('content')

@include('include.navbar.default')


<!-- 
https://laravel.com/docs/5.6/controllers
-->
<div style="padding-right: 5%; padding-left: 5%;">


	<form class="form-group" action="/institutions" method="POST">
		@csrf

		<br/><br/>

		<div class="row">
			<!-- Imagen left -->
			<div class="col s12 m12 l6 xl6 hide-on-med-and-down" style="padding-right: 0px; padding-left: 0px;">

				<img class="responsive-img" style="height: 663.219px; min-width: 600px; max-width: 612.453px;" src="{{ asset('images/img_complement/loginuniv.jpg') }}" >
			</div>


			<!-- form right-->
			<div class="col s12 m12 l6 xl6 white" style="padding: 20px; min-height:663.219px;">

				<div class="center">
					<div class="col s12 center-align ">
						<h5>
							<b>
								Crear Cuenta <b>DELPHOS</b>
							</b>
						</h5>
						<br/><br/>

					</div>

					@include('institutions.form')
					
					<button class="col s12 waves-effect waves-light btn btn-radius blue lighten-1 mt10" type="submit" id="btn-enviar" >
						Registrar
					</button>
				</div>


				
			</div>

		</div>
		


	</form>
	
</div>



<br/><br/>
@include('include.index.footer')

@endsection


@section('extra_scripts')

	<!-- Script listar municipios-->
	<script src="{{ asset('js/list_municipios.js') }}"></script>

	<script type="text/javascript">
		$(document).ready(function(){
			$('.fixed-action-btn').floatingActionButton(); 
		});

		var n = 0;
		window.setInterval(function(){

			n++;
		  //console.log("Valor de n: " + n);
		  if (n == 30) {
		  	$( "#btn-enviar" ).addClass( "pulse" );
		  } else {

		  	if (n == 40) {
		  		n = 0;
		  		$( "#btn-enviar" ).removeClass( "pulse" )
		  	}

		  }

		},1000);



		var mensaje = "";
			@if ($errors->any())

				//alert('mensaje: ' + {{ $errors->any() }});
				
				@foreach($errors->all() as $error)
					//console.log('{{ $error }}');

					mensaje += '{{ $error }}' + '\n';
				@endforeach

				//"error", "success" and "info"
				swal({
					icon  : 'error',
					title : 'Oups!',
					text  : mensaje,
					button: 'ok',
				});
				



			@endif


			@if( session()->has('message') )
			swal({
				icon  : 'success',
				title : 'Message',
				text  : '{{ session()->get('message') }}',
				button: 'Aceptar',
			});
			@endif
	</script>

@endsection