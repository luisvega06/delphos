@extends('layouts.app')

@section('title', 'Actualizar Institution')

@section('style_body', 'grey')

@section('content')
<!-- 
https://laravel.com/docs/5.6/controllers
-->

@include('include.navbar.institucion')

<ul class="sidenav grey darken-3  collapsible" id="slide-out">
	@include('include.sidenav.institucion')
</ul>


<br/><br/>
<div class="container white hoverable">
	<div class="container center ">

		<br/><br/>
		<h4 class="title"><b>Actualización institución</b></h4>


		<br/><br/><br/><br/>

		<form class="" action="/institutions/{{$institution->slug}}" method="POST" enctype="multipart/form-data">

			@method('PUT')
			@csrf


			<div class="row animated bounceInRight">
				<div class="container">
					<div class="col s12 m12 l12 xl12 ">
						<p class="">
							<i class="fas fa-edit black-text"></i> 

							<b> Cambiar contraseña </b>
						</p>					
					</div>

					<div class="input-field col s12 m12 l12 xl12">
						<i class="fas fa-lock-open prefix blue-text"></i>
						<input id="password1" name="password1" type="password" class="validate" minlength="6">
						<label for="password1">Anterior contraseña</label>
					</div>
					<div class="input-field col s12 m12 l12 xl12">
						<i class="fas fa-lock prefix blue-text"></i>
						<input id="password2" name="password2" type="password" class="validate" inlength="6">
						<label for="password2">Nueva contraseña</label>
					</div>
				</div>
			</div>

			<br/>
			
			<hr class="style5" />
			
			<br/><br/>

			<div class="row animated bounceInLeft">

				<div class="col s12 m12 l12 xl12 ">
					<p class="">
						<b> Telefono de contacto </b>
					</p>					
					<br/>
				</div>

				<div class="input-field  col s12 m12 l12 xl12">
					
					<i class="material-icons prefix">phone</i>
					<input type="tel" id="phone" name="phone" class="validate" value="{{$phone->number}}" onKeypress="if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;" maxlength="11" />
					<label for="phone">Telephone</label>

				</div>
			</div>

			<br/><br/>
			<hr class="style5" />
			<br/><br/>

			<div class="row animated bounceInRight">

				<div class="col s12 m12 l12 xl12 ">
					<p class="">
						<b> Información complementaria </b>
					</p>					
					<br/>
				</div>


				<div class="file-field input-field col s12 m12 l6 xl6">

					<div class="col s12 m12 l12 xl12">
						<span><b>Foto de perfil</b></span>
					</div>

					<div class="col s12 m12 l12 xl12">
						<div class="btn-floating blue lighten-1">
							<i class="fas fa-user"></i>
							<input type="file" name="avatar">
						</div>
						<div class="file-path-wrapper">
							<input class="file-path" type="text" placeholder="foto de perfil" disabled="true">
						</div>
					</div>


				</div>

				<div class="file-field input-field col s12 m12 l6 xl6">


					<div class="col s12 m12 l12 xl12">
						<span><b>Foto de portada</b></span>
					</div>

					<div class="col s12 m12 l12 xl12">
						<div class="btn-floating blue lighten-1">
							<i class="far fa-image"></i>
							<input type="file" name="avatar_cover">
						</div>
						<div class="file-path-wrapper">
							<input class="file-path" type="text" placeholder="foto de portada" disabled="true">
						</div>
					</div>


				</div>
			</div>

			<div class="row animated bounceInLeft">
				<div class="input-field col s12">
					<input id="address" name="address" type="text" class="validate" placeholder="Av institución calle 1" value="{{$institution->address}}" maxlength="254" >
					<label for="address">Dirección</label>
				</div>

				<div class="input-field col s12">
					<input id="ulr_inscriptions" name="ulr_inscriptions" type="text" class="validate" value="{{$institution->ulr_inscriptions}}">
					<label for="ulr_inscriptions">Url Inscripciones <i class="fas fa-link"></i></label>
				</div>

				<div class="input-field col s12">
					<input id="URL_video_present" name="URL_video_present" type="text" class="validate" value="{{$institution->URL_video_present}}">
					<label for="URL_video_present">URL video Presentación <i class="fas fa-link"></i></label>
				</div>
			</div>


			<div class="row animated bounceInRight">
				<div class="input-field col s12">
					<textarea id="textarea2" name="description" class="materialize-textarea"data-length="3000" maxlength="3000">{{$institution->description}}</textarea>
					<label for="textarea2">Descripción</label>
				</div>
			</div>

			<br/>
			<hr class="style5" />
			<br/><br/>

			<div class="row animated bounceInLeft">
				<div class="col s12 m12 l12 xl12">
					<div class="col s12 m12 l12 xl12 ">
						<p class="left">
							<i class="fas fa-map-marker-alt blue-text"></i> 
							<b> Coordenadas geograficas </b>
						</p>					
					</div>

					<div class="input-field col s12 m12 l6 xl6">
						<input id="latitude" name="latitude" type="number" step="any" placeholder="0" class="validate black-text" value="{{$institution->latitude}}" readonly>
						<label for="latitude">Latitud</label>
					</div>

					<div class="input-field col s12 m12 l6 xl6">
						<input id="longitude" name="longitude" type="number" step="any" placeholder="0" class="validate black-text" value="{{$institution->longitude}}" readonly>
						<label for="longitude">Longitud</label>
					</div>

					<div class="col s12 m12 l12 xl12 center">
						<button class="waves-effect waves-light btn grey lighten-3 black-text btn-radius z-depth-1" type="button" onclick="inicio()">
							Saved location <i class="fas fa-map-marker-alt"></i>
						</button>
					</div>
				</div>
			</div>

			<br/>
			<hr class="style5" />
			<br/><br/>
			
			<div class="row animated bounceInRight">
				<div class="col s12 m12 l12 xl12 center">
					<button class="col s12 waves-effect waves-light btn btn-radius blue darken-3" type="submit" id="btn-enviar">
						<b>GUARDAR</b>
						<i class="fas fa-save"></i>
					</button>
				</div>
			</div>
			
		</form>	

		<br/>
		<hr class="style3" />
		<br/><br/>

		<span class="card-title">
			No es necesario llenar los campos si no desea reemplazar la información anteriormente suministrada. <b>el sistema tomara unicamente</b> los campos con información y actualizara la información <i><b>(omitiendo aquellos campos vacios y manteniendo la información)</b>.</i>
		</span>
		<br/><br/>
	</div>
</div>

<br/><br/>

	@include('include.index.footer')
@endsection

@section('extra_scripts')
<!-- Script listar municipios-->
<script src="{{ asset('js/list_municipios.js') }}"></script>


<script type="text/javascript">

	var mensaje = "";
		@if ($errors->any())
			
			@foreach($errors->all() as $error)
				//console.log('{{ $error }}');

				mensaje += '{{ $error }}' + '\n';
			@endforeach

			//"error", "success" and "info"
			swal({
				icon  : 'error',
				title : 'Oups!',
				text  : mensaje,
				button: 'ok',
			});
		@endif


		@if( session()->has('message') )
		swal({
			icon  : 'success',
			title : 'Message',
			text  : '{{ session()->get('message') }}',
			button: 'Aceptar',
		});
		@endif

	var n = 0;
	window.setInterval(function(){

		n++;
	  //console.log("Valor de n: " + n);
	  if (n == 20) {
	  	$( "#btn-enviar" ).addClass( "pulse" );
	  } else {

	  	if (n == 25) {
	  		n = 0;
	  		$( "#btn-enviar" ).removeClass( "pulse" )
	  	}

	  }

	},1000);


</script>


	<script>
		//window.addEventListener("load", inicio);

		function inicio() {
			navigator.geolocation.getCurrentPosition(alExito, alError);
		}

		function alExito(info) {
			latitude.value  = info.coords.latitude;
			longitude.value = info.coords.longitude;
		}

		function alError(info) {
			alert("Ocurrio un error");
		}

	</script>


	@include('include.pusher.chanel_followers')
@endsection


