@extends('layouts.app_no_footer')

@section('title', 'Institución')
@section('extra_meta')
	
	@include('layouts.meta_shared.institucion')

	<meta name="userId" content="{{Auth::user()->id}}">
	 <!-- Scripts -->
  <script>
    window.Laravel = {!! json_encode([
      'csrfToken' => csrf_token(),
    ]) !!};
  </script>

@endsection
@section('content')

	@include('include.navbar.institucion_v2')

	<ul class="sidenav grey darken-3 collapsible" id="slide-out">
		@include('include.sidenav.institucion')
	</ul>

	<div class="row grey lighten-5">
		<div class="col s12 l3 xl3 left grey darken-3">
			<ul id="slide-out" class="sidenav sidenav-fixed grey darken-3 col l3 xl3 left collapsible " style="padding: 0px;left: 0px;">
				@include('include.sidenav.institucion')
			</ul>
		</div>

		<div class="col s12 m12 l9 xl9">


			<div class="row">

				{{-- INICIO: Banner --}}
				<div class="carousel carousel-slider" style="max-height: 250px;">
					<div class="carousel-fixed-item center white-text">
						
						<a class="waves-effect waves-blue btn btn-radius grey darken-3 capitalize" 
							href="{{ url('/presentacion/'.$institution->slug) }}">

								<b>Comunidad <i class="fas fa-external-link-alt white-text"></i></b>
						</a>

					</div>
					<a class="carousel-item">
						<img src="{{ asset($institution->avatar_cover) }}" style="max-height: 250px;">
					</a>
				</div>
				{{-- FIN: Banner --}}
			</div>

			<div class="row  animated bounceInUp">
				@if (sizeof ($publications) == 0)
					
				<!-- Programs -->
				<div class="col s12">
					<a href="" class="white-text">
						<div class="card blue-grey darken-1" style="padding-bottom: 10px; min-height: 170px;">
							<div class="card-image center">
								<br/>
								<i class="fas fa-exclamation-circle fa-4x white-text"></i>
							</div>
							<div class="card-content center">
								<span class="card-title"><b><b>DELPHOS ACADÉMICO</b></b></span>
								<p>No tienes publicacíones para mostrar.</p>
							</div>
							<div class="card-action center">
								<a href="{{ url('/publications')}}" class="waves-effect waves-light ">Crear publicaciónes</a>
							</div>
						</div>
					</a>
				</div>

				@else

					@foreach($publications as $publication)
					<div class=" col s12 m6 l4 xl4">

						<div class="card">
							<div class="card-image">
								<img class="responsive-img" src="{{ asset($publication->picture) }}" style="max-height: 150px; min-height: 150px;">
							</div>


							<div class="card-content" style="max-height: 150px; min-height: 100px;">
								<span class="truncate" style="min-width: 270px;">
									<b>{{ $publication->title }}</b>
								</span>
								<p style="max-height: 30px; min-height: 30px;">{{ substr($publication->description, 0, 70)  }}...</p>
							</div>

							<div class="card-action right-align">
								<a class="btn grey " href="/publications/{{$publication->slug}}"> Ver </a>
							</div>
						</div>
					</div>
					@endforeach
					<br/>
					<div class="col s12 center">
						{{ $publications->links('vendor.pagination.materialize') }}
					</div>
				@endif
			</div>



			@include('include.index.footer')
		</div>
	</div>


	<div class="fixed-action-btn">
		<a class="btn-floating btn-large blue" href="{{ url('/publications')}}">
			<i class="large material-icons">add</i>
		</a>
	</div>
@endsection

@section('extra_scripts')

	<script type="text/javascript">

		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#picture_preview')
					.attr('src', e.target.result);
				};

				reader.readAsDataURL(input.files[0]);
			}
		}


		
		var mensaje = "";
		@if ($errors->any())

			@foreach($errors->all() as $error)
				mensaje += '{{ $error }}' + '\n';
			@endforeach

			//"error", "success" and "info"
			swal({
				icon  : 'error',
				title : 'Oups!',
				text  : mensaje,
				button: 'ok',
			});

		@endif


		@if( session()->has('info') )
			swal({
				icon  : 'info',
				title : 'Message',
				text  : '{{ session()->get('info') }}',
				button: 'Aceptar',
			});
		@endif


		@if( session()->has('message') )
			swal({
				icon  : 'success',
				title : 'Message',
				text  : '{{ session()->get('message') }}',
				button: 'Aceptar',
			});
		@endif
		
	</script>

	@include('include.pusher.chanel_followers')
	
@endsection

