<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFavouritesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('favourites', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            $table->increments('id');

            $table->boolean('confirmed')->default(true);

            $table->integer('institutions_id')->unsigned();
            $table->foreign('institutions_id')->references('id')->on('institutions');
            
            
            $table->integer('programs_id')->unsigned();
            $table->foreign('programs_id')->references('id')->on('programs');


            $table->integer('users_id')->unsigned();
            $table->foreign('users_id')->references('id')->on('users');


            $table->timestamps();




        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('favourites');
    }
}
