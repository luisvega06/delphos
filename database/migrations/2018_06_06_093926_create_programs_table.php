<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programs', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            $table->increments('id');
            $table->string('slug')->unique();
            $table->string('name');
            $table->text('description');
            $table->string('avatar');
            $table->double('price');

            $table->double('inscription_price');

            $table->integer('tiempo'); //4
            
            $table->integer('duracions_id')->unsigned();
            $table->foreign('duracions_id')->references('id')->on('duracions');

            $table->integer('credits');

            $table->string('codigo_snies');
            $table->string('last_accreditation');



            $table->text('info_intercambios');
            $table->text('info_extranjero');

            $table->text('url_pensum');
            
            $table->integer('level_of_educations_id')->unsigned();
            $table->foreign('level_of_educations_id')->references('id')->on('level_of_educations');

            $table->integer('modalities_id')->unsigned();
            $table->foreign('modalities_id')->references('id')->on('modalities');

            $table->integer('institutions_id')->unsigned();
            $table->foreign('institutions_id')->references('id')->on('institutions');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programs');
    }
}
