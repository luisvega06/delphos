<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationPublicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_publications', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            $table->increments('id');


            $table->integer('users_id')->unsigned();
            $table->foreign('users_id')->references('id')->on('users');


            $table->string('slug');
            $table->boolean('confirmed')->default(false);
            $table->text('content');

            $table->integer('users_institution_id')->unsigned();
            $table->foreign('users_institution_id')->references('id')->on('users');

            
            $table->integer('publications_id')->unsigned();
            $table->foreign('publications_id')->references('id')->on('publications');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_publications');
    }
}
