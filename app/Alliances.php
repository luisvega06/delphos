<?php

namespace Delphos;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class Alliances extends Model
{
	protected $table = 'alliances';

    protected $fillable = [
    	'id',
		'description',
		'avatar',
		'institutions_id',
    ];
    
    public function getCreate()
    {
        Date::setLocale('es');
        return new Date($this->created_at);

    }

    public function getUpdate()
    {
        Date::setLocale('es');
        return new Date($this->updated_at);

    }
}
