<?php

namespace Delphos;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class Notifications extends Model
{
    protected $table = 'notifications';

    //Campos que se pueden actualizar
    protected $fillable = [
        'id',
        'type',
        'notifiable',
        'data',
        'read_at',
        'created_at',
        'updated_at',
            
    ];

    public function getCreate()
    {
        Date::setLocale('es');
        return new Date($this->created_at);

    }

    public function getUpdate()
    {
        Date::setLocale('es');
        return new Date($this->updated_at);

    }

}
