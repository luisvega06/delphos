<?php

namespace Delphos;

use Illuminate\Database\Eloquent\Model;

class Modality extends Model
{
    protected $table = 'modalities';

    //Campos que se pueden actualizar
    protected $fillable = ['id', 'name', 'created_at', 'updated_at'];


    public function institution() //Uno a muchos (inverso)

    {
        return $this->belongsTo('Delpos\Intitutions');
    }

}
