<?php

namespace Delphos;

use Illuminate\Database\Eloquent\Model;

class TypeInstitution extends Model
{
    protected $table = 'type_institutions';

    //Campos que se pueden actualizar
    protected $fillable = ['id', 'name',];


    public function institutions() //Uno a muchos (inverso)

    {
        return $this->belongsTo('Delpos\Institution');
    }
}
