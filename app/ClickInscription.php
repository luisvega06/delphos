<?php

namespace Delphos;

use Illuminate\Database\Eloquent\Model;

class ClickInscription extends Model
{
    protected $table = 'click_inscriptions';

    //Campos que se pueden actualizar
    protected $fillable = [
    	'id', 
        'institutions_id', 
        'programs_id', 
        'users_id',
        'created_at',
        'updated_at',
    ];
}
