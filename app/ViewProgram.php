<?php

namespace Delphos;

use Illuminate\Database\Eloquent\Model;

class ViewProgram extends Model
{
    protected $table = 'view_programs';

    //Campos que se pueden actualizar
    protected $fillable = [
    	'id', 
        'institutions_id',
        'programs_id', 
        'users_id',
        'created_at',
        'updated_at',
    ];
}
