<?php

namespace Delphos;

use Illuminate\Database\Eloquent\Model;

class Duracion extends Model
{
    protected $table = 'duracions';

    //Campos que se pueden actualizar
    protected $fillable = [
    	'id', 
        'name', 
        'created_at',
        'updated_at',
    ];

 
    public function programs() // Relacion de 1 : N

    {
        return $this->hasMany('Delphos\Program');
    }
}
