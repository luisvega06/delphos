<?php

namespace Delphos\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewPost extends Notification
{
    use Queueable;
    protected $institucion;
    protected $publication;
    

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($institucion, $publication)
    {
        $this->institucion = $institucion;
        $this->publication = $publication;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        return (new MailMessage)
                    ->subject('Notificación Delphos Académico')
                    ->greeting('Hola!')
                    ->line($this->institucion->name . ' acaba de publicar algo.')
                    ->action('Ver publicación', url('publications/'.$this->publication->slug))
                    ->line('¡Sigue activo en Delphos Académico!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
