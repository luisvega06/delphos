<?php

namespace Delphos\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class NewFavoriteNotification extends Notification implements ShouldQueue
{
    use Queueable;
    protected $community;
    protected $program;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($community , $program)
    {
        $this->community = $community;
        $this->program   = $program;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    public function toDataBase($notifiable)
    {
        return [
            'community' => $this->community,
            'program'   => $this->program,
        ];
    }

    public function toBroadcast($notifiable)
    {
        
        return new BroadcastMessage([
            'community' => $this->community,
            'program'   => $this->program,
        ]);

    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'community' => $this->community,
            'program'   => $this->program,
        ];
    }
}
