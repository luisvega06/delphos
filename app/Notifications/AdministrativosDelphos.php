<?php

namespace Delphos\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AdministrativosDelphos extends Notification
{
    use Queueable;
    protected $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */

    public function toMail($notifiable)
    {
        $porciones = explode(" ", $this->user->name);
        

        return (new MailMessage)
                    ->subject('Bienvenida al Team: Delphos Académico')
                    ->greeting('Hola ' . $porciones[0] . ', se ha creado:')
                    ->line('tu cuenta de usuario con la dirección de correo electrónico '. $this->user->email )

                    ->action(' Visitar ', url('https://www.delphosacademico.com/login'))
                    ->line('¡Te damos la bienvenida al Team Delphos Académico!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
