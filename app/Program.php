<?php

namespace Delphos;

use Illuminate\Database\Eloquent\Model;

use Jenssegers\Date\Date;

class Program extends Model
{
    
    
    protected $table = 'programs';

    //Campos que se pueden actualizar
    protected $fillable = [
        'id',
        'slug',
        'name',
        'description',
        'avatar',
        'price',
        'inscription_price',
        'tiempo',
        'duracions_id',
        'credits',
        'codigo_snies',
        'last_accreditation',
        'info_intercambios',
        'info_extranjero',
        'url_pensum',
        'level_of_educations_id',

        'modalities_id',
        'institutions_id',

        'created_at',
        'updated_at',
    	
	];

    public function getRouteKeyName()
    {
        return 'slug';
    }


    public function institution() //Uno a muchos (inverso)

    {
        return $this->belongsTo('Delphos\Institution', "institutions_id", "id");
    }

    public function duracion() //Uno a muchos (inverso)

    {
        return $this->belongsTo('Delphos\Duracion');
    }

    /**
     * The users that belong to the role.
     */
    public function favourites()
    {
        return $this->belongsToMany('Delphos\Favourite');
    }

        public function getCreate()
    {
        Date::setLocale('es');
        return new Date($this->created_at);

    }

    public function getUpdate()
    {
        Date::setLocale('es');
        return new Date($this->updated_at);

    }

}
