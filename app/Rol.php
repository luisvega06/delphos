<?php

namespace Delphos;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    protected $table = 'rols';

    /**
     * Los atributos que son asignados en masa.
     *
     * @var array
     */
    //Campos que se pueden actualizar
    protected $fillable = ['id', 'name',];

    public function users() // Relacion de 1 : N

    {
        return $this->hasMany('Delphos\User');
    }
}
