<?php

namespace Delphos;

use Illuminate\Database\Eloquent\Model;

class MateriasAFinesProgramas extends Model
{
    protected $table = 'materias_a_fines_programas';

    protected $fillable = [
    	'id',
		'programs_id',
		'materias_id',
        'created_at',
        'updated_at',
    ];
}
