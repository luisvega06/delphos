<?php

namespace Delphos;

use Illuminate\Notifications\Notifiable;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Database\Eloquent\Model;

use Jenssegers\Date\Date;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'username',
        'email', 
        'password',
        'avatar',
        'remember_token', 
        'created_at',
        'updated_at', 
        'rols_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function rol() //Uno a muchos (inverso)

    {
        return $this->belongsTo('Delpos\Rol');
    }


    public function usercommunities() // Relacion de 1 : N

    {
        return $this->hasMany('Delphos\UserCommunity');
    }

    public function institutions() // Relacion de 1 : N

    {
        return $this->hasOne('Delphos\Institution');
    }

    public function phones() // Relacion de 1 : N

    {
        return $this->hasMany('Delphos\Phone');
    }


    public function getRouteKeyName()
    {
        return 'id';
    }

    /**
     * The users that belong to the role.
     */
    public function favourites()
    {
        return $this->belongsToMany('Delphos\Favourite');
    }


    /**
     * The users that belong to the role.
     */
    public function views()
    {
        return $this->belongsToMany('Delphos\View');
    }

    public function receivesBroadcastNotificationsOn()
    {
        return 'users.'.$this->id;
    }


    public function getCreate()
    {
        Date::setLocale('es');
        return new Date($this->created_at);

    }

    public function getUpdate()
    {
        Date::setLocale('es');
        return new Date($this->updated_at);

    }


}
