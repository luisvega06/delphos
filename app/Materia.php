<?php

namespace Delphos;

use Illuminate\Database\Eloquent\Model;

class Materia extends Model
{
    protected $table = 'materias';

    protected $fillable = [
    	'id',
		'nombre',
    ];
}
