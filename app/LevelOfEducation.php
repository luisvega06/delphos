<?php

namespace Delphos;

use Illuminate\Database\Eloquent\Model;

class LevelOfEducation extends Model
{
    protected $table = 'level_of_educations';

    //Campos que se pueden actualizar
    protected $fillable = ['id', 'name', 'created_at', 'updated_at'];


    public function institution() //Uno a muchos (inverso)

    {
        return $this->belongsTo('Delpos\Intitutions');
    }
}
