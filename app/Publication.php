<?php

namespace Delphos;

use Illuminate\Database\Eloquent\Model;

use Jenssegers\Date\Date;

class Publication extends Model
{

    protected $table = 'publications';

    //Campos que se pueden actualizar
    protected $fillable = ['id', 'slug', 'title', 'description', 'picture', 'created_at', 'updated_at', 'institutions_id'];


    public function getRouteKeyName()
	{
	    return 'slug';
	}


    public function institution() //Uno a muchos (inverso)

    {
        return $this->belongsTo('Delpos\Intitutions');
    }

    /**
     * The users that belong to the role.
     */
    public function views()
    {
        return $this->belongsToMany('Delphos\View');
    }

    //use Jenssegers\Date\Date;
    public function getCreate()
    {
        Date::setLocale('es');
        return new Date($this->created_at);

    }

    public function getUpdate()
    {
        Date::setLocale('es');
        return new Date($this->updated_at);

    }

}
