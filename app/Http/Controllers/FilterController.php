<?php

namespace Delphos\Http\Controllers;

use DB;
use Session;
use Delphos\User;
use Delphos\Phone;
use Delphos\State;
use Delphos\Modality;
use Delphos\Institution;
use Delphos\Publication;
use Delphos\UserCommunity;
use Illuminate\Http\Request;
use Delphos\TypeInstitution;
use Delphos\ViewInstitution;
use Delphos\LevelOfEducation;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Validation\Validator;
use Delphos\Http\Controllers\Controller;
use Delphos\StoreUserCommunityUpdateRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Carbon\Carbon;
use Carbon\CarbonInterval;
use Jenssegers\Date\Date;


class FilterController extends Controller
{
    var $ROL_COMMUNITY   = 1; //ROL INSTITUCION
    var $ROL_DELPHOS     = 2; //ROL INSTITUCION
    var $ROL_INSTITUCION = 3; //ROL INSTITUCION
    var $ROL_SUPERUSER   = 4; //ROL INSTITUCION
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');
        
        return 'index';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');
        
        return redirect('/');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($criterio)
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');
        
        return redirect('/');//return 'Vine a show: '. $criterio;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function search (Request $request)
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');
        

        //return 'Vine a search: ' . $request['search'];

        $criterio = ucwords($request['search']);

        if (empty($criterio)) {
            #esta vacia
            if (Session::has('search')){
                $criterio = $value = Session::get('search');
                //echo " Obtuve un criterio = " . $criterio;
            }
        }else{
            Session::put('search', $criterio);
            //echo " Guarde un criterio = " . $criterio;
        }

        $allprogramas = DB::table('users')
                ->join('institutions', 'users.id', '=', 'institutions.users_id')
                ->join('programs', 'institutions.id', '=', 'programs.institutions_id')
                ->join('duracions', 'programs.duracions_id', '=', 'duracions.id')
                ->select('users.name AS uname', 'users.username AS uusername', 'users.avatar AS uavatar', 'programs.id AS pid', 'programs.name AS pname', 'programs.tiempo AS ptime', 'duracions.name AS dname')
                ->where('programs.name', $criterio)
                ->orWhere('programs.name', 'like', '%' . $criterio. '%')
                ->orWhere('users.name', 'like', '%' . $criterio. '%')
                ->paginate(10);

        //return $allprogramas;
        return view('filtro.search', compact('allprogramas', 'criterio'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');
        
        return redirect('/');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return redirect('/');
    }


    public function FiltrarOfertas()
    {
        $user  = Auth::User();
        
        $towns = DB::table('towns')
                    ->join('institutions', 'towns.id', '=', 'institutions.towns_id')
                    ->select('towns.id', 'towns.name')
                    ->orderby('towns.name', 'ASC')
                    ->distinct()
                    ->get();


        $modalities = DB::table('modalities')
                    ->select('modalities.id', 'modalities.name')
                    ->orderby('modalities.name', 'ASC')
                    ->get();
        
        $level_of_educations = DB::table('level_of_educations')
                    ->select('level_of_educations.id', 'level_of_educations.name')
                    ->orderby('level_of_educations.name', 'ASC')
                    ->get();

        return view('filtro.panel', compact('user', 'towns', 'modalities', 'level_of_educations' ) );
    }

    public function getResultFilterOfertas(Request $request)
    {


        
        $towns  = $request->input("towns");
        $level_of_educations = $request->input("level_of_educations");
        $modalities = $request->input("modalities");

        if ($towns == '' ) {
            $towns = 0;
        }
        if ($level_of_educations == '' ) {
            $level_of_educations = 0;
        }
        if ($modalities == '' ) {
            $modalities = 0;
        }

        if ($towns == 0 || $level_of_educations == 0) {
            return redirect('/descubrir-ofertas-academicas')
                ->withErrors(['errors'=>'Debe seleccionar una ciudad y una formación.'])->withInput();
        }

        //return $towns . ', '. $level_of_educations . ', '. $modalities;
        $allprogramas = '';
        if ($modalities != 0) {
            $allprogramas = DB::table('users')
                ->join('institutions', 'users.id', '=', 'institutions.users_id')
                ->join('programs', 'institutions.id', '=', 'programs.institutions_id')
                ->join('duracions', 'programs.duracions_id', '=', 'duracions.id')

                ->select('users.name AS uname', 'users.username AS uusername', 'users.avatar AS uavatar', 'programs.id AS pid', 'programs.name AS pname', 'programs.tiempo AS ptime', 'duracions.name AS dname')
                
                ->where('institutions.towns_id', '=', $towns)
                ->Where('programs.level_of_educations_id', '=', $level_of_educations)
                ->Where('programs.modalities_id', '=', $modalities)

                ->get();
        } else {
            $allprogramas = DB::table('users')
                ->join('institutions', 'users.id', '=', 'institutions.users_id')
                ->join('programs', 'institutions.id', '=', 'programs.institutions_id')
                ->join('duracions', 'programs.duracions_id', '=', 'duracions.id')

                ->select('users.name AS uname', 'users.username AS uusername', 'users.avatar AS uavatar', 'programs.id AS pid', 'programs.name AS pname', 'programs.tiempo AS ptime', 'duracions.name AS dname')
                
                ->where('institutions.towns_id', '=', $towns)
                ->Where('programs.level_of_educations_id', '=', $level_of_educations)
                ->get();
        }
        

        $user  = Auth::User();
        
        $towns = DB::table('towns')
                    ->join('institutions', 'towns.id', '=', 'institutions.towns_id')
                    ->select('towns.id', 'towns.name')
                    ->orderby('towns.name', 'ASC')
                    ->distinct()
                    ->get();


        $modalities = DB::table('modalities')
                    ->select('modalities.id', 'modalities.name')
                    ->orderby('modalities.name', 'ASC')
                    ->get();
        
        $level_of_educations = DB::table('level_of_educations')
                    ->select('level_of_educations.id', 'level_of_educations.name')
                    ->orderby('level_of_educations.name', 'ASC')
                    ->get();
        

        return view('filtro.result', compact('user', 'towns', 'modalities', 'level_of_educations', 'allprogramas') ); 
    }

    public function listar_ofertas_academicas($towns = 0, $level_of_educations = 0, $modalities = 0)
    {
        $allprogramas = '';
        if ($towns != 0 && $level_of_educations != 0 && $modalities != 0) {
            $allprogramas = DB::table('users')
                ->join('institutions', 'users.id', '=', 'institutions.users_id')
                ->join('programs', 'institutions.id', '=', 'programs.institutions_id')
                ->join('duracions', 'programs.duracions_id', '=', 'duracions.id')

                ->select('users.name AS uname', 'users.username AS uusername', 'users.avatar AS uavatar', 'programs.id AS pid', 'programs.name AS pname', 'programs.tiempo AS ptime', 'duracions.name AS dname')
                
                ->where('institutions.towns_id', '=', $towns)
                ->Where('programs.level_of_educations_id', '=', $level_of_educations)
                ->Where('programs.modalities_id', '=', $modalities)

                ->get();
        }else{
            if ($towns == 0 && $level_of_educations != 0 && $modalities != 0) {
                $allprogramas = DB::table('users')
                ->join('institutions', 'users.id', '=', 'institutions.users_id')
                ->join('programs', 'institutions.id', '=', 'programs.institutions_id')
                ->join('duracions', 'programs.duracions_id', '=', 'duracions.id')

                ->select('users.name AS uname', 'users.username AS uusername', 'users.avatar AS uavatar', 'programs.id AS pid', 'programs.name AS pname', 'programs.tiempo AS ptime', 'duracions.name AS dname')
                
                ->Where('programs.level_of_educations_id', '=', $level_of_educations)
                ->Where('programs.modalities_id', '=', $modalities)

                ->get();
                
            }else{

                if ($towns != 0 && $level_of_educations == 0 && $modalities != 0) {
                    $allprogramas = DB::table('users')
                        ->join('institutions', 'users.id', '=', 'institutions.users_id')
                        ->join('programs', 'institutions.id', '=', 'programs.institutions_id')
                        ->join('duracions', 'programs.duracions_id', '=', 'duracions.id')

                        ->select('users.name AS uname', 'users.username AS uusername', 'users.avatar AS uavatar', 'programs.id AS pid', 'programs.name AS pname', 'programs.tiempo AS ptime', 'duracions.name AS dname')
                        
                        ->where('institutions.towns_id', '=', $towns)
                        ->Where('programs.modalities_id', '=', $modalities)

                        ->get();
                        
                }else{
                    if ($towns != 0 && $level_of_educations != 0 && $modalities == 0) {
                        $allprogramas = DB::table('users')
                            ->join('institutions', 'users.id', '=', 'institutions.users_id')
                            ->join('programs', 'institutions.id', '=', 'programs.institutions_id')
                            ->join('duracions', 'programs.duracions_id', '=', 'duracions.id')

                            ->select('users.name AS uname', 'users.username AS uusername', 'users.avatar AS uavatar', 'programs.id AS pid', 'programs.name AS pname', 'programs.tiempo AS ptime', 'duracions.name AS dname')
                            
                            ->where('institutions.towns_id', '=', $towns)
                            ->Where('programs.level_of_educations_id', '=', $level_of_educations)

                            ->get();
                    }else{
                        return null;
                    }
                }

            }

        }

        return $allprogramas;
    }

    public function getInstitutions()
    {
        
        $users = User::where('rols_id', '=', $this->ROL_INSTITUCION)
                    ->select('name', 'avatar')
                    ->orderby('name')
                    ->get();


        return $users;

    }


    

    public function getDetails($username = '')
    {
        
        $users = DB::Table('users')
                    ->join('institutions', 'users.id', 'institutions.users_id')
                    ->join('phones', 'users.id', 'phones.users_id')
                    ->join('towns', 'institutions.towns_id','towns.id')
                    ->join('states', 'towns.states_id', 'states.id')
                    ->select(
                            'users.name',
                            'users.username',
                            'users.email',
                            'users.avatar',
                            'users.created_at AS fecha',
                            'phones.number',
                            'towns.name AS city',
                            'states.name AS state',
                            'institutions.address'
                        )
                    ->where('users.username', '=', $username)
                    ->orderby('name')
                    ->get();


        return $users;

    }  

    public function reporte_mes_adm($username = '')
    {
        
        $fecha_actual = $carbon = Date::now();
        $fecha_actual->month = $fecha_actual->month -1;
        $fecha_actual->day = 1;
        //return '// '. $fecha_actual;
        
        $institucion = DB::table('institutions')->where('slug', '=', $username)->first();

        $contenedor = array();

        /* Usuarios que han dado click a link inscripcion - MES ACTUAL*/
        $data_click_inscriptions = DB::table('click_inscriptions')
                    ->where('click_inscriptions.institutions_id', '=', $institucion->id)
                    ->where('click_inscriptions.created_at', '>=', $fecha_actual)
                    ->count();

        $object = (object) [
            'label' => 'Click inscripciones ' . $fecha_actual->format('F, Y'), 
            'count' => $data_click_inscriptions,
        ];
        array_push($contenedor, $object);


        /* Total de vistas publicaciones */
        $data_views = DB::table('views')
            ->where('views.institutions_id', '=', $institucion->id)
            ->where('views.created_at', '>=', $fecha_actual)
            ->count();

        $object = (object) [
            'label' => 'Vistas publicaciones ' . $fecha_actual->format('F, Y'), 
            'count' => $data_views,
        ];
        array_push($contenedor, $object);


        /* Favoritos */
        $data_favoritos = DB::Table('favourites')
            ->where('favourites.institutions_id', '=', $institucion->id)
            ->where('favourites.created_at', '>=', $fecha_actual)
            ->count();

        $object = (object) [
            'label' => 'Favoritos ' . $fecha_actual->format('F, Y'), 
            'count' => $data_favoritos,
        ];
        array_push($contenedor, $object);


        /* Vistas perfil INSTITUCION */
        $data_institucion = DB::table('view_institutions')
            ->where('view_institutions.institutions_id', '=', $institucion->id)
            ->where('view_institutions.created_at', '>=', $fecha_actual)
            ->count();

        $object = (object) [
            'label' => 'Vistas institución ' . $fecha_actual->format('F, Y'), 
            'count' => $data_favoritos,
        ];
        array_push($contenedor, $object);

        return $contenedor;

    }





}

