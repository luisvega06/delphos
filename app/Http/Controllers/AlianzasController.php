<?php

namespace Delphos\Http\Controllers;

use Delphos\User;
use Delphos\Phone;
use Delphos\State;
use Delphos\Modality;
use Delphos\Alliances;
use Delphos\Institution;
use Delphos\Publication;
use Delphos\UserCommunity;
use Illuminate\Http\Request;
use Delphos\TypeInstitution;
use Delphos\ViewInstitution;
use Delphos\LevelOfEducation;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Validation\Validator;
use Delphos\Http\Controllers\Controller;
use Delphos\StoreUserCommunityUpdateRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use DB;
use Carbon\Carbon;
use Carbon\CarbonInterval;

class AlianzasController extends Controller
{

    var $ROL_COMMUNITY   = 1; //ROL INSTITUCION
    var $ROL_DELPHOS     = 2; //ROL INSTITUCION
    var $ROL_INSTITUCION = 3; //ROL INSTITUCION
    var $ROL_SUPERUSER   = 4; //ROL INSTITUCION
    var $ROL_OFFLINE     = 5; //ROL INSTITUCION

    var $ALLIANCES_AVATAR = '/images/img_default/default_alliances.jpg';

     /**
     * Create a new controller instance.
     *
     * @return void
     */


    public function __construct()
    {
            //$this->middleware('auth');
        $this->middleware('auth');
    }
    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('/');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect('/');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        $user = Auth::user();
        $user_insti  = User::where('username', $slug)->first();
        $institution = Institution::where('users_id', $user_insti->id)->first();

        /* CREAR ALIANZAS */
        $alliances = Alliances::where('institutions_id', $institution->id)->first();

        return view('institutions.alianzas.index', 
            compact('user', 'user_insti' ,'institution', 'alliances')
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        if (Auth::check())
        {
            
            if ( Auth::User()->rols_id !=  $this->ROL_INSTITUCION ) {
                return redirect('/');
            }else{

                $user = Auth::user();
                $institution = Institution::where('users_id', $user->id)->first();

                /* CREAR ALIANZAS */
                $alliances = Alliances::where('institutions_id', $institution->id)->first();



                return view('institutions.alianzas.edit', 
                    compact('user', 'institution', 'alliances')
                );
            }

        }else{
            return redirect('/');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ( Auth::User()->id !=  $id ) {
                return redirect('/');
        }
        
        $alliances   = Alliances::where('id', $id)->first();
        $institution = Institution::where('id', $alliances->institutions_id)->first();

        $alliances->description = $request->input('description');
        
        $route_img = '/images/users/institutions/'.$institution->slug .'/alliances/';
        if($request->hasFile('avatar')){

            //Borramos la foto anterior
            if ( $alliances->avatar != $this->ALLIANCES_AVATAR ) 
            {

                $file_path = public_path() . $alliances->avatar;
                \File::delete($file_path);

            }

            $file = $request->file('avatar');

                //$name = time().$file->getClientOriginalName();
            $name = 'alianzas_' . Auth::User()->id . time() . '.jpg';
            $institution->avatar_cover = $route_img . $name;
            $file->move(public_path() .  $route_img , $name);


        }

        $alliances->save();

        return redirect('/institutions');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return redirect('/');
    }
}
