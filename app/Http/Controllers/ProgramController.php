<?php

namespace Delphos\Http\Controllers;

use Carbon\Carbon;
use Carbon\CarbonInterval;

use DB;
use Delphos\User;
use Delphos\Town;
use Delphos\State;
use Delphos\Phone;
use Delphos\Program;
use Delphos\Notifications\NewFavoriteNotification;
use Delphos\Duracion;
use Delphos\Materia;
use Delphos\Modality;
use Delphos\Favourite;
use Delphos\Publication;
use Delphos\Institution;
use Delphos\ViewProgram;
use Delphos\UserCommunity;
use Delphos\TypeInstitution;
use Illuminate\Http\Request;
use Delphos\LevelOfEducation;
use Delphos\ClickInscription;
use Delphos\MateriasAFinesProgramas;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Delphos\MateriasAFinesCommunities;
use Delphos\Http\Requests\StoreProgramRequest;
use Delphos\Http\Requests\StoreProgramUpdateRequest;

use Delphos\Events\StatusNotifications;
use Delphos\Events\NewFollowerNotification;


class ProgramController extends Controller
{

    var $ROL_COMMUNITY   = 1; //ROL INSTITUCION
    var $ROL_DELPHOS     = 2; //ROL INSTITUCION
    var $ROL_INSTITUCION = 3; //ROL INSTITUCION
    var $ROL_SUPERUSER   = 4; //ROL INSTITUCION
    var $ROL_OFFLINE     = 5; //ROL INSTITUCION
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['notification', 'list', 'show']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');
        
        if (Auth::check())
        {
            if (Auth::user()->rols_id != $this->ROL_INSTITUCION) {
                return redirect('/');
            }

            //return 'create program';
            $levelofeducations=LevelOfEducation::Select('id', 'name')
                                    ->orderBy('name','ASC')
                                    ->get();

            $modalities=Modality::Select('id', 'name')
                                    ->orderBy('name','ASC')
                                    ->get();
            $user = Auth::User();
            $institutions = Institution::where('users_id', Auth::User()->id)->first();
            
            $type_institutions = TypeInstitution::where('id', $institutions->type_institutions_id)->first();

            $programs = Program::where('institutions_id', $institutions->id)
                                    ->orderBy('level_of_educations_id','ASC')
                                    ->orderBy('modalities_id','ASC')
                                    ->orderBy('name','ASC')
                                    ->get();

            $ofertasacademicas = DB::table('level_of_educations')->join('programs', 'level_of_educations.id', '=', 'programs.level_of_educations_id')->join('institutions', 'institutions.id', '=', 'programs.institutions_id')->select('level_of_educations.*')->where('programs.institutions_id', $institutions->id)->orderBy('level_of_educations.id','ASC')->distinct()->get();


            //return $programs;

            return view('programs.index', compact('levelofeducations', 'modalities' , 'institutions', 'programs', 'type_institutions', 'ofertasacademicas', 'user', 'favourite'));

        }else{
            return redirect('/');
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        if (!Auth::check())
        {
            return redirect('/');

        }else{

            if (Auth::user()->rols_id != $this->ROL_INSTITUCION)
            {
                return redirect('/');

            }


           $materias = Materia::Select('id', 'nombre')
                    ->orderBy('nombre','ASC')
                    ->get();

            //return 'create program';
            $levelofeducations=LevelOfEducation::Select('id', 'name')
                                    ->orderBy('name','ASC')
                                    ->get();

            $modalities=Modality::Select('id', 'name')
                                    ->orderBy('name','ASC')
                                    ->get();
            $user = Auth::User();
            $institutions = Institution::where('users_id', Auth::User()->id)->first();

            $type_institutions = TypeInstitution::where('id', $institutions->type_institutions_id)->first();

            $duracions = Duracion::Select('id', 'name')->orderBy('name','ASC')->get();

            $ofertasacademicas = DB::table('level_of_educations')->join('programs', 'level_of_educations.id', '=', 'programs.level_of_educations_id')->orderBy('level_of_educations.id','ASC')->distinct('level_of_educations.id')->select('level_of_educations.*')->get();

            return view('programs.create', compact('levelofeducations', 'modalities' , 'institutions', 'type_institutions', 'ofertasacademicas', 'duracions', 'materias'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    //StoreProgramRequest
    public function store(StoreProgramRequest $request)
    {
        //return $request;

        $user = Auth::User();
        $institution = Institution::where('users_id', Auth::User()->id)->first();



        $Program = new Program();
        $name = '/images/img_default/default_cover.jpg';
        $route_programs = '/images/users/institutions/'.$institution->slug .'/programs/';
        if($request->hasFile('avatar')){
            $file = $request->file('avatar');
            $name = 'program_' . Auth::User()->id . '_' .time() . '.jpg';
            $Program->avatar = $route_programs . '/' . $name;
            $file->move(public_path() . $route_programs . '/', $name);
        }

        $Program->slug = time() . $institution->id;
        $Program->name = ucwords( $request->input('name') );
        $Program->description = $request->input('description');
        $Program->price = $request->input('price');

        $Program->inscription_price = $request->input('inscription_price');
        $Program->tiempo = $request->input('tiempo');                      //Nuevo
        $Program->duracions_id = $request->input('duracions_id');             //Nuevo
        $Program->credits = $request->input('credits');                       //Nuevo
        $Program->codigo_snies = $request->input('codigo_snies');             //Nuevo
        $Program->last_accreditation = $request->input('last_accreditation'); //NUevo


        $Program->info_intercambios = $request->input('info_intercambios');
        $Program->info_extranjero = $request->input('info_extranjero');
        $Program->url_pensum = $request->input('url_pensum');
        $Program->level_of_educations_id = $request->input('level_of_educations_id');
        $Program->modalities_id = $request->input('modalities_id');
        $Program->institutions_id = $institution->id;

        
        $Program->save();

        $materia_one = new MateriasAFinesProgramas();
        $materia_one->programs_id = $Program->id;
        $materia_one->materias_id = $request->input('select-materia-1');
        $materia_one->save();
        
        
        $materia_two = new MateriasAFinesProgramas();
        $materia_two->programs_id = $Program->id;
        $materia_two->materias_id = $request->input('select-materia-2');
        $materia_two->save();

        $materia_three = new MateriasAFinesProgramas();
        $materia_three->programs_id = $Program->id;
        $materia_three->materias_id = $request->input('select-materia-3');
        $materia_three->save();



        return redirect('/programs')->with('message', 'Programa creado.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        if (!Program::where('id', $id)->exists()) {
            return redirect('/')->withErrors(['errors'=>'Lo sentimos. La busqueda no arrojo resultados.' ]);
        }else{

            $program = Program::find($id);
            $institution = Institution::find($program->institutions_id);
            $perfil = User::find($institution->users_id);
            $levelofeducation = LevelOfEducation::find($program->level_of_educations_id);
            $modality = Modality::find($program->modalities_id);
            $totalprograms = Program::where('institutions_id', $institution->id)->count();
            $duracion = Duracion::find($program->duracions_id);
            $precio = 'COP $' . number_format($program->price,2,",",".");
            $town = Town::where('id', '=', $institution->towns_id)->first();
            $state = State::where('id', '=', $town->states_id)->first();
            $favourite = false;

            $list_materias_afines = DB::table('materias')
                                    ->join('materias_a_fines_programas', 'materias.id', 'materias_id')
                                    ->where('materias_a_fines_programas.programs_id', '=', $program->id)
                                    ->select('materias.*')
                                    ->orderby('materias.nombre','ASC')
                                    ->get();

            $contact = Phone::where('users_id', $perfil->id)->first();

            if (Auth::check()) {
                if (Auth::user()->rols_id == $this->ROL_COMMUNITY) {

                    if (Favourite::where('users_id', Auth::user()->id)->where('institutions_id', $institution->id)->where('programs_id', $program->id)->count() > 0) {
                        
                        $favourite = true;
                    }else{
                        $favourite = false;
                    }

                }
            }

            $viewprogram = new ViewProgram();
            $viewprogram->institutions_id = $institution->id;
            $viewprogram->programs_id = $program->id;


            if (Auth::check())
            {

                if (Auth::user()->rols_id == $this->ROL_COMMUNITY) 
                {
                    $viewprogram->users_id = Auth::user()->id;
                    $result = DB::table('view_programs')
                        ->where('institutions_id', '=', $institution->id )
                        ->where('programs_id', '=', $program->id )
                        ->where('users_id', '=', $viewprogram->users_id )
                        ->orderby('created_at','DESC')
                        ->take(1)
                        ->get();

                    if (sizeof($result) != 0) {
                        $aux_object = $result[0];
                        $fecha_ant = $aux_object->created_at;
                        $fecha_act = date('Y-m-d H:i:s');

                        $carbon1 = new Carbon($fecha_ant);
                        $carbon2 = new Carbon($fecha_act);


                        if ($carbon2->diffInMonths($carbon1) > 0) {
                            $viewprogram->save();
                        }

                    }else{
                        $viewprogram->save();
                    }
                }
            }

            return view('programs.show', compact('program', 'institution','perfil' , 'levelofeducation','modality', 'totalprograms', 'precio', 'duracion', 'favourite', 'list_materias_afines', 'contact','town', 'state'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        if (!Program::where('slug', $slug)->exists()) {
            return redirect('/')->withErrors(['errors'=>'Lo sentimos. No se encontro la carrera ingresada.' ]);
        }else{
            $program = Program::where('slug', $slug)->first();

            $levelofeducations = LevelOfEducation::Select('id', 'name')
                                    ->where('id', '!=', $program->level_of_educations_id)
                                    ->orderBy('name','ASC')
                                    ->get();

            $modalities = Modality::Select('id', 'name')
                                    ->where('id', '!=', $program->modalities_id)
                                    ->orderBy('name','ASC')
                                    ->get();

            $levelofeducation_act = LevelOfEducation::find($program->level_of_educations_id);

            $modality_act = Modality::find($program->modalities_id);

            $duracions = Duracion::Select('id', 'name')->where('id', '!=', $program->duracions_id)->orderBy('name','ASC')->get();
            $duracions_act = Duracion::find($program->duracions_id);

            $materias_afines = DB::table('materias')
                                ->join('materias_a_fines_programas', 'materias.id', 'materias_a_fines_programas.materias_id')
                                ->where('materias_a_fines_programas.programs_id', '=', $program->id)
                                ->select('materias.*')
                                ->get()->toArray();

            $materias = Materia::Select('id', 'nombre')
                    ->where('id', '<>', $materias_afines[0]->id)
                    ->Where('id', '<>', $materias_afines[1]->id)
                    ->Where('id', '<>', $materias_afines[2]->id)
                    ->orderBy('nombre','ASC')
                    ->get();

            $user = Auth::User();

            return view('programs.edit', compact('levelofeducations', 'modalities', 'ofertasacademicas', 'levelofeducation_act', 'modality_act', 'program', 'duracions', 'duracions_act', 'materias', 'materias_afines'));

        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreProgramUpdateRequest $request, $slug)
    {

        
        
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');
        //return $slug;

        if (!Program::where('slug', $slug)->exists()) {
                return redirect('/institutions')->withErrors(['errors'=>'Acceso denegado. Sus credenciales no pertenecen a una institución registrada.']);
        }else{

            $Program = Program::where('slug', $slug)->first();
            $institution = Institution::find($Program->institutions_id);

            if (Auth::User()->rols_id != $this->ROL_INSTITUCION ||  $institution->users_id !=Auth::User()->id) {
                return redirect('/')->withErrors(['errors'=>'No fue posible autenticar tu cuenta.'])->withInput(); 
            }


            $name = '/images/img_default/default_cover.jpg';
            $route_programs = '/images/users/institutions/'.$institution->slug .'/programs/';
            if($request->hasFile('avatar')){
                
                $file_path = public_path(). $Program->avatar;
                \File::delete($file_path);

                $file = $request->file('avatar');

                $name = 'program_' . Auth::User()->id . '_' .time() . '.jpg';
                $Program->avatar =          $route_programs . '/' . $name;
                $file->move(public_path() . $route_programs . '/', $name);


            }





            
            
            $Program->name = $request->input('name');
            $Program->description = $request->input('description');
            $Program->price = $request->input('price');

            $Program->inscription_price = $request->input('inscription_price');
            $Program->tiempo = $request->input('tiempo');                      //Nuevo
            $Program->duracions_id = $request->input('duracions_id');             //Nuevo
            $Program->credits = $request->input('credits');                       //Nuevo
            $Program->codigo_snies = $request->input('codigo_snies');             //Nuevo
            $Program->last_accreditation = $request->input('last_accreditation'); //NUevo


            $Program->info_intercambios = $request->input('info_intercambios');
            $Program->info_extranjero = $request->input('info_extranjero');
            $Program->url_pensum = $request->input('url_pensum');
            $Program->level_of_educations_id = $request->input('level_of_educations_id');
            $Program->modalities_id = $request->input('modalities_id');
            

            
            $Program->save();
            return redirect('/institutions')->with('message', 'Programa editado.');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');
        
        //return 'eliminar';
        //Si existe
        $institutions = Institution::Where('users_id', Auth::user()->id)->first();
        if (Program::where('institutions_id', $institutions->id)->where('id', $id)->count() == 0) {
            return redirect('/')->withErrors(['errors'=>'Acceso denegado.']);
        }

        if (Auth::User()->rols_id != $this->ROL_INSTITUCION ||  $institution->users_id != Auth::User()->id) {
                return redirect('/')->withErrors(['errors'=>'No fue posible autenticar tu cuenta.'])->withInput(); 
            }


        $program = Program::where('id', $id)->first();

        $row_f = Favourite::where('programs_id', '=', $id)->delete();
        $row_v = ViewProgram::where('programs_id', '=', $id)->delete();


        $lista_materias = MateriasAFinesProgramas::where('programs_id', $id)->get();
        foreach ( $lista_materias as $materia ) {
            $materia->delete();
        }

        $file_path = public_path(). $program->avatar;
        \File::delete($file_path);
    
        $program->delete();



        return redirect('institutions')->with('message', 'El programa '. $program->name .' se ha eliminado exitosamente. ' . $row_f . ' usuarios tenian el programa registrado en sus favoritos y ' . $row_v . ' vistas.');
    }

    public function like (Request $request) {
        


        $mensaje = '';
        $idinstitucion = $request->input('idinstitucion');
        $idprograma    = $request->input('idprograma');
        $iduser        = $request->input('iduser');

        $institution   = Institution::find($idinstitucion);
        $user          = User::find($institution->users_id);
        $community     = User::find($iduser);
        $program       = Program::find($idprograma);


        if (Favourite::where('institutions_id', '=', $idinstitucion)->where('programs_id', '=', $idprograma)->where('users_id', '=', $iduser)->count() == 0) 
        {
            
            
            $favourite = new Favourite();
            $favourite->confirmed       = false;
            $favourite->institutions_id = $idinstitucion;
            $favourite->programs_id     = $idprograma;
            $favourite->users_id        = $iduser;
            $favourite->save();
            
            
            $mensaje = "Agregado a tu lista de favoritos";

            $user->notify(new NewFavoriteNotification($community, $program));
            event(new NewFollowerNotification($user));

            
        }else{
            $favourite = Favourite::where('institutions_id', '=', $idinstitucion)->where('programs_id', '=', $idprograma)->where('users_id', '=', $iduser)->first();
            $favourite->delete();
            $mensaje = "Removido de tu lista de favoritos";
        }

        

        return $mensaje;
    }

    public function notification()
    {
        return auth()->user()->unreadNotifications;
    }

    /* Muestra todos los programas de una institucion */
    public function list($username) 
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        if (User::where('username', $username)->count() == 0) {
                return redirect('/')->withErrors(['errors'=>'Lo sentimos, la busqueda no arrojo informaci贸n.' ]);
            }else{

                $levelofeducations=LevelOfEducation::Select('id', 'name')
                                    ->orderBy('name','ASC')
                                    ->get();

                $modalities=Modality::Select('id', 'name')
                ->orderBy('name','ASC')
                ->get();

                $user = User::where('username', $username)->first();
                $institutions = Institution::where('users_id', $user->id)->first();
                
                

                $type_institutions = TypeInstitution::where('id', $institutions->type_institutions_id)->first();

                $programs = Program::where('institutions_id', $institutions->id)
                ->orderBy('level_of_educations_id','ASC')
                ->orderBy('modalities_id','ASC')
                ->orderBy('name','ASC')
                ->get();

                $duracions = Duracion::Select('id', 'name')->orderBy('name','ASC')->get();

                $ofertasacademicas = DB::table('level_of_educations')
                        ->join('programs', 'level_of_educations.id', '=', 'programs.level_of_educations_id')
                        ->join('institutions', 'institutions.id', '=', 'programs.institutions_id')
                        ->select('level_of_educations.*')
                        ->where('programs.institutions_id', $institutions->id)
                        ->orderBy('level_of_educations.id','ASC')
                        ->distinct()
                        ->get();

                return view('programs.index', compact('levelofeducations', 'modalities' , 'institutions', 'programs', 'type_institutions', 'ofertasacademicas', 'duracions', 'user'));

            }
    }

    public function FunctionCountClick(Request $request)
    {

        
        $iduser        = $request->input('arg1');
        $idinstitucion = $request->input('arg2');
        $idprograma    = $request->input('arg3');


        $clickinscription = new ClickInscription();
        $clickinscription->users_id = $iduser;
        $clickinscription->institutions_id = $idinstitucion;
        $clickinscription->programs_id = $idprograma;

        $result = DB::table('click_inscriptions')
        ->where('institutions_id', '=', $clickinscription->institutions_id )
        ->where('programs_id', '=', $clickinscription->programs_id )
        ->where('users_id', '=', $clickinscription->users_id )
        ->orderby('created_at','DESC')
        ->take(1)
        ->get();

        if (sizeof($result) != 0) {
            $aux_object = $result[0];
            $fecha_ant = $aux_object->created_at;
            $fecha_act = date('Y-m-d H:i:s');

            $carbon1 = new Carbon($fecha_ant);
            $carbon2 = new Carbon($fecha_act);


            if ($carbon2->diffInDays($carbon1) > 0) {
                $clickinscription->save();
                //return 'Saved-1';
            }else{
                //return 'No-Saved';
            }

        }else{
            $clickinscription->save();
            //return 'Saved-2';
        }
    }

    public function comparar_oferta($idprograma = 0)
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        if (Auth::User()->rols_id != $this->ROL_COMMUNITY) {
            return redirect('/')
                    ->withErrors(['errors'=>'Su ROL no tiene permitido el ingreso a esta vista.' ]);
        }

        if (Program::where('id', $idprograma)->count() == 0) {
            return redirect('/')
                    ->withErrors(['errors'=>'Vista exclusiva para usuarios de tipo comunidad Delphos.' ]);
        }else{

            $pivote = DB::table('programs')
                ->join('institutions', 'programs.institutions_id', '=', 'institutions.id')
                ->join('users', 'institutions.users_id', '=', 'users.id')
                ->where('programs.id', '=',$idprograma)
                ->select('users.name AS name','programs.avatar AS avatar' ,'programs.name AS program', 'programs.slug AS slug')
                ->get();

            $pivot = (object) [
                        'name'    => $pivote[0]->name,
                        'avatar'  => $pivote[0]->avatar,
                        'program' => $pivote[0]->program,
                        'slug'    => $pivote[0]->slug,
                        ];

            $contents = DB::table('favourites')
                ->join('institutions', 'favourites.institutions_id', '=', 'institutions.id')
                ->join('users', 'institutions.users_id', '=', 'users.id')
                ->join('programs', 'favourites.programs_id', '=', 'programs.id')
                ->where('favourites.users_id', Auth::User()->id)
                ->select('users.name AS name','programs.avatar AS avatar' ,'programs.name AS program', 'programs.slug AS slug')
                ->OrderBy('users.name', 'ASC')
                ->OrderBy('programs.name', 'ASC')
                ->get();

            return view('comparador.static_vs_dynamic', compact('pivot', 'contents'));
        }
    }
}
