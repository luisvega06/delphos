<?php

namespace Delphos\Http\Controllers;


use DB;
use Hash;
use Response;
use Delphos\User;
use Delphos\Town;
use Delphos\State;
use Delphos\Phone;
use Delphos\Gender;
use Delphos\Materia;
use Delphos\Program;
use Delphos\Favourite;
use Delphos\Institution;
use Delphos\Publication;
use Delphos\UserCommunity;
use Illuminate\Http\Request;
use Delphos\NotificationPublication;
use Illuminate\Support\Facades\Auth;
use Delphos\MateriasAFinesCommunities;
use Delphos\Mail\CursoMail;
use Mail;



class QuestionController extends Controller
{

    var $ROL_COMMUNITY   = 1; //ROL INSTITUCION
    var $ROL_DELPHOS     = 2; //ROL INSTITUCION
    var $ROL_INSTITUCION = 3; //ROL INSTITUCION
    var $ROL_SUPERUSER   = 4; //ROL INSTITUCION
    var $ROL_OFFLINE     = 5; //ROL INSTITUCION

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        return view('questions.questions', compact('institution', 'user', 'type_institutions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }

    public function questionOne()
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        return view('questions.questions');
    }

    public function store_QuestionOne(Request $request)
    {
        
    }

    public function questionTwo()
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        return view('questions.questionstwo');
    }


    public function store_QuestionTwo(Request $request)
    {
        //return $request->input('contenido');

        

        $user = Auth::User();
        
        if (MateriasAFinesCommunities::where('users_id', $user->id)->count() == 0) {

            $contenido = $request->input('contenido');

            foreach ($contenido as $materia) {

                if (Materia::where('nombre', $materia)->count() == 1) {

                    $mat = Materia::where('nombre', $materia)->first();

                    $m_afines               = new MateriasAFinesCommunities();
                    $m_afines->users_id     = $user->id;
                    $m_afines->materias_id  = $mat->id;
                    $m_afines->save();


                }else{

                    return $materia . ' No es una materia registrada en la base de datos. ';

                }
            }
        }else{
            return $user->name . ', ya tienes materias registradas.';
        }



    }

    public function questionThree()
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        return view('questions.questionsthree');
    }

    
    public function store_QuestionThree(Request $request)
    {
        
    }

    public function enviarEmail(Request $request){
        Mail::to($request->input("correo"))->send(new CursoMail($request->input("id")));
        return view ("gracias");
    }

    public function questionFour($id)
    {
        $programas;
        switch($id){
            case "1":
                $programas = Program::where("id", 3)->orWhere("id", 4)->orWhere("id", 1)->get();
                break;
            case "2":
                $programas = Program::where("id", 14)->orWhere("id", 15)->orWhere("id", 16)->get();
                break;
            case "3":
                $programas = Program::where("id", 17)->orWhere("id", 18)->orWhere("id", 19)->get();
                break;
            case "4":
                $programas = Program::where("id", 20)->orWhere("id", 21)->orWhere("id", 22)->get();
                break;
            case "5":
                $programas = Program::where("id", 2)->orWhere("id", 23)->orWhere("id", 24)->get();
                break;
            case "6":
                $programas = Program::where("id", 5)->orWhere("id", 25)->orWhere("id", 1)->get();
                break;
            case "7":
                $programas = Program::where("id", 26)->orWhere("id", 27)->orWhere("id", 1)->get();
                break;
            case "8":
                $programas = Program::where("id", 28)->orWhere("id", 29)->get();
                break;
            default:
                break;
        }
        foreach($programas as $programa){
            $programa->institution->user;
            $programa->institution->town;
        }
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        return view('questions.questionsfour', ["programas" => $programas]);
    }


}
