<?php

namespace Delphos\Http\Controllers;

use Socialite;
use Delphos\User;
use Delphos\Institution;
use Delphos\UserCommunity;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Authenticatabl;


class SocialiteController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        //return Socialite::driver('google')->redirect();
        return Socialite::driver('google')->redirect();

    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {

        //$user_google = Socialite::driver('google')->user();
        
        $user_google = Socialite::driver('google')->stateless()->user();
        $user_delphos = User::where(['email' => $user_google->getEmail()])->first();

        if ($user_delphos) {

            $user_delphos->avatar = $user_google->getAvatar();
            
            Auth::login($user_delphos);
        

            $mensaje = '';
            $URL = '/';
            switch ($user_delphos->rols_id) {
                case 1: // Community
                        $URL = '/community';
                    break;
                case 2: // Delphos
                        $URL = '/administrativos';
                    break;
                case 3: // Institutions
                        $URL = '/institutions';
                    break;
                case 4: // SuperUser
                        $URL = '/superuser';
                    break;
                
                default:
                        $URL = '/';
                    break;

                return $URL;

            }
            
            return redirect($URL)
                    ->with(['message'=> Auth::user()->name .' Bienvenido!'])
                    ->withInput();

        }else{

        	return redirect('/community/create')
                    ->withErrors(['errors'=>'Tus credenciales no se encuentran registradas.'])
                    ->withInput();

        }

    }
}
