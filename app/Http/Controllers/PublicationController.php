<?php

namespace Delphos\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;
use Carbon\CarbonInterval;

use DB;
use DateTime;
use DatePeriod;
use DateIntercal;
use Delphos\User;
use Delphos\View;
use Delphos\Phone;
use Delphos\State;
use Delphos\Favourite;
use Delphos\Institution;
use Delphos\Publication;
use Delphos\UserCommunity;
use Delphos\TypeInstitution;
use Delphos\NotificationPublication;
use Illuminate\Support\Facades\Auth;
use Delphos\Http\Requests\StorePublicationRequest;


use Notification;
use Delphos\Notifications\NewPost;

class PublicationController extends Controller
{

    
    var $ROL_COMMUNITY   = 1; //ROL INSTITUCION
    var $ROL_DELPHOS     = 2; //ROL INSTITUCION
    var $ROL_INSTITUCION = 3; //ROL INSTITUCION
    var $ROL_SUPERUSER   = 4; //ROL INSTITUCION
    var $ROL_OFFLINE     = 5; //ROL INSTITUCION

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');
        
        if (Auth::check())
        {
            if (Auth::User()->rols_id == $this->ROL_INSTITUCION){

                return redirect('/publications/create');    

            }

        }
        return redirect('/institution/login')->withErrors(['errors'=>'Acceso denegado. No es un usuario autenticado y autorizado para realizar este tipo de publicaciones.'])->withInput();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');
        
        if (Auth::check())
        {
            if (Auth::User()->rols_id == $this->ROL_INSTITUCION){

                $user = Auth::User();
                $institution = Institution::where('users_id', Auth::User()->id)->first();

                $type_institutions = TypeInstitution::where('id', $institution->type_institutions_id)->first();

                return view('publications.create', compact('institution', 'user', 'type_institutions'));   
            }else{

                return redirect('/institution/login')->withErrors(['errors'=>'Acceso denegado. No tienes el rol indicado para realizar este tipo de acciones.'])->withInput();

            }
        }
        
        return redirect('/institution/login')->withErrors(['errors'=>'Acceso denegado. No es un usuario autorizado para realizar este tipo de acciones.'])->withInput();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // StorePublicationRequest
    public function store(StorePublicationRequest $request)
    {

        $user = Auth::User();
        $institution = Institution::where('users_id', Auth::User()->id)->first();

        //$texto = str_replace(" ", "-", $user->name);

        $publication = new Publication();
        $publication->title = $request->input('title');
        $publication->description = $request->input('description');
        $publication->slug = time() . $institution->id;
        $publication->institutions_id = $institution->id;



        
        $name = '/images/img_default/default_cover.jpg';
        $route_publication = '/images/users/institutions/'.$institution->slug .'/publications/';

        if($request->hasFile('picture')){
            $file = $request->file('picture');
            $name = 'publication_' . Auth::User()->id . '_' .time() . '.jpg';
            $publication->picture =     $route_publication . '/'. $name;
            $file->move(public_path() . $route_publication . '/', $name);

        }

            //return 'Saved';
        $publication->save();

        
        $list_favourite = Favourite::where('institutions_id', $institution->id)->select('users_id')->distinct('users_id')->get();

        //return $list_favourite;

        
        foreach ($list_favourite as $persona) {
            
            $notificationpublication = new NotificationPublication();
            $notificationpublication->users_id = $persona->users_id;
            $notificationpublication->confirmed = false;
            $notificationpublication->slug = $publication->slug;
            $notificationpublication->content = $user->name . ' acaba de publicar algo.';
            $notificationpublication->users_institution_id = $institution->id;
            $notificationpublication->publications_id = $publication->id;
            $notificationpublication->created_at = $publication->created_at;
            $notificationpublication->save();

            $user_community = User::find($persona->users_id);
            Notification::route('mail', $user_community->email)->notify(new NewPost($user, $publication));
            
        }

        


        return redirect('/institutions');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        if (Publication::where('slug', $slug)->count() > 0) {
            $publication = Publication::where('slug', $slug)->first();
            $institution = Institution::where('id', $publication->institutions_id)->first();
        }else{
            return redirect('/')->withErrors(['errors'=>'La busqueda no arrojo resultados.']);
        }

        $user_institution = User::find($institution->users_id);
        $type_institutions = TypeInstitution::where('id', $institution->type_institutions_id)->first();

        $viewpublication = new View();
        $viewpublication->institutions_id = $institution->id;
        $viewpublication->publications_id = $publication->id;


        if (Auth::check())
        {
            if (Auth::user()->rols_id == $this->ROL_COMMUNITY) {

                $notificaciones = NotificationPublication::Where('users_id', '=', Auth::User()->id)
                                            ->Where('publications_id', '=', $publication->id)
                                            ->first();
                if (!empty($notificaciones)) {
                    
                    $notificaciones->confirmed = true;
                    $notificaciones->save();
                }


                $viewpublication->users_id = Auth::user()->id;
                $result = DB::table('views')->where('institutions_id', '=', $institution->id )->where('publications_id', '=', $publication->id)->where('users_id', '=', $viewpublication->users_id )->orderby('created_at','DESC')->take(1)->get();

                if (sizeof($result) != 0) {
                    $aux_object = $result[0];
                    $fecha_ant = $aux_object->created_at;
                    $fecha_act = date('Y-m-d H:i:s');

                    $carbon1 = new Carbon($fecha_ant);
                    $carbon2 = new Carbon($fecha_act);


                    if ($carbon1->diffInMonths($carbon2) > 0) {
                        $viewpublication->save();
                    }
                }else{
                    $viewpublication->save();
                }
            }
        }

        setlocale(LC_TIME, 'es');
        //$tiempo = new Carbon($publication->created_at);
        
        //return 'Fecha: ' . ucwords($tiempo->formatLocalized("%A, %d %B %G"));

        //$publication->created_at = ucwords($tiempo->formatLocalized("%A, %d %B %G"));

        return view('publications.show', compact('institution', 'user_institution', 'publication', 'type_institutions'));
    }

    public function analytics($idinstitucion=0,  $slug=0)
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        //return 'llego';

        if (Publication::where('slug', $slug)->count() > 0) {
            $publication = Publication::where('slug', $slug)->first();
            $institution = Institution::where('id', $publication->institutions_id)->first();
        }else{
            return redirect('/')->withErrors(['errors'=>'La busqueda no arrojo resultados.']);
        }

        $total = DB::table('views')
                        ->where('publications_id', '=', $publication->id)
                        ->where('views.users_id', '!=', $institution->users_id)
                        ->count();
        if ($total > 0) {
            return view('publications.analytics', compact('institution', 'user', 'publication'));
        }else{
            return redirect('/institutions')->withErrors(['errors'=>'No tiene datos para mostrar. ']);
        }

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');
        //return 'editar';
        


        if (!Auth::check()) {

            return redirect('/institution/login')->withErrors(['errors'=>'Usuario no autenticado.']);

        }

        if (Auth::user()->rols_id != $this->ROL_INSTITUCION) {

            return redirect('/institution/login')->withErrors(['errors'=>'No cuentas con autorización.']);

        }



        $user = Auth::User();
        $institution = Institution::where('users_id', Auth::User()->id)->first();
        
        $type_institutions = TypeInstitution::where('id', $institution->type_institutions_id)->first();
        
        if (Publication::where('slug', $slug)->where('institutions_id', $institution->id)->exists()) {
            //Si existe
            $publication = Publication::where('slug', $slug)->first();

            return view('publications.edit', compact('publication', 'type_institutions'));

        }else{
            return redirect('institutions')->withErrors(['errors'=>'Petición denegada, esta publicación esta registrada bajo otro propietario.']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {

        $user = Auth::User();
        $institution = Institution::where('users_id', Auth::User()->id)->first();
        
        
        if (Publication::where('slug', $slug)->where('institutions_id', $institution->id)->exists()) {
            //Si existe
            $publication = Publication::where('slug', $slug)->first();


            // Aqui es donde debes editar

            $publication->title = $request->input('title');
            $publication->description = $request->input('description');

            $route_publication = '/images/users/institutions/'.$institution->slug .'/publications/';
            if($request->hasFile('picture')){

                $file_path = public_path(). $publication->picture;
                \File::delete($file_path);

                $file = $request->file('picture');

                $name = 'publication_' . Auth::User()->id . time() . '.jpg';
                $publication->picture =     $route_publication . $institution->slug . '/'. $name;
                $file->move(public_path() . $route_publication . $institution->slug . '/', $name);


            }

            //return 'Saved';
            $publication->save();

            return redirect('institutions')->with('message', 'Publicación editada.');


        }else{
            return redirect('institutions')->withErrors(['errors'=>'Petición de modificación en la publicación denegada, esta publicación esta registrada bajo otro propietario.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');
        
        //return 'eliminar';
        //return $id;
        if (!Auth::check()) {

            return redirect('/institution/login')->withErrors(['errors'=>'Usuario no autenticado.']);

        }

        if (Auth::user()->rols_id < 2 || Auth::user()->rols_id > 4) {

            return redirect('/institution/login')->withErrors(['errors'=>'No cuentas con autorización.']);

        }



        $user = Auth::User();
        $institution = Institution::where('users_id', Auth::User()->id)->first();
        
        
        if (Publication::where('slug', $slug)->where('institutions_id', $institution->id)->exists()) {
            //Si existe
            $publication = Publication::where('slug', $slug)->first();

            View::where('publications_id', '=', $publication->id)->delete();
	    NotificationPublication::where('publications_id', '=', $publication->id)->delete();

            $file_path = public_path(). $publication->picture;
            \File::delete($file_path);
        
            $publication->delete();

            return redirect('institutions')->with('message', 'Publicación eliminada');


        }else{
            return redirect('institutions')->withErrors(['errors'=>'Petición denegada, esta publicación esta registrada bajo otro propietario.']);
        }
    }
}
