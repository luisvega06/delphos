<?php

namespace Delphos\Http\Controllers;

use Delphos\User;
use Delphos\Phone;
use Delphos\State;
use Delphos\Modality;
use Delphos\Institution;
use Delphos\Publication;
use Delphos\UserCommunity;
use Illuminate\Http\Request;
use Delphos\TypeInstitution;
use Delphos\ViewInstitution;
use Delphos\LevelOfEducation;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Validation\Validator;
use Delphos\Http\Controllers\Controller;
use Delphos\StoreUserCommunityUpdateRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


use DB;
use Carbon\Carbon;
use Carbon\CarbonInterval;

class GraphicsController extends Controller
{

    
    var $ROL_COMMUNITY   = 1; //ROL INSTITUCION
    var $ROL_DELPHOS     = 2; //ROL INSTITUCION
    var $ROL_INSTITUCION = 3; //ROL INSTITUCION
    var $ROL_SUPERUSER   = 4; //ROL INSTITUCION
    var $ROL_OFFLINE     = 5; //ROL INSTITUCION


    var $N_PAGINATIONS   = 6; 



    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        return redirect('/');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        return redirect('/');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($username)
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        if (Auth::check()){

            if (Auth::User()->rols_id != $this->ROL_INSTITUCION && Auth::User()->rols_id != $this->ROL_DELPHOS){
                return redirect('/');
            }

            #Creamos los objects
            $user = User::where('username', $username)->first();
            $institution = Institution::where('slug', $username)->first();

            #Verificamos los datos en la DB
            if (!$user || !$institution) {
                return redirect('/')->withErrors(['errors'=>'Acceso denegado. Sus credenciales no pertenecen a un usuario registrado.'])->withInput();
            }

            
            $consulta = 'SELECT DISTINCT YEAR(created_at) AS year, MONTH(created_at) AS month FROM view_institutions WHERE institutions_id = ' . $institution->id;

            $fechas = DB::Select($consulta);


            if (sizeof($fechas) == 0) {
                
                return redirect('institutions/')
                    ->with(['info'=> 'No tiene información para mostrar.'])
                    ->withInput();
            }

            return view('graphics.index', compact('user', 'institution', 'fechas'));

        }else{
            return redirect('/');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        return redirect('/');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return redirect('/');
    }



    //FUNCIONES

    /* Numero de Vistas por INSTITUCION */
    public function getReportesVeiwsInstitucion($idinstitucion = 0)
    {

        $contenedor = array();


        /* Total de usuarios que han visualizado el contenido de una institucion (Impresiones)*/
        $totalviews = DB::table('view_institutions')
                        
                        ->join('users', 'view_institutions.users_id', '=', 'users.id')
                        
                        ->join('phones', 'users.id', '=', 'phones.users_id')
                        ->join('user_communities', 'users.id', '=', 'user_communities.users_id')
                        ->join('towns', 'user_communities.towns_id', '=', 'towns.id')

                        ->where('users.rols_id', '=', $this->ROL_COMMUNITY)
                        ->where('view_institutions.institutions_id', '=', $idinstitucion)

                        ->select('users.username AS username', 'users.name AS name', 'users.email AS email', 'phones.number AS phone', 'user_communities.birthdate AS date', 'towns.name AS municipio', 'view_institutions.created_at AS visito')

                        ->OrderBy('view_institutions.created_at', 'DESC')
                        ->get();

        $object = (object) [
                            'add'=> false,
                            'label' => 'Total vistas',
                            'descripción' => 'Cuantas veces han visualizado tu contenido.',
                            'count' => sizeof($totalviews),
                            'users' => $totalviews
                        ];
                        
        array_push($contenedor, $object);



        /* Total de usuarios que han visualizado el contenido de una institucion (Impresiones) EN EL MES ACTUAL*/
        $fecha = Carbon::now();
        $year  = $fecha->year;
        $month = $fecha->month;
        
        $totalviews_mes = DB::table('view_institutions')
                        ->join('users', 'view_institutions.users_id', '=', 'users.id')
                        ->where('users.rols_id', '=', $this->ROL_COMMUNITY)
                        ->where('view_institutions.institutions_id', '=', $idinstitucion)
                        ->whereYear('view_institutions.created_at', '=', $year)
                        ->whereMonth('view_institutions.created_at', '=', $month)
                        ->select('users.avatar AS avatar', 'users.name AS name', 'users.username AS username', 'users.email AS email', 'view_institutions.created_at AS fecha')
                        ->OrderBy('view_institutions.created_at', 'DESC')
                        ->get();

        $object = (object) [
                            'add'=> false,
                            'label' => 'Ultimo mes',
                            'descripción' => 'Cuantas veces han visualizado tu contenido en el año ' . $year . ' del mes ' . $month,
                            'count' => sizeof($totalviews_mes),
                            'users' => $totalviews_mes
                        ];
                        
        array_push($contenedor, $object);
        

        /* Total de usuarios que han visualizado el contenido de una institucion (sin repetir usuario/Vista unica) */
        $totalviews_unique = DB::table('view_institutions')
                            ->join('users', 'view_institutions.users_id', '=', 'users.id')

                            ->join('phones', 'users.id', '=', 'phones.users_id')
                            ->join('user_communities', 'users.id', '=', 'user_communities.users_id')
                            ->join('towns', 'user_communities.towns_id', '=', 'towns.id')


                            ->where('users.rols_id', '=', $this->ROL_COMMUNITY)
                            ->where('view_institutions.institutions_id', '=', $idinstitucion)
                            
                            ->select('users.username AS username', 'users.name AS name', 'users.email AS email', 'phones.number AS phone', 'user_communities.birthdate AS date', 'towns.name AS municipio', 'view_institutions.created_at AS visito')

                            ->distinct('view_institutions.users_id')
                            ->OrderBy('view_institutions.created_at', 'DESC')
                            ->OrderBy('users.name', 'ASC')
                            ->get();

        $object = (object) [
                            'add'=> true,
                            'label' => 'Vista unica',
                            'descripción' => 'Cuantas cuentas han visto tu contenido (Una cuenta NO puede estar más de una vez).',
                            'count' => sizeof($totalviews_unique),
                            'users' => $totalviews_unique
                        ];

        array_push($contenedor, $object);
        
         // Vistas community x Genero
        $genders = DB::table('genders')
                ->join('user_communities', 'genders.id', '=', 'user_communities.genders_id')
                ->Distinct('genders.id')
                ->OrderBy('genders.name')
                ->select('genders.*')
                ->get();

        foreach ($genders as $gener) {

            // Genero #1
            $view_community_gender = DB::table('view_institutions')
            ->join('users', 'view_institutions.users_id', '=', 'users.id')
            ->join('user_communities', 'users.id', '=', 'user_communities.users_id')
            ->where('view_institutions.institutions_id', '=', $idinstitucion)
            ->whereYear('view_institutions.created_at', '=', $year)
            ->whereMonth('view_institutions.created_at', '=', $month)
            ->where('user_communities.genders_id','=' , $gener->id)
            ->select('users.avatar AS avatar', 'users.name AS name', 'users.username AS username', 'users.email AS email', 'view_institutions.created_at AS fecha')
            ->get();


            $object = (object) [
                            'add'=> false,
                            'label' => $gener->name,
                            'descripción' => 'Cuentas pertenecientes al genero ' . $gener->name . ' ha visualizado tu contenido en el mes actual (Una cuenta puede estar más de una vez).',
                            'count' => sizeof($view_community_gender),
                            'users' => $view_community_gender
                        ];
            array_push($contenedor, $object);

       }


        

        for ($i=1; $i < sizeof($contenedor) ; $i++) { 
            for ($j = 0; $j < sizeof($contenedor) - 1 ; $j++) { 

                if ($contenedor[$j]->count < $contenedor[$j+1]->count) {

                    $object = (object) $contenedor[$j];
                    $contenedor[$j]   = $contenedor[$j+1];
                    $contenedor[$j+1] = $object;
                }

            }
        }
        
        return $contenedor;

    }

    public function getReportesViewsMostPopularPrograms($idinstitucion = 0)
    {

        $contenedor = array();

        $ListMostPopular = DB::Table('favourites')
        ->join('programs' , 'favourites.programs_id', '=', 'programs.id' )
        ->select('favourites.institutions_id AS intitucion','programs.id AS idprogram', 'programs.name AS label', DB::raw('COUNT(programs.id) AS count'))
        ->where('favourites.institutions_id', '=', $idinstitucion)
        ->groupBy('programs.id')
        ->get();

        

        foreach ($ListMostPopular as $objecto) {
            $ListPersons = DB::Table('favourites')
            ->join('users' , 'favourites.users_id', '=', 'users.id' )

            ->join('phones', 'users.id', '=', 'phones.users_id')
            ->join('user_communities', 'users.id', '=', 'user_communities.users_id')
            ->join('towns', 'user_communities.towns_id', '=', 'towns.id')

            ->where('favourites.programs_id', '=', $objecto->idprogram)
            

            ->select('users.username AS username', 'users.name AS name', 'users.email AS email', 'phones.number AS phone', 'user_communities.birthdate AS date', 'towns.name AS municipio', 'favourites.created_at AS visito')

            ->OrderBy('favourites.created_at', 'DESC')
            ->get();

            $object = (object) [
                'add'=> true, 
                'idprogram'=> $objecto->idprogram,
                'label'=> $objecto->label,
                'count' => $objecto->count,
                'users' => $ListPersons
            ];
            array_push($contenedor, $object);
            
        }

        for ($i=1; $i < sizeof($contenedor) ; $i++) { 
            for ($j = 0; $j < sizeof($contenedor) - 1 ; $j++) { 

                if ($contenedor[$j]->count < $contenedor[$j+1]->count) {

                    $object = (object) $contenedor[$j];
                    $contenedor[$j]   = $contenedor[$j+1];
                    $contenedor[$j+1] = $object;
                }

            }
        }

        return $contenedor;
        
    }


    public function getReportesViewsResultPublication($idinstitucion = 0, $idpublicacion = 0)
    {

        $contenedor = array();

        $institution = Institution::where('id', $idinstitucion)->first();
        //return $institution;

        $fecha = Carbon::now();
        
        $year  = $fecha->year;
        $month = $fecha->month;

        $post_count_total = DB::table('views')
            ->join('users', 'views.users_id', '=', 'users.id')

            ->join('phones', 'users.id', '=', 'phones.users_id')
            ->join('user_communities', 'users.id', '=', 'user_communities.users_id')
            ->join('towns', 'user_communities.towns_id', '=', 'towns.id')

            ->where('institutions_id', '=', $idinstitucion)
            ->where('publications_id', '=', $idpublicacion)
            ->where('users.id', '!=', $institution->users_id)
            ->where('users.rols_id', '=', $this->ROL_COMMUNITY)

            ->select('users.username AS username', 'users.name AS name', 'users.email AS email', 'phones.number AS phone', 'user_communities.birthdate AS date', 'towns.name AS municipio', 'views.created_at AS visito')

            ->OrderBy('views.created_at', 'DESC')
            ->get();

        $object = (object) [
                'add'=> false,
                'label'=> 'TOTAL VISTAS', 
                'count' => sizeof($post_count_total),
                'users' => $post_count_total
            ];
        array_push($contenedor, $object);

        
        $post_count_actual = DB::table('views')
                            ->join('users', 'views.users_id', '=', 'users.id')
                            ->where('institutions_id', '=', $idinstitucion)
                            ->where('publications_id', '=', $idpublicacion)
                            ->where('views.institutions_id', $idinstitucion)
                            ->where('users.rols_id', '=', $this->ROL_COMMUNITY)
                            ->whereYear('views.created_at', '=', $year)
                            ->whereMonth('views.created_at', '=', $month)
                            ->OrderBy('views.created_at','DESC')
                            ->get();
        $object = (object) [
                'add'=> false,
                'label'=> 'ULTIMO MES', 
                'count' => sizeof($post_count_actual),
                'users' => $post_count_actual
            ];
        array_push($contenedor, $object);



        // Vistas community x Genero
        $genders = DB::table('genders')
                ->join('user_communities', 'genders.id', '=', 'user_communities.genders_id')
                ->Distinct('genders.id')
                ->OrderBy('genders.name')
                ->select('genders.*')
                ->get();


        foreach ($genders as $gender) {
            $post_count_gender = DB::table('views')
                ->join('users', 'views.users_id', '=', 'users.id')

                ->join('phones', 'users.id', '=', 'phones.users_id')
                ->join('user_communities', 'users.id', '=', 'user_communities.users_id')
                ->join('towns', 'user_communities.towns_id', '=', 'towns.id')
                

                ->where('institutions_id', '=', $idinstitucion)
                ->where('publications_id', '=', $idpublicacion)
                ->where('users.id', '!=', $institution->users_id)
                ->where('user_communities.genders_id','=' , $gender->id)
                
                ->select('users.username AS username', 'users.name AS name', 'users.email AS email', 'phones.number AS phone', 'user_communities.birthdate AS date', 'towns.name AS municipio', 'views.created_at AS visito')

                ->OrderBy('views.created_at', 'DESC')
                ->get();

            if (sizeof($post_count_gender) > 0) {
                $object = (object) [
                    'add'=> true,
                    'label'=> $gender->name, 
                    'count' => sizeof($post_count_gender),
                    'users' => $post_count_gender
                ];

                array_push($contenedor, $object);
                    
            }

        }

        for ($i=1; $i < sizeof($contenedor) ; $i++) { 
            for ($j = 0; $j < sizeof($contenedor) - 1 ; $j++) { 

                if ($contenedor[$j]->count < $contenedor[$j+1]->count) {

                    $object = (object) $contenedor[$j];
                    $contenedor[$j]   = $contenedor[$j+1];
                    $contenedor[$j+1] = $object;
                }

            }
        }

        

        return $contenedor;

    }

    public function getTotalClickInscripcion($idinstitucion = 0)
    {
        
        $fecha_actual = Carbon::now();
        $fecha_actual->day = 1;

        $fecha = date('Y-m-j');
        $fecha_3month = strtotime ( '-3 month' , strtotime ( $fecha ) ) ;
        $fecha_3month = date ( 'Y-m-j' , $fecha_3month );
        $fecha_3month = new Carbon($fecha_3month);
        $fecha_3month->day = 1;


        //return 'pilla fecha actual : ' . $fecha . '<br/> pilla fecha de test: ' . $fecha_3month;



        
        $contenedor = array();
        $institution = Institution::where('id', $idinstitucion)->first();

        $list_programs_click = DB::table('click_inscriptions')
                ->join('programs', 'click_inscriptions.programs_id', '=', 'programs.id')
                ->where('click_inscriptions.institutions_id', '=', $idinstitucion)
                ->where('click_inscriptions.created_at', '>=', $fecha_3month)
                ->select('click_inscriptions.*', 'programs.id AS idprogram', 'programs.name AS program_name')
                ->groupby('click_inscriptions.programs_id')
                ->OrderBy('programs.name', 'ASC')
                ->get();

        //return $list_programs_click;

        foreach ($list_programs_click as $programs_click)
        {
            $list_users_click_programs = DB::table('click_inscriptions')
                ->join('users', 'click_inscriptions.users_id', '=', 'users.id')


                ->join('phones', 'users.id', '=', 'phones.users_id')
                ->join('user_communities', 'users.id', '=', 'user_communities.users_id')
                ->join('towns', 'user_communities.towns_id', '=', 'towns.id')

                ->where('click_inscriptions.institutions_id', '=', $idinstitucion)
                ->where('click_inscriptions.programs_id'    , '=', $programs_click->idprogram)
                ->where('click_inscriptions.created_at', '>=', $fecha_3month)

                ->select('users.username AS username', 'users.name AS name', 'users.email AS email', 'phones.number AS phone', 'user_communities.birthdate AS date', 'towns.name AS municipio')

                ->OrderBy('click_inscriptions.created_at', 'DESC')
                ->get();

            if (sizeof($list_users_click_programs) > 0) {
                $object = (object) [
                    'add'   => true,
                    'label' => $programs_click->program_name, 
                    'count' => sizeof($list_users_click_programs),
                    'users' => $list_users_click_programs
                ];

                array_push($contenedor, $object);
            }
        }

        /* Usuarios que han dado click a link inscripcion - MES ACTUAL*/
        $list_users_click_programs_mes = DB::table('click_inscriptions')
                ->join('users', 'click_inscriptions.users_id', '=', 'users.id')

                ->join('phones', 'users.id', '=', 'phones.users_id')
                ->join('user_communities', 'users.id', '=', 'user_communities.users_id')
                ->join('towns', 'user_communities.towns_id', '=', 'towns.id')

                ->where('click_inscriptions.institutions_id', '=', $idinstitucion)
                ->where('click_inscriptions.created_at', '>=', $fecha_actual)

                ->select('users.username AS username', 'users.name AS name', 'users.email AS email', 'phones.number AS phone', 'user_communities.birthdate AS date', 'towns.name AS municipio')
                
                ->OrderBy('click_inscriptions.programs_id', 'ASC')
                ->OrderBy('click_inscriptions.created_at', 'DESC')
                ->get();

            if (sizeof($list_users_click_programs_mes) > 0) {
                $object = (object) [
                    'add'   => true,
                    'label' => 'Click general ultimo mes', 
                    'count' => sizeof($list_users_click_programs_mes),
                    'users' => $list_users_click_programs_mes
                ];

                array_push($contenedor, $object);
            }


                
        return $contenedor;


    }

}

