<?php

namespace Delphos\Http\Controllers;


use Hash;
use Delphos\User;
use Delphos\Town;
use Delphos\State;
use Delphos\Phone;
use Delphos\Program;
use Delphos\Materia;
use Delphos\Modality;
use Delphos\Alliances;
use Delphos\Favourite;
use Delphos\Institution;
use Delphos\Publication;
use Delphos\UserCommunity;
use Illuminate\Http\Request;
use Delphos\TypeInstitution;
use Delphos\ViewInstitution;
use Delphos\LevelOfEducation;
use Delphos\Http\Requests\StoreInstitutionRequest;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Validation\Validator;
use Delphos\Notifications\MailWelcome;
use Delphos\Http\Controllers\Controller;
use Delphos\StoreUserCommunityUpdateRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


use DB;
use Carbon\Carbon;
use Carbon\CarbonInterval;


use Notification;
use Delphos\Notifications\NewPost;



class InstitutionController extends Controller
{

    var $ROL_COMMUNITY   = 1; //ROL INSTITUCION
    var $ROL_DELPHOS     = 2; //ROL INSTITUCION
    var $ROL_INSTITUCION = 3; //ROL INSTITUCION
    var $ROL_SUPERUSER   = 4; //ROL INSTITUCION
    var $ROL_OFFLINE     = 5; //ROL INSTITUCION
    
    var $N_PAGINATIONS   = 6; 


    var $AVATAR           = '/images/img_default/default.jpg';
    var $AVATAR_COVER     = '/images/img_default/default_cover.jpg';
    var $ALLIANCES_AVATAR = '/images/img_default/default_alliances.jpg';
    var $VIDEO_PRESENT    = 'ak_pxQMTXa8';
    

    /**
     * Create a new controller instance.
     *
     * @return void
     */


    public function __construct()
    {
        //$this->middleware('auth');
        $this->middleware('auth', ['except' => 
                [
                    'create', 
                    'show', 
                    'store',
                    'presentacion',
                    'notificacion_followers',
                ]
        ]);
    }
    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        /*
        $user = User::find(2);
        $institution = User::find(1);
        $publication = Publication::find(1);

        Notification::route('mail', $user->email)->notify(new NewPost($institution, $publication));
        */


        if (Auth::check())
        {

            if (Auth::User()->rols_id != $this->ROL_INSTITUCION){

                return redirect('/')->withErrors(['errors'=>'Acceso denegado. Sus credenciales no pertenecen a una institución registrada.'])->withInput();    
            }
            
            // Si tenemos sesión activa mostrará la página de inicio
            $user = Auth::User();

            if (!Institution::where('users_id', Auth::User()->id)->exists()) {
                return redirect('/institutions')->withErrors(['errors'=>'Acceso denegado. Sus credenciales no pertenecen a una institución registrada.'])->withInput();
            }


            $institution = Institution::where('users_id', Auth::User()->id)->first();
            $type_institutions = TypeInstitution::where('id', $institution->type_institutions_id)->first();


            //return 'mira: '. $institution;

            $publications = Publication::where('institutions_id', $institution->id)
                                ->orderBy('id','DESC')
                                ->paginate($this->N_PAGINATIONS);


            $ofertasacademicas = LevelOfEducation::join('programs', 'level_of_educations.id', '=', 'programs.level_of_educations_id')->orderBy('level_of_educations.id','ASC')->distinct('level_of_educations.id')->select('level_of_educations.*')->get();


            
            $count_followers = count(auth()->user()->unreadNotifications);
            return view('institutions.index', compact('institution', 'user', 'publications', 'type_institutions', 'ofertasacademicas', 'count_followers'));

        }else{
            return redirect('/login')->withErrors(['errors'=>'No cuentas con autorización para ingresar'])->withInput();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');


        /*
            Esta vista debe ser pensada en rols = 4 (Super usuario)
        
        */

        /*
        if ( Auth::User()->rols_id !=  $this->ROL_DELPHOS) {
            return redirect('/');
        }
        */

        $user = Auth::User();
        $states = State::Select('id', 'name')->get();

        $typeinstitutions = TypeInstitution::Select('id', 'name')
                            ->orderBy('name','ASC')
                            ->get();


        return view('institutions.create', compact('states', 'typeinstitutions'));
        

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreInstitutionRequest $request)
    {

        //return $request;
        /*
            https://www.youtube.com/watch?v=khXfXYOLfzU
            // Example
            $email = 'name@example.com';
            $domain = strstr_after($email, '@');
            echo $domain; // prints example.com

            $link = str_replace("https://www.youtube.com/watch?v=", "", $request->input('URL_video_present'));
        */

        if(User::where('email', $request->input('email'))->count() == 0){


            $user = User::create([
                'name'            =>  ucwords( $request->input('name') ),
                'username'        =>  strtolower( $request->input('username') ),
                'email'           =>  strtolower( $request->input('email') ),
                'password'        =>  Hash::make( $request->input('password') ),
                'avatar'          =>  $this->AVATAR,
                'remember_token'  =>  $request->input('_token'),
                'rols_id'         =>  $this->ROL_INSTITUCION,
            ]);

            $institucion = Institution::create([
                'avatar_cover'         => $this->AVATAR_COVER,
                'description'          => 'sin definir',
                'address'              => 'sin definir',
                'ulr_inscriptions'     => 'sin definir',
                'latitude'             => 0,
                'longitude'            => 0,
                'slug'                 => $user->username,
                'users_id'             => $user->id,
                'towns_id'             => $request->input('Municipio'),
                'type_institutions_id' => $request->input('Tipo'),
                'URL_video_present'    => $this->VIDEO_PRESENT,
            ]);

            
            $email       = $user->email;
            $password    = $request->input('password');
            $credentials = ['email' => $email, 'password' => $password];

            
            /* CREAR ALIANZAS */
            $alliances                  = new Alliances();
            $alliances->description     = $institucion->description;
            $alliances->avatar          = $this->ALLIANCES_AVATAR;
            $alliances->institutions_id = $institucion->id;
            $alliances->save();

            /* CREAR PHONE */
            $phone           = new Phone();
            $phone->number   = $request->input('number');
            $phone->users_id = $user->id;
            $phone->save();

            /* Correo de Bienvenida */
           // $user->notify(new MailWelcome($user));

            if (Auth::attempt($credentials)) {
                return redirect('institutions')->with('message', 'Bienvenido a Delphos Académico');
            }else{
                return redirect('/')->withErrors(['errors'=>'No fue posible autenticar tu cuenta.'])->withInput();
            }

            

        }

        return redirect('/')->withErrors(['errors'=>'No fue posible autenticar tu cuenta.'])->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($username)
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        //return 'show institutions: ' . $id;
        // Si tenemos sesión activa mostrará la página de inicio
        if (User::where('username', $username)->where('rols_id', $this->ROL_INSTITUCION)->count() < 1) {
            return back();
        }
        
        $user         = User::where('username', $username)->first();
        $institution = Institution::where('users_id', $user->id)->first();

        $contact = Phone::where('users_id', $user->id)->first();

        $type_institutions = TypeInstitution::where('id', $institution->type_institutions_id)->first();

        $publications = Publication::where('institutions_id', $institution->id)
                        ->orderBy('id','DESC')
                        ->paginate($this->N_PAGINATIONS);


        $ofertasacademicas = LevelOfEducation::join('programs', 'level_of_educations.id', '=', 'programs.level_of_educations_id')->orderBy('level_of_educations.id','ASC')->distinct('level_of_educations.id')->select('level_of_educations.*')->get();

        $viewinstitution = new ViewInstitution();
        $viewinstitution->institutions_id = $institution->id;


        if (Auth::check())
        {
            if (Auth::user()->rols_id == $this->ROL_COMMUNITY) {

                $viewinstitution->users_id = Auth::user()->id;
                $result = DB::table('view_institutions')->where('institutions_id', '=', $institution->id )->where('users_id', '=', $viewinstitution->users_id )->orderby('created_at', 'DESC')->take(1)->get();

                

                if (sizeof($result) != 0) {

                    $aux_object = $result[0];
                    $fecha_ant = $aux_object->created_at;

                    $fecha_act = date('Y-m-d H:i:s');

                    $carbon1 = new Carbon($fecha_ant);
                    $carbon2 = new Carbon($fecha_act);

                    if ($carbon1->diffInMonths($carbon2) > 0) {
                        //return 'saved-1=> <br/> MES ANT: ' . $fecha_ant . '<br/> MES ACT: ' . $fecha_act . '<br/> Diferen:: ' . $carbon1->diffInMonths($carbon2);
                        $viewinstitution->save();
                    }else{
                        //return 'saved-12=> <br/> MES ANT: ' . $fecha_ant . '<br/> MES ACT: ' . $fecha_act . '<br/> Diferen:: ' . $carbon1->diffInMonths($carbon2);

                    }
                }else{
                    //return 'saved-2';
                    //return 'saved-2=> <br/> MES ANT: ' . $fecha_ant . '<br/> MES ACT: ' . $fecha_act . '<br/> Diferen:: ' . $carbon1->diffInMonths($carbon2);
                    $viewinstitution->save();
                }
            }
        }
        

        return view('institutions.show', compact('institution', 'user', 'publications', 'type_institutions', 'ofertasacademicas', 'contact'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        //return $id;
        //return 'vine a editar';


        if (Auth::check())
        {

            if (Auth::User()->rols_id != $this->ROL_INSTITUCION){

                return redirect('/')->withErrors(['errors'=>'Acceso denegado. Sus credenciales no pertenecen a una institución registrada.'])->withInput(); 
            }

            // Si tenemos sesión activa mostrará la página de inicio
            $user = Auth::User();
            if ($user->id != $id){
                return redirect('/')->withErrors(['errors'=>'Acceso denegado.'])->withInput();
            }else{

                $institution = Institution::where('users_id', Auth::User()->id)->first();
                $type_institutions = TypeInstitution::where('id', $institution->type_institutions_id)->first();

                $phone = new Phone();
                $phone->number = null;

                if (Phone::where('users_id', Auth::User()->id)->exists()) {
                    $phone = Phone::where('users_id', Auth::User()->id)->first();
                }

                $ofertasacademicas = LevelOfEducation::join('programs', 'level_of_educations.id', '=', 'programs.level_of_educations_id')->orderBy('level_of_educations.id','ASC')->distinct('level_of_educations.id')->select('level_of_educations.*')->get();
                $count_followers = count(auth()->user()->unreadNotifications);

                return view('institutions.edit', compact('institution', 'user', 'phone', 'type_institutions', 'ofertasacademicas', 'count_followers'));

            }

        }else{
            return redirect('/')->withErrors(['errors'=>'No fue posible autenticar tu cuenta.'])->withInput();
        }          
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Institution $institution)
    {
        //return $request;
        //return $institution;

        if (Auth::check())
        {
            if (Auth::User()->rols_id != $this->ROL_INSTITUCION ||  $institution->users_id !=Auth::User()->id) {
                return redirect('/')->withErrors(['errors'=>'No fue posible autenticar tu cuenta.'])->withInput(); 
            }

            $mensaje = "";



            $ruta_avatar_ant = Auth::User()->avatar;
            

            $user = Auth::User();

            $address = $request->input('address');
            if( $address != null && $institution->address != $address){
                $institution->address = $address;
                $mensaje = $mensaje . '\nDirección actualizada.';
            }
            
            $description = $request->input('description');
            if( $description != null && $institution->description != $description){
                $institution->description = $description;
                $mensaje = $mensaje . '\nDescripción actualizada.';
            }

            // Locations
            $latitude = $request->input('latitude');
            if( $latitude != null && $institution->latitude != $latitude){
                $institution->latitude = $latitude;
                $mensaje = $mensaje . '\nLatitud actualizada.';
            }
            $longitude = $request->input('longitude');
            if( $longitude != null && $institution->longitude != $longitude){
                $institution->longitude = $longitude;
                $mensaje = $mensaje . '\nLongitud actualizada.';
            }

            $ulr_inscriptions = $request->input('ulr_inscriptions');
            if( $ulr_inscriptions != null && $institution->ulr_inscriptions != $ulr_inscriptions){
                $institution->ulr_inscriptions = $ulr_inscriptions;
                $mensaje = $mensaje . '\nURL Inscripción actualizada.';
            }

            $institution->fill($request->except('avatar_cover'));
            $route_img = '/images/users/institutions/'.$institution->slug .'/profile/';
            if($request->hasFile('avatar_cover')){

                //Borramos la foto anterior
                if ( $institution->avatar_cover != $this->AVATAR_COVER ) 
                {

                    $file_path = public_path().$institution->avatar_cover;
                    \File::delete($file_path);

                }

                $file = $request->file('avatar_cover');
                
                //$name = time().$file->getClientOriginalName();
                $name = 'profile_cover_' . Auth::User()->id . time() . '.jpg';
                $institution->avatar_cover = $route_img . $name;
                $file->move(public_path() .  $route_img , $name);

                $mensaje = $mensaje . '\nFoto de portara actualizada.';

            }


            if($request->hasFile('avatar')){

                //Borramos la foto anterior
                if ( $user->avatar != $this->AVATAR ) 
                {

                    $file_path = public_path().$user->avatar;
                    \File::delete($file_path);

                }

                $file = $request->file('avatar');
                $name = 'profile_avatar' . Auth::User()->id . time() . '.jpg';
                $user->avatar = $route_img . $name;
                $file->move(public_path() . $route_img , $name);

                $mensaje = $mensaje . '\nFoto de perfil actualizada.';

            }

            $password1 = $request->input('password1');
            $password2 = $request->input('password2');
            //bcrypt($request->input('password'))
            if ($password1 != null && $password2 != null) {

                $password2 = Hash::make($password2);

                if (password_verify($password1, $user->password)) {
                    $user->password = $password2;
                    $user->save();
                    $mensaje = $mensaje . '\nContraseña actualizada. ';

                } else {
                    $mensaje = $mensaje . '\nLa contraseña no es válida. ';
                }

            }

            $phone = $request->input('phone');
            if ($phone != null) {

                $myphone = Phone::where('users_id', Auth::User()->id)->first();

                if (Phone::where('number', $phone)->count() == 0) {

                    if(Phone::where('users_id', Auth::User()->id)->count() == 0){
                        $newphone = new Phone();
                        $newphone->number = $phone;
                        $newphone->users_id = Auth::user()->id;
                        
                    }else{
                        $newphone = Phone::where('users_id', Auth::User()->id)->first();
                        $newphone->number = $phone;
                        
                    }

                        $newphone->save();
                        $mensaje = $mensaje . '\nSe agrego número de contacto.';
                    
                }else{
                    if ($myphone->number != $phone) {

                        return redirect('/institutions')->withErrors(['errors'=>'No esta disponible el número ingresado.' ])->withInput();
                    }
                }

            }

            
            $link = str_replace("https://www.youtube.com/watch?v=", "", $request->input('URL_video_present'));

            $institution->URL_video_present = $link;

            $user->save();
            $institution->save();


            return redirect('institutions')->with('message', $mensaje);
        }else{
            return redirect('/')->withErrors(['errors'=>'Acceso denegado.'])->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function ranking()
    {

        $user = Auth::user();
        $contenedor = array();

        $fecha_actual = Carbon::now();
        $fecha_actual->day = 1;
        


        $institutions = DB::table('view_institutions')
                        ->join('institutions', 'view_institutions.institutions_id' , '=', 'institutions.id')
                        ->join('users', 'institutions.users_id' , '=', 'users.id')
                        ->select('institutions.id', 'users.avatar', 'users.username', 'users.name')
                        ->where('view_institutions.created_at', '>=', $fecha_actual)
                        ->distinct()
                        ->get();
    


        foreach ($institutions as $institution) {
            
            $count = DB::table('view_institutions')
                        ->where('view_institutions.institutions_id', '=', $institution->id)
                        ->where('view_institutions.created_at', '>=', $fecha_actual)
                        ->count();
            

                $object = (object) [
                            'avatar'   => $institution->avatar,
                            'username' => $institution->username,
                            'count'    => number_format($count,0,",","."),
                            
                        ];
                        
                array_push($contenedor, $object);
            
        }

        

        for ($i=1; $i < sizeof($contenedor) ; $i++) { 
            for ($j = 0; $j < sizeof($contenedor) - 1 ; $j++) { 

                if ($contenedor[$j]->count < $contenedor[$j+1]->count) {

                    $object = (object) $contenedor[$j];
                    $contenedor[$j]   = $contenedor[$j+1];
                    $contenedor[$j+1] = $object;
                }

            }
        }
        

        return view('ranking.index', compact('contenedor', 'user'));
    }

    public function view_maps($username = '')
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        $institution = Institution::where('slug', $username)->first();
        if ($institution) {

            if ($institution->latitude == 0 && $institution->longitude ==0) {
                
                return redirect($this->redirect_institution());
            }else{
                
                $user  = User::where('username', $username)->first();
                $town  = Town::where('id', $institution->towns_id)->first();
                $state = State::where('id', $town->states_id)->first();

                return view('institutions.content_extra.googlemaps', compact('user', 'institution', 'town', 'state'));
            }
            
        }else{
            return redirect($this->redirect_institution());
        }
        
        
    }

    public function presentacion ($username = '')
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        if ( $institution = Institution::where('slug', $username)->count() == 0 ) {

            return back();

        }

        $user   = User::where('username', $username)->first();       
        $institution = Institution::where('users_id', $user->id)->first();
        
        $town        = Town::where('id', $institution->towns_id)->first();
        $state       = State::where('id', $town->states_id)->first();

        $programs    = Program::where('institutions_id', $institution->id)->count();
        $phone       = Phone::where('users_id', $user->id)->first();

        

        return view('presentinstitution.index', 
                    compact('user', 'institution', 'programs', 'phone', 'town', 'state') );
    }

    public function notificacion_followers( $institution = 0 )
    {
        /*
        FALSE = 0
        TRUE  = 1
        */

        $followers = Favourite::where('institutions_id', '=', $institution)
            ->where('confirmed', '=', false)
            ->get();

        $mostrar = false;
        if (sizeof($followers) > 0) {
            $mostrar = true;
        }

        $object = [
                    'add'   => $mostrar,
                    'list' => sizeof($followers),
                ];


        return $object;
    }

    public function list_followers()
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        if (!Institution::where('users_id', Auth::User()->id)->exists()) {
            return redirect('/institutions')->withErrors(['errors'=>'Acceso denegado. Sus credenciales no pertenecen a una institución registrada.'])->withInput();
        }

        $user = Auth::User();
        $institution = Institution::where('users_id', Auth::User()->id)->first();
        $type_institutions = TypeInstitution::where('id', $institution->type_institutions_id)
                            ->first();
        $count_followers = count(auth()->user()->unreadNotifications);

        $ListPersons = DB::Table('favourites')
            ->join('users' , 'favourites.users_id', '=', 'users.id' )
            ->join('programs' , 'favourites.programs_id', '=', 'programs.id' )

            ->join('phones', 'users.id', '=', 'phones.users_id')
            ->join('user_communities', 'users.id', '=', 'user_communities.users_id')
            ->join('genders' , 'user_communities.genders_id', '=', 'genders.id' )
            ->join('towns', 'user_communities.towns_id', '=', 'towns.id')
            ->join('states', 'towns.states_id', '=', 'states.id')

            ->where('favourites.institutions_id', '=', $institution->id)
            ->select(
                'users.name AS name', 
                'users.email AS email', 
                'user_communities.birthdate AS date', 
                'programs.name AS carrera', 
                'phones.number AS phone', 
                'genders.name AS gender', 
                'towns.name AS town', 
                'states.name AS state', 
                'favourites.created_at AS visito'
            )
            ->OrderBy('programs.name', 'ASC')
            ->OrderBy('states.name', 'ASC')
            ->OrderBy('towns.name', 'ASC')
            ->OrderBy('favourites.created_at', 'DESC')
            ->paginate($this->N_PAGINATIONS);

        $fecha_actual = Carbon::now();
        DB::Table('notifications')
            ->where('notifiable_id', '=' , $user->id)
            ->where('read_at', '=' , null)
            ->update(['read_at' => $fecha_actual]);


        //return $ListPersons;
        return view('institutions.content_extra.lista_followers', 
                    compact('institution', 'user', 'type_institutions', 'count_followers', 'ListPersons'));

    }

    public function redirect_institution()
    {
        if (Auth::User()->rols_id != $this->ROL_INSTITUCION) {
            
            return '/institutions/' . Auth::User()->username;
        }else{
            
            return '/institutions';
        }
    }

    

}

