<?php

namespace Delphos\Http\Controllers;


use DB;
use Hash;
use Response;
use Delphos\User;
use Delphos\Phone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Delphos\Http\Requests\StoreAdministradoresDelphosRequest;
use Delphos\Notifications\AdministrativosDelphos;

use Jenssegers\Date\Date;

use Delphos\Town;
use Delphos\State;
use Delphos\Institution;



class DelphosController extends Controller
{
    var $ROL_COMMUNITY   = 1; //ROL INSTITUCION
    var $ROL_DELPHOS     = 2; //ROL INSTITUCION
    var $ROL_INSTITUCION = 3; //ROL INSTITUCION
    var $ROL_SUPERUSER   = 4; //ROL INSTITUCION

    var $URL_AVATAR_DEFAULT = '/images/img_default/default.jpg';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');


        $user = Auth::user();

        if ($user->rols_id != $this->ROL_DELPHOS) {
            return back();
        }
    
        $instituciones = DB::table('users')->where('rols_id', '=', $this->ROL_INSTITUCION)
                            ->join('phones', 'users.id', 'phones.users_id')
                            ->select('users.name', 'users.username', 'users.created_at', 'phones.number')
                            ->orderby('name')
                            ->orderby('created_at')
                            ->paginate(10);
        return view('delphos.index', compact('user', 'instituciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        $user = Auth::user();
        if ($user->rols_id != $this->ROL_DELPHOS && $user->username != 'admin') {
            return back();
        }

        return view('delphos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAdministradoresDelphosRequest $request)
    {
        $porciones = explode(" ", ucwords( $request->input('name') ));
        

        $user = new User();
        $user->name     = ucwords( strtolower( $request->input('name') ) );
        $user->email    = strtolower( $request->input('email') );
        $user->password = Hash::make( $request->input('password'));
        $user->avatar   = $this->URL_AVATAR_DEFAULT;
        $user->username = $porciones[0].time();
        $user->rols_id = $this->ROL_DELPHOS;
        $user->save();

        $phone = new Phone();
        $phone->number   = $request->input('number');
        $phone->users_id = $user->id;
        $phone->save();

        
        $user->notify(new AdministrativosDelphos($user));

        return redirect('/administrativos/create')
                    ->with(['message'=> 'Nuevo Usuario\n Nombre: ' . $user->name . '\n Correo: ' . $phone->number])
                    ->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();

        if ($user->rols_id != $this->ROL_DELPHOS) {
            return back();
        }

        $phone = DB::table('phones')->where('users_id', '=', $user->id)->first();

        return view('delphos.show', compact('user', 'instituciones', 'phone'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();

        if ($user->rols_id != $this->ROL_DELPHOS && $user->rols_id != $id) {
            return back();
        }

        $phone = DB::table('phones')->where('users_id', '=', $user->id)->first();

        return view('delphos.edit', compact('user', 'instituciones', 'phone'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $username)
    {
        
        $user = User::where('username', '=', $username)->first();

        $msg = '\nDatos Actualizados:\n.... ... .. .\n';
        if ($this->update_phone($user->id, $request->number)) {
            $msg = $msg . '\n Celular Actualizado.';
        }
        if ($this->update_email($user->id, $request->email)) {
            $msg = $msg . '\n Correo Actualizado.';
        }
        
        if ($request->password != '' && $request->password_confirmation != '') {
            if ($request->password == $request->password_confirmation) {
                $user->password = Hash::make( $request->input('password'));
                $user->save();
                $msg = $msg . '\n Contraseña modificada.';
            }else{
                $msg = $msg . '\n Las contraseña no coinciden.';
            }
        }



        //Avatar
        $route_img = '/images/users/Administradores/' . $user->username .'/';
        if($request->hasFile('avatar')){

                        //Borramos la foto anterior
            if ( $user->avatar != $this->URL_AVATAR_DEFAULT ) 
            {

                $file_path = public_path().$user->avatar;
                \File::delete($file_path);

            }


            $file = $request->file('avatar');
                        //$name = time().$file->getClientOriginalName();

            $name = 'profile_avatar' . Auth::User()->id . time() . '.jpg';
            $user->avatar = $route_img . $name;
            $file->move(public_path() . $route_img , $name);
            $user->save();


            $msg = $msg . '\nFoto de perfil actualizada.';

        }
        

        return redirect('/administrativos/' . $user->username)
                    ->with(['info'=> $msg])
                    ->withInput();
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function update_phone($iduser, $number = ' ')
    {
        
        $phone = Phone::where('users_id', '=', $iduser)->first();

        $aux_phone = DB::table('phones')->where('number', '=', $number)->first();

        if ($aux_phone) {
            
            if ($aux_phone->users_id != $iduser) {
                return false;
            }else{
                if ($aux_phone->users_id == $iduser) {
                    return false;
                }
            }
            
        }
        $phone->number = $number;
        $phone->save();
        return true;
        
    }

    public function update_email($iduser, $email = '' )
    {
        $user = User::where('id', '=', $iduser)->first();

        $aux_user = DB::table('users')
                        ->where('email','=', $email)
                        ->first();

        if ($aux_user) {
            
            if ($aux_user->id != $iduser) {
                return false;
            }else{
                if ($aux_user->id == $iduser) {
                    return false;
                }
            }
            
        }
        $user->email = $email;
        $user->save();
        return true;
        
    }



    public function reporte_mes_adm($username = '')
    {
        
        $user = Auth::user();

        $fecha_actual = $carbon = Date::now();
        $fecha_actual->month = $fecha_actual->month -1;
        $fecha_actual->day = 1;
        //return '// '. $fecha_actual;
        
        $institucion = DB::table('institutions')
                        ->where('slug', '=', $username)
                        ->first();
        if ($institucion) {
            $user_inst   = DB::table('users')->where('username', '=', $username)->first();
        }else{
            
            $user_inst   = DB::table('users')->where('name', '=', $username)->first();

            if ($user_inst) {
                $institucion = DB::table('institutions')
                        ->where('institutions.users_id', '=', $user_inst->id)
                        ->first();    
            }else{

                return back()->withErrors(['errors'=>'No encontramos información para: ' . $username])->withInput();

            }

            
        }

        
        $town        = DB::table('towns')->where('id', '=', $institucion->towns_id)->first();
        $state       = DB::table('states')->where('id', '=', $town->states_id)->first();

        $contenedor = array();

        /* Usuarios que han dado click a link inscripcion - MES ACTUAL*/
        $data_click_inscriptions = DB::table('click_inscriptions')
                    ->where('click_inscriptions.institutions_id', '=', $institucion->id)
                    ->where('click_inscriptions.created_at', '>=', $fecha_actual)
                    ->count();

        $object = (object) [
            'label' => 'Clicked Inscripción ', 
            'count' => $data_click_inscriptions,
        ];
        array_push($contenedor, $object);


        /* Favoritos */
        $data_favoritos = DB::Table('favourites')
            ->where('favourites.institutions_id', '=', $institucion->id)
            ->where('favourites.created_at', '>=', $fecha_actual)
            ->count();

        $object = (object) [
            'label' => 'Favoritos', 
            'count' => $data_favoritos,
        ];
        array_push($contenedor, $object);


        /* Vistas perfil INSTITUCION */
        $data_institucion = DB::table('view_institutions')
            ->where('view_institutions.institutions_id', '=', $institucion->id)
            ->where('view_institutions.created_at', '>=', $fecha_actual)
            ->count();

        $object = (object) [
            'label' => 'Views Institución', 
            'count' => $data_favoritos,
        ];
        array_push($contenedor, $object);


        /* Total de vistas publicaciones */
        $data_views = DB::table('views')
            ->where('views.institutions_id', '=', $institucion->id)
            ->where('views.created_at', '>=', $fecha_actual)
            ->count();

        $object = (object) [
            'label' => 'Views Publicación', 
            'count' => $data_views,
        ];
        array_push($contenedor, $object);

        $fecha_facturada = ucwords($fecha_actual->format('F Y'));

        return view('delphos.factura', compact('user', 'contenedor', 'institucion', 'user_inst', 'fecha_facturada', 'town', 'state'));

    }

    
}
