<?php


namespace Delphos\Http\Controllers;

use DB;
use Hash;
use Response;
use Delphos\User;
use Delphos\Town;
use Delphos\State;
use Delphos\Phone;
use Delphos\Gender;
use Delphos\Materia;
use Delphos\Program;
use Delphos\Favourite;
use Delphos\Institution;
use Delphos\Publication;
use Delphos\UserCommunity;
use Illuminate\Http\Request;
use Delphos\NotificationPublication;
use Illuminate\Support\Facades\Auth;
use Delphos\MateriasAFinesProgramas;
use Delphos\MateriasAFinesCommunities;
use Delphos\Notifications\MailWelcome;
use Delphos\Http\Requests\StoreUserCommunityRequest;
use Delphos\Http\Requests\StoreUserCommunityUpdateRequest;


class UserCommunityController extends Controller
{

    var $ROL_COMMUNITY   = 1; //ROL INSTITUCION
    var $ROL_DELPHOS     = 2; //ROL INSTITUCION
    var $ROL_INSTITUCION = 3; //ROL INSTITUCION
    var $ROL_SUPERUSER   = 4; //ROL INSTITUCION

    var $URL_AVATAR_DEFAULT = '/images/img_default/default.jpg';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth', ['except' => [
                                        'create', 
                                        'store', 
                                        'fav_institutions',
                                        'comparador_principal',
                                        'comparador_result'
                                    ]]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        //return 'Bienvenido a UserCommunityController.';
        
        if ( Auth::User()->rols_id != $this->ROL_COMMUNITY ) {

            return redirect('/')->withErrors(['errors'=>'Acceso denegado. Sus credenciales no pertenecen a una usuario registrado.'])->withInput();
        }

        $user = Auth::User();
        $publicaciones = array();

        $ubicaciones = DB::table('towns')
                            ->join('institutions', 'towns.id', '=', 'institutions.towns_id')
                            ->select('towns.id', 'towns.name')
                            ->Distinct()
                            ->OrderBy('towns.name', 'ASC')
                            ->get();
        //return $ubicaciones;

        foreach ( $ubicaciones as $location ) {
            $list_institutions = DB::table('institutions')
                            ->join('users', 'institutions.users_id', '=', 'users.id')
                            ->select('users.id', 'users.avatar', 'users.username', 'users.name', 'institutions.id AS idInstitution')
                            ->where('institutions.towns_id' , '=', $location->id)
                            ->get();
            
            //return $list_institutions;
            
            $contenido = array();
        
            foreach ($list_institutions as $inst ) 
            {
                $publications = DB::table('publications')
                            ->select('publications.slug', 'publications.title', 'publications.description', 'publications.picture' )
                            ->where('institutions_id', '=', $inst->idInstitution)
                            ->orderby('publications.created_at','DESC')
                            ->take(1)
                            ->get();
                
                
                if (sizeof ($publications) > 0) {
                    $object = (object) [
                        'institutions' => $inst,
                        'publications' => $publications[0]
                    ];
                    array_push($contenido, $object);
                }
                
            }

            //return $contenido;

            if (sizeof ($contenido) > 0) {
                $object = (object) [
                    'idtown'       => $location->id,
                    'name_town'    => $location->name,
                    'contenido' => $contenido
                ];
                array_push($publicaciones, $object);
            }

        }



        $notificaciones = DB::table('notification_publications')
                                ->Where('users_id', '=', $user->id)
                                ->Where('confirmed', '=', false)
                                ->get();

        
        //return $notificaciones;
        //return DB::table('notification_publications')->get();
                                
        return view('usercommunity.index', compact('publicaciones', 'notificaciones') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        //return 'Bienvenido a UserCommunityController/create.';
         if ( Auth::check() ) {

            return redirect('/community')->withErrors(['errors'=>'Acceso denegado. Ya tienes una secion.'])->withInput();
        }

        $states = State::Select('id', 'name')->get();
        $genders = Gender::Select('id', 'name')->get();

        return view('usercommunity.create', compact('states', 'genders'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /*Crea o registra la informacion en la base de datos*/
    public function store(StoreUserCommunityRequest $request)
    {

        if ( Auth::check() ) {

            return redirect('/community')->withErrors(['errors'=>'Acceso denegado. Ya tienes una secion.'])->withInput();
        }


        //return $request;
        $_token         = $request->input('_token');
        $name           = ucwords( strtolower( $request->input('name') ));
        $username       = strtolower( $request->input('username') );

        $number         = $request->input('number');
        
        $email          = strtolower( $request->input('email') );
        $password       = $request->input('password');
        $departamento   = $request->input('departamento');
        $municipio      = $request->input('municipio');
        
        $birthdate      = $request->input('birthdate');
        $birthdate      = date("Y-m-d",strtotime($birthdate));

        $genero      = $request->input('genero');

        $terminos       = $request->input('terminos');
        $rols_id        = $this->ROL_COMMUNITY;


        // strtoupper('cadena') -> pasa a MAYUSCULAS
        // strtolower($string)  -> pasa a MINUSCULAS
        // ucwords($string)     -> Las primeras en Mayuscula 



        if (User::where('email', $email)->count() > 0) {

            return redirect('/community/create')->withErrors(['errors'=> $email . ' No esta disponible.'])->withInput();   

        }else{

            $firstname = explode(" ", $name);
            $slug = str_replace(" ","-",$name);

            //Creamos User
            $user = User::create([
                'name' => $name,
                'username' => $username,
                'email' => $email,
                'remember_token' => $_token,
                'avatar' => $this->URL_AVATAR_DEFAULT ,
                'password' => Hash::make($password),//bcrypt($password),
                'rols_id' => 1,
            ]);

            $phone = new Phone();
            $phone->number   = $number;
            $phone->users_id = $user->id;
            $phone->save();

            
            //Aqui creamos usercommunity
            $slug = $slug . $user->id;
            $usercommunity = new UserCommunity();
            $usercommunity->birthdate  = $birthdate;
            $usercommunity->genders_id = $genero;
            $usercommunity->slug       = $user->username;
            $usercommunity->users_id   = $user->id;
            $usercommunity->towns_id   = $municipio;
            $usercommunity->save();

            $credentials = ['email' => $email, 'password' => $password];

            
            /* Correo de Bienvenida */
            //$user->notify(new MailWelcome($user));


            if (Auth::attempt($credentials)) {
                return redirect('/question-one');
            }else{
                return redirect('/')->withErrors(['errors'=>'No fue posible autenticar tu cuenta.'])->withInput();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        //return 'mostrar user community';
        

        if ( Auth::check() ) {

            if ( Auth::User()->rols_id == $this->ROL_COMMUNITY ) {

                if (UserCommunity::where('users_id', $id)->count() > 0 && Auth::User()->id == $id) {

                    $user = Auth::User();
                    $usercommunity = UserCommunity::where('users_id', $id)->first();
                    $municipio = Town::where('id', $usercommunity->towns_id)->first();
                    $departamento = State::where('id', $municipio->states_id)->first();
                    $genero = Gender::where('id', $usercommunity->genders_id)->first();
                    $phone = new Phone();
                    $phone->name = '(Sin asignar.)';

                    $notificaciones = DB::table('notification_publications')
                                ->Where('users_id', '=', $user->id)
                                ->Where('confirmed', '=', false)
                                ->get();


                    $ilike = Favourite::where('users_id', $user->id)->where('confirmed', true)->count();

                    if (Phone::where('users_id', Auth::User()->id)->count() > 0) {
                        $phone = Phone::where('users_id', Auth::User()->id)->first();
                    }

                   return view('usercommunity.show', compact('user', 'usercommunity', 'departamento', 'municipio', 'phone', 'genero', 'ilike', 'notificaciones'));
                }else{
                    return redirect('/')->withErrors(['errors'=>'No existen registro de este usuario.'])->withInput();
                }
                
            }

        }

        return redirect('/')->withErrors(['errors'=>'No es posible mostrar información respecto al usuario dado.'])->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        //return 'editar';
        if ( Auth::check() ) {

            if ( Auth::User()->rols_id == $this->ROL_COMMUNITY && Auth::User()->id == $id) {

                if (UserCommunity::where('users_id', $id)->count() > 0 ) {
                    
                    $usercommunity = UserCommunity::where('users_id', $id)->first();
                    $genero = Gender::where('id', $usercommunity->genders_id)->first();
                    
                    $municipio = Town::where('id', $usercommunity->towns_id)->first();
                    $departamento = State::where('id', $municipio->states_id)->first();
                    $phone = new Phone();
                    $phone->name = '(Sin asignar.)';

                    if (Phone::where('users_id', Auth::User()->id)->count() > 0) {
                        $phone = Phone::where('users_id', Auth::User()->id)->first();
                    }

                    $notificaciones = DB::table('notification_publications')
                                ->Where('users_id', '=', Auth::user()->id)
                                ->Where('confirmed', '=', false)
                                ->get();

                    return view('usercommunity.edit', compact('usercommunity', 'genero', 'municipio','departamento', 'phone', 'notificaciones'));
                }else{
                    return redirect('/')->withErrors(['errors'=>'No existen registro de este usuario.'])->withInput();
                }
                
            }

        }

        return redirect('/')->withErrors(['errors'=>'No es posible mostrar información respecto al usuario dado.'])->withInput();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        //return 'llegue';
        
        
        //return $request;
        $mensaje = 'Se omitiran aquellos campos vacios y se actualizaran los que contengan información.\n';

        if ( Auth::check() ) {

            if (UserCommunity::where('slug', $slug)->count() == 0) {

                return redirect('/')->withErrors(['errors'=>'Acceso denegado. Sus credenciales no permiten actualizar un perfil ajeno.'])->withInput();
            }

            $usercommunity = UserCommunity::where('slug', $slug)->first();
            $user = User::find($usercommunity->users_id);

            if ( Auth::User()->rols_id == $this->ROL_COMMUNITY && 
                Auth::User()->id == $user->id) { 
                    
                    
                    $genero = Gender::where('id', $usercommunity->genders_id)->first();
                    
                    $municipio = Town::where('id', $usercommunity->towns_id)->first();
                    $departamento = State::where('id', $municipio->states_id)->first();
                    $phone = new Phone();
                    $phone->name = '(Sin asignar.)';

                    if (Phone::where('users_id', Auth::User()->id)->count() > 0) {
                        $phone = Phone::where('users_id', Auth::User()->id)->first();
                    }

                    $route_img = '/images/users/community/' . $usercommunity->slug .'/';
                    if($request->hasFile('avatar')){
                        
                        //Borramos la foto anterior
                        if ( $user->avatar != $this->URL_AVATAR_DEFAULT ) 
                        {
                            
                            $file_path = public_path().$user->avatar;
                            \File::delete($file_path);

                        }


                        $file = $request->file('avatar');
                        //$name = time().$file->getClientOriginalName();

                        $name = 'profile_avatar' . Auth::User()->id . time() . '.jpg';
                        $user->avatar = $route_img . $name;
                        $file->move(public_path() . $route_img , $name);


                        $mensaje = $mensaje . '\nFoto de perfil actualizada.';

                    }

                    if ($request->input('username') != null && $user->username != $request->input('username')) {

                        $user->username = $request->input('username');
                        $mensaje = $mensaje . '\nUsername actualizado.';
                    }

                    if ($request->input('name') != null && $user->name != $request->input('name')) {
                        $user->name = $request->input('name');
                        $mensaje = $mensaje . '\nNombre actualizado.';
                    }

                    if ($request->input('email') != null && $user->email != $request->input('email')) {
                        if (User::where('email', $request->input('email'))->count() == 0) {
                            $user->name = $request->input('email');
                            $mensaje = $mensaje . '\nCorreo actualizado.';
                        }else{

                            if($request->input('email') != $user->email){
                                $mensaje = $mensaje . '\nCorreo no disponible.';
                                return redirect('/community/'.$user->id)->withErrors(['errors'=>'El correo no esta disponible.'])->withInput();
                            }
                        }
                    }

                    if ($request->input('password1') != null && $request->input('password2') != null ) {

                        if (password_verify($request->input('password1'), Auth::User()->password )) {

                            if (strlen($request->input('password2')) > 5 ) {

                                $user->password = Hash::make($request->input('password2'));
                                $mensaje = $mensaje . '\nContraseña actualizado.';

                            }else{
                                $mensaje = $mensaje . '\nLas contraseñas no coinciden.';
                                return redirect('/community/'.$user->id)->withErrors(['errors'=>'La contraseña debe tener por lo menos 6 caracteres.'])->withInput();
                            }

                        } else {
                            
                            return redirect('/community/'.$user->id)->withErrors(['errors'=>'La contraseña antigua no es válida.'])->withInput();
                        }
                        
                    }

                    if ($request->input('phone') != null) {

                        if(Phone::where('number', $request->input('phone'))->count() != 0 ){

                            if(Phone::where('users_id', $user->id)->where('number', $request->input('phone'))->count() > 0 ){

                                $phone  = Phone::where('users_id', $user->id)->first();

                                if ($phone->number != $request->input('phone')) {
                                    $mensaje = $mensaje . '\nNúmero de contacto actualizado.';
                                }

                                
                                $phone->number   = $request->input('phone');
                                $phone->users_id = $user->id;
                                $phone->save();
                                
                            }else{
                                return redirect('/community/'.$user->id)->withErrors(['errors'=>'Error número de contacto en uso.'])->withInput();
                            }
                        }else{
                            $phone  = Phone::where('users_id', $user->id)->first();
                            $phone->number   = $request->input('phone');
                            $phone->save();
                            $mensaje = $mensaje . '\nNúmero de contacto actualizado.';
                        }
                    }
            }

            $user->save();
            $usercommunity->save();


            return redirect('/community/'.$user->id)->with('message',  $mensaje);
        }
        
        return redirect('/')->withErrors(['errors'=>'Acceso denegado. No puede actualizar la información de este perfil.'])->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');
        //
    }


    public function misprograms()
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        //return 'aqui';
        if ( Auth::User()->rols_id == $this->ROL_COMMUNITY) {

            $allprogramas = DB::table('users')->join('institutions', 'users.id', '=', 'institutions.users_id')->join('programs', 'institutions.id', '=', 'programs.institutions_id')->join('favourites', 'programs.id', '=', 'favourites.programs_id')->join('duracions', 'programs.duracions_id', '=', 'duracions.id')->select('users.name AS uname', 'users.username AS uusername', 'users.avatar AS uavatar', 'programs.id AS pid', 'programs.name AS pname', 'programs.tiempo AS ptime', 'duracions.name AS dname')->where('favourites.users_id', Auth::User()->id)->where('favourites.confirmed', true)->paginate(8);

            $notificaciones = NotificationPublication::Where('users_id', '=', Auth::User()->id)
                                                   ->Where('confirmed', '=', false)
                                                   ->get();

            return view('usercommunity.misprograms', compact('allprogramas', 'notificaciones'));

        }else{
            return redirect('/');
        }
    }

    /* Comparador */
    public function comparador_principal()
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

         if ( !Auth::check() ) {

            return redirect('/login')->withErrors(['errors'=>'Acceso denegado. Debes Iniciar sesión.'])->withInput();
        }

        $contents = DB::table('favourites')
                ->join('institutions', 'favourites.institutions_id', '=', 'institutions.id')
                ->join('users', 'institutions.users_id', '=', 'users.id')
                ->join('programs', 'favourites.programs_id', '=', 'programs.id')
                ->where('favourites.users_id', Auth::User()->id)
                ->select('users.name AS name','programs.avatar AS avatar' ,'programs.name AS program', 'programs.slug AS slug')
                ->OrderBy('users.name', 'ASC')
                ->OrderBy('programs.name', 'ASC')
                ->get();

        $notificaciones = NotificationPublication::Where('users_id', '=', Auth::User()->id)
                                                   ->Where('confirmed', '', false)
                                                   ->get();

        //return $contents;

        return view('usercommunity.comparar', compact('contents', 'notificaciones'));
    }

    /* Comparador */
    public function comparador_result($slug_a = 0, $slug_b = 0)
    {
        //return 'Comparador: ' . (string)$slug_a . ' Vs ' . (string)$slug_b;

        
        $consulta = "SELECT users.avatar, users.name, users.username, users.email, users.created_at AS creacion_institucion, institutions.address , institutions.description AS institucion_descripcion, institutions.slug AS i_slug, institutions.latitude, institutions.longitude, type_institutions.name AS tipo_institucion, CONCAT(states.name ,', ' ,towns.name) AS ubicacion, programs.id AS idprogram, programs.slug AS program_slug, programs.name AS programs_name, programs.description AS program_description, programs.avatar AS program_avatar, programs.price AS program_price, programs.inscription_price AS program_inscription_price, CONCAT(programs.tiempo, ' ',duracions.name) duracion, programs.credits AS program_credits, programs.codigo_snies AS program_codigo_snies, programs.last_accreditation AS program_last_accreditation, programs.info_intercambios AS program_info_intercambios, programs.info_extranjero AS program_info_extranjero,  programs.url_pensum AS program_url_pensum,  level_of_educations.name AS formacion, modalities.name AS modalidad FROM users INNER JOIN institutions ON (users.id = institutions.users_id) INNER JOIN type_institutions ON (institutions.type_institutions_id = type_institutions.id) INNER JOIN towns ON (institutions.towns_id = towns.id) INNER JOIN states ON (towns.states_id = states.id) INNER JOIN programs ON ( institutions.id = programs.institutions_id ) INNER JOIN level_of_educations ON ( programs.level_of_educations_id = level_of_educations.id ) INNER JOIN modalities ON ( programs.modalities_id = modalities.id ) INNER JOIN duracions ON ( programs.duracions_id = duracions.id ) WHERE programs.slug = ";


        //return $consulta . '"' . $slug_a. '"';


        $institucion_a = DB::Select($consulta . '"' . $slug_a. '"' . '  LIMIT 1');
        $institucion_b = DB::Select($consulta . '"' . $slug_b. '"' . '  LIMIT 1');

        $institucion_a[0]->program_price = $precio = 'COP $' . number_format($institucion_a[0]->program_price,2,",",".");
        $institucion_a[0]->program_inscription_price = $precio = 'COP $' . number_format($institucion_a[0]->program_inscription_price,2,",",".");


        $institucion_b[0]->program_price = $precio = 'COP $' . number_format($institucion_b[0]->program_price,2,",",".");
        $institucion_b[0]->program_inscription_price = $precio = 'COP $' . number_format($institucion_b[0]->program_inscription_price,2,",",".");


        $contenedor = array();
        $list_afines_a = array();
        $list_afines_b = array();
        
        $meterias_afines_a = DB::table('materias')
                ->join('materias_a_fines_programas', 'materias.id', '=', 'materias_id')
                ->where('materias_a_fines_programas.programs_id', $institucion_a[0]->idprogram)
                ->select('materias.nombre')
                ->OrderBy('materias.nombre', 'ASC')
                ->get();


        $meterias_afines_b = DB::table('materias')
                ->join('materias_a_fines_programas', 'materias.id', '=', 'materias_id')
                ->where('materias_a_fines_programas.programs_id', $institucion_b[0]->idprogram)
                ->select('materias.nombre')
                ->OrderBy('materias.nombre', 'ASC')
                ->get();


        foreach ($meterias_afines_a as $objecto_a) {
            array_push($list_afines_a, $objecto_a->nombre);
        }

        foreach ($meterias_afines_b as $objecto_b) {
            array_push($list_afines_b, $objecto_b->nombre);
        }

        $objecta = (object) [
                'index'   => 1,
                'content' => $institucion_a[0],
                'afines'  => $list_afines_a
            ];
        $objectb = (object) [
                'index'   => 2,
                'content' => $institucion_b[0],
                'afines'  => $list_afines_b
            ];
        
        array_push($contenedor, $objecta);
        array_push($contenedor, $objectb);

        return $contenedor;
    }

    /* notificaciones */
    public function notificaciones()
    {
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        $user = Auth::User();

        if ( Auth::User()->rols_id != $this->ROL_COMMUNITY ) {

            return redirect('/')->withErrors(['errors'=>'Acceso denegado. Sus credenciales no pertenecen a una usuario registrado.'])->withInput();
        }

        
        $list_notificaciones = NotificationPublication::Where('users_id', '=', Auth::User()->id)
                            ->OrderBy('confirmed', 'ASC')
                            ->OrderBy('created_at', 'DESC')
                            ->paginate(10);

        $notificaciones = DB::table('notification_publications')
                                ->Where('users_id', '=', $user->id)
                                ->Where('confirmed', '=', false)
                                ->get();

        //return 'tienes ' . sizeof($notificaciones) . ' notificaciones.';

        return view('usercommunity.notificaciones', compact('contents', 'notificaciones', 'list_notificaciones'));
    }
}
