<?php

namespace Delphos\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserCommunityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return [
            'name' => 'Nombre Completo',
            'username' => 'Username',
            'number' => 'Celular',
            'email' => 'Correo electrónico',
            'password' => 'Contraseña',
            'departamento' => 'Departamento',
            'municipio' => 'Municipio',
            'birthdate' => 'Fecha de Nacimiento',
            'genero' => 'Genero',
            'terminos' => 'Terminos y condiciones',
        ];
    }

    public function messages()
    {
        return [
            'name.required'         => 'El campo nombre completo es requerido',
            'username.required'     => 'El campo username es requerido',
            'number.required'       => 'El campo celular es requerido',
            'email.required'        => 'El campo correo electronico es requerido',
            'password.required'     => 'El campo contraseña es requerido',
            'departamento.required' => 'El campo departamento es requerido',
            'municipio.required'    => 'El campo municipio es requerido',
            'birthdate.required'    => 'El campo fecha de nacimiento es requerido',
            'genero.required'       => 'El campo genero es requerido',
            'terminos.required'     => 'El campo terminos y condiciones es requerido',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'         => 'required|string|max:255',
            'username'     => 'required|string|max:255|unique:users',
            'number'       => 'required|string|max:11|unique:phones',
            'email'        => 'required|string|email|max:255|unique:users',
            'password'     => 'required|string|min:6|confirmed',
            'departamento' => 'required|integer',
            'municipio'    => 'required|integer',
            'birthdate'    => 'required|date',
            'genero'       => 'required|integer',
            'terminos'     => 'required|accepted:true',
        ];
    }
}
