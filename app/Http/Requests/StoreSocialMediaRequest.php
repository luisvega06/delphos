<?php

namespace Delphos\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSocialMediaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return [
            'name'     => 'Nombre Red Social',
            'URL'      => 'URL Red Social',
            'username' => 'Username',
        ];
    }

    public function messages()
    {
        return [
            'name.required'         => 'El campo Nombre Red Social es requerido',
            'URL.required'          => 'El campo URL Red Social es requerido',
            'username.required'     => 'El campo Username es requerido',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'       => 'required|string|max:255',
            'URL'        => 'required|string|max:255',
            'username'   => 'required|string|max:255',
        ];
    }
}
