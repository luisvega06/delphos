<?php

namespace Delphos\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProgramUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return [
            'name'                      => 'Nombre',
            'description'               => 'Descripción',
            'price'                     => 'Precio Del Semestre',
            'inscription_price'         => 'Precio Inscripción',
            'tiempo'                    => 'Duración',
            'duracions_id'              => 'Unidad de Tiempo',
            'credits'                   => 'Creditos',
            'codigo_snies'              => 'Codigo SNIES',
            'last_accreditation'        => 'Ultima certificación',
            'info_intercambios'         => 'Información Para Intercambios',
            'info_extranjero'           => 'Información Para Extranjeros',
            'url_pensum'                => 'URL Pensum',
            'level_of_educations_id'    => 'Formación Académica',
            'modalities_id'             => 'Modalidad de Estudio',
            'select-materia-1'          => 'Materia #1',
            'select-materia-2'          => 'Materia #2',
            'select-materia-3'          => 'Materia #3',
        ];
    }

    public function messages()
    {

        return [
            
            'name.required'                      => 'El campo Nombre es requerido.',
            'description.required'               => 'El campo Descripción es requerido.',
            'price.required'                     => 'El campo Precio del Semestre es requerido.',
            'inscription_price.required'         => 'El campo Precio Inscripción es requerido.',
            'tiempo.required'                    => 'El campo Duración es requerido.',
            'duracions_id.required'              => 'El campo Unidad de Tiempo es requerido.',
            'credits.required'                   => 'El campo Creditos es requerido.',
            'codigo_snies.required'              => 'El campo Codigo SNIES es requerido.',
            'last_accreditation.required'        => 'El campo Ultima certificación es requerido.',
            'info_intercambios.required'         => 'El campo Información Para Intercambios es requerido.',
            'info_extranjero.required'           => 'El campo Información Para Extranjeros es requerido.',
            'url_pensum.required'                => 'El campo URL Pensum es requerido.',
            'level_of_educations_id.required'    => 'El campo Formación Académica es requerido.',
            'modalities_id.required'             => 'El campo Modalidad de Estudio es requerido.',

            'select-materia-1.required' => 'Campo Materia #1 requerido.',
            'select-materia-2.required' => 'Campo Materia #2 requerido.',
            'select-materia-3.required' => 'Campo Materia #3 requerido.',

        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                      => 'required|string|max:255',
            'description'               => 'required',
            'price'                     => 'required',
            'inscription_price'         => 'required',
            'tiempo'                    => 'required',
            'duracions_id'              => 'required',
            'credits'                   => 'required',
            'codigo_snies'              => 'required',
            'last_accreditation'        => 'required',
            'info_intercambios'         => 'required',
            'info_extranjero'           => 'required',
            'url_pensum'                => 'required',
            'level_of_educations_id'    => 'required',
            'modalities_id'             => 'required',

            'select-materia-1'          => 'required',
            'select-materia-2'          => 'required',
            'select-materia-3'          => 'required',
        ];
    }
}
