<?php

namespace Delphos\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePublicationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'title.required'        => 'El campo titulo es requerido.',
            'description.required'  => 'El campo descripcion es requerido.',
            'picture.required'      => 'Es necesario adjuntar una imagen.',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max: 255',
            'description' => 'required',
            'picture' => 'required|image',
        ];
    }
}
