<?php

namespace Delphos\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreInstitutionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return [
            'name'                 => 'Nombre',
            'username'             => 'Username',
            'email'                => 'Correo',
            'password'             => 'Contraseña',
            'Tipo'                 => 'Tipo de Institución',
            'Departamento'         => 'Departamento',
            'Municipio'            => 'Municipio',
            'number'               => 'Celular',
        ];
    }

    public function messages()
    {

        return [
            
            'name.required'                 => 'El campo Nombre es requerido.',
            'username.required'             => 'El campo Username es requerido.',
            'email.required'                => 'El campo Correo es requerido.',
            'password.required'             => 'El campo Contraseña es requerido.',
            'Tipo.required'                 => 'El campo Tipo de Institución es requerido.',
            'Departamento.required'         => 'El campo Departamento es requerido.',
            'Municipio.required'            => 'El campo Municipio es requerido.',
            'number.required'               => 'El campo celular es requerido',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                 => 'required|string|max:255',
            'username'             => 'required|string|max:30|unique:users',
            'email'                => 'required|string|email|max:255|unique:users',
            'password'             => 'required|string|min:6|confirmed',
            'Tipo'                 => 'required|integer',
            'Departamento'         => 'required|integer',
            'Municipio'            => 'required|integer',
            'number'               => 'required|string|max:11|unique:phones',
        ];

    }
}
