<?php

namespace Delphos\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAdministradoresDelphosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return [
            'name'                 => 'Nombre',
            'email'                => 'Correo',
            'password'             => 'Contraseña',
            'number'               => 'Celular',
        ];
    }

    public function messages()
    {

        return [
            
            'name.required'                 => 'Campo Nombre: es requerido.',
            'email.required'                => 'Campo Correo: es requerido.',
            'password.required'             => 'Campo Contraseña: es requerido.',
            'number.required'               => 'Campo Celular: es requerido',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                 => 'required|string|max:255',
            'email'                => 'required|string|email|max:255|unique:users',
            'password'             => 'required|string|min:6|confirmed',
            'number'               => 'required|string|max:11|unique:phones',
        ];

    }
}
