<?php

namespace Delphos;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class Institution extends Model
{

    protected $table = 'institutions';

    //Campos que se pueden actualizar
    protected $fillable = [
        'id',
        'avatar_cover',
        'description',
        'address',
        'ulr_inscriptions',
        'latitude',
        'longitude',
        'slug',
        'users_id',
        'towns_id',
        'type_institutions_id',
        'URL_video_present',
    	
	];

    public function getRouteKeyName()
    {
        return 'slug';
    }
    
    public function user() //Uno a muchos (inverso)

    {
        return $this->belongsTo('Delphos\User', "users_id", "id");
    }

    public function town() //Uno a muchos (inverso)

    {
        return $this->belongsTo('Delphos\Town', "towns_id", "id");
    }

    public function typeinstitution() //Uno a muchos (inverso)

    {
        return $this->belongsTo('Delphos\TypeInstitution');
    }

    public function publications() // Relacion de 1 : N

    {
        return $this->hasMany('Delphos\Publication');
    }

    public function programs() //Uno a muchos (inverso)

    {
        return $this->hasMany('Delphos\Program');
    }

    //use Jenssegers\Date\Date;
    public function getCreate()
    {
        Date::setLocale('es');
        return new Date($this->created_at);

    }

    public function getUpdate()
    {
        Date::setLocale('es');
        return new Date($this->updated_at);

    }

}
