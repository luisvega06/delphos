<?php

namespace Delphos;

use Illuminate\Database\Eloquent\Model;

use Jenssegers\Date\Date;

class UserCommunity extends Model
{

    protected $table = 'user_communities';
    
    //Campos que se pueden actualizar
    protected $fillable = ['avatar', 
        'birthdate', 
        'slug',
        'created_at',
        'updated_at',
        'users_id',
        'genders_id',
        'towns_id', 
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }
    
    public function user() //Uno a muchos (inverso)

    {
        return $this->belongsTo('Delpos\User');
    }

    public function gener() //Uno a muchos (inverso)

    {
        return $this->belongsTo('Delpos\Gender');
    }

    public function town() //Uno a muchos (inverso)

    {
        return $this->belongsTo('Delpos\Town');
    }



    public function getBirthdate()
    {
        Date::setLocale('es');
        return new Date($this->birthdate);

    }

    public function getCreate()
    {
        Date::setLocale('es');
        return new Date($this->created_at);

    }

    public function getUpdate()
    {
        Date::setLocale('es');
        return new Date($this->updated_at);

    }
    

}
