<?php

namespace Delphos;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class Favourite extends Model
{
    protected $table = 'favourites';
    //Campos que se pueden actualizar
    protected $fillable = [
        'id', 
        'confirmed', 
        'institutions_id',
        'programs_id',
        'users_id',
        'created_at',
        'updated_at',
    ];
    
    /**
     * The roles that belong to the user.
     */
    public function users()
    {
        return $this->belongsToMany('Delphos\User');
    }

    /**
     * The roles that belong to the user.
     */
    public function programs()
    {
        return $this->belongsToMany('Delphos\Program');
    }

    public function getCreate()
    {
        Date::setLocale('es');
        return new Date($this->created_at);

    }

    public function getUpdate()
    {
        Date::setLocale('es');
        return new Date($this->updated_at);

    }


}
