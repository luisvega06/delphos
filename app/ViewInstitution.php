<?php

namespace Delphos;

use Illuminate\Database\Eloquent\Model;

class ViewInstitution extends Model
{

	protected $table = 'view_institutions';

    //Campos que se pueden actualizar
    protected $fillable = [
    	'id', 
        'institutions_id', 
        'users_id',
        'created_at',
        'updated_at',
    ];

}
