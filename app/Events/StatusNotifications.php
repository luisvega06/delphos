<?php

namespace Delphos\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;


class StatusNotifications implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $data;


    public function __construct($user, $data)
    {
        $this->user = $user;
        $this->data = $data;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('App.User.'{$this->user->id});
    }

    public function broadcastWith()
    {
        return ['id' => $this->user->id];
    }

    public function broadcastAs()
    {
        return 'server.created';
    }







}
