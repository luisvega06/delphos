<?php

namespace Delphos;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class NotificationPublication extends Model
{
    protected $table = 'notification_publications';

    //Campos que se pueden actualizar
    protected $fillable = [
            'id',
            'users_id',
            'slug',
            'confirmed',
            'content',
            'users_institution_id',
            'publications_id',
            'created_at',
            'updated_at',
            
        ];

    public function getCreate()
    {
        Date::setLocale('es');
        return new Date($this->created_at);

    }

    public function getUpdate()
    {
        Date::setLocale('es');
        return new Date($this->updated_at);

    }

}
