<?php

namespace Delphos;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
	protected $table = 'phones';

    //Campos que se pueden actualizar
    protected $fillable = [
    		'id', 
    		'number',
    		'created_at',
    		'updated_at',
    		'users_id',
    	];


    public function user() //Uno a muchos (inverso)

    {
        return $this->belongsTo('Delpos\User');
    }
}
