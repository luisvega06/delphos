<?php

namespace Delphos;

use Illuminate\Database\Eloquent\Model;

class View extends Model
{
    //Campos que se pueden actualizar
    protected $fillable = [
        'id', 
        'institutions_id', 
        'publications_id', 
        'users_id',
        'created_at',
        'updated_at',
    ];



    /**
     * The roles that belong to the user.
     */
    public function users()
    {
        return $this->belongsToMany('Delphos\User');
    }
    
    /**
     * The roles that belong to the user.
     */
    public function publications()
    {
        return $this->belongsToMany('Delphos\Publication');
    }
}
