<?php

namespace Delphos;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
    
    protected $table = 'genders';

    protected $fillable = [
        'id',
        'name', 
    ];

    public function usercommunities() // Relacion de 1 : N

    {
        return $this->hasMany('Delphos\UserCommunity');
    }
}
