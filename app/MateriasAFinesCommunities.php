<?php

namespace Delphos;

use Illuminate\Database\Eloquent\Model;

class MateriasAFinesCommunities extends Model
{
    protected $table = 'materias_a_fines_communities';

    protected $fillable = [
    	'id',
		'users_id',
		'materias_id',
        'created_at',
        'updated_at',
    ];
}
